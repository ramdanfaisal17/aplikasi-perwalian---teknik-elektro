$(document).ready(function(){

  //Pas baru load, langsung disabled, semisalnya data proyekpendidikannya udah diselect
  let proyekpendidikanJenisSemester = $('#proyek_pendidikan_id').find(':selected').data('jenis_semester');
  disabledInputSemester(proyekpendidikanJenisSemester);

  //buat trigger ganti proyekpendidikan
  $('#proyek_pendidikan_id').on('change', function(){
      let proyekpendidikanJenisSemester = $(this).find(":selected").data('jenis_semester');
      disabledInputSemester(proyekpendidikanJenisSemester);
  });


  function disabledInputSemester(proyekpendidikanJenisSemester){
    if(proyekpendidikanJenisSemester == 'genap'){
      $('#start_ganjil').prop('disabled', true);
      $('#start_genap').prop('disabled', false);
      $('#start_antara').prop('disabled', true);
      $('#end_ganjil').prop('disabled', true);
      $('#end_genap').prop('disabled', false);
      $('#end_antara').prop('disabled', true);
    }else if(proyekpendidikanJenisSemester == 'ganjil'){
      $('#start_ganjil').prop('disabled', false);
      $('#start_genap').prop('disabled', true);
      $('#start_antara').prop('disabled', true);
      $('#end_ganjil').prop('disabled', false);
      $('#end_genap').prop('disabled', true);
      $('#end_antara').prop('disabled', true);
    }else{
      $('#start_ganjil').prop('disabled', true);
      $('#start_genap').prop('disabled', true);
      $('#start_antara').prop('disabled', false);
      $('#end_ganjil').prop('disabled', true);
      $('#end_genap').prop('disabled', true);
      $('#end_antara').prop('disabled', false);
    }
  }

});
