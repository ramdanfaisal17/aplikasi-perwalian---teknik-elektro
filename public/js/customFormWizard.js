function submit_user_group(myForm){
  var url = myForm.attr('action'),
      method = $('input[name=_method]').val() == undefined ? 'POST' : 'PUT';

  // console.log(url);
  console.table(myForm.serializeArray());
  $.ajax({
    url: url,
    data: myForm.serialize(),
    method:method,
    success: function(response) {
      swal({
        type: 'success',
        title: "Berhasil",
        text: "Data Berhasil Tersimpan",
        showCancelButton: false,
        confirmButtonColo: '#3085d6',
        confirmButtonText: 'Oke'
      }).then(result=>{
        if(result.value){
          window.location.replace(response.redirecTo);
        }
      });

    },
    error: function(xhr) {
      var res = xhr.responseJSON;
      if ($.isEmptyObject(res) == false){
        swal({
          type : 'error',
          title : 'Gagal!',
          text : 'Terjadi kesalahan mohon periksa kembali'
        });
      }
    }
  });

}
// end function submit user group
function submit_open_po(myForm){
  check_total_qty();

  var url = myForm.attr('action'),
      method = $('input[name=_method]').val() == undefined ? 'POST' : 'PUT';
  $.ajax({
    url: url,
    data: myForm.serialize(),
    method: method,
    success: function(response) {
        if(response.status == "success"){
            swal({
                type: 'success',
                title: "Berhasil",
                text: "Data Berhasil Tersimpan",
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Oke'
              }).then(result=>{
                if(result.value){
                  window.location.replace(response.redirecTo);
                }
              });
        }
    },
    error: function(xhr) {
        var res = xhr.responseJSON;
        if($.isEmptyObject(res) == false){
            // $.each(res.errors , function (k, v){
            swal({
                type : 'error',
                title : 'Gagal!',
                text : 'Terjadi kesalahan mohon periksa kembali'
            });
            // });
        }

    }
  });
}
//end function open po

function submit_po(myForm) {
    check_total_qty();
    var url = myForm.attr('action'),
        method = $('input[name=_method]').val() == undefined ? 'POST' : 'PUT';

    $.ajax({
        url: url,
        data: myForm.serialize(),
        method: method,
        success: function (response) {
            swal({
                type: 'success',
                title: "Berhasil",
                text: "Data Berhasil Tersimpan",
                showCancelButton: false,
                confirmButtonColo: '#3085d6',
                confirmButtonText: 'Oke'
            }).then(result => {
                if (result.value) {
                    // window.location.replace(response.redirecTo);
                }
            });

        },
        error: function (xhr) {
            var res = xhr.responseJSON;
            if ($.isEmptyObject(res) == false) {
                swal({
                    type: 'error',
                    title: 'Gagal!',
                    text: 'Terjadi kesalahan mohon periksa kembali'
                });
            }
        }
    });
}

function common_submit(myForm){
  var url = myForm.attr('action'),
      method = $('input[name=_method]').val() == undefined ? 'POST' : 'PUT';
  // console.log(url);

  $.ajax({
    url: url,
    data: new FormData(myForm[0]),
    cache: false,
    contentType: false,
    processData: false,
    method: 'POST',
    success: function (response) {
      // me.trigger('reset');
        if(response.status === "success"){
            swal({
                type : 'success',
                title : 'Berhasil!',
                text : 'Data Berhasil Tersimpan',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Oke'
            }).then(result=>{
                if(result.value){
                    if(response.redirect_to){

                        window.location.replace(response.redirect_to);
                    } else {
                        window.location.replace(response.redirecTo);

                    }
                }
            })
        } else {
            swal({
                type : 'error',
                title : 'Gagal!',
                text : 'Terjadi kesalahan mohon periksa kembali'
            });
        }

    },
    error: function (xhr) {
      var res = xhr.responseJSON;
      if ($.isEmptyObject(res) == false){
        swal({
          type : 'error',
          title : 'Gagal!',
          text : 'Terjadi kesalahan mohon periksa kembalic'
        });
      }
    }
  });
}

//form wizard

$(".tab-wizard").steps({
  headerTag: "h6",
  bodyTag: "section",
  transitionEffect: "fade",
  titleTemplate: '<span class="step">#index#</span> #title#',
  labels: {
      finish: "Submit"
  },
  onFinished: function (event, currentIndex) {

    var myForm = $(this).closest('form'),
        type = myForm.data('type');

    if(type == 'openpo'){
      submit_open_po(myForm);
    } else if(type == 'po') {
      submit_open_po(myForm);
    } else if(type == 'user-group') {
      submit_user_group(myForm);
    } else {
        common_submit(myForm);
    }

  }
});
