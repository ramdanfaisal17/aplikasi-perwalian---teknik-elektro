<?php

use Illuminate\Database\Seeder;
use Add\Models\perwalian_detail;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
   {
      DB::table('users')->insert([
        'nik' => 'admin',
        'name' => 'admin',
        'email' => 'admin.maranatha@gmail.com',
        'password' => bcrypt('password'),
        'relasi' => '',
      ]);
      DB::table('program_studis')->insert([
        'kode' => 'PS01',
        'nama' => 'S1-TEKNIK ELEKTRO',
      ]);

      DB::table('proyek_pendidikans')->insert([
        'kode' => 'PP201819S2',
        'nama' => 'Semester Genap Tahun Ajaran 2018/2019',
        'mulai' => '2018-07-01',
        'berakhir' => '2018-12-31',
      ]);

      DB::table('proyek_pendidikans')->insert([
        'kode' => 'PP201920S1',
        'nama' => 'Semester Ganjil Tahun Ajaran 2019/2020',
        'mulai' => '2019-01-01',
        'berakhir' => '2019-06-30',
      ]);

      DB::table('proyek_pendidikans')->insert([
        'kode' => 'PP201920S2',
        'nama' => 'Semester Genap Tahun Ajaran 2019/2020',
        'mulai' => '2019-07-01',
        'berakhir' => '2019-12-31',
      ]);

      // $programstudi = factory(Add\Models\programstudi::class, 10)->create();
      $dosens = factory(Add\Models\dosen::class, 10)->create();
      $dosen_wali = factory(Add\Models\dosenwali::class, 1)->create();
      $dosen_wali_detail = factory(Add\Models\dosenwali_detail::class, 10)->create();
      $mahasiswa = factory(Add\Models\mahasiswa::class, 10)->create();
      //$matakuliah = factory(Add\Models\matakuliah::class, 10)->create();

      // $proyekpendidikan = factory(Add\Models\proyekpendidikan::class, 10)->create();

      DB::table('jadwal_perwalian')->insert([
        'kode_perwalian' => 'PRW/19/12/001',
        'tanggal' => '2019-11-13',
        'proyek_pendidikan_id' => 2,
        'program_studi_id' => 1,
        'start_prs' => '2019-02-13',
        'end_prs' => '2019-02-20',
        'start_konsultasi1' => '2019-03-21',
        'end_konsultasi1' => '2019-03-28',
        'start_konsultasi2' => '2019-04-29',
        'end_konsultasi2' => '2019-04-30',
        'start_antara' => '2019-05-01',
        'end_antara' => '2019-05-07',
        'start_ganjil' => '2019-01-08',
        'end_ganjil' => '2019-01-15',
      ]);

      DB::table('jadwal_perwalian')->insert([
        'kode_perwalian' => 'PRW/19/12/002',
        'tanggal' => '2019-12-13',
        'proyek_pendidikan_id' => 3,
        'program_studi_id' => 1,
        'start_prs' => '2020-01-1',
        'end_prs' => '2019-01-20',
        'start_konsultasi1' => '2020-02-21',
        'end_konsultasi1' => '2020-02-28',
        'start_konsultasi2' => '2020-03-29',
        'end_konsultasi2' => '2020-03-30',
        'start_antara' => '2020-04-20',
        'end_antara' => '2020-04-21',
        'start_ganjil' => '2020-05-08',
        'end_ganjil' => '2020-05-15',
      ]);

      DB::table('tabel_perwalian')->insert([
        'dosen_id' => 1,
        'jadwal_perwalian_id' => 2,
      ]);

      $mahasiswaWali1 = DB::table('dosen_wali_details')->where('dosen_wali_id', '=', 1)->get();
      foreach ($mahasiswaWali1 as $data) {
          $perwalian_detail = perwalian_detail::create(['perwalian_id' => 1, 'mahasiswa_id' => $data->mahasiswa_id]);
          $konsultasi = DB::table('konsultasi')->insert(['perwalian_detail_id' => $perwalian_detail->id, 'ke' => 1]);
          $konsultasi2 = DB::table('konsultasi')->insert(['perwalian_detail_id' => $perwalian_detail->id, 'ke' => 2]);
      }


      $this->call([
        PengumumanTableSeeder::class,
      ]);
    }
}
