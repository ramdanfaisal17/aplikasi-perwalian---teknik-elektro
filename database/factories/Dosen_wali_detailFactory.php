<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Add\Models\dosenwali_detail;
use Faker\Generator as Faker;

$factory->define(Add\Models\dosenwali_detail::class, function (Faker $faker) {
  static $mahasiswa_id = 1;
    return [
      'dosen_wali_id' => 1,
      'mahasiswa_id' => $mahasiswa_id++,
    ];
});
