<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Add\Models\programstudi;
use Faker\Generator as Faker;

$factory->define(Add\Models\programstudi::class, function (Faker $faker) {
    return [
      'kode' => str_random(4),
      'nama' => $faker->name,
    ];
});
