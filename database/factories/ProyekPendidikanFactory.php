<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Add\Models\proyekpendidikan;
use Faker\Generator as Faker;

$factory->define(Add\Models\proyekpendidikan::class, function (Faker $faker) {
    return [
      'kode' => (string) mt_rand(1000000, 9999999),
      'nama' => $faker->name,
    ];
});
