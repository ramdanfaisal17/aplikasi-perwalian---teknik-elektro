<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Add\Models\dosenwali;
use Faker\Generator as Faker;

$factory->define(Add\Models\dosenwali::class, function (Faker $faker) {
    return [
      'tanggal' => date('Y-m-d'),
      'nomor' => (string) mt_rand(0, 1000),
      'dosen_id' => 1,
      'total_mahasiswa' => (string) mt_rand(0, 1000),
      'aktif' => mt_rand(0, 1),
    ];
});
