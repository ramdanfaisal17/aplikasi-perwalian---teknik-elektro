<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Add\Models\matakuliah;
use Faker\Generator as Faker;

$factory->define(Add\Models\matakuliah::class, function (Faker $faker) {
    return [
      'kode' => (string) mt_rand(1000000, 9999999),
      'nama' => $faker->name,
      'sks' =>  mt_rand(1, 9),
      'prasyarat' => str_random(10),
      'semester' => (string) mt_rand(1, 8),
      'sifat' => array_rand(['wajib', 'sunah']),
    ];
});
