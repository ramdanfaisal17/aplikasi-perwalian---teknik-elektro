<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Add\Models\mahasiswa;
use Faker\Generator as Faker;

$factory->define(Add\Models\mahasiswa::class, function (Faker $faker) {
    return [
      'nrp' => (string) mt_rand(1000000, 9999999),
      'nama' => $faker->name,
      'tanggal_lahir' =>  $faker->date,
      'tempat_lahir' => $faker->city,
      'jenis_kelamin' => array_rand(['laki-laki','perempuan']),
      'agama' => array_rand(['islam', 'kristen', 'hindu', 'budha', 'konghucu']),
      'email' => $faker->email,
      'no_hp' => $faker->phoneNumber,
      'program_studi_id' => mt_rand(1, 10),
    ];
});
