<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Add\Models\dosen;
use Faker\Generator as Faker;

$factory->define(Add\Models\dosen::class, function (Faker $faker) {
    return [
        'nik' => (string) mt_rand(1000000, 9999999),
        'nama' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'total_mahasiswa' => (string) mt_rand(0, 1000),
    ];
});
