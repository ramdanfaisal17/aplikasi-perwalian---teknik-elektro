<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyekPendidikansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyek_pendidikans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('kode')->nullable();
            $table->String('nama')->nullable();
            $table->date('mulai')->nullable();
            $table->date('berakhir')->nullable();
            $table->enum('jenis_semester',['ganjil', 'genap', 'antara']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyek_pendidikans');
    }
}
