<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MahasiswaKeluarga extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa_keluargas', function (Blueprint $table) {
            $table->bigIncrements('id');      
            $table->bigInteger('mahasiswa_id');      
            $table->String('hubungan')->nullable();
            $table->String('nama')->nullable();
            $table->Date('tanggal_lahir')->nullable();
            $table->String('tempat_lahir')->nullable();
            $table->String('alamat_pekerjaan')->nullable();
            $table->String('pendidikan_terakhir')->nullable();
            $table->String('no_hp')->nullable();
            $table->String('email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa_keluargas');
    }
}
