<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Nilai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilais', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Date('tanggal')->nullable();
            $table->String('nomor')->nullable();
            $table->bigInteger('mahasiswa_id')->nullable();
            $table->bigInteger('jadwal_perwalian_id')->nullable();
            $table->String('semester')->nullable();
            $table->String('tahun_ajaran')->nullable();
            $table->String('status')->nullable()->default('sementara');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilais');
        
    }
}
