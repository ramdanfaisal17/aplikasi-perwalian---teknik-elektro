<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelPengumuman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabel_pengumuman', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('jenis_pengumuman_id')->unsigned();
            $table->text('thumbnail_1');
            $table->text('thumbnail_2')->nullable();
            $table->text('thumbnail_3')->nullable();    
            $table->Date('tanggal')->nullable();
            $table->String('judul')->nullable();
            $table->String('status')->nullable();
            $table->text('isi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabel_pengumuman');
    }
}
