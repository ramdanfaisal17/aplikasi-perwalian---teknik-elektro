<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelPerwalianDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabel_perwalian_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('perwalian_id');
            $table->bigInteger('mahasiswa_id');
            $table->datetime('status_pilih_matakuliah')->nullable();
            $table->datetime('status_prs')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabel_perwalian_detail');
    }
}
