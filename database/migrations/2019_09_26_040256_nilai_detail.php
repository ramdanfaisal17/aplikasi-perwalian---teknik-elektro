<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NilaiDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::create('nilai_details', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('nilai_id')->nullable();
        $table->bigInteger('mata_kuliah_id')->nullable();
        $table->String('H')->nullable()->default('0');
        $table->String('A')->nullable()->default('0');
        $table->String('K')->nullable()->default('0');
        $table->String('M')->nullable()->default('0');
        $table->timestamps();
    });
 }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilai_details');
        
    }
}
