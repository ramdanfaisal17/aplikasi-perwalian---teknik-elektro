<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Dosenwali extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('dosen_walis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Date('tanggal')->nullable();
            $table->String('nomor')->nullable();
            $table->bigInteger('dosen_id');
            $table->String('total_mahasiswa')->default('0');
            $table->Boolean('aktif')->nullable()->default('1');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dosen_walis');
        
    }
}
