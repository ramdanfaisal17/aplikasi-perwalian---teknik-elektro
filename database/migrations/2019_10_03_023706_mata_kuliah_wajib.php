<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MataKuliahWajib extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mata_kuliah_wajibs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Date('tanggal')->nullable();
            $table->String('nomor')->nullable();
            $table->String('semester')->nullable();
            $table->Boolean('aktif')->nullable()->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mata_kuliah_wajibs');
    }
}
