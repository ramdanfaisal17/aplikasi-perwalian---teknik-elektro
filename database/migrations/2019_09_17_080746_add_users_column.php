<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->BigInteger('relasi_id')->after('id')->nullable();
            $table->string('nik')->after('relasi_id');
            $table->string('relasi')->after('nik')->default('');
            $table->string('level')->after('relasi')->default('');
            $table->string('profile_update')->after('level')->default('0');
            //$table->renameColumn('data1', 'call_owner');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //$table->renameColumn('call_owner', 'data1');
            $table->dropColumn('profile_update');
            $table->dropColumn('level');
            $table->dropColumn('relasi');
            $table->dropColumn('nik');
            $table->dropColumn('relasi_id');
        });
    }
}
