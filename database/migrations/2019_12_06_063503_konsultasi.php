<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Konsultasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('konsultasi', function (Blueprint $table) {
            $table->increments('id');
            $table->biginteger('perwalian_detail_id');
            $table->String('pembahasan')->nullable();
            $table->datetime('status_konsultasi')->nullable();
            $table->String('ke');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('konsultasi');
    }
}
