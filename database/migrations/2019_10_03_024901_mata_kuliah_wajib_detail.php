<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MataKuliahWajibDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mata_kuliah_wajib_details', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->BigInteger('mata_kuliah_wajib_id')->nullable();
        $table->BigInteger('mata_kuliah_id')->nullable();
        $table->timestamps();
    });
 }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mata_kuliah_wajib_details');
        
    }
}
