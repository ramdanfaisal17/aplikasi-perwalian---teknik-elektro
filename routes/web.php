<?php

//Route::get('/', function () {
//    return view('home');
//});

Auth::routes();

Route::group(['middleware' => ['web', 'auth']], function() {
  Route::get('/', 'HomeController@index')->name('home');
  Route::get('/home', 'HomeController@index')->name('home');

  Route::resource('dosen', 'DosenController');
  Route::post('dosen/deletes', 'DosenController@deletes')->name('dosen.Deletes');
  Route::post('dosen/import', 'DosenController@import')->name('dosen.import');
  Route::get('/data/dosen', 'DosenController@dataTable_for_show_data')->name('data.dosen');
  Route::get('/data/dosen/show', 'DosenController@show_data')->name('data.dosen.show');
  Route::get('get_dosen', 'DosenController@GetDosen');


  Route::resource('user', 'UsersController');
  Route::post('user/deletes', 'UsersController@deletes')->name('user.Deletes');
  Route::get('user/reset/{id}', 'UsersController@reset')->name('user.reset');
  Route::post('user/gantipassword', 'UsersController@gantipassword')->name('user.gantipassword');
  Route::post('user/import', 'UsersController@import')->name('user.import');

  Route::resource('kelas', 'KelaController');
  Route::post('kelas/deletes', 'KelaController@deletes')->name('kelas.deletes');
  Route::post('kelas/import', 'KelaController@import')->name('kelas.import');


  Route::resource('programstudi', 'ProgramStudiController');
  Route::post('programstudi/deletes', 'ProgramStudiController@deletes')->name('programstudi.deletes');
  Route::post('programstudi/import', 'ProgramStudiController@import')->name('programstudi.import');


  Route::post('proyekpendidikan/deletes', 'ProyekPendidikanController@deletes')->name('proyekpendidikan.deletes');
  Route::post('proyekpendidikan/import', 'ProyekPendidikanController@import')->name('proyekpendidikan.import');
  Route::resource('proyekpendidikan', 'ProyekPendidikanController');


  Route::resource('matakuliah', 'MataKuliahController');
  Route::post('matakuliah/deletes', 'MataKuliahController@deletes')->name('matakuliah.deletes');
  Route::post('matakuliah/import', 'MataKuliahController@import')->name('matakuliah.import');
  Route::get('/data/matakuliah', 'MataKuliahController@dataTable_for_show_data')->name('data.matakuliah');
  Route::get('/data/matakuliah/show', 'MataKuliahController@show_data')->name('data.matakuliah.show');


  Route::resource('mahasiswa', 'MahasiswaController');
  Route::post('mahasiswa/deletes', 'MahasiswaController@deletes')->name('mahasiswa.Deletes');
  Route::post('mahasiswa/updatedosenwali', 'MahasiswaController@updatedosenwali')->name('mahasiswa.updatedosenwali');
  Route::post('mahasiswa/import', 'MahasiswaController@import')->name('mahasiswa.import');
  Route::get('/data/mahasiswa', 'MahasiswaController@dataTable_for_show_data')->name('data.mahasiswa');
  Route::get('/data/mahasiswa/show', 'MahasiswaController@show_data')->name('data.mahasiswa.show');



  Route::resource('mahasiswakeluarga','MahasiswaKeluargaController');
  Route::get('indexkeluarga/mahasiswakeluarga/{id}','MahasiswaKeluargaController@creates');


  Route::get('/data/mahasiswa/showfiltered', 'MahasiswaController@show_data_filtered')->name('data.mahasiswa.showfiltered');

  Route::get('getkota', 'MahasiswaController@getkota');
  Route::get('getkecamatan', 'MahasiswaController@getkecamatan');
  Route::get('getdesa', 'MahasiswaController@getdesa');
  Route::get('getjadwalperwalian', 'JadwalPerwalianController@getjadwalperwalian');
  Route::get('indexkeluarga/{id}', 'MahasiswaController@indexkeluarga');

  Route::resource('mahasiswakeluarga', 'MahasiswaKeluargaController');
  Route::get('indexkeluarga/mahasiswakeluarga/{id}', 'MahasiswaKeluargaController@creates');


  Route::post('mahasiswakeluarga/deletes', 'MahasiswaKeluargaController@deletes')->name('mahasiswakeluarga.Deletes');


  Route::get('indexberkas/{id}', 'MahasiswaBerkaController@indexberkas');
  Route::post('/mahasiswaberkas/upload', 'MahasiswaBerkaController@proses_upload')->name('mahasiswaberkas.upload');
  Route::get('/mahasiswaberkas/delete/{id}', 'MahasiswaBerkaController@hapus')->name('mahasiswaberkas.delete');

  Route::resource('jadwalPerwalian', 'JadwalPerwalianController');
  Route::post('jadwalPerwalian/deletes', 'JadwalPerwalianController@deletes')->name('jadwalPerwalian.deletes');

  Route::get('perwalian', 'PerwalianController@index')->name('perwalian.index');
  Route::get('perwalian/{id}', 'PerwalianController@show')->name('perwalian.show');
  Route::get('perwalian/{id}/detail/{dosen_id}', 'PerwalianController@show_detail')->name('perwalian.show_detail');

  // Route::post('jadwalPerwalian/import', 'jadwalPerwalianController@import')->name('jadwalPerwalian.import');


  Route::resource('nilai', 'NilaiController');
  Route::resource('nilaiberkas', 'NilaiBerkasController');
  Route::get('stream/{filePath}', 'NilaiBerkasController@pdfStream')->name('stream.pdf');

  Route::post('nilai/deletes', 'NilaiController@deletes')->name('nilai.deletes');
  Route::get('getmatakuliahperwalian', 'NilaiController@getmatakuliahperwalian');
  Route::get('laporannilai', 'NilaiController@laporan_nilai')->name('nilai.laporan');
  Route::get('laporannilai/{id}', 'NilaiController@laporan_nilai_show')->name('nilai.laporan.show');

  Route::resource('dosenwali', 'DosenWaliController');
  Route::post('dosenwali/deletes', 'DosenWaliController@deletes')->name('dosenwali.deletes');
  Route::get('dosenwali/active/{id}', 'DosenWaliController@active')->name('dosenwali.active');
  Route::get('dosenwali/deactive/{id}', 'DosenWaliController@deactive')->name('dosenwali.deactive');

  Route::resource('matakuliahwajib', 'MataKuliahWajibController');
  Route::post('matakuliahwajib/deletes', 'MataKuliahWajibController@deletes')->name('matakuliahwajib.deletes');
  Route::get('matakuliahwajib/active/{id}', 'MataKuliahWajibController@active')->name('matakuliahwajib.active');
  Route::get('matakuliahwajib/deactive/{id}', 'MataKuliahWajibController@deactive')->name('matakuliahwajib.deactive');


  Route::post('berita/deletes', 'PengumumanController@deletes')->name('pengumuman.deletes');
  Route::get('berita/akademik', 'PengumumanController@index_akademik')->name('berita.akademik.index');
  Route::geT('berita/akademik/create', 'PengumumanController@create')->name('berita.akademik.create');
  Route::get('berita/pengumuman', 'PengumumanController@index_pengumuman')->name('berita.pengumuman.index');
  Route::get('berita/pengumuman/create', 'PengumumanController@create')->name('berita.pengumuman.create');
  Route::get('berita/prestasi', 'PengumumanController@index_prestasi')->name('berita.prestasi.index');
  Route::put('berita/prestasi', 'PengumumanController@prestasi_approve')->name('berita.approve');
  Route::resource('berita', 'PengumumanController');

  Route::get('hasilperwalian/print', 'HasilPerwalianController@print');
  Route::resource('hasilperwalian', 'HasilPerwalianController');

  Route::get('laporanabsen', 'PengumumanController@laporanabsen');
  Route::get('laporanpilihmatkul', 'LaporanController@pilihmatkul');
  Route::get('detailmatakuliahmhs/{id}', 'LaporanController@detailpilihmatkul');
  Route::get('laporankonsul', 'LaporanController@indexkonsul');
  Route::get('laporankonsul/{id}', 'LaporanController@show_konsul');
  Route::get('detailkonsul1/{id}', 'LaporanController@detailkonsul1');
  Route::get('detailkonsul2/{id}', 'LaporanController@detailkonsul2');
  Route::get('laporanhasil', 'LaporanController@indexhasil');
  
});
