<?php

Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');
Route::post('gantipassword', 'UsersController@gantipassword');

Route::group(['middleware' => 'auth:api'], function(){
  Route::post('details', 'UserController@details');
});

Route::group(['namespace' => 'Api'], function () {

  Route::resource('mahasiswa', 'MahasiswaController');

  Route::get('indexkeluarga/{id}','MahasiswaController@indexkeluarga');
  Route::get('getprovinsi', 'MahasiswaController@getprovinsi');
  Route::get('getkota', 'MahasiswaController@getkota');
  Route::get('getkecamatan', 'MahasiswaController@getkecamatan');
  Route::get('getdesa', 'MahasiswaController@getdesa');

  Route::post('nilai/prs', 'NilaiController@store_prs');

  Route::resource('dosen', 'DosenController');
  Route::resource('kela', 'KelaController');
  Route::resource('matakuliah', 'MataKuliahController');
  Route::resource('programstudi', 'ProgramStudiController');
  Route::resource('proyekpendidikan', 'ProyekPendidikanController');
  Route::resource('mahasiswakeluarga','MahasiswaKeluargaController');
  //Route::resource('perwalian', 'PerwalianController');
  Route::resource('users', 'UsersController');
  Route::resource('nilai', 'NilaiController');

//   Route::get('nilai', 'NilaiController@show');
//   Route::post('nilai', 'NilaiController@store');

  //Modul perwalian
  Route::get('perwalian', 'PerwalianController@index');
  Route::post('perwalian', 'PerwalianController@store');
  Route::get('perwalian/{id}', 'PerwalianController@show');
  Route::get('perwalian/{id}/dosen/{dosen_id}', 'PerwalianController@show_detail');
  Route::post('perwalian/konsultasi', 'PerwalianController@postKonsultasi');
  Route::get('indexkeluarga/mahasiswakeluarga/{id}','MahasiswaKeluargaController@creates');

  Route::get('getperwaliannowdosen/{dosen_id}', 'PerwalianController@getPerwalianNowDosen');


  Route::post('/mahasiswaberkas/upload', 'MahasiswaBerkaController@proses_upload');
  Route::get('/mahasiswaberkas/delete/{id}', 'MahasiswaBerkaController@hapus');

  // Route::resource('jadwalperwalian','JadwalPerwalianController');

  Route::get('indexberkas/{id}', 'MahasiswaBerkaController@indexberkas');
  Route::get('indexberkasfoto/{id}', 'MahasiswaBerkaController@indexberkasfoto');
  Route::get('indexberkasfotoktp/{id}', 'MahasiswaBerkaController@indexberkasfotoktp');
  Route::get('indexberkasfotoktpayah/{id}', 'MahasiswaBerkaController@indexberkasfotoktpayah');
  Route::get('indexberkasfotoktpibu/{id}', 'MahasiswaBerkaController@indexberkasfotoktpibu');
  Route::get('indexberkasfotokk/{id}', 'MahasiswaBerkaController@indexberkasfotokk');

  Route::get('berita', 'PengumumanController@index');
  Route::get('beritaakademik', 'PengumumanController@index_akademik');

  Route::get('berita/{id}', 'PengumumanController@detail');
  Route::post('berita', 'PengumumanController@store')->name('pengumuman.api.store');

  Route::get('berita/terbaru', 'PengumumanController@index_berita_terbaru');
  Route::post('berita/deletes', 'PengumumanController@deletes');
  //Route::get('berita/akademik', 'PengumumanController@index_akademik');
  Route::get('berita/pengumuman', 'PengumumanController@index_pengumuman');
  Route::get('berita/prestasi', 'PengumumanController@index_prestasi');
  Route::post('berita/prestasi', 'PengumumanController@prestasi_approve');
  Route::post('berita/prestasinew', 'PengumumanController@simpanprestasi');
  Route::put('berita/update/{id}', 'PengumumanController@update');
  Route::delete('berita/{id}', 'PengumumanController@destroy');
  Route::post('prestasi/upload', 'PengumumanController@uploadprestasi');

  Route::get('konsultasi', 'KonsultasiController@index');
  Route::post('konsultasi/store', 'KonsultasiController@store');
  Route::get('konsultasi/{id}', 'KonsultasiController@show');

  Route::get('absen/{id}', 'PerwalianController@absen');
  Route::post('createabsen', 'PerwalianController@createabsen');
  //Route::get('nilai/{id}', 'PerwalianController@carinilai');
  Route::get('absendetail/{id}', 'PerwalianController@detailabsen');
  Route::get('matakuliah2/{id}', 'MataKuliahController@matakuliahbyid');
  Route::get('matakuliahmahasiswa/{id}', 'MataKuliahController@matakuliahbyid2');

  Route::post('konsultasinew', 'PerwalianController@createkonsul');
  Route::get('statusnilai/{id}', 'MataKuliahController@statusnilai');
  Route::post('updatestatusnilai', 'MataKuliahController@updatestatusnilai');
  Route::get('tarikkonsultasi1/{id}', 'MataKuliahController@carikonsul1');
  Route::get('tarikkonsultasi2/{id}', 'MataKuliahController@carikonsul2');


});
