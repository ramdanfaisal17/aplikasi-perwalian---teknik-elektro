<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ $title }}</title>
    <link rel="icon" href="{{ asset('Gambar/marnat_6_orig.png') }}" type="image/x-icon">
    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->

    {{-- <link rel="stylesheet" type="text/css" href="Login_v13/css/util.css"> --}}
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="Login_v13/css/main.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
</head>
<body style="background-color: none;">
    <div class="container-fluid">
        <div class="container-login100">
            <div class="login100-more"></div>

            <div class="wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="container" style="padding-top: 10px;width: 80%;">
                        <div class="row">
                          <div class="col-md-12 ">
                            <div class="form-group">
                                <center><img src="Gambar/marnat-2_orig.png" height="200px" width="200px;">
                                </center>
                            </div>
                            <div class="form-group">
                                <center><h4>APLIKASI PERWALIAN</h4></center>
                                <center><h4>PROGRAM STUDI TEKNIK ELEKTRO</h4></center>
                            </div>
                            <div class="form-group" style="padding-top: 10px;">
                                <label for="nik">NIK</label>
                                <input type="text" id="nik" name="nik" class="form-control" required autofocus>
                            </div>
                            <div class="form-group" style="padding-top: 10px;">
                                <label for="password">Password</label>
                                <input type="password" id="password" name="password" class="form-control" required>
                            </div>

                            <div class="form-check" style="padding-top: 20px;" hidden>
                                <center><input class="form-check-input" type="checkbox" name="remember" id="remember" >remember me</center>
                            </div>

                        </div>
                    </div>
                    <div class="row" style="padding-top: 40px;">
                        {{-- <div class="col-md-6">
                            <!--<button class="btn btn-warning" style="width: 100%;">register</button>-->
                            <a href="{{route('register')}}" class="btn btn-warning" style="width: 100%;">register</a>
                        </div> --}}
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary" style="width: 389PX;">Login</button>
                        </div>
                    </div>
                    <div class="row" style="padding-top: 40px;" hidden>
                        <div class="col-md-12">
                            <center><a class="btn btn-link " href="{{ route('password.request') }}">forgot password ?</a></center>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
</div>

</body>
</html>
