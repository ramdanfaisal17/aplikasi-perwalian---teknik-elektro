<div class="row">
  <div class="col-lg-12">
    <table class="table table-hovered table-bordered" id="datatable" style="width: 100%;" data-table="tbody-matakuliah">
      <thead>
        <tr>
          <th></th>
          <th>No.</th>
          <th>kode</th>
          <th>Nama</th>
          <th>Semester</th>
        </tr>
      </thead>
      <tbody></tbody>
     
    </table>
  </div>
</div>


<script>
  var dataTablesColumns = [
    {},
    {data:"id", name:"id"},
    {data: "kode", name: 'kode'},
    {data: "nama", name: 'nama'},
    {data: "semester", name: 'semester'},
  ];

  var url = "{{ route('data.matakuliah') }}";
</script>
<script src="{{ asset('js/customDataTable.js') }}"></script>
