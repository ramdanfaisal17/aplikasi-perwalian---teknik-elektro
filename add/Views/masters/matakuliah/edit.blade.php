@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('matakuliah.index') }}">Master Matakuliah</a></li>
    <li class="breadcrumb-item active" aria-current="page">Ubah Data - {{ $matakuliah->kode }}</li>
  </ol>
</nav>
@endsection

@section('content')

<div class="card">
    <div class="card-header">Ubah Mata Kuliah {{ $matakuliah->nama }}</div>

    <div class="card-body">
        <form action="{{ route("matakuliah.update", [$matakuliah->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <input type="hidden" name="id" value="{{ $matakuliah->id }}">

            @include('masters.matakuliah.form')

            <div class="col-lg-12" id='iddosen'>
              <label id="iddosen">Pilih Dosen</label>
              <table class="table table-hovered table-bordered table-striped" id="tabeldetail">
                <thead class="text-center">
                  <tr>
                    <th width="5%">
                      <a href='{{ route("data.dosen.show") }}' id="modal-btn-add" title='Tambah Dosen' class="btn btn-success modal-add-row">
                        <i width="5%" class="fa fa-plus"></i>
                      </a>
                    </th>
                    <th>NIK</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody id="tbody-dosen">
                  @if(isset($matakuliah->matakuliahdetail))
                    @foreach($matakuliah->matakuliahdetail as $key => $detail)
                    <tr><input type="hidden" value="{{ $detail->id }}" name="iddetail[]">
                      <td class="td-id" data-id="{{ $detail->id }}"><center>{{ $key + 1 }}</center></td>
                      <td>{{$detail->dosen['nik']}}</td>
                      <td>{{$detail->dosen['nama']}}</td>
                      <td>{{$detail->dosen['email']}}</td>
                      <td> <a href="#" class="btn btn-danger btn-sm btn-remove-row" id={{$key + 1}}><i class="fa fa-trash"></i></a></td>
                    </tr>
                    @endforeach
                  @else
                    <tr>
                      <td colspan="5" class="text-center">No data available</td>
                    </tr>
                  @endif
              </tbody>
             </table>
            </div>

            <div>
                <a href="{{ route('matakuliah.index') }}" class="btn btn-warning">
                    <i class="fas fa-arrow-left"></i> Kembali
                </a>
                <input class="btn btn-success" type="submit" value="Simpan">
            </div>
        </form>
    </div>
</div>

@include('layouts.modal')
<script src="{{ asset('jquery-3.4.1.min.js') }}"></script>

<script>
//function for array checker
function check_array(value, arr){
  var status = false;
  for(var i=0; i<arr.length;i++ ){
    var needle = arr[i].id;
    if(needle == value){
      status = true;
      break;
    }
  }

  return status;
}
    $(document).on("ready",function(){
        console.log("naonweh");
    });

    $('body').on('click', '#modal-btn-add', function(event){
    event.preventDefault();

    var table = $('#datatable').DataTable();

    var data = table.rows({selected: true}).data();

    var tbody = 'tbody-dosen';

    console.table(data);

    var currentData = [];
      $('#'+tbody+' .td-id').each(function(){
        currentData.push({ id:$(this).data('id') });
      });
    // console.table(currentData);
    var no = currentData.length;
    var currentNo = 0;
    for(var i = 0; i < data.length; i++){
      var check = check_array(data[i].idbarang, currentData);
        // console.log(check);
      if(!check){
        currentNo++;
        $('#'+tbody).append('<tr>'
                    +' <input type="hidden" value="'+data[i].id+'" name="iddetail[]">'
                    +' <td class="td-id" data-id="'+data[i].id+'">' + (currentNo+no) + '</td>'
                    +' <td> ' + data[i].nik + '</td>'
                    +' <td> ' + data[i].nama + '</td>'
                    +' <td> ' + data[i].email + '</td>'
                    +' <td> <a href="#" class="btn btn-danger btn-sm btn-remove-row"><i class="fa fa-trash"></i></a>'
                    +'</tr>');
      }
    }

      });

     $('body').on('click','.modal-add-row', function(event){
      event.preventDefault();

      var me = $(this),
          url = me.attr('href'),
          title = me.attr('title');

      $('#modal-title').text(title);
      $('#modal-btn-save').addClass('hide');
      $('#modal-btn-custom').addClass("hide");
      $('.modal-dialog').addClass('modal-lg');

      $('#modal-btn-add').removeClass('hide');

      $.ajax({
        url: url,
        dataType: 'html',
        success: function (response) {
          $('#datatable').DataTable().destroy();
          $('#modal-body').html(response);
        }
      });

      $("#modal").modal('show');

    });

    $('body').on('click','.btn-remove-row',function(event){
      event.preventDefault();

      var baris = $(this).attr('id');
      document.getElementById("tabeldetail").deleteRow(baris);
      var totalbaris = $('#tabeldetail tr').length;
      var i = 0;
      for (i = 1; i < totalbaris; i++) {
        document.getElementById('tabeldetail').rows[i].cells[0].innerHTML = '<center>' + i + '</center>' ;
     }

    });

</script>

@endsection
