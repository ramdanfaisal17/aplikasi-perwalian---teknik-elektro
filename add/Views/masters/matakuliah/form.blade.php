  <div class="form-group">
    <label for="kode">Kode Mata Kuliah</label>
    <input type="text" id="kode" name="kode" class="form-control" required autocomplete="off" value="{{ old('kode', $matakuliah->kode) }}">
    @if($errors->has('kode'))
      <small class="form-text text-danger">*{{ $errors->first('kode') }}</small>
    @endif
</div>
<div class="form-group">
    <label for="nama">Nama Mata Kuliah</label>
    <input type="text" id="nama" name="nama" class="form-control" required autocomplete="off" value="{{ old('nama', $matakuliah->nama) }}">
    @if($errors->has('nama'))
      <small class="form-text text-danger">*{{ $errors->first('nama') }}</small>
    @endif
</div>
<div class="form-group">
    <label for="sks">SKS</label>
    <input type="text" id="sks" name="sks" class="form-control" required autocomplete="off" value="{{ old('sks', $matakuliah->sks) }}">
    @if($errors->has('sks'))
      <small class="form-text text-danger">*{{ $errors->first('sks') }}</small>
    @endif
</div>
<div class="form-group">
    <label for="prasyarat">Prasyarat</label>
    <input type="text" id="prasyarat" name="prasyarat" class="form-control" required autocomplete="off" value="{{ old('prasyarat', $matakuliah->prasyarat) }}">
    @if($errors->has('prasyarat'))
      <small class="form-text text-danger">*{{ $errors->first('prasyarat') }}</small>
    @endif
</div>
<div class="form-group">
    <label for="semester">Semester</label>
    <input type="number" id="semester" name="semester" class="form-control" required autocomplete="off" value="{{ old('semester', $matakuliah->semester) }}">
    @if($errors->has('semester'))
      <small class="form-text text-danger">*{{ $errors->first('semester') }}</small>
    @endif
</div>
<div class="form-group">
    <label for="jenis_semester">Jenis Semester</label>
    <select class="form-control" name="jenis_semester">
      <option value="">-- Pilih Jenis Semester --</option>
      <option value="ganjil" {{$matakuliah->jenis_semester == 'ganjil' ? 'selected' : ''}}>Ganjil</option>
      <option value="genap" {{$matakuliah->jenis_semester == 'genap' ? 'selected' : ''}}>Genap</option>
      <option value="antara" {{$matakuliah->jenis_semester == 'antara' ? 'selected' : ''}}>Antara</option>
    </select>
    @if($errors->has('jenis_semester'))
      <small class="form-text text-danger">*{{ $errors->first('jenis_semester') }}</small>
    @endif
</div>
<div class="form-group">
    <label for="sifat">Sifat mata kuliah</label>
    <!--<input type="text" id="sifat" name="sifat" class="form-control" required autocomplete="off" value="{{ old('sifat', $matakuliah->sifat) }}">-->
    <!--@if($errors->has('sifat'))-->
      <!--<small class="form-text text-danger">*{{ $errors->first('sifat') }}</small>-->
    <!--@endif-->
    <select class="form-control" name="sifat">
<option selected value="1">wajib</option>
<option value="0">pendukung</option>
    </select>
</div>
