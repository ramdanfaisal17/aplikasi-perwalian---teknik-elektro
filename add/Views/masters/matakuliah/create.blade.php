@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('matakuliah.index') }}">Master Matakuliah</a></li>
    <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
  </ol>
</nav>
@endsection

@section('content')

<div class="card">
    <div class="card-header">Tambah Mata Kuliah</div>
    <div class="card-body">
        <form action="{{ route("matakuliah.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('post')

            @include('masters.matakuliah.form')


            <div class="col-lg-12" id='iddosen'>
              <label id="iddosen">Pilih Dosen</label>
              <table class="table table-hovered table-bordered table-striped">
                <thead class="text-center">
                  <tr>
                    <th width="5%">
                      <a href='{{ route("data.dosen.show") }}' id="modal-btn-add" title='Tambah Dosen' class="btn btn-success modal-add-row">
                        <i width="5%" class="fa fa-plus"></i>
                      </a>
                    </th>
                    <th>NIK</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody id="tbody-dosen"></tbody>
              </table>
            </div>


            <div>
                <a href="{{ route('matakuliah.index') }}" class="btn btn-warning">
                    <i class="fas fa-arrow-left"></i> Kembali
                </a>
                <input class="btn btn-success" type="submit" value="Simpan">
            </div>
        </form>
    </div>
</div>




@include('layouts.modal')
<script src="{{ asset('jquery-3.4.1.min.js') }}"></script>

<script>
//function for array checker
function check_array(value, arr){
  var status = false;
  for(var i=0; i<arr.length;i++ ){
    var needle = arr[i].id;
    if(needle == value){
      status = true;
      break;
    }
  }

  return status;
}
    $(document).on("ready",function(){
        console.log("naonweh");
    });

    $('body').on('click', '#modal-btn-add', function(event){
    event.preventDefault();

    var table = $('#datatable').DataTable();

    var data = table.rows({selected: true}).data();

    var tbody = 'tbody-dosen';

    console.table(data);

    var currentData = [];
      $('#'+tbody+' .td-id').each(function(){
        currentData.push({ id:$(this).data('id') });
      });
    // console.table(currentData);
    var no = currentData.length;
    var currentNo = 0;
    for(var i = 0; i < data.length; i++){
      var check = check_array(data[i].idbarang, currentData);
        // console.log(check);
      if(!check){
        currentNo++;
        $('#'+tbody).append('<tr>'
                    +' <input type="hidden" value="'+data[i].id+'" name="iddetail[]">'
                    +' <td class="td-id" data-id="'+data[i].id+'">' + (currentNo+no) + '</td>'
                    +' <td> ' + data[i].nik + '</td>'
                    +' <td> ' + data[i].nama + '</td>'
                    +' <td> ' + data[i].email + '</td>'
                    +' <td> <a href="#" class="btn btn-danger btn-sm btn-remove-row"><i class="fa fa-trash"></i></a>'
                    +'</tr>');
      }
    }

      });

     $('body').on('click','.modal-add-row', function(event){
      event.preventDefault();

      var me = $(this),
          url = me.attr('href'),
          title = me.attr('title');

      $('#modal-title').text(title);
      $('#modal-btn-save').addClass('hide');
      $('#modal-btn-custom').addClass("hide");
      $('.modal-dialog').addClass('modal-lg');

      $('#modal-btn-add').removeClass('hide');

      $.ajax({
        url: url,
        dataType: 'html',
        success: function (response) {
          $('#datatable').DataTable().destroy();
          $('#modal-body').html(response);
        }
      });

      $("#modal").modal('show');

    });

</script>

@endsection
