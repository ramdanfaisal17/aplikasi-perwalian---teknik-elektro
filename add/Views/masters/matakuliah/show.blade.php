@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('matakuliah.index') }}">Master Matakuliah</a></li>
    <li class="breadcrumb-item active" aria-current="page">Lihat Data - {{ $model->kode }}</li>
  </ol>
</nav>
@endsection

@section('content')

<div class="card">
    <div class="card-header">Mata Kuliah  {{ $model->nama }}</div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>Kode Mata Kuliah</th>
                    <td>{{ $model->kode }}</td>
                </tr>
                <tr>
                    <th>Nama Mata Kuliah</th>
                    <td>{{ $model->nama}}</td>
                </tr>
                <tr>
                    <th>SKS</th>
                    <td>{{ $model->sks}}</td>
                </tr>
                <tr>
                    <th>Prasyarat</th>
                    <td>{{ $model->prasyarat}}</td>
                </tr>
                <tr>
                    <th>Semester</th>
                    <td>{{ $model->semester}}</td>
                </tr>
                <tr>
                    <th>Sifat</th>
                    <td>{{ $model->sifat}}</td>
                </tr>
            </tbody>
        </table>
        <h3>Data Dosen </h3>
        <div class="col-md-12">
            <table class="table table-hovered">
              <thead>
                <th>NIK</th>
                <th>Nama Dosen</th>
                <th>Email</th>
              </thead>
              @foreach ($matkuldetail as $item)
              <tbody>

                <td>{{$item->nik}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->email}}</td>
              </tbody>
              @endforeach
            </table>
        </div>

        <a href="{{ route('matakuliah.index') }}" class="btn btn-warning">
            <i class="fas fa-arrow-left"></i> Kembali
        </a>
    </div>


</div>

@endsection
