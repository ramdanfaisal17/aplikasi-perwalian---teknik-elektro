@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('user.index') }}">Master User</a></li>
    <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
  </ol>
</nav>
@endsection

@section('content')


<div class="card">
    <div class="card-header">Data User Baru</div>
    <div class="card-body">
        <form method="POST" action="{{ route('user.store') }}">
            @csrf

            <div class="form-group row">
                <label for="nik" class="col-md-4 col-form-label text-md-right">NIK/NRP</label>

                <div class="col-md-6">
                    <input id="nik" type="number" class="form-control @error('nik') is-invalid @enderror" name="nik" value="{{ old('nik') }}" required autofocus>

                    @if($errors->has('nik'))
                      <small class="form-text text-danger">*{{ $errors->first('nik') }}</small>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">Nama User</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" >

                    @if($errors->has('name'))
                      <small class="form-text text-danger">*{{ $errors->first('name') }}</small>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="relasi" class="col-md-4 col-form-label text-md-right">Level</label>
                <div class="col-md-6">
                    <select name="relasi" id="relasi" class="form-control @error('relasi') is-invalid @enderror">
                        <option disabled selected value="-">- Pilih -</option>
                        <option value="Dosen">Dosen</option>
                        <option value="Mahasiswa">Mahasiswa</option>
                        <option value="Admin">Admin</option>
                    </select>
                    @if($errors->has('relasi'))
                    <small class="form-text text-danger">*{{ $errors->first('relasi') }}</small>
                    @endif
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-success">
                        Daftarkan
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
