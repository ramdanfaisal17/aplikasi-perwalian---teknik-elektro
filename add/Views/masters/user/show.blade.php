@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('user.index') }}">Master User</a></li>
    <li class="breadcrumb-item active" aria-current="page">Lihat Data - {{$user->nik}}</li>
  </ol>
</nav>
@endsection
@section('content')

<div class="card">
    <div class="card-header">Informasi User {{ $user->nik }} - {{ $user->name }}</div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>NIK</th>
                    <td>{{ $user->nik }}</td>
                </tr>
                <tr>
                    <th>Nama User</th>
                    <td>{{ $user->name}}</td>
                </tr>
                <tr>
                    <th>Jabatan</th>
                    <td>{{ $user->relasi }}</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>{{ $user->email }}</td>
                </tr>
            </tbody>
        </table>
        <a href="{{ route('user.index') }}" class="btn btn-warning">
          <i class="fas fa-arrow-left"></i> Kembali
        </a>
    </div>
</div>

@endsection
