@extends('layouts.admin')
@section('content')

@if (session('error'))
<div class="alert alert-danger">
{{ session('error') }}
</div>
@endif
@if (session('success'))
<div class="alert alert-success">
{{ session('success') }}
</div>
@endif

<div class="card">
    <div class="card-header">ganti password {{ $user->name }}</div>

    <div class="card-body">
        <form action="{{ route("user.gantipassword")}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('POST')
            <div class="form-group row">
                <input type="text" name="id" id="id" value="{{$user->id}}" hidden>
                <label for="current-password" class="col-md-4 col-form-label text-md-right">password lama</label>

                <div class="col-md-6">
                    <input id="current-password" type="password" class="form-control @error('current-password') is-invalid @enderror" name="current-password" required autofocus value="">

                    @error('current-password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="password-confirmation" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                <div class="col-md-6">
                    <input id="password-confirmation" type="password" class="form-control" name="password-confirmation" required autocomplete="new-password">
                </div>
            </div>

            <div>
                <a href="{{ route('user.index') }}">
                    <input class="btn btn-warning" type="button" value="back">
                </a>
                <input class="btn btn-success" type="submit" value="save">
            </div>
        </form>
    </div>
</div>

@endsection