<div class="form-group">
    <label for="kode">Kode Program Studi</label>
    <input type="text" id="kode" name="kode" class="form-control" required autocomplete="off" value="{{ old('kode', $programstudi->kode) }}">
    @if($errors->has('kode'))
      <small class="form-text text-danger">*{{ $errors->first('kode') }}</small>
    @endif
</div>
<div class="form-group">
    <label for="nama">Nama Program Studi</label>
    <input type="text" id="nama" name="nama" class="form-control" required autocomplete="off" value="{{ old('nama', $programstudi->nama) }}">
    @if($errors->has('nama'))
      <small class="form-text text-danger">*{{ $errors->first('nama') }}</small>
    @endif
</div>
