@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('programstudi.index') }}">Master Program Studi</a></li>
    <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
  </ol>
</nav>
@endsection
@section('content')

<div class="card">
    <div class="card-header">Tambah Program Studi</div>
    <div class="card-body">
        <form action="{{ route("programstudi.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('post')

            @include('masters.programstudi.form')

            <div>
                <button type="submit" name="button" class="btn-success btn">Simpan</button>
            </div>

        </form>
    </div>
</div>

@endsection
