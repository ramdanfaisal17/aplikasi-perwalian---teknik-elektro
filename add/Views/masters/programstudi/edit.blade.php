@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('programstudi.index') }}">Master Program Studi</a></li>
    <li class="breadcrumb-item active" aria-current="page">Ubah Data - {{ $programstudi->kode }}</li>
  </ol>
</nav>
@endsection

@section('content')

<div class="card">
    <div class="card-header">Ubah {{ $programstudi->nama }}</div>

    <div class="card-body">
        <form action="{{ route("programstudi.update", [$programstudi->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <input type="hidden" name="id" value="{{ $programstudi->id }}">

            @include('masters.programstudi.form')

            <div>
                <input class="btn btn-success" type="submit" value="Simpan">
            </div>
        </form>
    </div>
</div>

@endsection
