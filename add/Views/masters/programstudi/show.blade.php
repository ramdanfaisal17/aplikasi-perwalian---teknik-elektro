@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('programstudi.index') }}">Master Program Studi</a></li>
    <li class="breadcrumb-item active" aria-current="page">Lihat Data - {{ $programstudi->kode }}</li>
  </ol>
</nav>
@endsection
@section('content')

<div class="card">
    <div class="card-header">Program Studi {{ $programstudi->nama }}</div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>Kode Program Studi</th>
                    <td>{{ $programstudi->kode }}</td>
                </tr>
                <tr>
                    <th>Nama Program Studi</th>
                    <td>{{ $programstudi->nama}}</td>
                </tr>
            </tbody>
        </table>
        <a href="{{ route('programstudi.index') }}" class="btn btn-warning">
            <i class="fas fa-arrow-left"></i> Kembali
        </a>
    </div>
</div>

@endsection
