<div class="tab-content" id="pills-tabContent" style="margin-bottom: 10px;">
    {{-- tab1 --}}
    <div class="tab-pane fade show active" id="pills-personal-info" role="tabpanel" aria-labelledby="pills-personal-info-tab">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="nrp">NRP</label>
                    <input type="number" id="nrp" name="nrp" class="form-control" required autocomplete="off" autofocus value="{{ old('nrp', $mahasiswa->nrp) }}">
                    @if($errors->has('nrp'))
                      <small class="form-text text-danger">*{{ $errors->first('nrp') }}</small>
                    @endif
                </div>
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" id="nama" name="nama" class="form-control" required autocomplete="off" value="{{ old('nama', $mahasiswa->nama) }}">
                    @if($errors->has('nama'))
                      <small class="form-text text-danger">*{{ $errors->first('nama') }}</small>
                    @endif
                </div>
                <div class="form-group">
                    <label for="tanggal_lahir">Tanggal Lahir</label>
                    <input type="date" id="tanggal_lahir" name="tanggal_lahir" class="form-control" required autocomplete="off" value="{{ old('tanggal_lahir', $mahasiswa->tanggal_lahir) }}">
                    @if($errors->has('tanggal_lahir'))
                      <small class="form-text text-danger">*{{ $errors->first('tanggal_lahir') }}</small>
                    @endif
                </div>
                <div class="form-group">
                    <label for="tempat_lahir">Tempat Lahir</label>
                    <input type="text" id="tempat_lahir" name="tempat_lahir" class="form-control" required autocomplete="off" value="{{ old('tempat_lahir', $mahasiswa->tempat_lahir) }}">
                    @if($errors->has('tempat_lahir'))
                      <small class="form-text text-danger">*{{ $errors->first('tempat_lahir') }}</small>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Jenis Kelamin</label>

                    <div class="form-control">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" name="jenis_kelamin" id="jenis_kelamin1" value="laki-laki" checked>
                            <label class="custom-control-label" for="jenis_kelamin1">LAKI-LAKI</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" name="jenis_kelamin" id="jenis_kelamin2" value="perempuan">
                            <label class="custom-control-label" for="jenis_kelamin2">PEREMPUAN</label>
                        </div>
                    </div>
                    @if($errors->has('jenis_kelamin'))
                      <small class="form-text text-danger">*{{ $errors->first('jenis_kelamin') }}</small>
                    @endif

                </div>
                <div class="form-group">
                    <label for="agama">Agama</label>
                    <select name="agama" id="agama" class="form-control">
                        <option disabled>- Pilih Agama -</option>
                        <option value="budha" {{ ($mahasiswa->agama == 'budha') ? 'selected' : '' }}>budha</option>
                        <option value="hindu" {{ ($mahasiswa->agama == 'hindu') ? 'selected' : '' }}>hindu</option>
                        <option value="islam" {{ ($mahasiswa->agama == 'islam') ? 'selected' : '' }}>islam</option>
                        <option value="katolik" {{ ($mahasiswa->agama == 'katolik') ? 'selected' : '' }}>katolik</option>
                        <option value="protestan" {{ ($mahasiswa->agama == 'protestan') ? 'selected' : '' }}>protestan</option>
                    </select>
                    @if($errors->has('agama'))
                      <small class="form-text text-danger">*{{ $errors->first('agama') }}</small>
                    @endif
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" id="email" name="email" class="form-control" required autocomplete="off"value="{{ old('email', $mahasiswa->email) }}">
                    @if($errors->has('email'))
                      <small class="form-text text-danger">*{{ $errors->first('email') }}</small>
                    @endif
                </div>

                <div class="form-group">
                    <label for="no_hp">No HP</label>
                    <input type="text" id="no_hp" name="no_hp" class="form-control" required autocomplete="off" value="{{ old('no_hp', $mahasiswa->no_hp) }}">
                    @if($errors->has('no_hp'))
                      <small class="form-text text-danger">*{{ $errors->first('no_hp') }}</small>
                    @endif
                </div>
            </div>
        </div>
    </div>
    {{-- tab2 --}}
    <div class="tab-pane fade" id="pills-alamat" role="tabpanel" aria-labelledby="pills-alamat-tab">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <textarea name="alamat" id="alamat" cols="30" rows="8" class="form-control" value="">{{ old('alamat', $mahasiswa->alamat) }}</textarea>
                    @if($errors->has('alamat'))
                      <small class="form-text text-danger">*{{ $errors->first('alamat') }}</small>
                    @endif
                </div>
                <div class="form-group">
                    <label for="warga_negara">Warga Negara</label>
                    <input type="text" id="warga_negara" name="warga_negara" class="form-control" autocomplete="off" value="{{ old('warga_negara', $mahasiswa->warga_negara) }}">
                    @if($errors->has('warga_negara'))
                      <small class="form-text text-danger">*{{ $errors->first('warga_negara') }}</small>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="provinsi">Provinsi</label>
                    <select name="provinsi" id="provinsi_id" class="form-control">
                        <option value="" selected disabled>- Pilih Provinsi -</option>
                        @foreach($provinsis as $key => $provinsi)
                        <option value="{{$provinsi->id}}" {{ ($mahasiswa->provinsi) ? (($mahasiswa->provinsi->id == $provinsi->id) ? 'selected' : '') : '' }}> {{$provinsi->name}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('provinsi'))
                      <small class="form-text text-danger">*{{ $errors->first('provinsi') }}</small>
                    @endif
                </div>
                <div class="form-group">
                    <label for="kota">Kota</label>
                    <select name="kota" id="kota_id" class="form-control">
                      <option value="" selected disabled>- Pilih Kota -</option>
                      @if(!$kota->isEmpty())
                        @foreach($kota as $kot)
                        <option value="{{$kot->id}}" {{ ($mahasiswa->kota) ? (($mahasiswa->kota->id == $kot->id) ? 'selected' : '') : '' }}>{{$kot->name}}</option>
                        @endforeach
                      @endif
                    </select>
                    @if($errors->has('kota'))
                      <small class="form-text text-danger">*{{ $errors->first('kota') }}</small>
                    @endif
                </div>

                <div class="form-group">
                    <label for="kecamatan">Kecamatan</label>
                    <select name="kecamatan" id="kecamatan_id" class="form-control">
                        <option value="" selected disabled>- Pilih Kecamatan -</option>
                        @if(!$kecamatan->isEmpty())
                          @foreach($kecamatan as $kec)
                          <option value="{{$kec->id}}" {{ ($mahasiswa->kecamatan) ? (($mahasiswa->kecamatan->id == $kec->id) ? 'selected' : '') : '' }}>{{$kec->name}}</option>
                          @endforeach
                        @endif

                    </select>
                    @if($errors->has('kecamatan'))
                      <small class="form-text text-danger">*{{ $errors->first('kecamatan') }}</small>
                    @endif
                </div>
                <div class="form-group">
                    <label for="desa">Desa</label>
                    <select name="desa" id="desa_id" class="form-control">
                        <option value="" selected disabled>- Pilih Desa -</option>
                        @if(!$desa->isEmpty())
                          @foreach($desa as $des)
                            <option value="{{$des->id}}" {{ ($mahasiswa->desa) ? (($mahasiswa->desa->id == $des->id) ? 'selected' : '') : '' }}>{{$des->name}}</option>
                          @endforeach
                        @endif
                    </select>
                    @if($errors->has('desa'))
                      <small class="form-text text-danger">*{{ $errors->first('desa') }}</small>
                    @endif
                </div>

                <div class="form-group">
                    <label for="kode_pos">Kode Pos</label>
                    <input type="number" id="kode_pos" name="kode_pos" class="form-control" autocomplete="off" value="{{ old('kode_pos', $mahasiswa->kode_pos) }}">
                    @if($errors->has('kode_pos'))
                      <small class="form-text text-danger">*{{ $errors->first('kode_pos') }}</small>
                    @endif
                </div>
            </div>
        </div>
    </div>
    {{-- tab3 --}}
    <div class="tab-pane fade" id="pills-detail-info" role="tabpanel" aria-labelledby="pills-detail-info-tab">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="golongan_darah">Golongan Darah</label>
                    <select name="golongan_darah" id="golongan_darah" class="form-control">
                        <option value="-" selected disabled>- Pilih -</option>
                        <option value="A" {{ ($mahasiswa->golongan_darah == "A") ? 'selected' : '' }}>A</option>
                        <option value="B" {{ ($mahasiswa->golongan_darah == "B") ? 'selected' : '' }}>B</option>
                        <option value="AB" {{ ($mahasiswa->golongan_darah == "AB") ? 'selected' : '' }}>AB</option>
                        <option value="O" {{ ($mahasiswa->golongan_darah == "O") ? 'selected' : '' }}>O</option>
                    </select>
                    @if($errors->has('golongan_darah'))
                      <small class="form-text text-danger">*{{ $errors->first('golongan_darah') }}</small>
                    @endif
                </div>
                <div class="form-group">
                    <label for="rhesus">Rhesus</label>
                    <input type="text" id="rhesus" name="rhesus" class="form-control" autocomplete="off" value="{{ old('rhesus', $mahasiswa->rhesus) }}">
                    @if($errors->has('rhesus'))
                      <small class="form-text text-danger">*{{ $errors->first('rhesus') }}</small>
                    @endif
                </div>
                <div class="form-group">
                    <label for="no_ijazah">No Ijazah</label>
                    <input type="text" id="no_ijazah" name="no_ijazah" class="form-control" autocomplete="off" value="{{ old('no_ijazah', $mahasiswa->no_ijazah) }}">
                    @if($errors->has('no_ijazah'))
                      <small class="form-text text-danger">*{{ $errors->first('no_ijazah') }}</small>
                    @endif
                </div>
                <div class="form-group">
                    <label for="tanggal_ijazah">Tanggal Ijazah</label>
                    <input type="date" id="tanggal_ijazah" name="tanggal_ijazah" class="form-control" autocomplete="off" value="{{ old('tanggal_ijazah', $mahasiswa->tanggal_ijazah) }}">
                    @if($errors->has('tanggal_ijazah'))
                      <small class="form-text text-danger">*{{ $errors->first('tanggal_ijazah') }}</small>
                    @endif
                </div>
                <div class="form-group">
                    <label for="dosen_id">dosen wali</label>
                    <select name="dosen_id" id="dosen_id" class="form-control" required>
                        <option value="" selected disabled>- Pilih Dosen Wali -</option>
                        @foreach($dosens as $dosen)
                        <option value="{{$dosen->id}}" {{ $mahasiswa->dosen_id == $dosen->id || old('dosen_id') ? 'selected' : ''}}>{{$dosen->nik}} - {{$dosen->nama}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('dosen_id'))
                      <small class="form-text text-danger">*{{ $errors->first('dosen_id') }}</small>
                    @endif
                </div>
            </div>

            <div class="col-md-6">

                <div class="form-group">
                    <label for="nem">NEM</label>
                    <input type="number" id="nem" name="nem" class="form-control" autocomplete="off" value="{{ old('nem', $mahasiswa->nem) }}">
                    @if($errors->has('nem'))
                      <small class="form-text text-danger">*{{ $errors->first('nem') }}</small>
                    @endif
                </div>
                <div class="form-group">
                    <label for="nilai_toefel">Nilai Toefl</label>
                    <input type="number" id="nilai_toefel" name="nilai_toefel" class="form-control" autocomplete="off" value="{{ old('nilai_toefel', $mahasiswa->nilai_toefel) }}">
                    @if($errors->has('nilai_toefel'))
                      <small class="form-text text-danger">*{{ $errors->first('nilai_toefel') }}</small>
                    @endif
                </div>
                <div class="form-group">
                    <label for="jalur_masuk">Jalur Masuk</label>
                    <input type="text" id="jalur_masuk" name="jalur_masuk" class="form-control" autocomplete="off" value="{{ old('jalur_masuk', $mahasiswa->jalur_masuk) }}">
                    @if($errors->has('jalur_masuk'))
                      <small class="form-text text-danger">*{{ $errors->first('jalur_masuk') }}</small>
                    @endif
                </div>
                <div class="form-group">
                    <label for="status_mahasiswa">Status Mahasiswa</label>
                    <input type="text" id="status_mahasiswa" name="status_mahasiswa" class="form-control" autocomplete="off" value="{{ old('status_mahasiswa', $mahasiswa->status_mahasiswa) }}">
                    @if($errors->has('status_mahasiswa'))
                      <small class="form-text text-danger">*{{ $errors->first('status_mahasiswa') }}</small>
                    @endif
                </div>
                <div class="form-group">
                    <label for="status_mahasiswa">Semester</label>
                    <input type="text" id="semester" name="semester" class="form-control" autocomplete="off" value="{{ old('semester', $mahasiswa->semester) }}">
                    @if($errors->has('semester'))
                      <small class="form-text text-danger">*{{ $errors->first('semester') }}</small>
                    @endif
                </div>
                <div class="form-group">
                    <label for="program_studi_id">program studi</label>
                    <select name="program_studi_id" id="program_studi_id" class="form-control" required>
                        @foreach($programstudis as $programstudi)
                        <option value="{{$programstudi->id}}" {{ $mahasiswa->programstudi_id == $programstudi->id ? 'selected' : '' }}>{{$programstudi->kode}} - {{$programstudi->nama}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('program_studi_id'))
                      <small class="form-text text-danger">*{{ $errors->first('program_studi_id') }}</small>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
