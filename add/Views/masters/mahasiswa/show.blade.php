@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('mahasiswa.index') }}">Master Mahasiswa</a></li>
    <li class="breadcrumb-item active" aria-current="page">Lihat Data - {{ $mahasiswa->nrp }}</li>
  </ol>
</nav>
@endsection

@section('content')


<div class="card">
    <div class="card-header">{{ $mahasiswa->nama }} - {{$mahasiswa->programstudi['nama']}} - {{$mahasiswa->dosen['nama']}}</div>
    <div class="card-body">
        <table class="table table-bordered table-striped" style="float:left; width:47%">
            <tbody>
                <tr>
                    <th width="30%">NRP</th>
                    <td width="70%">{{ $mahasiswa->nrp }}</td>

                </tr>
                <tr>
                    <th>Nama</th>
                    <td>{{ $mahasiswa->nama}}</td>

                <tr>
                    <th>Tanggal Lahir</th>
                    {{-- <td >{{ $mahasiswa->tanggal_lahir }}</td> --}}
                    <td><?php echo date("d/m/Y", strtotime($mahasiswa->tanggal_lahir)) ; ?></td>

                </tr>
                <tr>
                    <th>Tempat Lahir</th>
                    <td>{{ $mahasiswa->tempat_lahir }}</td>

                </tr>
                <tr>
                    <th>Jenis Kelamin</th>
                    <td>{{ $mahasiswa->jenis_kelamin }}</td>

                </tr>
                <tr>
                    <th>Agama</th>
                    <td>{{ $mahasiswa->agama }}</td>

                </tr>

                <tr>
                    <th>Email</th>
                    <td>{{ $mahasiswa->email }}</td>

                </tr>

                <tr>
                    <th>No HP</th>
                    <td>{{ $mahasiswa->no_hp }}</td>

                </tr>
                <tr>
                    <th>Golongan Darah</th>
                    <td>{{ $mahasiswa->golongan_darah }}</td>

                </tr>
                <tr>
                    <th>Rhesus</th>
                    <td>{{ $mahasiswa->rhesus }}</td>

                </tr>
                <tr>
                    <th>No Ijazah</th>
                    <td>{{ $mahasiswa->no_ijazah }}</td>

                </tr>
                <tr>
                    <th>Tanggal Ijazah</th>
                    <td>{{ $mahasiswa->tanggal_ijazah }}</td>

                </tr>

            </tbody>
        </table>
        <table class="table table-bordered table-striped" style="float:right; width:47%; margin-left:2%">
            <tbody>
                <tr>
                    <th width="200px">Alamat</th>
                    <td>{{ $mahasiswa->alamat }}</td>

                </tr>
                <tr>
                   <th>Provinsi</th>
                    @foreach($provinsi as $prov)
                    <td>{{ $prov->name }}</td>
                    @endforeach
                </tr>

                </tr>
                <tr>
                     <th>Kota</th>
                    @foreach($kota as $kot)
                    <td>{{ $kot->name }}</td>
                    @endforeach

                </tr>
                <tr>
                     <th>Kecamatan</th>
                    @foreach($kecamatan as $kec)
                    <td>{{ $kec->name }}</td>
                    @endforeach

                </tr>
                <tr>
                    <th>Desa</th>
                    @foreach($desa as $des)
                    <td>{{ $des->name }}</td>
                    @endforeach
                </tr>
                <tr>
                     <th>Kode Pos</th>
                    <td>{{ $mahasiswa->kode_pos }}</td>

                </tr>

                <tr>
                    <th>Warga Negara</th>
                    <td>{{ $mahasiswa->warga_negara }}</td>

                </tr>

                <tr>
                    <th>NEM</th>
                    <td>{{ $mahasiswa->nem }}</td>
                </tr>
                <tr>
                     <th>Nilai Toefl</th>
                    <td>{{ $mahasiswa->nilai_toefel }}</td>
                </tr>
                <tr>
                  <th>Jalur Masuk</th>
                    <td>{{ $mahasiswa->jalur_masuk }}</td>

                </tr>
                <tr>
                   <th>Status Mahasiswa</th>
                    <td>{{ $mahasiswa->status_mahasiswa }}</td>

                </tr>
                <tr>
                     <th>semester</th>
                    <td>{{ $mahasiswa->semester }}</td>

                </tr>

            </tbody>
        </table>

        <a href="{{ route('mahasiswa.index') }}" class="btn btn-warning">
          <i class="fas fa-arrow-left"></i> Kembali
        </a>
    </div>
</div>

@endsection
