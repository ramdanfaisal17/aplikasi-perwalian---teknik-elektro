@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    @if($pengumuman->jenis_pengumuman_id == 1)
    <li class="breadcrumb-item"><a href="{{ route('berita.pengumuman.index') }}">Berita Pengumuman</a></li>
    @elseif($pengumuman->jenis_pengumuman_id == 2)
    <li class="breadcrumb-item"><a href="{{ route('berita.akademik.index') }}">Berita Info Akademik</a></li>
    @else
    <li class="breadcrumb-item"><a href="{{ route('berita.prestasi.index') }}">Berita Input Prestasi</a></li>
    @endif
    <li class="breadcrumb-item active" aria-current="page">Add Data</li>
  </ol>
</nav>
@endsection
@section('content')

<div class="card">
    <div class="card-header">Pengumuman {{ $pengumuman->judul }}</div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>Tanggal</th>
                    <td>{{ $pengumuman->tanggal }}</td>
                </tr>
                <tr>
                    <th>Judul</th>
                    <td>{{ $pengumuman->judul}}</td>
                </tr>
                <tr>
                    <th>Dibagikan ke ..</th>
                    <td>{{ ($pengumuman->status == 1 ? 'Mahasiswa' : ($pengumuman->status == 2 ? 'Dosen' : 'Mahasiswa dan Dosen' )) }}</td>
                </tr>
            </tbody>
        </table>

        <div class="col-12 col-md-12 text-center mb-4">
          <h3>ISI PENGUMUMAN</h3>
        </div>
        <div class="p-3 col-12">
          {!! $pengumuman->isi !!}
        </div>
        <div class="col-12">
            <?php if($pengumuman->jenis_pengumuman_id=='3'){?>
                <img src="http://mobile-maranatha.berkatsoft.com/newtemp/uploadprestasi/<?php echo $pengumuman->thumbnail_1; ?>">
                <br><br>
                <img src="http://mobile-maranatha.berkatsoft.com/newtemp/uploadprestasi/<?php echo $pengumuman->thumbnail_2; ?>">
                <br><br>
                <img src="http://mobile-maranatha.berkatsoft.com/newtemp/uploadprestasi/<?php echo $pengumuman->thumbnail_3; ?>">
            <?php }else{ ?>
                <img src="https://maranatha.berkatsoft.com/storage/berita/<?php echo $pengumuman->thumbnail_1; ?>">
                <br><br>
                <img src="https://maranatha.berkatsoft.com/storage/berita/<?php echo $pengumuman->thumbnail_2; ?>">
                <br><br>
                <img src="https://maranatha.berkatsoft.com/storage/berita/<?php echo $pengumuman->thumbnail_3; ?>">
            <?php } ?>
        </div>
<!--
        <a href="{{ route('berita.index') }}">
            <input type="button" class="btn btn-warning" value="Back">
        </a> -->
    </div>
</div>

@endsection
