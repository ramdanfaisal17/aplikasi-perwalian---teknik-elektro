<div class="form-group">
   <label for="tanggal">Tanggal</label>
    <input type="date" id="tanggal" name="tanggal" class="form-control" autocomplete="off" value="<?php echo date('Y-m-d'); ?>" readonly>
    @if($errors->has('tanggal'))
      <small class="form-text text-danger">*{{ $errors->first('tanggal') }}</small>
    @endif
</div>
<div class="form-group">
    <label for="judul">Judul</label>
    <input type="text" id="judul" name="judul" class="form-control" required autocomplete="off" value="{{ old('judul', $pengumuman->judul) }}">
    @if($errors->has('judul'))
      <small class="form-text text-danger">*{{ $errors->first('judul') }}</small>
    @endif
</div>
<div class="form-group">
     <label for="status">Bagikan ke.. (Yang dapat melihat)</label>
      <select name="status" id="status" class="form-control">
            <option value="1" {{ $pengumuman->status == 1 ? 'checked' : '' }}>Mahasiswa</option>
            <option value="2" {{ $pengumuman->status == 2 ? 'checked' : ''}}>Dosen</option>
            <option value="3" {{ $pengumuman->status == 3 ? 'checked' : '' }}>Dosen & Mahasiswa</option>
      </select>
</div>

<div class="form-group">
    <label for="judul">Thumbnail</label>
    <input type="file" id="fileInput" name="thumbnail[]" class="form-control" multiple>
    @if($pengumuman)
      <div class="form-group py-2">
        <img src="{{ $pengumuman->thumbnail_1 ? asset('storage/berita/'. $pengumuman->thumbnail_1) : '' }}" class="img-thumbnail" id="filePreview0" alt="" height="200px" width="200px">
        <img src="{{ $pengumuman->thumbnail_2 ? asset('storage/berita/'. $pengumuman->thumbnail_2) : '' }}" class="img-thumbnail" id="filePreview1" alt="" height="200px" width="200px">
        <img src="{{ $pengumuman->thumbnail_3 ? asset('storage/berita/'. $pengumuman->thumbnail_3) : '' }}" class="img-thumbnail" id="filePreview2" alt="" height="200px" width="200px">
      </div>
    @endif
    <small class="form-text text-info">*Pilih maksimal 3 gambar yang dipilih untuk dijadikan thumbnail</small>
    @if($errors->has('thumbnail'))
      <small class="form-text text-danger">*{{ $errors->first('thumbnail') }}</small>
    @endif
</div>

<div class="form-group">
    <label for="isi">Isi Pengumuman</label>
    <!-- <input type="text" id="nama" name="nama" class="form-control" required autocomplete="off"> -->
     <textarea id="isi" name="isi" class="my-editor" placeholder="Masukan konten disini..." cols="80" rows="15">{!! old('isi',  $pengumuman->isi) !!}</textarea>
     @if($errors->has('isi'))
       <small class="form-text text-danger">*{{ $errors->first('isi ') }}</small>
     @endif
</div>

<div>
    <input class="btn btn-success" type="submit" value="Simpan">
</div>
