@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Berita</a></li>
    <li class="breadcrumb-item active" aria-current="page">Berita Info Akademik</li>
  </ol>
</nav>
@endsection

@section('content')

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Berita Info Akademik</h3>
  </div>

  <div class="col-lg-8 text-md-right">
      <button class="btn btn-danger" onclick="$('.dtb-delete').click();" style="min-width: 100px;">
          <i class="fas fa-trash-alt"></i> Hapus Pilihan
      </button>
      <a class="btn btn-success" href="{{ route("berita.akademik.create") }}" style="min-width: 100px;">
          <i class="fas fa-plus"></i> Tambah Data
      </a>


  </div>


</div>

<!-- Flash message here -->
@include('_partial.flash_message')

<div class="card">

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table datatable" id="mytable">
                <thead>
                    <tr>
                        <th width="10">
                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck2">
                            <label class="custom-control-label" for="customCheck2"></label>
                        </div>
                    </th>
                        <th width="100">Action</th>
                        <th>Tanggal</th>
                        <th>Judul</th>
                        <th>Dibagikan ke ..</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($akademik as $item)
                    <tr data-entry-id="{{ $item->id }}">
                        <td></td>
                        <td>
                            <form action="{{ route('berita.destroy', $item->id) }}" method="POST" Title="Hapus Data"onsubmit="return confirm('yakin nih?');" style="display: inline-block;">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" name="button" class="btn btn-xs btn-danger">
                                  <i class="fa fa-trash"></i>
                                </button>
                            </form>
                            <a class="btn btn-xs btn-primary" Title="Ubah Data" href="{{ route('berita.edit', $item->id) }}">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a class="btn btn-xs btn-success" Title="Lihat Data" href="{{ route('berita.show', $item->id) }}">
                                <i class="fas fa-eye"></i>
                            </a>
                        </td>
                        <td>{{ date('d-m-Y', strtotime($item->tanggal)) }}</td>
                        <td>{{ $item->judul }}</td>
                        <td>{{ ($item->status == 1 ? 'Mahasiswa' : ($item->status == 2 ? 'Dosen' : 'Mahasiswa dan Dosen' )) }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
  $(function () {
    let deleteButtonTrans = 'delete selected'
    let deleteButton = {
      text: deleteButtonTrans,
      className: 'btn-danger dtb-delete hilang',
      action: function (e, dt, node, config) {
        var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
        });

        if (ids.length === 0) {
          alert('tidak ada yang dipilih !')
          return
        }
        if (confirm('beneran ??')) {
          $.ajax({
            headers: {'x-csrf-token': _token},
            type: "POST",
            data: {ids:ids},
            url: "berita/deletes",
            success: function(){
             location.reload();
           }
         });
        }
      }
    }


    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

    dtButtons.push(deleteButton)


    $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  })

  function deleteSelected(){
    $('.tomboldeleteall').click();
  }

  $("#customCheck2").change( function() {
    if ($("th.select-checkbox").hasClass("selected")) {
      $('#mytable').DataTable().rows().deselect();
      $("th.select-checkbox").removeClass("selected");
    } else {
      $('#mytable').DataTable().rows().select();
      $("th.select-checkbox").addClass("selected");
    }
  }).on("select deselect", function() {
    ("Some selection or deselection going on")
    if ($('#mytable').DataTable().rows({
      selected: true
    }).count() !== $('#mytable').DataTable().rows().count()) {
      $("th.select-checkbox").removeClass("selected");
    } else {
      $("th.select-checkbox").addClass("selected");
    }
  });

</script>

@endsection
@endsection
