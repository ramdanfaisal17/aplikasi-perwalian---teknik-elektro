@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    @if(Request::segment(2) == 'pengumuman')
    <li class="breadcrumb-item"><a href="{{ route('berita.pengumuman.index') }}">Berita Pengumuman</a></li>
    @else
    <li class="breadcrumb-item"><a href="{{ route('berita.akademik.index') }}">Berita Info Akademik</a></li>
    @endif
    <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
  </ol>
</nav>
@endsection

@section('content')


<head>
  <title>Text Editor TinyMCE</title>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="{{ asset('tinymce/js/tinymce/tinymce.min.js') }}"></script>

  <style>
  form {
    width:800px;
  }
</style>
<script>
  var editor_config = {
     path_absolute : '/',
     selector: "textarea.my-editor",
     plugins: [
       "advlist autolink lists link image charmap print preview hr anchor pagebreak",
       "searchreplace wordcount visualblocks visualchars code fullscreen",
       "insertdatetime media nonbreaking save table contextmenu directionality",
       "emoticons template paste textcolor colorpicker textpattern"
     ],
     toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
     relative_urls: false,
     file_picker_callback: function (callback, value, meta) {
         let x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
         let y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

         let type = 'image' === meta.filetype ? 'Images' : 'Files',
             url  = editor_config.path_absolute + 'laravel-filemanager?editor=tinymce5&type=' + type;

         tinymce.activeEditor.windowManager.openUrl({
             url : url,
             title : 'Filemanager',
             width : x * 0.8,
             height : y * 0.8,
             onMessage: (api, message) => {
                 callback(message.content);
             }
         });
     }
   };

   tinymce.init(editor_config);
</script>
</head>

<!-- Flash message here -->
@include('_partial.flash_message')

<div class="card">
    <div class="card-header">Tambah Pengumuman</div>
    <div class="card-body">
        <form action="{{ route("berita.store") }}" method="POST" enctype="multipart/form-data">
            @method('post')
            @csrf
            <input type="hidden" name="jenis_pengumuman_id" value="{{ $berita }}">

            @include('masters.pengumuman.form')

        </form>
    </div>
</div>

@endsection
