@extends('layouts.admin')
@section('content')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item active" aria-current="page">Berita Input Prestasi</li>
  </ol>
</nav>
@endsection

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
      <h3>Berita Prestasi</h3>
  </div>

  <div class="col-lg-8 text-md-right">
      <button class="btn btn-danger" onclick="$('.dtb-delete').click();" style="min-width: 100px;">
          <i class="fas fa-trash-alt"></i> Hapus Pilihan
      </button>

  </div>
</div>

<!-- Flash message here -->
@include('_partial.flash_message')

<div class="card">

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table datatable" id="mytable">
                <thead>
                    <tr>
                        <th width="10">
                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck2">
                            <label class="custom-control-label" for="customCheck2"></label>
                        </div>
                    </th>
                        <th width="100">Action</th>
                        <th>Tanggal</th>
                        <th>Judul</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($prestasi as $item)
                    <tr data-entry-id="{{ $item->id }}">
                        <td></td>
                        <td>
                            <form action="{{ route('berita.destroy', $item->id) }}" method="POST" Title="Hapus Data"onsubmit="return confirm('yakin nih?');" style="display: inline-block;">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" name="button" class="btn btn-xs btn-danger">
                                  <i class="fa fa-trash"></i>
                                </button>
                            </form>
                            <a class="btn btn-xs btn-success" Title="Lihat Data" href="{{ route('berita.show', $item->id) }}">
                                <i class="fas fa-eye"></i>
                            </a>
                            @if(!$item->status)
                            <form action="{{ route('berita.approve') }}" method="POST" Title="Setujui Data" onsubmit="return confirm('yakin nih?');" style="display: inline-block;">
                                @csrf
                                @method('put')
                                <input type="hidden" name="id" value="{{ $item->id }}">
                                <button type="submit" name="button" class="btn btn-xs btn-primary">
                                  <i class="fas fa-thumbs-up"></i>
                                </button>
                            </form>

                            @endif
                        </td>
                        <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td>
                        <td>{{ $item->judul }}</td>
                        <td><span class="badge badge-pill {{ $item->status ? 'badge-success' : 'badge-danger' }}">{{ ($item->status ? 'Disetujui' : 'Belum disetujui') }}</span></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
  $(function () {
    let deleteButtonTrans = 'delete selected'
    let deleteButton = {
      text: deleteButtonTrans,
      className: 'btn-danger dtb-delete hilang',
      action: function (e, dt, node, config) {
        var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
        });

        if (ids.length === 0) {
          alert('tidak ada yang dipilih !')
          return
        }
        if (confirm('beneran ??')) {
          $.ajax({
            headers: {'x-csrf-token': _token},
            type: "POST",
            data: {ids:ids},
            url: "berita/deletes",
            success: function(){
             location.reload();
           }
         });
        }
      }
    }


    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

    dtButtons.push(deleteButton)


    $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  })

  function deleteSelected(){
    $('.tomboldeleteall').click();
  }

  $("#customCheck2").change( function() {
    if ($("th.select-checkbox").hasClass("selected")) {
      $('#mytable').DataTable().rows().deselect();
      $("th.select-checkbox").removeClass("selected");
    } else {
      $('#mytable').DataTable().rows().select();
      $("th.select-checkbox").addClass("selected");
    }
  }).on("select deselect", function() {
    ("Some selection or deselection going on")
    if ($('#mytable').DataTable().rows({
      selected: true
    }).count() !== $('#mytable').DataTable().rows().count()) {
      $("th.select-checkbox").removeClass("selected");
    } else {
      $("th.select-checkbox").addClass("selected");
    }
  });

</script>

@endsection
@endsection
