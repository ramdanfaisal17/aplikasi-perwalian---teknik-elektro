@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">kelas  {{ $kela->nama }}</div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>kode</th>
                    <td>{{ $kela->kode }}</td>
                </tr>
                <tr>
                    <th>nama</th>
                    <td>{{ $kela->nama}}</td>
                </tr>
            </tbody>
        </table>
        <a href="{{ route('kelas.index') }}">
            <input type="button" class="btn btn-warning" value="back">
        </a>
    </div>
</div>

@endsection