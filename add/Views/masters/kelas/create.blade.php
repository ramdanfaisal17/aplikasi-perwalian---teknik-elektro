@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">new kelas</div>
    <div class="card-body">
        <form action="{{ route("kelas.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="kode">kode</label>
                <input type="text" id="kode" name="kode" class="form-control" required autocomplete="off">
            </div>
            <div class="form-group">
                <label for="nama">nama</label>
                <input type="text" id="nama" name="nama" class="form-control" required autocomplete="off">
            </div>
            <div>
                <input class="btn btn-success" type="submit" value="Create">
            </div>
        </form>
    </div>
</div>

@endsection