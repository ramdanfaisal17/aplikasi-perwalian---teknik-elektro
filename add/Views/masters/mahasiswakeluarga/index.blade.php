@extends('layouts.admin')
@section('content')

@foreach($mahasiswas as $mahasiswa)@endforeach

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Keluarga {{$mahasiswa->nama}}</h3>
</div>
<div class="col-lg-8 text-md-right">
    <a class="btn btn-warning" href="{{route('mahasiswa.index')}}">
        Back
    </a>
    <button class="btn btn-danger" onclick="$('.dtb-delete').click();" style="min-width: 100px;">
      delete Selected
  </button>

  <a class="btn btn-success" href="mahasiswakeluarga/{{$mahasiswa->id}}" style="min-width: 100px;">
      create New
  </a>
</div>
</div>



<div class="card">

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table datatable" id="mytable">
                <thead>
                    <tr>
                        <th width="10">
                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck2">
                            <label class="custom-control-label" for="customCheck2"></label>
                        </div>
                    </th>
                    <th width="150">Action</th>
                    <th>Hubungan</th>
                    <th>Nama</th>
                    <th>No_HP</th>
                    <th>Email</th>
                    <th>Alamat Pekerjaan</th>
                </tr>
            </thead>
            <tbody>
                @foreach($mahasiswakeluargas as $item)
                <tr data-entry-id="{{ $item->id }}">
                    <td></td>
                    <td>
                        <form action="{{ route('mahasiswakeluarga.destroy', $item->id) }}" method="POST" title="Hapus Data" onsubmit="return confirm('yakin nih?');" style="display: inline-block;">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" class="btn btn-xs btn-danger" value="X">
                        </form>
                        <a class="btn btn-xs btn-primary" title="Edit Data" href="{{ route('mahasiswakeluarga.edit', $item->id) }}">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn btn-xs btn-success" title="Lihat Data"href="{{ route('mahasiswakeluarga.show', $item->id) }}">
                            <i class="fa fa-check"></i>
                        </a>                         
                    </td>
                    <td>{{ $item->hubungan }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->no_hp }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->alamat_pekerjaan }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
@section('scripts')

<script>
  $(function () {
    let deleteButtonTrans = 'delete selected'
    let deleteButton = {
      text: deleteButtonTrans,
      className: 'btn-danger dtb-delete hilang',
      action: function (e, dt, node, config) {
        var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
        });

        if (ids.length === 0) {
          alert('tidak ada yang dipilih !')
          return
        }
        if (confirm('beneran ??')) {
          $.ajax({
            headers: {'x-csrf-token': _token},
            type: "POST",
            data: {ids:ids},
            url: "/mahasiswakeluarga/deletes",
            success: function(){
             location.reload();
           }
         });
        }
      }
    }


    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

    dtButtons.push(deleteButton)


    $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  })

  function deleteSelected(){
    $('.tomboldeleteall').click();
  }

  $("#customCheck2").change( function() {
    if ($("th.select-checkbox").hasClass("selected")) {
      $('#mytable').DataTable().rows().deselect();
      $("th.select-checkbox").removeClass("selected");
    } else {
      $('#mytable').DataTable().rows().select();
      $("th.select-checkbox").addClass("selected");
    }
  }).on("select deselect", function() {
    ("Some selection or deselection going on")
    if ($('#mytable').DataTable().rows({
      selected: true
    }).count() !== $('#mytable').DataTable().rows().count()) {
      $("th.select-checkbox").removeClass("selected");
    } else {
      $("th.select-checkbox").addClass("selected");
    }
  });

</script>

@endsection
@endsection