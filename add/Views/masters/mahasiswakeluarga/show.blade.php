@extends('layouts.admin')
@section('content')
@foreach($mahasiswas as $mahasiswa)@endforeach

<div class="card">
    <div class="card-header">Keluarga  {{ $mahasiswa->nama }}</div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>Hubungan</th>
                    <td>{{ $mahasiswakeluarga->hubungan }}</td>
                </tr>
                <tr>
                    <th>Nama</th>
                    <td>{{ $mahasiswakeluarga->nama}}</td>
                </tr>
                <tr>
                    <th>Tanggal Lahir</th>
                    <td>{{ $mahasiswakeluarga->tanggal_lahir}}</td>
                </tr>
                <tr>
                    <th>Tempat Lahir</th>
                    <td>{{ $mahasiswakeluarga->tempat_lahir}}</td>
                </tr>
                <tr>
                    <th>Alamat Pekerjaan</th>
                    <td>{{ $mahasiswakeluarga->alamat_pekerjaan}}</td>
                </tr>
                <tr>
                    <th>Pendidikan Terakhir</th>
                    <td>{{ $mahasiswakeluarga->pendidikan_terakhir}}</td>
                </tr>
                <tr>
                    <th>No HP</th>
                    <td>{{ $mahasiswakeluarga->no_hp}}</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>{{ $mahasiswakeluarga->email }}</td>
                </tr>
            </tbody>
        </table>
        <a href="/indexkeluarga/{{$mahasiswakeluarga->mahasiswa_id }}">
            <input type="button" class="btn btn-warning" value="Back">
        </a>
    </div>
</div>

@endsection