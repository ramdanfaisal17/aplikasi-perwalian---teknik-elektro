<div class="form-group">
    <label for="kode">Kode Proyek Pendidikan</label>
    <input type="text" id="kode" name="kode" class="form-control" required autocomplete="off" value="{{ old('kode', $proyekpendidikan->kode) }}">
    @if($errors->has('kode'))
      <small class="form-text text-danger">*{{ $errors->first('kode') }}</small>
    @endif
</div>

<div class="form-group">
    <label for="nama">Nama Proyek Pendidikan</label>
    <input type="text" id="nama" name="nama" class="form-control" required autocomplete="off" value="{{ old('nama', $proyekpendidikan->nama) }}">
    @if($errors->has('nama'))
      <small class="form-text text-danger">*{{ $errors->first('nama') }}</small>
    @endif
</div>

<div class="form-group">
    <label for="nama">Jenis Semester</label>
    <select class="form-control" name="jenis_semester" id="exampleFormControlSelect1">
      <option value="ganjil" {{ $proyekpendidikan->jenis_semester == 'ganjil' ? 'selected' : '' }}>Ganjil</option>
      <option value="genap" {{ $proyekpendidikan->jenis_semester == 'genap' ? 'selected' : '' }}>Genap</option>
      <option value="antara" {{ $proyekpendidikan->jenis_semester == 'antara' ? 'selected' : '' }}>Antara</option>
    </select>
    @if($errors->has('jenis_semester'))
      <small class="form-text text-danger">*{{ $errors->first('jenis_semester') }}</small>
    @endif
</div>

<div class="form-group">
    <label for="end_prs">Mulai</label>
    <input type="text" data-provide="datepicker" id="mulai" class="form-control datepicker" name="mulai" value="{{ old('mulai', $proyekpendidikan->mulai) }}">
    @if($errors->has('mulai'))
      <small class="form-text text-danger">*{{ $errors->first('mulai') }}</small>
    @endif
</div>

<div class="form-group">
    <label for="end_prs">Berakhir</label>
    <input type="text" data-provide="datepicker" id="berakhir" class="form-control datepicker" name="berakhir" autocomplete="off" value="{{ old('berakhir', $proyekpendidikan->berakhir) }}">
    @if($errors->has('berakhir'))
      <small class="form-text text-danger">*{{ $errors->first('berakhir') }}</small>
    @endif
</div>
