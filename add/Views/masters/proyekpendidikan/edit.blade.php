@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('proyekpendidikan.index') }}">Master Proyek Pendidikan</a></li>
    <li class="breadcrumb-item active" aria-current="page">Ubah Data - {{ $proyekpendidikan->kode }}</li>
  </ol>
</nav>
@endsection

@section('content')

<div class="card">
    <div class="card-header">Ubah Proyek Pendidikan {{ $proyekpendidikan->nama }}</div>

    <div class="card-body">
        <form action="{{ route("proyekpendidikan.update", [$proyekpendidikan->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <input type="hidden" name="id" value="{{ $proyekpendidikan->id }}">

            @include('masters.proyekpendidikan.form')

            <div>
                <a href="{{ route('proyekpendidikan.index') }}" class="btn btn-warning">
                    <i class="fas fa-arrow-left"></i> Kembali
                </a>
                <input class="btn btn-success" type="submit" value="Simpan">
            </div>
        </form>
    </div>
</div>

@endsection
