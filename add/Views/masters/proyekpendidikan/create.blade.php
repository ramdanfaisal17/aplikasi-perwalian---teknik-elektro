@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('proyekpendidikan.index') }}">Master Proyek Pendidikan</a></li>
    <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
  </ol>
</nav>
@endsection

@section('content')

<div class="card">
    <div class="card-header">Tambah Proyek Pendidikan</div>
    <div class="card-body">
        <form action="{{ route("proyekpendidikan.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('post')

            @include('masters.proyekpendidikan.form')

            <div>
                <input class="btn btn-success" type="submit" value="Simpan">
            </div>
        </form>
    </div>
</div>

@endsection
