@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('proyekpendidikan.index') }}">Master Proyek Pendidikan</a></li>
    <li class="breadcrumb-item active" aria-current="page">Lihat Data - {{ $proyekpendidikan->kode }}</li>
  </ol>
</nav>
@endsection

@section('content')

<div class="card">
    <div class="card-header">Proyek Pendidikan {{ $proyekpendidikan->nama }}</div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>Kode Proyek Pendidikan</th>
                    <td>{{ $proyekpendidikan->kode }}</td>
                </tr>
                <tr>
                    <th>Nama Proyek Pendidikan</th>
                    <td>{{ $proyekpendidikan->nama}}</td>
                </tr>
                <tr>
                    <th>Jenis Semester</th>
                    <td>{{ $proyekpendidikan->jenis_semester}}</td>
                </tr>
                <tr>
                    <th>Mulai</th>
                    <td>{{ $proyekpendidikan->mulai ? date('d/m/Y', strtotime($proyekpendidikan->mulai)) : ' - ' }}</td>
                </tr>
                <tr>
                    <th>Berakhir</th>
                    <td>{{ $proyekpendidikan->berakhir ? date('d/m/Y', strtotime($proyekpendidikan->berakhir)) : ' - '}}</td>
                </tr>
            </tbody>
        </table>
        <a href="{{ route('proyekpendidikan.index') }}" class="btn btn-warning">
            <i class="fas fa-arrow-left"></i> Kembali
        </a>
    </div>
</div>

@endsection
