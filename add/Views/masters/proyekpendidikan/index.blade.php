@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item active" aria-current="page">Master Proyek Pendidikan</li>
  </ol>
</nav>
@endsection

@section('content')


<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Data Proyek Pendidikan</h3>
</div>
<div class="col-lg-8 text-md-right">
    <button class="btn btn-danger" onclick="$('.dtb-delete').click();" style="min-width: 100px;">
      <i class="fas fa-trash"></i> Hapus Pilihan
  </button>
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#importExcel" style="min-width: 100px;">
      <i class="fas fa-file-upload"></i> Impor Excel
  </button>
  <button type="button" class="btn btn-warning" onclick="$('.dtb-excel').click();" style="min-width:100px;">
      <i class="fas fa-print"></i> Ekspor Excel
  </button>
  <a class="btn btn-success" href="{{ route("proyekpendidikan.create") }}" style="min-width: 100px;">
      <i class="fas fa-plus"></i> Tambah Data
  </a>



  {{-- notifikasi form validasi --}}
  @if ($errors->has('file'))
  <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('file') }}</strong>
  </span>
  @endif

  <!-- Import Excel -->
  <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <form method="post" action="{{ route('proyekpendidikan.import') }}" enctype="multipart/form-data">

          <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
              </div>
              <div class="modal-body">
                  @csrf
                  <label>Pilih file excel</label>
                  <div class="form-group">
                    <input type="file" name="file" required="required">
                </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Import</button>
          </div>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>

<!-- Flash message here -->
@include('_partial.flash_message')

<div class="card">

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table datatable" id="mytable">
                <thead>
                    <tr>
                        <th width="10">
                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck2">
                            <label class="custom-control-label" for="customCheck2"></label>
                        </div>
                    </th>
                    <th width="10%">Aksi</th>
                    <th width="20%">Kode Proyek Pendidikan</th>
                    <th width="30%">Nama Proyek Pendidikan</th>
                    <th width="15%">Mulai</th>
                    <th width="15%">Berakhir</th>
                    <th width="10%">Jenis Semester</th>
                </tr>
            </thead>
            <tbody>
                @foreach($proyekpendidikans as $item)
                <tr data-entry-id="{{ $item->id }}">
                    <td></td>
                    <td>
                        <form action="{{ route('proyekpendidikan.destroy', $item->id) }}" method="POST" title=" Hapus Data"onsubmit="return confirm('Apakah anda yakin ?');" style="display: inline-block;">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button type="submit" name="button" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i></button>
                        </form>
                        <a class="btn btn-xs btn-primary" title="Ubah Data"href="{{ route('proyekpendidikan.edit', $item->id) }}">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a class="btn btn-xs btn-success" title="Lihat Data" href="{{ route('proyekpendidikan.show', $item->id) }}">
                            <i class="fa fa-eye"></i>
                        </a>
                    </td>
                    <td>{{ $item->kode }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->mulai ? $item->mulai : '-' }}</td>
                    <td>{{ $item->berakhir ? $item->berakhir : '-' }}</td>
                    <td>{{ $item->jenis_semester }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
@section('scripts')
@parent
<script>
  $(function () {
    let deleteButtonTrans = 'delete selected'
    let deleteButton = {
      text: deleteButtonTrans,
      className: 'btn-danger dtb-delete hilang',
      action: function (e, dt, node, config) {
        var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
        });

        if (ids.length === 0) {
          alert('tidak ada yang dipilih !')
          return
        }
        if (confirm('beneran ??')) {
          $.ajax({
            headers: {'x-csrf-token': _token},
            type: "POST",
            data: {ids:ids},
            url: "proyekpendidikan/deletes",
            success: function(){
             location.reload();
           }
         });
        }
      }
    }


    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

    dtButtons.push(deleteButton)


    $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  })

  function deleteSelected(){
    $('.tomboldeleteall').click();
  }

  $("#customCheck2").change( function() {
    if ($("th.select-checkbox").hasClass("selected")) {
      $('#mytable').DataTable().rows().deselect();
      $("th.select-checkbox").removeClass("selected");
    } else {
      $('#mytable').DataTable().rows().select();
      $("th.select-checkbox").addClass("selected");
    }
  }).on("select deselect", function() {
    ("Some selection or deselection going on")
    if ($('#mytable').DataTable().rows({
      selected: true
    }).count() !== $('#mytable').DataTable().rows().count()) {
      $("th.select-checkbox").removeClass("selected");
    } else {
      $("th.select-checkbox").addClass("selected");
    }
  });

</script>

@endsection
@endsection
