@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('dosen.index') }}">Master Dosen</a></li>
    <li class="breadcrumb-item active" aria-current="page">Lihat Data - {{$dosen->nik}}</li>
  </ol>
</nav>
@endsection

@section('content')

<div class="card">
    <div class="card-header">Dosen  {{ $dosen->nama }}</div>

    <div class="card-body">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <th>NIK</th>
                    <td>{{ $dosen->nik }}</td>
                </tr>
                <tr>
                    <th>Nama Dosen</th>
                    <td>{{ $dosen->nama}}</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>{{ $dosen->email }}</td>
                </tr>
                <tr>
                    <th>total mahasiswa</th>
                    <td>{{ $dosen->total_mahasiswa }}</td>
                </tr>

            </tbody>
        </table>

        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th width="10">No</th>
                    <th>NRP</th>
                    <th>Nama</th>
                    <th>Semester</th>
                </tr>
            </thead>
            <tbody>
                @if($dosendetails->count() > 0)
                  @foreach($dosendetails as $key => $mahasiswa)
                  <tr>
                      <td>{{$key + 1}}</td>
                      <td>{{$mahasiswa['nrp']}}</td>
                      <td>{{$mahasiswa['nama']}}</td>
                      <td>{{$mahasiswa['semester']}}</td>
                  </tr>
                  @endforeach
                @else
                  <tr>
                    <td colspan="4" class="text-center">No Data Available</td>
                  </tr>
                @endif
            </tbody>
        </table>
        <a href="{{ route('dosen.index') }}" class="btn btn-warning">
            <i class="fas fa-arrow-left"></i> Kembali
        </a>
    </div>
</div>

@endsection
