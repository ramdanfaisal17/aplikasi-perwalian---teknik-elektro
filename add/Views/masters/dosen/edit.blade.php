@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('dosen.index') }}">Master Dosen</a></li>
    <li class="breadcrumb-item active" aria-current="page">Ubah Data - {{ $dosen->nik }}</li>
  </ol>
</nav>
@endsection

@section('content')


<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Dosen Wali</h3>
  </div>
</div>

<form action="{{ route("dosen.update", [$dosen->id]) }}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('PUT')
  <input type="hidden" name="id" value="{{ $dosen->id }}">

  <div class="card" style="margin-top: -15px;">

    <div class="card-body">


      <div class="tab-pane fade show active" id="pills-personal-info" role="tabpanel" aria-labelledby="pills-personal-info-tab">
        <div class="row">
          <div class="col-md-6">
              <div class="form-group">
                <label for="nik">NIK</label>
                <input type="number" id="nik" name="nik" class="form-control" required autocomplete="off" value="{{ old('nik', $dosen->nik) }}">
                @if($errors->has('nik'))
                  <small class="form-text text-danger">*{{ $errors->first('nik') }}</small>
                @endif
            </div>
            <div class="form-group">
                <label for="nama">Nama Dosen</label>
                <input type="text" id="nama" name="nama" class="form-control" required autocomplete="off" value="{{ old('nama', $dosen->nama) }}">
                @if($errors->has('nama'))
                  <small class="form-text text-danger">*{{ $errors->first('nama') }}</small>
                @endif
            </div>


        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" id="email" name="email" class="form-control" required autocomplete="off" value="{{ old('email', $dosen->email) }}">
                @if($errors->has('email'))
                  <small class="form-text text-danger">*{{ $errors->first('email') }}</small>
                @endif
            </div>
            <div class="form-group">
                <label for="total_mahasiswa">total mahasiswa</label>
                <input type="text" id="total_mahasiswa" readonly name="total_mahasiswa" class="form-control" required autocomplete="off" value="{{$dosen->total_mahasiswa}}">
            </div>
        </div>


    </div>


        <div class="row">
          <div class="col-md-12">

            <table class="table datatable" id="tabeldetail">
              <thead>
                <tr>
                  <th width="10">
                   <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal" style="margin:-5px -10px !important;"><i class="fa fa-plus"></i></button> -->
                   <a href='{{ route("data.mahasiswa.show") }}' title='Tambah Mahasiswa' class="btn btn-primary modal-add-row">
                    <i class="fa fa-plus"></i>
                  </a>
                </th>
                <th width="100">Action</th>
                <th>NRP</th>
                <th>Nama</th>
                <th>Status Mahasiswa</th>
              </tr>
            </thead>
            <tbody id="tbody-mahasiswa">
              @if($details->count() > 0)
                @foreach($details as $key => $detail)
                <tr><input type="hidden" value="{{ $detail->id }}" name="iddetail[]"><input type="hidden" value="{{ $detail->mahasiswa_id }}" name="mahasiswa_id[]">
                  <td class="td-id" data-id="{{ $detail->id }}"><center>{{ $key + 1 }}</center></td>
                  <td> <a href="#" class="btn btn-danger btn-sm btn-remove-row" id={{$key + 1}}><i class="fa fa-trash"></i></a></td>
                  <td>{{$detail['nrp']}}</td>
                  <td>{{$detail['nama']}}</td>
                  <td>{{$detail['status_mahasiswa']}}</td>
                </tr>
                @endforeach
              @else
                <tr>
                  <td colspan="5" class="text-center">No data available</td>
                </tr>
              @endif
            </tbody>

          </table>

        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
        </div>
        <div class="col-md-6 text-md-right">

        </div>
      </div>
    </div>

  </div>







</div>
<button class="btn btn-success" type="submit">Simpan</button>

</div>
{{-- modaltambah --}}

</form>
@endsection
@include('layouts.modal')
<script src="{{ asset('jquery-3.4.1.min.js') }}"></script>

<script>
//function for array checker
function check_array(value, arr){
  var status = false;
  for(var i=0; i<arr.length;i++ ){
    var needle = arr[i].id;
    if(needle == value){
      status = true;
      break;
    }
  }

  return status;
}


$('body').on('click', '#modal-btn-add', function(event){
  event.preventDefault();

  var table = $('#datatable').DataTable();

  var data = table.rows({selected: true}).data();

  var tbody = 'tbody-mahasiswa';

  console.table(data);

  var currentData = [];
  $('#'+tbody+' .td-id').each(function(){
    currentData.push({ id:$(this).data('id') });
  });
    console.table(currentData);
    var no = currentData.length;
    var currentNo = 0;
    for(var i = 0; i < data.length; i++){
      var check = check_array(data[i].idbarang, currentData);
        console.log(check);
        if(!check){
          currentNo++;
          $('#'+tbody).append('<tr>'
            +' <input type="hidden" value="'+data[i].id+'" name="iddetail[]"><input type="hidden" value="'+data[i].id+'" name="mahasiswa_id[]">'
            +' <td class="td-id" data-id="'+data[i].id+'"><center>' + (currentNo+no) + '</center></td>'
            +' <td> <a href="#" class="btn btn-danger btn-sm btn-remove-row" id='+ (currentNo+no)+'><i class="fa fa-trash"></i></a></td>'
            +' <td> ' + data[i].nrp + '</td>'
            +' <td> ' + data[i].nama + '</td>'
            +' <td> ' + data[i].status_mahasiswa + '</td>'
            +'</tr>');
        }
      }
      $('#total_mahasiswa').val(currentNo+no);
      $("#modal").modal('hide');

    });

$('body').on('click','.modal-add-row', function(event){
  event.preventDefault();

  var me = $(this),
  url = me.attr('href'),
  title = me.attr('title');

  $('#modal-title').text(title);
  $('#modal-btn-save').addClass('hide');
  $('#modal-btn-custom').addClass("hide");
  $('.modal-dialog').addClass('modal-lg');

  $('#modal-btn-add').removeClass('hide');

  $.ajax({
    url: url,
    dataType: 'html',
    success: function (response) {
      $('#datatable').DataTable().destroy();
      $('#modal-body').html(response);
    }
  });

  $("#modal").modal('show');

});


$('body').on('click','.btn-remove-row',function(event){
  event.preventDefault();

  var baris = $(this).attr('id');
  document.getElementById("tabeldetail").deleteRow(baris);
  var totalbaris = $('#tabeldetail tr').length;
  var i = 0;
  for (i = 1; i < totalbaris; i++) {
    document.getElementById('tabeldetail').rows[i].cells[0].innerHTML = '<center>' + i + '</center>' ;
 }
 $('#total_mahasiswa').val(totalbaris-1);

});


</script>
