<div class="modal fade" tabindex="-1" role="dialog" id="modaldosen">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body">
        <?php $no='';?>
        {{$no}}
        <form action="mahasiswa/updatedosenwali" id="formupdate" method="POST" enctype="multipart/form-data">
          @csrf
          @method('POST')
          <input type="text" name="id" id="idupdate" value="" hidden />

          <div class="form-group">
            <label for="dosen_id">Dosen</label>
            <select class="form-control" id="dosen_id" name="dosen_id" required>
              <option value="" selected disabled>pilih dosen</option>
              @foreach($dosens as $dosen)
              <option value="{{$dosen->id}}">{{$dosen->nik}}  --  {{$dosen->nama}}</option>
              @endforeach
            </select>
          </div>

          <div>
            <input class="btn btn-success" type="submit" value="Save">
          </div>
        </form>

      </div>
      <div class="modal-footer">

        {{-- <button type="button" class="btn btn-primary" id="modal-btn-add">Add</button> --}}
        {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
      </div>
    </div>
  </div>
</div>
