<div class="row">
  <div class="col-lg-12">
    <table class="table table-hovered table-bordered" id="datatable" style="width: 100%;" data-table="tbody-dosen">
      <thead>
        <tr>
          <th></th>
          <th>No.</th>
          <th>Nik</th>
          <th>Nama</th>
          <th>Email</th>
        </tr>
      </thead>
      <tbody></tbody>
      <tfoot>
        <th></th>
        <th>No.</th>
        <th>Nik</th>
        <th>Nama</th>
        <th>Email</th>
      </tfoot>
    </table>
  </div>
</div>


<script>
  var dataTablesColumns = [
    {},
    {data:"id", name:"id"},
    {data: "nik", name: 'nik'},
    {data: "nama", name: 'nama'},
    {data: "email", name: 'email'},
  ];

  var url = "{{ route('data.dosen') }}";
</script>
<script src="{{ asset('js/customDataTable.js') }}"></script>
