<style type="text/css">
    .sidebar .nav-dropdown-toggle::before {
        right: 1.3rem !important;
    }
    .sidebar .nav-link.active {
        background: #073c63 !important;
    }
    .wrapper {
      display: flex;
      width: 100%;
      align-items: stretch;
  }
</style>

    <!-- Sidebar -->
    <nav id="sidebar">
        <div class="sidebar-header container-fluid">
          <div class="col-12 text-md-left">
            <!-- <img src="{{ asset('Gambar/marnat-3-w_orig.png') }}" height="55px" width="160px;"> -->
              <i style="color: white ; font-size: 15px;">Fakultas Teknik Elektro</i>

          </div>
        </div>

        <ul class="list-unstyled components sidebar">
            <li class="{{ $halaman == 'dashboard' ? 'active' : '' }}">
                <a href="{{ route("home") }}" class="nav-link">
                      <i class="fas fa-tachometer-alt"></i> Dashboard
                </a>
            </li>
            <li class="">
                <a href="#masterSubMenu" data-toggle="collapse" data-target="#masterSubMenu" aria-expanded="false" aria-controls="masterSubMenu" class="dropdown-toggle">
                    <i class="fas fa-users nav-icon"></i> Master
                </a>
                <ul class="list-unstyled sidebarSubMenu collapse {{ $masterHalaman == 'master' ? 'show' : '' }}" id="masterSubMenu">
                    @if (Auth::user()->name == 'admin')
                    <li class="nav-item {{ $halaman == 'user' ? 'active' : '' }}">
                        <a href="{{ route("user.index") }}" class="nav-link">
                            <i class="fas fa-angle-right"></i> User
                        </a>
                    </li>
                    @endif
                    <li class="nav-item {{ $halaman == 'dosen' ? 'active' : '' }}">
                        <a href="{{ route("dosen.index") }}" class="nav-link">
                            <i class="fas fa-angle-right"></i> Dosen
                        </a>
                    </li>
                    <li class="nav-item {{ $halaman == 'programstudi' ? 'active' : '' }}">
                        <a href="{{ route("programstudi.index") }}" class="nav-link">
                            <i class="fas fa-angle-right"></i> Program Studi
                        </a>
                    </li>
                    <li class="nav-item {{ $halaman == 'proyekpendidikan' ? 'active' : '' }}">
                        <a href="{{ route("proyekpendidikan.index") }}" class="nav-link">
                            <i class="fas fa-angle-right"></i> Proyek Pendidikan
                        </a>
                    </li>
                    <li class="nav-item {{ $halaman == 'matakuliah' ? 'active' : '' }}">
                        <a href="{{ route("matakuliah.index") }}" class="nav-link">
                            <i class="fas fa-angle-right"></i> Mata Kuliah
                        </a>
                    </li>
                    <li class="nav-item {{ $halaman == 'mahasiswa' ? 'active' : '' }}">
                        <a href="{{ route("mahasiswa.index") }}" class="nav-link">
                            <i class="fas fa-angle-right"></i> Mahasiswa
                        </a>
                    </li>

                </ul>
            </li>
            <li class="">
                <a href="#transaksiSubMenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-refresh nav-icon"></i> Transaksi
                </a>
                <ul class="list-unstyled collapse {{ $masterHalaman == 'transaksi' ? 'show' : '' }}" id="transaksiSubMenu">
                    <li class="nav-item {{ $halaman == 'matakuliahwajib' ? 'active' : '' }}">
                        <a href="{{ route("matakuliahwajib.index") }}" class="nav-link">
                            <i class="fas fa-angle-right"></i> Mata Kuliah wajib
                        </a>
                    </li>
                    <li class="">
                        <a href="#perwalianSubMenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <i class="fa fa-minus nav-icon"></i> Perwalian
                        </a>
                        <ul class="collapse {{ $halaman == 'jadwalPerwalian' || $halaman == 'perwalian' ? 'show' : '' }} list-unstyled" id="perwalianSubMenu" style="margin-left:10%">
                            <li class="nav-item {{ $halaman == 'jadwalPerwalian' ? 'active' : '' }}">
                              <a href="{{ route("jadwalPerwalian.index") }}">
                                  <i class="fas fa-angle-right"></i> Jadwal
                              </a>
                            </li>
                            <li class="nav-item {{ $halaman == 'perwalian' ? 'active' : '' }}">
                              <a href="{{ route('perwalian.index') }}">
                                <i class="fas fa-angle-right"></i> Hasil
                              </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item {{ $halaman == 'nilai' ? 'active' : '' }}">
                        <a href="{{ route("nilai.index") }}" class="nav-link">
                            <i class="fas fa-angle-right"></i> Nilai
                        </a>
                    </li>

                    <li class="nav-item {{ $halaman == 'nilaiberkas' ? 'active' : '' }}">
                        <a href="{{ route("nilaiberkas.index") }}" class="nav-link">
                            <i class="fas fa-angle-right"></i> Nilai Berkas
                        </a>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="#beritaSubMenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-newspaper nav-icon"></i> Berita
                </a>
                <ul class="collapse list-unstyled" id="beritaSubMenu">
                    <li class="nav-item">
                        <a href="{{ route("berita.pengumuman.index") }}" class="nav-link">
                            <i class="fas fa-angle-right"></i> Pengumuman
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('berita.akademik.index') }}" class="nav-link">
                            <i class="fas fa-angle-right"></i> Info Akademik
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('berita.prestasi.index') }}" class="nav-link">
                            <i class="fas fa-angle-right"></i> Input Prestasi
                        </a>
                    </li>
              </ul>
            </li>
            <li class="">
                <a href="#laporanSubMenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fa fa-sticky-note nav-icon"></i> Laporan
                </a>
                <ul class="collapse list-unstyled" id="laporanSubMenu">
                    <li class="nav-item">
                            <a href="{{url('laporanabsen')}}" class="nav-link">
                                <i class="fas fa-angle-right"></i> Absensi Perwalian
                            </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('laporanpilihmatkul')}}" class="nav-link">
                            <i class="fas fa-angle-right"></i> Hasil Pilih Mata Kuliah
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('laporankonsul')}}" class="nav-link">
                            <i class="fas fa-angle-right"></i> Konsultasi
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('nilai.laporan') }}" class="nav-link">
                            <i class="fas fa-angle-right"></i> Nilai
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('laporanhasil')}}" class="nav-link">
                            <i class="fas fa-angle-right"></i> Hasil Perwalian
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>

    <style type="text/css">
    .sidebar .nav-dropdown-toggle::before {
        right: 1.3rem !important;
    }
    .sidebar .nav-link.active {
        background: #073c63 !important;
    }
</style>
