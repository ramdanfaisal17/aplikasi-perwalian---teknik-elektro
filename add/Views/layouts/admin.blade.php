<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ $title }}</title>

  <link rel="icon" href="{{ asset('Gambar/marnat_6_orig.png') }}" type="image/x-icon"> 

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
  <link href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css" rel="stylesheet" />
  {{-- <link href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css" rel="stylesheet" /> --}}
  <link href="{{ asset('css/select.css') }}" rel="stylesheet" />
  <link href="https://unpkg.com/@coreui/coreui/dist/css/coreui.min.css" rel="stylesheet" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
  <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />
  <link href="{{ asset('css/myCss.css') }}" rel="stylesheet" />
  <link href="{{ asset('css/myStyle.css') }}" rel="stylesheet" />
  <link href="{{ asset('css/radiobutton.css') }}" rel="stylesheet" />


  @yield('styles')
</head>

  <body class="app header-fixed sidebar-fixed aside-menu-fixed pace-done sidebar-lg-show">
    <div class="wrapper">

    @include('layouts.sidebar')

    <div id="content">
      @include('layouts.header')
      @yield('breadcrumb')
      <div class="app-body">
        <main class="main">
          <div style="padding-top: 20px" class="container-fluid">
            @yield('content')
          </div>
        </main>
        <form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
        </div>
    </div>

  </div>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/@coreui/coreui/dist/js/coreui.min.js"></script>
        <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script src="//cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
        <script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
        <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
        <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
        <script src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
        <script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/classic/ckeditor.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
        <script src="{{ asset('js/main.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/imageReader.js') }}"></script>
        {{-- <script src="{{ asset('coreui/js/dt-setting.js') }}"></script> --}}

        @yield('varNameExport')

        <script type="text/javascript">
          $(function() {
            let copyButtonTrans = 'Copy'
            let csvButtonTrans = 'CSV'
            let excelButtonTrans = 'Excel'
            let pdfButtonTrans = 'PDF'
            let printButtonTrans = 'Print'
            let colvisButtonTrans = 'Hide'

            let languages = {
              'en': 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/English.json'
            };

            $.extend(true, $.fn.dataTable.Buttons.defaults.dom.button, { className: 'btn' })
            $.extend(true, $.fn.dataTable.defaults, {
              language: {
                url: languages.{{ app()->getLocale() }}
              },
              columnDefs: [
              {
               orderable: false,
               className: 'select-checkbox',
               targets: 0
             },
             {
              orderable: false,
              searchable: false,
              targets: [0,1]
            }
            ],

            select: {
              style:    'multi+shift',
              selector: 'td:first-child'
            },
            order: [],
            scrollX: true,
            pageLength: 10,
            dom: 'lBfrtip<"actions">',


            buttons: [
            {
              extend: 'copy',
              className: 'btn-default dtb-copy hilang',
              text: copyButtonTrans,
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'csv',
              className: 'btn-default dtb-csv hilang',
              text: csvButtonTrans,
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'excel',
              className: 'btn-default dtb-excel hilang',
              text: excelButtonTrans,
              title:  typeof exportFileName !== "undefined" ? exportFileName : 'Aplikasi Perwalian',
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'pdf',
              className: 'btn-default dtb-pdf hilang',
              text: pdfButtonTrans,
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'print',
              className: 'btn-default dtb-print hilang',
              text: printButtonTrans,
              exportOptions: {
                columns: ':visible'
              }
            },
            {
              extend: 'colvis',
              className: 'btn-default dtb-colvis hilang',
              text: colvisButtonTrans,
              exportOptions: {
                columns: ':visible'
              }
            }
            ]
          });

            $.fn.dataTable.ext.classes.sPageButton = '';
          });


        </script>
        <script type="text/javascript">
          $(document).ready(function () {

            $('#sidebarCollapse').on('click', function (e) {
                e.preventDefault();
                $('#sidebar').toggleClass('active');
            });

          });
        </script>

        <script type="text/javascript">

          $('.datepicker').datepicker({
              format: 'dd-mm-yyyy',
          });

        </script>
        @yield('scripts')
      </body>

      </html>

      <style>
      .hilang{
        display: none;
      }

      td>.dropdown>.dropdown-toggle:after { content: none }
      td>.dropdown>.dropdown-toggle { padding: 0px 10px; border-color: grey; }
      td>.dropdown>.dropdown-menu>.dropdown-item:hover { background: #007bff; color: white; opacity: 0.8;}
      td>.dropdown>.dropdown-menu> #hapus:hover { background: #F64846; color: white; opacity: 0.8;cursor: pointer;}
      td>.dorpdown{max-height: 20px;margin-top: 22px;}


    </style>
