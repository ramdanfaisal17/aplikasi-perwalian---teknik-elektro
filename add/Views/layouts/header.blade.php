<nav class="navbar navbar-expand-lg navbar-light" id="navbar-top">
  <div class="container-fluid">
      <a href="button" id="sidebarCollapse" class="">
          <i class="fa fa-bars" style="color: white ; font-size: 16px;"></i>
      </a>
      <ul class="nav navbar-nav ml-auto mr-3" >
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-cog" style="color:white;"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right shadow mt-2">
                  <a class="dropdown-item">
                      <i class="fas fa-user" style="color:#002865;"></i> &nbsp; {{ Auth::user()->nik }} - {{ Auth::user()->name }}
                      {{-- <small class="text-muted">{{ Auth::user()->email }}</small> --}}
                  </a>
                  {{-- @if (auth::user()->relasi == 'Dosen')
                  <a class="dropdown-item" href="{{route('dosen.edit' , auth::user()->relasi_id)}}">
                      <i class="fas fa-user"></i>&nbsp; Profile
                  </a>
                  @endif --}}

                  @if (auth::user()->relasi == 'Mahasiswa')
                  <a class="dropdown-item" href="{{route('mahasiswa.edit' , auth::user()->relasi_id)}}">
                      <i class="fas fa-user"></i>	&nbsp; Profile
                  </a>
                  @endif

                  <div class="divider"></div>
                  <a class="dropdown-item" href="user/gantipassword">
                      <i class="fas fa-key" style="color:#002865;"></i>	&nbsp; Password
                  </a>
                  <div class="divider"></div>
                  <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                      <i class="fas fa-sign-out-alt" style="color:#002865;"></i>	&nbsp; {{ __('Logout') }}
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
              </div>
          </li>
      </ul>
  </div>
</nav>
