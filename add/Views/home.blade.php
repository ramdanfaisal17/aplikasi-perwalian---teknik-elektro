    @extends('layouts.admin')
    @section('content')
    <div class="content">
        <div class="row">
            <div class="col-xl-3 col-sm-6 py-2">
               <div class="card bg-success text-white h-100">
                   <div class="card-body bg-success">
                       <div class="rotate">
                           <i class="fa fa-user fa-4x"></i>
                       </div>
                       <h6 class="text-uppercase">Dosen Wali</h6>
                       <h1 class="display-4">{{ isset($jadwalperwalian) ? $jadwalperwalian->dosenWaliCount : '' }}</h1>
                   </div>
               </div>
           </div>
           <div class="col-xl-3 col-sm-6 py-2">
                <div class="card text-white bg-info h-100">
                    <div class="card-body bg-info">
                        <div class="rotate">
                            <i class="fas fa-user-graduate fa-4x"></i>
                        </div>
                        <h6 class="text-uppercase">Mahasiswa</h6>
                        <h1 class="display-4">{{ isset($jadwalperwalian) ? $jadwalperwalian->mahasiswaCount : '' }}</h1>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 py-2">
                 <div class="card text-white bg-primary h-100">
                     <div class="card-body bg-primary">
                         <div class="rotate">
                             <i class="far fa-calendar-alt fa-4x"></i>
                         </div>
                         <h6 class="text-uppercase">Hadir</h6>
                         <h1 class="display-4">{{ isset($jadwalperwalian) ? $jadwalperwalian->mahasiswaHadirCount : '' }}</h1>
                     </div>
                 </div>
             </div>
             <div class="col-xl-3 col-sm-6 py-2">
                 <div class="card text-white bg-danger h-100">
                     <div class="card-body bg-danger">
                         <div class="rotate">
                             <i class="fa fa-share fa-4x"></i>
                         </div>
                         <h6 class="text-uppercase">Tidak Hadir</h6>
                         <h1 class="display-4">{{ isset($jadwalperwalian) ? ($jadwalperwalian->mahasiswaCount - $jadwalperwalian->mahasiswaHadirCount) : '' }}</h1>
                     </div>
                 </div>
             </div>
        </div>

<div style="margin-bottom: 10px;" class="row mt-3">
  <div class="col-md-12">
    <h3>LAPORAN PERWALIAN</h3>
        <table style="height:10%;" class="table table-bordered">
            <tbody>
                <tr>
                    <th >PROYEK PENDIDIKAN</th>
                    <td colspan="2">{{ isset($proyekpendidikan) ? strtoupper($proyekpendidikan->nama) : '' }}</td>
                    <tH style="text-align:center; vertical-align: inherit" rowspan="2">KEHADIRAN</td>
                    <tH style="text-align:center; vertical-align: inherit" rowspan="2">STATUS</td>
                </tr>
                <tr>
                    <th>PROGRAM STUDI</th>
                    <td colspan="2">S1 - TEKNIK ELEKTRO</td>
                </tr>
                <tr>
                    <th style="vertical-align: inherit">1. PERUBAHAN RENCANA STUDI</th>
                    <td style="text-align:center; vertical-align: inherit">{{ isset($jadwalperwalian) ? date('d F Y', strtotime($jadwalperwalian->start_prs)) : ' - ' }}</td>
                    <td style="text-align:center; vertical-align: inherit">{{ isset($jadwalperwalian) ? date('d F Y', strtotime($jadwalperwalian->end_prs)) : ' - ' }}</td>
                    <td style="text-align:center;">
                        {{ isset($jadwalperwalian) ? $jadwalperwalian->mahasiswaHadirPRSCount : ' - ' }}
                    </td>
                    <td style="text-align:center;">
                        {!! isset($jadwalperwalian) && (date('Y-m-d') >= $jadwalperwalian->start_prs) ? (date('Y-m-d') <= $jadwalperwalian->end_prs ? '<span class="badge badge-warning"> Sedang Berjalan </span>' : '<span class="badge badge-success"> Selesai </span>') : '<span class="badge badge-secondary"> Belum dimulai </span>' !!}
                    </td>
                </tr>

                <tr>
                    <th style="vertical-align: inherit">2. KONSULTASI 1</th>
                    <td style="text-align:center; vertical-align: inherit">{{ isset($jadwalperwalian) ? date('d F Y', strtotime($jadwalperwalian->start_konsultasi1)) : ' - ' }}</td>
                    <td style="text-align:center; vertical-align: inherit">{{ isset($jadwalperwalian) ? date('d F Y', strtotime($jadwalperwalian->end_konsultasi1)) : ' - ' }}</td>
                    <td style="text-align:center;">
                      {{ isset($jadwalperwalian) ? $jadwalperwalian->mahasiswaHadirKonsultasi1Count : ' - ' }}
                    </td>
                    <td style="text-align:center;">
                        {!! isset($jadwalperwalian) && (date('Y-m-d') >= $jadwalperwalian->start_konsultasi1) ? (date('Y-m-d') <= $jadwalperwalian->end_konsultasi1 ? '<span class="badge badge-warning"> Sedang Berjalan </span>' : '<span class="badge badge-success"> Selesai </span>') : '<span class="badge badge-secondary"> Belum dimulai </span>' !!}
                    </td>
                </tr>
                <tr>
                    <th style="vertical-align: inherit">3. KONSULTASI 2</th>
                    <td style="text-align:center; vertical-align: inherit">{{ isset($jadwalperwalian) ? date('d F Y', strtotime($jadwalperwalian->start_konsultasi2)) : ' - ' }}</td>
                    <td style="text-align:center; vertical-align: inherit">{{ isset($jadwalperwalian) ? date('d F Y', strtotime($jadwalperwalian->end_konsultasi2)) : ' - ' }}</td>
                    <td style="text-align:center;">
                      {{ isset($jadwalperwalian) ? $jadwalperwalian->mahasiswaHadirKonsultasi2Count : ' - ' }}
                    </td>
                    <td style="text-align:center;">
                        {!! isset($jadwalperwalian) && (date('Y-m-d') >= $jadwalperwalian->start_konsultasi2) ? (date('Y-m-d') <= $jadwalperwalian->end_konsultasi2 ? '<span class="badge badge-warning"> Sedang Berjalan </span>' : '<span class="badge badge-success"> Selesai </span>') : '<span class="badge badge-secondary"> Belum dimulai </span>' !!}
                    </td>
                </tr>
                <tr>
                    <th style="vertical-align: inherit">4. ANTARA</th>
                    <td style="text-align:center; vertical-align: inherit">{{ isset($jadwalperwalian) ? date('d F Y', strtotime($jadwalperwalian->start_antara)) : ' - ' }}</td>
                    <td style="text-align:center; vertical-align: inherit">{{ isset($jadwalperwalian) ? date('d F Y', strtotime($jadwalperwalian->end_antara)) : ' - ' }}</td>
                    <td style="text-align:center;">
                        0
                    </td>
                    <td style="text-align:center;">
                        {!! isset($jadwalperwalian) && $jadwalperwalian->start_antara ? (date('Y-m-d') >= $jadwalperwalian->start_antara) ? (date('Y-m-d') <= $jadwalperwalian->end_antara ? '<span class="badge badge-warning"> Sedang Berjalan </span>' : '<span class="badge badge-success"> Selesai </span>') : '<span class="badge badge-secondary"> Belum dimulai </span>' : '-' !!}
                    </td>
                </tr>
                <tr>
                    <th style="vertical-align: inherit">  GANJIL</th>
                    <td style="text-align:center; vertical-align: inherit">{{ isset($jadwalperwalian) && $jadwalperwalian->start_ganjil ? date('d F Y', strtotime($jadwalperwalian->start_ganjil)) : '-'}}</td>
                    <td style="text-align:center; vertical-align: inherit">{{ isset($jadwalperwalian) && $jadwalperwalian->end_ganjil ? date('d F Y', strtotime($jadwalperwalian->end_ganjil)) : '-' }}</td>
                    <td style="text-align:center;">
                        0
                    </td>
                    <td style="text-align:center;">
                        {!! isset($jadwalperwalian) && $jadwalperwalian->start_ganjil ? (date('Y-m-d') >= $jadwalperwalian->start_ganjil) ? (date('Y-m-d') <= $jadwalperwalian->end_ganjil ? '<span class="badge badge-warning"> Sedang Berjalan </span>' : '<span class="badge badge-success"> Selesai </span>') : '<span class="badge badge-secondary"> Belum dimulai </span>' : '-' !!}
                    </td>
                </tr>
                <tr>
                    <th style="vertical-align: inherit">  GENAP</th>
                    <td style="text-align:center; vertical-align: inherit">{{ isset($jadwalperwalian) && $jadwalperwalian->start_genap ? date('d F Y', strtotime($jadwalperwalian->start_genap)) : '-' }}</td>
                    <td style="text-align:center; vertical-align: inherit">{{ isset($jadwalperwalian) && $jadwalperwalian->end_genap ? date('d F Y', strtotime($jadwalperwalian->end_genap)) : '-' }}</td>
                    <td style="text-align:center;">
                        0
                    </td>
                    <td style="text-align:center;">
                        {!! isset($jadwalperwalian) && $jadwalperwalian->start_genap ? (date('Y-m-d') >= $jadwalperwalian->start_genap) ? (date('Y-m-d') <= $jadwalperwalian->end_genap ? '<span class="badge badge-warning"> Sedang Berjalan </span>' : '<span class="badge badge-success"> Selesai </span>') : '<span class="badge badge-secondary"> Belum dimulai </span>' : '-' !!}
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>
@endsection
@section('scripts')
@parent
@endsection
