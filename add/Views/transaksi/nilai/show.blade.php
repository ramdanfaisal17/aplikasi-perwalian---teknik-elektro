@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('nilai.index') }}">Nilai Index</a></li>
    <li class="breadcrumb-item active" aria-current="page">Lihat Data - {{ $nilai->mahasiswa->nrp }}</li>
  </ol>
</nav>
@endsection

@section('content')

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Nilai {{$nilai->mahasiswa->nama}} - {{$nilai->mahasiswa->nrp}}</h3>
</div>

</div>

<div class="card">
    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <td>Tanggal</td>
                    <td>{{ $nilai->tanggal }}</td>
                </tr>
                <tr>
                    <td>Nomor</td>
                    <td>{{ $nilai->nomor }}</td>
                </tr>

                <tr>
                    <td>Semester</td>
                    <td>{{ $nilai->semester }}</td>
                </tr>
                <tr>
                    <td>Tahun Ajaran</td>
                    <td>{{ $nilai->tahun_ajaran }}</td>
                </tr>
            </tbody>
        </table>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th width="49%">Mata Kuliah</th>
                    <th>H</th>
                    <th>A</th>
                    <th>K</th>
                    <th>M</th>
                </tr>
            </thead>
            <tbody>

                @foreach($nilaidetails as $nilaidetail)
                <tr>
                    <td>{{ $nilaidetail->matakuliah->nama }}</td>
                    <td>{{ $nilaidetail->H }}</td>
                    <td>{{ $nilaidetail->A }}</td>
                    <td>{{ $nilaidetail->K }}</td>
                    <td>{{ $nilaidetail->M }}</td>
                </tr>
                @endforeach

            </tbody>
        </table>
        <a href="{{ route('nilai.index') }}" class="btn btn-warning">
            <i class="fas fa-arrow-left"></i> Kembali
        </a>
    </div>
</div>

@endsection
