@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('nilai.index') }}">Nilai Index</a></li>
    <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
  </ol>
</nav>
@endsection

@section('content')

<?php use Add\Controllers\NilaiController; ?>

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Nilai</h3>
</div>
</div>

<form action="{{ route("nilai.store")}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('POST')

    <div class="card" style="margin-top: -15px;">

        <div class="card-body">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="tanggal">Tanggal</label>
                        <input readonly type="date" id="tanggal" name="tanggal" class="form-control" required autocomplete="off">
                    </div>
                    <div class="form-group" >
                        <label for="mahasiswa_id">NRP</label>
                        <select name="mahasiswa_id" id="mahasiswa_id" class="select2 form-control">
                            <option disabled selected value="-">Pilih Mahasiswa</option>
                            @foreach($mahasiswas as $mahasiswa)
                            <option value="{{$mahasiswa->id}}" nama_mahasiswa="{{$mahasiswa->nama}}" semester="{{$mahasiswa->semester}}"
                                >{{$mahasiswa->nrp}} - {{$mahasiswa->nama}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="nama_mahasiswa">Nama</label>
                            <input readonly type="text" id="nama_mahasiswa" class="form-control" >
                        </div>

<!--                         <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tahun_ajaran">Tahun Ajaran</label>
                                    <input type="number" id="tahun_ajaran" name="tahun_ajaran" class="form-control" required autocomplete="off"  >
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="semester">Semester</label>
                                    <input readonly type="number" id="semester" name="semester" class="form-control" required autocomplete="off">
                                </div>
                            </div>
                        </div> -->
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nomor">Nomor</label>
                            <input readonly type="text" id="nomor" name="nomor" class="form-control" required autocomplete="off" value="{{NilaiController::autonumber()}}">
                        </div>


                        <div class="form-group" hidden>
                            <div class="form-group">
                                <label for="jadwal_perwalian_id">Perwalian</label>
                                <select name="jadwal_perwalian_id" id="jadwal_perwalian_id" class="select2 form-control">
                                    <option disabled selected value="-">Pilih perwalian</option>
                                    @foreach($jadwalperwalians as $jadwalperwalian)
                                    <option
                                    value="{{$jadwalperwalian->id_jadwal_perwalian}}"
                                    nama_programstudi="{{$jadwalperwalian->programstudi['nama']}}"
                                    nama_proyekpendidikan="{{$jadwalperwalian->proyekpendidikan['nama']}}">
                                    {{$jadwalperwalian->kode_perwalian}} -
                                    {{$jadwalperwalian->proyekpendidikan['nama']}} -
                                    {{$jadwalperwalian->programstudi['nama']}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="form-group" hidden>
                            <label for="proyek_pendidikan">Proyek Pendidikan</label>
                            <input readonly type="text" id="proyek_pendidikan" class="form-control" >
                        </div>
                        <div class="form-group" hidden>
                            <label for="program_studi">Program Studi</label>
                            <input readonly type="text" id="program_studi" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="tahun_ajaran">Tahun Ajaran</label>
                            <input type="number" id="tahun_ajaran" name="tahun_ajaran" class="form-control" required autocomplete="off"  >
                        </div>
                        <div class="form-group">
                            <label for="semester">Semester</label>
                            <input readonly type="number" id="semester" name="semester" class="form-control" required autocomplete="off">
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-12">

                        <table class="table datatable" id="tabeldetail">
                            <thead>
                                <tr>
                                    <th width="10">No</th>
                                    <!-- <th width="100">Action</th> -->
                                    <th>Mata Kuliah</th>
                                    <th>H</th>
                                    <th>A</th>
                                    <th>K</th>
                                    <th>M</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                </div>

            </div>

        </div>








        <button class="btn btn-success" type="submit">Simpan</button>


    </form>
    @endsection


    @section('scripts')
    <script>

     $(function () {
        $('#myTab li:last-child a').tab('show');
    });


     $('#jadwal_perwalian_id').on('change',function(){
        $('#program_studi').val($('option:selected', this).attr('nama_programstudi'));
        $('#proyek_pendidikan').val($('option:selected', this).attr('nama_proyekpendidikan'));

        var mahasiswa_id = $('#mahasiswa_id').val();
        var semester = $('#semester').val();
        var jadwal_perwalian_id = $('#jadwal_perwalian_id').val();

        if ((jadwal_perwalian_id != null) && (mahasiswa_id != null) ){
            getmatakuliahperwalian(mahasiswa_id,semester,jadwal_perwalian_id);
            $('#pills-alamat').addClass('active');
            $('#pills-alamat').addClass('show');
            $('#pills-personal-info').removeClass('active');
            $('#pills-personal-info').removeClass('show');

        }
    });

     $('#mahasiswa_id').on('change',function(){
        $('#nama_mahasiswa').val($('option:selected', this).attr('nama_mahasiswa'));
        $('#semester').val($('option:selected', this).attr('semester'));


        var mahasiswa_id = $('#mahasiswa_id').val();
        var semester = $('#semester').val();
        var jadwal_perwalian_id = $('#jadwal_perwalian_id').val();

        if  (mahasiswa_id != null) {
            getmatakuliahperwalian(mahasiswa_id,semester,jadwal_perwalian_id);
            $('#pills-alamat').addClass('active');
            $('#pills-alamat').addClass('show');
            $('#pills-personal-info').removeClass('active');
            $('#pills-personal-info').removeClass('show');

        }
    });



     $(function(){
         var now = new Date();
         var day = ("0" + now.getDate()).slice(-2);
         var month = ("0" + (now.getMonth() + 1)).slice(-2);

         var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
         var thisyear = now.getFullYear();
         $('#tanggal').val(today);
         $('#tahun_ajaran').val(thisyear);
     });

    //    function tambahdatanilai(){
    //     var id = $('#modaltambah_mata_kuliah_id').val();
    //     var nama = $('option:selected' , '#modaltambah_mata_kuliah_id').attr('nama');
    //     var namastring = "'"+nama+"'";
    //     var h = $('#modaltambah_H').val();
    //     var a = $('#modaltambah_A').val();
    //     var k = $('#modaltambah_K').val();
    //     var m = $('#modaltambah_M').val();

    //     if ((h=='') || (a=='') || (k=='') || (m=='')){
    //         alert('data nilai tidak boleh kosong');
    //     }
    //     else{
    //         var totalkolom = $('#tabeldetail th').length;
    //         var totalbaris = $('#tabeldetail tr').length;

    //         var row = document.getElementById('tabeldetail').insertRow(totalbaris);

    //         row.insertCell(0).innerHTML = totalbaris;
    //         row.insertCell(1).innerHTML = '<button type="button" class="btn btn-xs btn-danger" onclick="hapusdatanilai('+totalbaris+')"><i class="fa fa-times"></i></button>';
    //         row.insertCell(2).innerHTML = '<input type="text" name="mata_kuliah_id[]" value='+id+' hidden>'+nama;
    //         row.insertCell(3).innerHTML = '<input type="text" name="h[]" value='+h+' hidden>'+h;
    //         row.insertCell(4).innerHTML = '<input type="text" name="a[]" value='+a+' hidden>'+a;
    //         row.insertCell(5).innerHTML = '<input type="text" name="k[]" value='+k+' hidden>'+k;
    //         row.insertCell(6).innerHTML = '<input type="text" name="m[]" value='+m+' hidden>'+m;

    //         $('option:selected' , '#modaltambah_mata_kuliah_id').remove();
    //         $('#modaltambah_H').val('');
    //         $('#modaltambah_A').val('');
    //         $('#modaltambah_K').val('');
    //         $('#modaltambah_M').val('');
    //     }
    // }

    function hapusdatanilai(baris){
        if (confirm('yakin ?')){
            document.getElementById("tabeldetail").deleteRow(baris);
            var totalbaris = $('#tabeldetail tr').length;
            var i = 0;
            for (i = 0; i < totalbaris; i++) {
             document.getElementById('tabeldetail').rows[i].cells[0].innerHTML = i ;
         }
     }

 }

 function pilihnilai(baris){
     var nilai=($('#h'+baris).val());
     var sks=($('#a'+baris).val());
     var ip=0;
     var total=0;
     if (nilai=='A'){
        ip = 4;
    }
    if (nilai=='B+'){
        ip = 3.5;
    }
    if (nilai=='B'){
        ip = 3;
    }
    if (nilai=='C+'){
        ip = 2.5;
    }
    if (nilai=='C'){
        ip = 2;
    }

    total = ip * sks
    $('#k'+baris).val(ip);
    $('#m'+baris).val(total);
}


function getmatakuliahperwalian(mahasiswa_id,semester,jadwal_perwalian_id){
    var data_mahasiswa_id='';
    var data_semester='';
    var data_jadwal_perwalian_id='';


    var totalkolom = $('#tabeldetail th').length;
    var totalbaris = $('#tabeldetail tr').length;

    var row = document.getElementById('tabeldetail').insertRow(totalbaris);
    $("#tabeldetail tbody tr").remove();

    $.ajax({
        method:"GET",
        url:"/getmatakuliahperwalian",
        headers: {'x-csrf-token': _token},
        data:{data_mahasiswa_id:mahasiswa_id,data_semester:semester,data_jadwal_perwalian_id:jadwal_perwalian_id},
        type:'text',
        success:function(response){
        console.log(response);
            $.each(response,function(key,value){
                var table = document.getElementById('tabeldetail');
                var no = key+1;
                $(table).find('tbody').append( "<tr>"+
                    "<td>"+no+"</td>"+
                    "<td><input type='text' name='mata_kuliah_id[]' value="+value[0].id+" hidden>"+value[0].nama+"</td>"+
                    "<td>"+
                    "<select name='h[]' required id='h"+key+"' onchange='pilihnilai("+key+")' class='form-control'>"+
                    "<option value='-' disabled selected>Pilih</option>"+
                    "<option value='A'>A</option>"+
                    "<option value='B+'>B+</option>"+
                    "<option value='B'>B</option>"+
                    "<option value='C+'>C+</option>"+
                    "<option value='C'>C</option>"+
                    "</select>"+
                    "</td>"+

                    "<td>"+
                    "<center>"+
                    "<input type='text' name='a[]' id='a"+key+"' class='editable' value="+ value[0].sks +" readonly>"+
                    "</center>"+
                    "</td>"+

                    "<td>"+
                    "<input type='text' name='k[]' id='k"+key+"' class='editable' readonly required autocomplete='off' value='0'>"+
                    "</td>"+

                    "<td><input type='text' name='m[]' id='m"+key+"' class='editable' readonly required autocomplete='off' value='0'></td>"+

                    "</tr>" );

            });
        }


    });
}



</script>

@endsection


<style>
    .editable{
        width: 60px;
        padding: 0px;
        margin: 0px;
        text-align: center;
    }
    tr td input{
        padding: 0px;
    }
    #pills-tab-content{
        border: none !important;
        outline: none !important;
    }
</style>
