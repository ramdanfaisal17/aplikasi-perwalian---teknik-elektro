<div class="modal fade" id="addData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">tambah data matakuliah dan nilai</h5>
          </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="form-group">
                        <label for="modaltambah_mata_kuliah_id">mata kuliah</label>
                        <select type="text" id="modaltambah_mata_kuliah_id" class="form-control select2" required>
                            {{-- <option disabled selected value="-">pilih mata kuliah</option> --}}
                            @foreach($matakuliahs as $matakuliah)
                            <option value="{{$matakuliah->id}}" nama="{{$matakuliah->nama}}">{{$matakuliah->kode}}  -  {{$matakuliah->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="motaltambah_H">H</label>
                        <input type="text" id="modaltambah_H" required value="" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="modaltambah_A">A</label>
                        <input type="text" id="modaltambah_A" required value="" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="modaltambah_K">K</label>
                        <input type="text" id="modaltambah_K" required value="" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="modaltambah_M">M</label>
                        <input type="text" id="modaltambah_M" required value="" class="form-control">
                    </div>

                </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">kembali</button>
          <button type="button" class="btn btn-primary" onclick="tambahdatanilai()">tambahkan</button>
      </div>
  </div>

</div>
</div>