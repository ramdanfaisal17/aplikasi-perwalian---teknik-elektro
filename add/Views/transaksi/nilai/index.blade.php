@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item active" aria-current="page">Nilai Index</li>
  </ol>
</nav>
@endsection

@section('content')

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Nilai</h3>
  </div>
  <div class="col-lg-8 text-md-right">

    <a class="btn btn-success" href="{{ route("nilai.create") }}" style="min-width: 100px;">
      <i class="fas fa-plus"></i> Tambah Data
    </a>


  </div>
</div>

<!-- Flash Message -->
@include('_partial.flash_message')

<div class="card">

  <div class="card-body">
    <div class="table-responsive">
      <table class=" table datatable" id="mytable">
        <thead>
          <tr align="center">
            <th width="10">
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="customCheck2">
                <label class="custom-control-label" for="customCheck2"></label>
              </div>
            </th>
            <th width="100">Aksi</th>
            <th>Tanggal</th>
            <th>Nomor</th>
            <th>Nama</th>
            <th>Semester</th>
            <th>Tahun Ajaran</th>
          </tr>
        </thead>
        <tbody>
          @foreach($nilais as $item)
          <tr data-entry-id="{{ $item->id }}">
            <td></td>
            <td>
              <form action="{{ route('nilai.destroy', $item->id) }}" method="POST" onsubmit="return confirm('yakin nih?');" style="display: inline-block;" hidden>
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="submit" class="btn btn-xs btn-danger" value="X">
              </form>
              <a class="btn btn-xs btn-primary" href="{{ route('nilai.edit', $item->id) }}" title="Ubah Data">
                <i class="fa fa-edit"></i>
              </a>
              <a class="btn btn-xs btn-success" href="{{ route('nilai.show', $item->id) }}" title="Lihat Data">
                <i class="fa fa-eye"></i>
              </a>
            </td>
            <td>{{ date('d-m-Y', strtotime($item->tanggal)) }}</td>
            <td>{{ $item->nomor }}</td>
            <td>{{ isset($item->mahasiswa->nama) ? $item->mahasiswa->nama : $item->mahasiswa }}</td>
            <td>{{ $item->semester }}</td>
            <td>{{ $item->tahun_ajaran }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@section('scripts')
@parent
<script>
  $(function () {
    let deleteButtonTrans = 'delete selected'
    let deleteButton = {
      text: deleteButtonTrans,
      className: 'btn-danger dtb-delete hilang',
      action: function (e, dt, node, config) {
        var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
        });

        if (ids.length === 0) {
          alert('tidak ada yang dipilih !')
          return
        }
        if (confirm('beneran ??')) {
          $.ajax({
            headers: {'x-csrf-token': _token},
            type: "POST",
            data: {ids:ids},
            url: "nilai/deletes",
            success: function(){
             location.reload();
           }
         });
        }
      }
    }


    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

    dtButtons.push(deleteButton)


    $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  })

  function deleteSelected(){
    $('.tomboldeleteall').click();
  }

  $("#customCheck2").change( function() {
    if ($("th.select-checkbox").hasClass("selected")) {
      $('#mytable').DataTable().rows().deselect();
      $("th.select-checkbox").removeClass("selected");
    } else {
      $('#mytable').DataTable().rows().select();
      $("th.select-checkbox").addClass("selected");
    }
  }).on("select deselect", function() {
    ("Some selection or deselection going on")
    if ($('#mytable').DataTable().rows({
      selected: true
    }).count() !== $('#mytable').DataTable().rows().count()) {
      $("th.select-checkbox").removeClass("selected");
    } else {
      $("th.select-checkbox").addClass("selected");
    }
  });

</script>

@endsection
@endsection
