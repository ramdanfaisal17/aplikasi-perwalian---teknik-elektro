@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('nilaiberkas.index') }}">Nilai Berkas</a></li>
    <li class="breadcrumb-item active" aria-current="page">Lihat Data - {{ $mahasiswa->nrp }} - {{ $mahasiswa->nama }}</li>
  </ol>
</nav>
@endsection

@section('content')

<!-- Flash message here -->
@include('_partial.flash_message')

<div class="card">
    <div class="card-header">Nilai Berkas {{ $mahasiswa->nama }} - {{$mahasiswa->programstudi->nama}}</div>
    <div class="card-body">
        <div class="mb-3">
          <a class="btn btn-xs btn-success"  title="Tambah/Upload Data baru" href="{{ route('nilaiberkas.create', ['mahasiswa_id' => $mahasiswa->id]) }}">
            <i class="fa fa-plus"></i>
          </a>
        </div>

        <div class="row">
          @foreach($arr_nilaiBerkas as $nilaiBerkas)
            <div class="col-12 col-sm-6 col-md-6 col-lg-4 d-flex">
              <div class="card" style="width: 18rem; ">
                <!-- <img class="card-img-top" src="{{ Storage::disk('nilai-berkas')->url($nilaiBerkas->url) }}" alt="Card image cap"> -->
                <div class="card-body">
                  <p class="card-text">{{ $nilaiBerkas->keterangan }}</p>
                </div>
                <div class="card-footer">
                  <div class="row">
                    <div class="col-12 col-sm-12 col-md-6">
                      <small class="card-text text-info text-small">Diunggah {{ date('d-m-Y', strtotime($nilaiBerkas->created_at)) }}</small>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 text-right">
                      <form action="{{ route('nilaiberkas.destroy', $nilaiBerkas->id) }}" method="POST" title="Hapus Data" onsubmit="return confirm('Apakah anda yakin ?');" style="display: inline-block;">
                        @method('DELETE')
                        @csrf
                        <button type="submit" name="button" class="btn btn-xs btn-danger">
                          <i class="fa fa-trash"></i>
                        </button>
                      </form>
                      <a href="{{ Storage::disk('nilai-berkas')->url($nilaiBerkas->url) }}" target="_blank" class="btn btn-info btn-xs" title="Lihat PDF"><i class="fas fa-eye"></i></a>
                      <a href="{{ Storage::disk('nilai-berkas')->url($nilaiBerkas->url) }}" class="btn btn-primary btn-xs" title="Download PDF"><i class="fas fa-download"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endforeach
        </div>

        <a href="{{ route('nilaiberkas.index') }}" class="btn btn-warning">
          <i class="fas fa-arrow-left"></i> Kembali
        </a>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

@endsection
