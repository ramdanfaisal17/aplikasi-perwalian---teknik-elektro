<div class="row">
  <div class="col-lg-12">
    <table class="table table-hovered table-bordered" id="datatable" style="width: 100%;" data-table="tbody-mahasiswa">
      <thead>
        <tr>
          <th></th>
          <th>No</th>
          <th>NRP</th>
          <th>Nama</th>
          <th>Status Mahasiswa</th>
        </tr>
      </thead>
      <tbody></tbody>
    </table>
  </div>
</div>


<script>
  var dataTablesColumns = [
    {},
    {data: "id", name:"id"},
    {data: "nrp", name: 'nrp'},
    {data: "nama", name: 'nama'},
    {data: "status_mahasiswa", name: 'status_mahasiswa'},
  ];

  var url = "{{ route('data.mahasiswa') }}";
</script>
<script src="{{ asset('js/customDataTable.js') }}"></script>
