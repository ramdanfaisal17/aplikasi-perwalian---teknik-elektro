<div class="form-group">
  <label for="exampleFormControlSelect1">Mahasiswa</label>
  <select class="form-control" name="mahasiswa_id" id="exampleFormControlSelect1">
    @foreach($mahasiswas as $mahasiswa)
      <option value="{{ $mahasiswa->id }}" {{ ($mahasiswa->id == Request::get('mahasiswa_id')) ? 'selected' : '' }}> {{ $mahasiswa->nrp }} - {{ $mahasiswa->nama }} </option>
    @endforeach
  </select>
  @if($errors->has('mahasiswa_id'))
    <small class="form-text text-danger">*{{ $errors->first('mahasiswa_id') }}</small>
  @endif
</div>
 <div class="form-group">
   <label for="exampleFormControlFile1">Pilih berkas PDF..</label>
   <input type="file" name="pdfFile" class="form-control-file" id="exampleFormControlFile1">
   @if($errors->has('pdfFile'))
     <small class="form-text text-danger">*{{ $errors->first('pdfFile') }}</small>
   @endif
 </div>
 <div class="form-group">
  <label for="exampleFormControlTextarea1">Keterangan</label>
  <textarea class="form-control" name="keterangan" id="exampleFormControlTextarea1" rows="3"></textarea>
  @if($errors->has('keterangan'))
    <small class="form-text text-danger">*{{ $errors->first('keterangan') }}</small>
  @endif
</div>
