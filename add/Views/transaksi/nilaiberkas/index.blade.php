@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item active" aria-current="page">Nilai Berkas</li>
  </ol>
</nav>
@endsection

@section('content')

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Data Nilai Berkas</h3>
  </div>
  <div class="col-lg-8 text-md-right">

    <a class="btn btn-success" href="{{ route("nilaiberkas.create") }}" style="min-width: 100px;">
      <i class="fas fa-plus"></i> Tambah Data
    </a>

  </div>
</div>

<!-- Flash message here -->
@include('_partial.flash_message')
<div class="card">
  <div class="card-body">
    <div class="table-responsive">
      <table class="table datatable" style="width:100%" id="nilaiBerkas">
        <thead>
          <tr>
            <th style="display: none"></th>
            <th>Aksi</th>
            <th>NRP</th>
            <th>Nama Mahasiswa</th>
            <th>Tahun Angkatan</th>
            <th>Jurusan</th>
            <th>Dosen Wali</th>
          </tr>
        </thead>
        <tbody>
          @foreach($mahasiswas as $item)
          <tr data-entry-id="{{ $item->id }}">
            <td style="display: none"></td>
            <td>
              <a class="btn btn-xs btn-success" title="Tambah Berkas PDF" href="{{ route('nilaiberkas.create', ['mahasiswa_id' => $item->id]) }}">
                <i class="fa fa-plus"></i>
              </a>
              <a class="btn btn-xs btn-info" title="Lihat Detail" href="{{ route('nilaiberkas.show', $item->id) }}">
                <i class="fa fa-eye"></i>
              </a>
              @if(isset($item->nilaiberkas))
              <a class="btn btn-xs btn-primary" title="Download PDF Terbaru" href="{{ Storage::disk('nilai-berkas')->url($item->latestPDF['url']) }}">
                <i class="fa fa-download"></i>
              </a>
              @endif
          </td>
            <td>{{ $item->nrp }}</td>
            <td>{{ $item->nama }}</td>
            <td>{{ $item->angkatan }}</td>
            <td>{{ $item->programstudi['nama'] }}</td>
            <td>{{ $item->dosen['nama'] }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@section('scripts')
@parent
<script>
  $(document).ready(function() {
    $('#nilaiBerkas').DataTable();
    } );
</script>

@endsection
@endsection
