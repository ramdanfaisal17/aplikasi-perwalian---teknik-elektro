@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('mahasiswa.index') }}">Master Mahasiswa</a></li>
    <li class="breadcrumb-item active" aria-current="page">Ubah Data - {{ $mahasiswa->nrp }}</li>
  </ol>
</nav>
@endsection

@section('content')

<div class="card" style="margin-top: -15px;">
    <div class="card-header" >Ubah Mahasiswa {{$mahasiswa->nama}}</div>
    <div class="card-body">


        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-personal-info-tab" data-toggle="pill" href="#pills-personal-info" role="tab" aria-controls="pills-personal-info" aria-selected="true">Informasi Personal</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-alamat-tab" data-toggle="pill" href="#pills-alamat" role="tab" aria-controls="pills-alamat" aria-selected="false">Alamat</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-detail-info-tab" data-toggle="pill" href="#pills-detail-info" role="tab" aria-controls="pills-detail-info" aria-selected="false">Informasi Detail</a>
            </li>
        </ul>

        <form action="{{ route("mahasiswa.update", [$mahasiswa->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <input type="hidden" name="id" value="{{ $mahasiswa->id }}">
            @include('masters.mahasiswa.form')
            <a href="{{ route('mahasiswa.index') }}" class="btn btn-warning">
                <i class="fas fa-arrow-left"></i> Kembali
            </a>
            <input class="btn btn-success" type="submit" value="Simpan">
        </form>




    </div>
</div>

@endsection

@section('scripts')
<script>

   $(function () {
    $('#myTab li:last-child a').tab('show');
});

   $('#provinsi').on('change',function(){
    var province_id = $(this).val();

    $.ajax({
     url:"{{url('getkota')}}?province_id="+province_id,
     type:"GET",
     success:function(res){
        if(res){
            $("#kota").empty();
            $("#kota").append('<option disabled selected>pilih kota</option>');
            $.each(res,function(key,value){
                $("#kota").append('<option value='+key+'>'+value+'</option>');
            });

        }
        else{
          $("#kota").empty();
          $("#kecamatan").empty();
          $("#desa").empty();
      }
  }
});
});

   $('#kota').on('change',function(){
    var city_id = $(this).val();

    $.ajax({
     url:"{{url('getkecamatan')}}?city_id="+city_id,
     type:"GET",
     success:function(res){
        if(res){
            $("#kecamatan").empty();
            $("#kecamatan").append('<option disabled selected>pilih kecamatan</option>');
            $("#kecamatan").append('<option>lainnya</option>');
            $.each(res,function(key,value){
                $("#kecamatan").append('<option value='+key+'>'+value+'</option>');
            });

        }
        else{
          $("#kecamatan").empty();
          $("#desa").empty();
      }
  }
});
});

   $('#kecamatan').on('change',function(){
    var district_id = $(this).val();

    $.ajax({
     url:"{{url('getdesa')}}?district_id="+district_id,
     type:"GET",
     success:function(res){
        if(res){
            $("#desa").empty();
            $("#desa").append('<option disabled selected>pilih desa</option>');
            $("#desa").append('<option>lainnya</option>');
            $.each(res,function(key,value){
                $("#desa").append('<option value='+key+'>'+value+'</option>');
            });

        }
        else{
          $("#desa").empty();
      }
  }
});
});

</script>

@endsection
