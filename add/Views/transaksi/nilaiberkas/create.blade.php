@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('mahasiswa.index') }}">Master Mahasiswa</a></li>
    <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
  </ol>
</nav>
@endsection

@section('content')

<div class="card" style="margin-top: -15px;">
    <div class="card-header" >Tambah Data PDF</div>
    <div class="card-body">

        <form action="{{ route("nilaiberkas.store")}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('post')

            @include('transaksi.nilaiberkas.form')

            <div class="form-group">
              <a href="{{ route('nilaiberkas.index') }}" class="btn btn-warning">
                  <i class="fas fa-arrow-left"></i> Kembali
              </a>
              <button class="btn btn-primary" type="submit">Submit form</button>
            </div>
        </form>




    </div>
</div>

@endsection

@section('scripts')
<script>

   $(function () {
    $('#myTab li:last-child a').tab('show');
});

   $('#provinsi_id').on('change',function(){
    var province_id = $(this).val();

    $.ajax({
     url:"{{url('getkota')}}?province_id="+province_id,
     type:"GET",
     success:function(res){
        if(res){
            $("#kota_id").empty();
            $("#kota_id").append('<option disabled selected>- Pilih Kota -</option>');
            $.each(res,function(key,value){
                $("#kota_id").append('<option value='+key+'>'+value+'</option>');
            });

        }
        else{
          $("#kota_id").empty();
          $("#kecamatan_id").empty();
          $("#desa_id").empty();
      }
  }
});
});

   $('#kota_id').on('change',function(){
    var city_id = $(this).val();

    $.ajax({
     url:"{{url('getkecamatan')}}?city_id="+city_id,
     type:"GET",
     success:function(res){
        if(res){
            $("#kecamatan_id").empty();
            $("#kecamatan_id").append('<option disabled selected>- Pilih Kecamatan -</option>');
            $("#kecamatan_id").append('<option>lainnya</option>');
            $.each(res,function(key,value){
                $("#kecamatan_id").append('<option value='+key+'>'+value+'</option>');
            });

        }
        else{
          $("#kecamatan_id").empty();
          $("#desa_id").empty();
      }
  }
});
});

   $('#kecamatan_id').on('change',function(){
    var district_id = $(this).val();

    $.ajax({
     url:"{{url('getdesa')}}?district_id="+district_id,
     type:"GET",
     success:function(res){
        if(res){
            $("#desa_id").empty();
            $("#desa_id").append('<option disabled selected>- Pilih Kelurahan/Desa -</option>');
            $("#desa_id").append('<option>lainnya</option>');
            $.each(res,function(key,value){
                $("#desa_id").append('<option value='+key+'>'+value+'</option>');
            });

        }
        else{
          $("#desa_id").empty();
      }
  }
});
});

</script>

@endsection
