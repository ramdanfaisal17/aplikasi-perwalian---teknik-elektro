@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('jadwalPerwalian.index') }}">Jadwal Perwalian</a></li>
    <li class="breadcrumb-item active" aria-current="page">Ubah Jadwal Perwalian - {{ $jadwalperwalian->kode_perwalian }}</li>
  </ol>
</nav>
@endsection
@section('content')

<div class="card" style="margin-top: -15px;">
    <div class="card-body">

        <form action="{{ route('jadwalPerwalian.update', $jadwalperwalian->id_jadwal_perwalian) }}" method="POST" enctype="multipart/form-data">
            @method('PUT')
            @CSRF
            <input type="hidden" name="id" value="{{ $jadwalperwalian->id_jadwal_perwalian }}">
            @include('transaksi.jadwalperwalian.form')
        </form>




    </div>
</div>

@endsection

@section('scripts')
  <script type="text/javascript" src="{{ asset('js/jadwalperwalian/form.js') }}"></script>
@endsection
