@extends('layouts.admin')
@section('content')

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Hasil Perwalian</h3>
</div>
</div>

    <div class="card" style="margin-top: -15px;">

        <div class="card-body">

          <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                      <label for="tanggal">Perwalian</label>
                      <input readonly type="date" id="tanggal" name="tanggal" class="form-control" required autocomplete="off" value="">
                  </div>
              </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group" >
                <label for="mahasiswa_id">Program Studi</label>
                <select class="form-control" name="program_studi">
                  <option value="0">Semua</option>
                  @foreach($all_programstudi as $programstudi)
                    <option value="{{ $programstudi->id }}">{{ $programstudi->nama }}</option>
                  @endforeach
                </select>
             </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="nomor">Proyek Pendidikan</label>
                    <select class="form-control" name="proyek_pendidikan">
                      <option value="0">Semua</option>
                      @foreach($all_proyekpendidikan as $proyekpendidikan)
                        <option value="{{ $proyekpendidikan->id }}">{{ $proyekpendidikan->nama }}</option>
                      @endforeach
                    </select>
                </div>
            </div>
          </div>

          <div class="row">
              <div class="col-md-12 table-responsive">
                  <table class="table datatable" id="mytable">
                      <thead>
                          <tr align="center">
                              <th>No</th>
                              <th>NRP</th>
                              <th>NAMA</th>
                              <th>SEMESTER</th>
                              <th>DOSEN WALI</th>
                              <th>PRS</th>
                              <th>KONSULTASI I</th>
                              <th>KONSULTASI II</th>
                              <th>PILIH MK</th>
                          </tr>
                      </thead>
                      <tbody>
                          @foreach($all_mahasiswa as $key => $mahasiswa)
                          <tr>
                           <td>{{ $key + 1 }}</td>
                           <td>{{ $mahasiswa->nrp }}</td>
                           <td>{{ $mahasiswa->nama }}</td>
                           <td>{{ $mahasiswa->semester }}</td>
                           <td>{{ $mahasiswa->dosen->nama }}</td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                         </tr>
                      @endforeach


                  </tbody>
              </table>

          </div>
      </div>

    </div>

  </div>

@endsection


@section('scripts')
@parents
<script>
  $(function () {
    let deleteButtonTrans = 'delete selected'
    let deleteButton = {
      text: deleteButtonTrans,
      className: 'btn-danger dtb-delete hilang',
      action: function (e, dt, node, config) {
        var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
        });

        if (ids.length === 0) {
          alert('tidak ada yang dipilih !')
          return
        }
        if (confirm('beneran ??')) {
          $.ajax({
            headers: {'x-csrf-token': _token},
            type: "POST",
            data: {ids:ids},
            url: "dosen/deletes",
            success: function(){
             location.reload();
           }
         });
        }
      }
    }


    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

    dtButtons.push(deleteButton)


    $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })

    $('#proyek_pendidikan_id').on('change',function(){

            location.href='/hasilperwalian?proyek_pendidikan_id='+ $('#proyek_pendidikan_id').val()+'&semester='+$('#semester').val()+'&angkatan='+$('#angkatan').val();
    })

    $('#semester').on('change',function(){
            location.href='/hasilperwalian?proyek_pendidikan_id='+ $('#proyek_pendidikan_id').val()+'&semester='+$('#semester').val()+'&angkatan='+$('#angkatan').val();
    })

       $('#angkatan').on('change',function(){
            location.href='/hasilperwalian?proyek_pendidikan_id='+ $('#proyek_pendidikan_id').val()+'&semester='+$('#semester').val()+'&angkatan='+$('#angkatan').val();
    })

    $('#btn').on('click', function() {
      window.open('hasilperwalian/print', "cetak", 'width=1000,height=800');
    })

  })


  function deleteSelected(){
    $('.tomboldeleteall').click();
  }

  function getid(id){
    $("#idupdate").val(id);
  }



</script>

@endsection


<style>
    .editable{
        width: 60px;
        padding: 0px;
        margin: 0px;
    }
    tr td input{
        padding: 0px;
    }
    #pills-tab-content{
        border: none !important;
        outline: none !important;
    }
</style>
