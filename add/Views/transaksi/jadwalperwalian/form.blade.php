      <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="kode_perwalian" class="font-weight-bold">Kode Perwalian</label>
                <input type="text" class="form-control" name="kode_perwalian" readonly value="{{ ($jadwalperwalian->kode_perwalian == "") ? Add\Controllers\JadwalPerwalianController::autonumber() : $jadwalperwalian->kode_perwalian }}">
                @if($errors->has('kode_perwalian'))
                  <small class="form-text text-danger">*{{ $errors->first('start_prs') }}</small>
                @endif
            </div>

        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="tanggal" class="font-weight-bold">Tanggal</label>
                <input type="text" id="tanggal" name="tanggal" data-provide="datepicker"class="form-control datepicker" autocomplete="off" value="<?php echo date('d-m-Y'); ?>" readonly>
                @if($errors->has('tanggal'))
                  <small class="form-text text-danger">*{{ $errors->first('tanggal') }}</small>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="proyek_pendidikan_id" class="font-weight-bold">Proyek Pendidikan</label>
                <select name="proyek_pendidikan_id" id="proyek_pendidikan_id" class="form-control">
                    <option value="" disabled>Pilih Proyek Pendidikan</option>
                    @foreach($proyek_pendidikans as $key => $proyekpendidikan)
                    <option data-jenis_semester="{{$proyekpendidikan->jenis_semester}}" value="{{$proyekpendidikan->id}}" {{ $jadwalperwalian->proyek_pendidikan_id == $proyekpendidikan->id ? 'selected' : '' }}> {{$proyekpendidikan->nama.' - '.ucfirst($proyekpendidikan->jenis_semester)}}</option>
                    @endforeach
                </select>
                @if($errors->has('proyek_pendidikan_id'))
                  <small class="form-text text-danger">*{{ $errors->first('proyek_pendidikan_id') }}</small>
                @endif

            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="program_studi_id" class="font-weight-bold">Program Studi</label>
                <select name="program_studi_id" id="program_studi_id" class="form-control">
                    <option value="" disabled>Pilih Program Studi</option>
                    @foreach($programstudis as $key => $programstudi)
                    <option value="{{$programstudi->id}}" {{ $jadwalperwalian->program_studi_id == $programstudi->id ? 'selected' : ''}}> {{$programstudi->nama}}</option>
                    @endforeach
                </select>
                @if($errors->has('program_studi_id'))
                  <small class="form-text text-danger">*{{ $errors->first('program_studi_id') }}</small>
                @endif

            </div>
        </div>
    </div>

    <div class="row">
      <div class="col-12 col-md-6">
          <label for="start_prs" class="font-weight-bold">1. Perubahan Rencana Studi (PRS)</label>
          <div class="row">
            <div class="col-12 col-sm-6">
                <label for="start_prs">Mulai</label>
                <div class="form-group">
                    <input type="text" data-provide="datepicker" class="form-control datepicker" name="start_prs" autocomplete="off" value="{{ old('start_prs', $jadwalperwalian->start_prs ? date('d-m-Y', strtotime($jadwalperwalian->start_prs)) : '') }}" placeholder="Isi tanggal mulai">
                    @if($errors->has('start_prs'))
                      <small class="form-text text-danger">*{{ $errors->first('start_prs') }}</small>
                    @endif
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label for="end_prs">Berakhir</label>
                    <input type="text" data-provide="datepicker" class="form-control datepicker" id="end_prs" name="end_prs" autocomplete="off" value="{{ old('end_prs', $jadwalperwalian->end_prs ? date('d-m-Y', strtotime($jadwalperwalian->end_prs)) : '') }}" placeholder="Isi tanggal berakhir">
                    @if($errors->has('end_prs'))
                      <small class="form-text text-danger">*{{ $errors->first('end_prs') }}</small>
                    @endif
                </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-6">
          <label for="start_konsultasi1" class="font-weight-bold">2. Konsultasi 1</label>
          <div class="row">
            <div class="col-12 col-sm-6">
               <div class="form-group">
                    <label for="start_konsultasi1">Mulai</label>
                    <input type="text" data-provide="datepicker" class="form-control datepicker" id="start_konsultasi1" name="start_konsultasi1" autocomplete="off" value="{{ old('start_konsultasi1', $jadwalperwalian->start_konsultasi1 ? date('d-m-Y', strtotime($jadwalperwalian->start_konsultasi1)): '') }}" placeholder="Isi tanggal mulai">
                    @if($errors->has('start_konsultasi1'))
                      <small class="form-text text-danger">*{{ $errors->first('start_konsultasi1') }}</small>
                    @endif
               </div>
            </div>
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label for="end_konsultasi1">Berakhir</label>
                    <input type="text" data-provide="datepicker" class="form-control datepicker" id="end_konsultasi1" name="end_konsultasi1" autocomplete="off" value="{{ old('end_konsultasi1', $jadwalperwalian->end_konsultasi1 ? date('d-m-Y', strtotime($jadwalperwalian->end_konsultasi1)) : '') }}" placeholder="Isi tanggal berakhir">
                    @if($errors->has('end_konsultasi1'))
                      <small class="form-text text-danger">*{{ $errors->first('end_konsultasi1') }}</small>
                    @endif
                </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-6">
          <label for="start_konsultasi2" class="font-weight-bold">3. Konsultasi 2</label>
          <div class="row">
            <div class="col-12 col-sm-6">
               <div class="form-group">
                    <label for="start_konsultasi2">Mulai</label>
                    <input type="text" data-provide="datepicker" class="form-control datepicker" id="start_konsultasi2" name="start_konsultasi2" autocomplete="off" value="{{ old('start_konsultasi2', $jadwalperwalian->start_konsultasi2 ? date('d-m-Y', strtotime($jadwalperwalian->start_konsultasi2)): '') }}" placeholder="Isi tanggal mulai">
                    @if($errors->has('start_konsultasi2'))
                      <small class="form-text text-danger">*{{ $errors->first('start_konsultasi2') }}</small>
                    @endif
                </div>
            </div>
            <div class="col-12 col-sm-6">
               <div class="form-group">
                  <label for="end_konsultasi2">Berakhir</label>
                  <input type="text" data-provide="datepicker" class="form-control datepicker" id="end_konsultasi2" name="end_konsultasi2" autocomplete="off" value="{{ old('end_konsultasi2', $jadwalperwalian->end_konsultasi2 ? date('d-m-Y', strtotime($jadwalperwalian->end_konsultasi2)) : '') }}" placeholder="Isi tanggal berakhir">
                  @if($errors->has('end_konsultasi2'))
                   <small class="form-text text-danger">*{{ $errors->first('end_konsultasi2') }}</small>
                  @endif
                </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-6">
          <label for="start_antara" class="font-weight-bold">4. Untuk semester Depan (Antara)</label>
          <div class="row">
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label for="start_antara">Mulai</label>
                    <input type="text" data-provide="datepicker" class="form-control datepicker" id="start_antara" class="form-control" name="start_antara" autocomplete="off" value="{{ old('start_antara', $jadwalperwalian->start_antara ? date('d-m-Y', strtotime($jadwalperwalian->start_antara)) : '') }}" placeholder="Isi tanggal mulai">
                    @if($errors->has('start_antara'))
                      <small class="form-text text-danger">*{{ $errors->first('start_antara') }}</small>
                    @endif
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label for="end_antara">Berakhir</label>
                    <input type="text" data-provide="datepicker" class="form-control datepicker" id="end_antara" class="form-control" name="end_antara" autocomplete="off" value="{{ old('end_antara', $jadwalperwalian->end_antara ? date('d-m-Y', strtotime($jadwalperwalian->end_antara)): '') }}" placeholder="Isi tanggal berakhir">
                    @if($errors->has('end_antara'))
                      <small class="form-text text-danger">*{{ $errors->first('end_antara') }}</small>
                    @endif
                </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-6">
          <label for="start_ganjil" class="font-weight-bold">5. Untuk semester Depan (Ganjil)</label>
          <div class="row">
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label for="start_ganjil">Mulai</label>
                    <input type="text" data-provide="datepicker" class="form-control datepicker" id="start_ganjil" name="start_ganjil" autocomplete="off" value="{{ old('start_ganjil', $jadwalperwalian->start_ganjil ? date('d-m-Y', strtotime($jadwalperwalian->start_ganjil)) : '') }}"  placeholder="Isi tanggal mulai">
                    @if($errors->has('start_ganjil'))
                      <small class="form-text text-danger">*{{ $errors->first('start_ganjil') }}</small>
                    @endif
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label for="end_ganjil">Berakhir</label>
                    <input type="text" data-provide="datepicker" class="form-control datepicker" id="end_ganjil" name="end_ganjil" autocomplete="off" value="{{ old('end_ganjil', $jadwalperwalian->end_ganjil ? date('d-m-Y', strtotime($jadwalperwalian->end_ganjil)): '') }}" placeholder="Isi tanggal berakhir">
                    @if($errors->has('end_ganjil'))
                      <small class="form-text text-danger">*{{ $errors->first('end_ganjil') }}</small>
                    @endif
                </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-6">
          <label for="start_genap" class="font-weight-bold">6. Untuk semester Depan (Genap)</label>
          <div class="row">
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label for="start_genap">Mulai</label>
                    <input type="text" data-provide="datepicker" class="form-control datepicker" id="start_genap"  name="start_genap" autocomplete="off" value="{{ old('start_genap', $jadwalperwalian->start_genap ? date('d-m-Y', strtotime($jadwalperwalian->start_genap)) :'') }}" placeholder="Isi tanggal mulai">
                    @if($errors->has('start_gemap'))
                      <small class="form-text text-danger">*{{ $errors->first('start_gennap') }}</small>
                    @endif
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <div class="form-group">
                    <label for="end_genap">Berakhir</label>
                    <input type="text" data-provide="datepicker" class="form-control datepicker" id="end_genap" name="end_genap" autocomplete="off" value="{{ old('end_genap', $jadwalperwalian->end_genap ? date('d-m-Y', strtotime($jadwalperwalian->end_genap)) : '') }}" placeholder="Isi tanggal berakhir">
                    @if($errors->has('end_genap'))
                      <small class="form-text text-danger">*{{ $errors->first('end_genap') }}</small>
                    @endif
                </div>
            </div>
          </div>
        </div>

        <input class="btn btn-success" type="submit" value="Simpan">
    </div>
