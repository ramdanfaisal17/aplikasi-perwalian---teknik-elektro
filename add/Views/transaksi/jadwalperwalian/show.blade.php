@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('jadwalPerwalian.index') }}">Jadwal Perwalian</a></li>
    <li class="breadcrumb-item active" aria-current="page">Lihat Data - {{ $jadwalperwalian->kode_perwalian }}</li>
  </ol>
</nav>
@endsection
@section('content')

<h3>Jadwal Perwalian</h3>

<div class="card">
    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th width="30%">Kode Perwalian</th>
                    <td>{{ $jadwalperwalian->kode_perwalian }}</td>
                </tr>

                <tr>
                    <th>Proyek Pendidikan</th>
                    <td>{{ $jadwalperwalian->proyekpendidikan->nama}}</td>
                </tr>
                 <tr>
                    <th>Tanggal</th>
                    <td><?php echo date("d/m/Y", strtotime($jadwalperwalian->tanggal)); ?></td>
                </tr>
                 <tr>
                    <th>Program Studi</th>
                    <td>{{ $jadwalperwalian->ProgramStudi->nama}}</td>
                </tr>
            </tbody>
        </table>

        <table class="table">
            <tbody>
                <tr>
                    <th width="30%"></th>
                    <th>Mulai</th>
                    <th>Berakhir</th>
                </tr>
                <tr>
                    <th >1. Perubahan Rencana Studi</th>
                    <td><?php echo $jadwalperwalian->start_prs ? date("d/m/Y", strtotime($jadwalperwalian->start_prs)) : '-'; ?></td>
                    <td><?php echo $jadwalperwalian->end_prs ? date("d/m/Y", strtotime($jadwalperwalian->end_prs)) : '-'; ?></td>
                </tr>

                <tr>
                    <th>2. Konsultasi 1</th>
                    <td><?php echo $jadwalperwalian->start_konsultasi1 ? date("d/m/Y", strtotime($jadwalperwalian->start_konsultasi1)) : '-'; ?></td>
                    <td><?php echo $jadwalperwalian->end_konsultasi1 ? date("d/m/Y", strtotime($jadwalperwalian->end_konsultasi1)) : '-'; ?></td>
                </tr>
                <tr>
                    <th>3. Konsultasi 2</th>
                    <td><?php echo $jadwalperwalian->start_konsultasi2 ? date("d/m/Y", strtotime($jadwalperwalian->start_konsultasi2)) : '-'; ?></td>
                    <td><?php echo $jadwalperwalian->end_konsultasi2 ? date("d/m/Y", strtotime($jadwalperwalian->end_konsultasi2)) : '-'; ?></td>
                </tr>
                <tr>
                    <th>4. Antara</th>
                    <td><?php echo $jadwalperwalian->start_antara ? date("d/m/Y", strtotime($jadwalperwalian->start_antara)) : '-'; ?></td>
                    <td><?php echo $jadwalperwalian->end_antara ? date("d/m/Y", strtotime($jadwalperwalian->end_antara)) : '-'; ?></td>
                </tr>
                <tr>
                    <th>  Ganjil</th>
                    <td><?php echo $jadwalperwalian->start_ganjil ? date("d/m/Y", strtotime($jadwalperwalian->start_ganjil)) : '-'; ?></td>
                    <td><?php echo $jadwalperwalian->end_ganjil ? date("d/m/Y", strtotime($jadwalperwalian->end_ganjil)) : '-'; ?></td>
                </tr>
                <tr>
                    <th>  Genap</th>
                    <td><?php echo $jadwalperwalian->start_genap ? date("d/m/Y", strtotime($jadwalperwalian->start_genap)) : '-'; ?></td>
                    <td><?php echo $jadwalperwalian->end_genap ? date("d/m/Y", strtotime($jadwalperwalian->end_genap)) : '-'; ?></td>
                </tr>

            </tbody>
        </table>
        <a href="{{ route('jadwalPerwalian.index') }}" class="btn btn-warning">
            <i class="fas fa-arrow-left"></i> Kembali
        </a>
    </div>
</div>

@endsection
