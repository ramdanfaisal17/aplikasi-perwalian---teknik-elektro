@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('jadwalPerwalian.index') }}">Jadwal Perwalian</a></li>
    <li class="breadcrumb-item active" aria-current="page">Tambah Data</li>
  </ol>
</nav>
@endsection
@section('content')
<?php use Add\Controllers\JadwalPerwalianController;?>
<div class="card">
    <div class="card-header">Tambah Jadwal Perwalian</div>
    <div class="card-body">
        <form action="{{ route("jadwalPerwalian.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('post')
            @include('transaksi.jadwalperwalian.form')
        </form>
    </div>
</div>

@endsection
@section('scripts')
  <script type="text/javascript" src="{{ asset('js/jadwalperwalian/form.js') }}"></script>
@endsection
