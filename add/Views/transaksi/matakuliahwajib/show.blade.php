@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('matakuliahwajib.index') }}">Matakuliah Wajib</a></li>
    <li class="breadcrumb-item active" aria-current="page">Show Data</li>
  </ol>
</nav>
@endsection

@section('content')

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Mata Kuliah Wajib Semester {{$matakuliahwajib->semester}}</h3>
</div>

</div>

<div class="card">
    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th width="20%">Tanggal</th>
                    <td><?php echo date("d/m/Y", strtotime($matakuliahwajib->tanggal)); ?></td>
                </tr>
                <tr>
                    <th>Nomor</th>
                    <td>{{ $matakuliahwajib->nomor }}</td>
                </tr>
            </tbody>
        </table>

        <table class="table table-bordered table-striped">
            <thead>
                <tr align="center">
                    <th width="20%">Kode</th>
                    <th >Mata Kuliah</th>
                </tr>
            </thead>
            <tbody>
             @foreach($details as $item)
             <tr>
                <td>{{ $item->matakuliah->kode }}</td>
                <td>{{ $item->matakuliah->nama }}</td>
            </tr>
            @endforeach

        </tbody>
    </table>
    <a href="{{ route('matakuliahwajib.index') }}">
        <input type="button" class="btn btn-warning" value="Back">
    </a>
</div>
</div>

@endsection
