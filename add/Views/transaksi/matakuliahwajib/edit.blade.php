@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('matakuliahwajib.index') }}">Matakuliah Wajib</a></li>
    <li class="breadcrumb-item active" aria-current="page">Ubah Data - {{ $matakuliahwajib->id }}</li>
  </ol>
</nav>
@endsection

@section('content')


<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Mata Kuliah Wajib</h3>
  </div>
</div>

<!-- Flash message -->
include('_partial.flash_message')

<form action="{{ route("matakuliahwajib.update", [$matakuliahwajib->id]) }}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('PUT')

  <div class="card" style="margin-top: -15px;">

    <div class="card-body">


      <div class="tab-pane fade show active" id="pills-personal-info" role="tabpanel" aria-labelledby="pills-personal-info-tab">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="tanggal">Tanggal</label>
              <input readonly type="date" id="tanggal" name="tanggal" class="form-control" required autocomplete="off" value="{{$matakuliahwajib->tanggal}}">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label for="nomor">Nomor</label>
              <input readonly type="text" id="nomor" name="nomor" class="form-control" required autocomplete="off" value="{{$matakuliahwajib->nomor}}">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="semester">Semester</label>
              <select name="semester" id="semester" class="form-control" required>
                <option value="1" selected>{{$matakuliahwajib->semester}}</option>
                <option value="1">1</option>
                <option value="2">2</option>
              </select>
            </div>
          </div>

        </div>


        <div class="row">
          <div class="col-md-12">

            <table class="table datatable" id="tabeldetail">
              <thead>
                <tr>
                  <th width="10">
                   {{--  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal" style="margin:-5px -10px !important;"><i class="fa fa-plus"></i></button> --}}
                   <a href='{{ route("data.matakuliah.show") }}' title='Tambah Mata Kuliah' class="btn btn-primary modal-add-row">
                    <i class="fa fa-plus"></i>
                  </a>
                </th>
                <th width="100">Aksi</th>
                <th>Kode</th>
                <th>Mata Kuliah</th>
              </tr>
            </thead>
            <tbody id="tbody-matakuliah">
                <?php $no=0 ?>
                @foreach($details as $detail)
                <?php $no=$no+1 ?>
<tr><input type="hidden" value="{{$detail->id}}" name="iddetail[]"><input type="hidden" value="{{$detail->mata_kuliah_id}}" name="mata_kuliah_id[]">
    <td class="td-id" data-id="{{$detail->id}}"><center>{{$no}}</center></td>
    <td> <a href="#" class="btn btn-danger btn-sm btn-remove-row" id={{$no}}><i class="fa fa-trash"></i></a></td>
    <td>{{$detail->matakuliah->kode}}</td>
    <td>{{$detail->matakuliah->nama}}</td>
</tr>
                @endforeach
            </tbody>

          </table>

        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
        </div>
        <div class="col-md-6 text-md-right">

        </div>
      </div>
    </div>

  </div>







</div>
<button class="btn btn-success" type="submit">Simpan</button>

</div>
{{-- modaltambah --}}

</form>
@endsection
@include('layouts.modal')
<script src="{{ asset('jquery-3.4.1.min.js') }}"></script>

<script>
//function for array checker
function check_array(value, arr){
  var status = false;
  for(var i=0; i<arr.length;i++ ){
    var needle = arr[i].id;
    if(needle == value){
      status = true;
      break;
    }
  }

  return status;
}


$('body').on('click', '#modal-btn-add', function(event){
  event.preventDefault();

  var table = $('#datatable').DataTable();

  var data = table.rows({selected: true}).data();

  var tbody = 'tbody-matakuliah';

  console.table(data);

  var currentData = [];
  $('#'+tbody+' .td-id').each(function(){
    currentData.push({ id:$(this).data('id') });
  });
    // console.table(currentData);
    var no = currentData.length;
    var currentNo = 0;
    for(var i = 0; i < data.length; i++){
      var check = check_array(data[i].idbarang, currentData);
        // console.log(check);
        if(!check){
          currentNo++;
          $('#'+tbody).append('<tr>'
            +' <input type="hidden" value="'+data[i].id+'" name="iddetail[]">'
            +' <td class="td-id" data-id="'+data[i].id+'"><center>' + (currentNo+no) + '</center></td>'
            +' <td> <a href="#" class="btn btn-danger btn-sm btn-remove-row" id='+ (currentNo+no)+'><i class="fa fa-trash"></i></a></td>'
            +' <td> ' + data[i].kode + '</td>'
            +' <td> ' + data[i].nama + '</td>'
            +'</tr>');
        }
      }

    });

$('body').on('click','.modal-add-row', function(event){
  event.preventDefault();

  var me = $(this),
  url = me.attr('href'),
  title = me.attr('title');

  $('#modal-title').text(title);
  $('#modal-btn-save').addClass('hide');
  $('#modal-btn-custom').addClass("hide");
  $('.modal-dialog').addClass('modal-lg');

  $('#modal-btn-add').removeClass('hide');

  $.ajax({
    url: url,
    dataType: 'html',
    success: function (response) {
      $('#datatable').DataTable().destroy();
      $('#modal-body').html(response);
    }
  });

  $("#modal").modal('show');

});


$('body').on('click','.btn-remove-row',function(event){
  event.preventDefault();

  var baris = $(this).attr('id');
  document.getElementById("tabeldetail").deleteRow(baris);
  var totalbaris = $('#tabeldetail tr').length;
  var i = 0;
  for (i = 1; i < totalbaris; i++) {
   document.getElementById('tabeldetail').rows[i].cells[0].innerHTML = '<center>' + i + '</center>' ;
 }
});


// $(function(){
//  var now = new Date();
//  var day = ("0" + now.getDate()).slice(-2);
//  var month = ("0" + (now.getMonth() + 1)).slice(-2);

//  var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
//  var thisyear = now.getFullYear();
//  $('#tanggal').val(today);

// });
</script>
