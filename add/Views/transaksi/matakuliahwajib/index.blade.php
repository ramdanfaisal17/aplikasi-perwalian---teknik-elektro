@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item active" aria-current="page">Matakuliah Wajib</li>
  </ol>
</nav>
@endsection

@section('content')

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Mata Kuliah Wajib</h3>
  </div>
  <div class="col-lg-8 text-md-right">
    <button class="btn btn-primary" onclick="$('.dtb-delete').click();" style="min-width: 100px;">
      <i class="fas fa-check-circle"></i> Aktif Pilihan
    </button>
    <a class="btn btn-success" href="{{ route("matakuliahwajib.create") }}" style="min-width: 100px;">
      <i class="fas fa-plus"></i> Tambah Data
    </a>


  </div>
</div>
<div class="card">

  <div class="card-body">
    <div class="table-responsive">
      <table class=" table datatable" id="mytable">
        <thead>
          <tr align="center">
            <th width="10" hidden>
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="customCheck2">
                <label class="custom-control-label" for="customCheck2"></label>
              </div>
            </th>
            <th width="100">Aksi</th>
            <th>Tanggal</th>
            <th>Nomor</th>
            <th>Semester</th>
            <th width="10">Aktif</th>

          </tr>
        </thead>
        <tbody>
          @foreach($matakuliahwajibs as $item)
          <tr data-entry-id="{{ $item->id }}">
            <td hidden></td>
            <td>
             {{--  <form action="{{ route('matakuliahwajib.destroy', $item->id) }}" method="POST" onsubmit="return confirm('yakin nih?');" style="display: inline-block;">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" name="button" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i></button>
              </form> --}}
              <a class="btn btn-xs btn-danger" title="Non-aktifkan Data" onclick="return confirm('yakin akand di non-aktif kan ?')" href="matakuliahwajib/deactive/{{$item->id}}">
                <i class="fa fa-times"></i>
              </a>
              <a class="btn btn-xs btn-primary" title="Aktifkan Data"  href="matakuliahwajib/active/{{$item->id}}">
                <i class="fas fa-check-circle"></i>
              </a>
              <a class="btn btn-xs btn-primary" title="Ubah Data" href="{{ route('matakuliahwajib.edit', $item->id) }}">
                <i class="fa fa-edit"></i>
              </a>
              <a class="btn btn-xs btn-success" title="Lihat Data" href="{{ route('matakuliahwajib.show', $item->id) }}">
                <i class="fa fa-eye"></i>
              </a>
            </td>
            <td><?php echo date("d/m/Y", strtotime($item->tanggal)); ?></td>
            <td>{{ $item->nomor }}</td>
            <td>{{ $item->semester }}</td>
            @if ($item->aktif == 1)
            <td>
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" checked>
                <label class="custom-control-label" ></label>
              </div>
            </td>
            @endif
            @if ($item->aktif == 0)
            <td>
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input">
                <label class="custom-control-label" ></label>
              </div>
            </td>
            @endif

          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@section('scripts')
@parent
<script>
  $(function () {
    let deleteButtonTrans = 'delete selected'
    let deleteButton = {
      text: deleteButtonTrans,
      className: 'btn-danger dtb-delete hilang',
      action: function (e, dt, node, config) {
        var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
        });

        if (ids.length === 0) {
          alert('tidak ada yang dipilih !')
          return
        }
        if (confirm('beneran ??')) {
          $.ajax({
            headers: {'x-csrf-token': _token},
            type: "POST",
            data: {ids:ids},
            url: "matakuliahwajib/deletes",
            success: function(){
             location.reload();
           }
         });
        }
      }
    }


    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

    dtButtons.push(deleteButton)


    $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  })

  function deleteSelected(){
    $('.tomboldeleteall').click();
  }

  $("#customCheck2").change( function() {
    if ($("th.select-checkbox").hasClass("selected")) {
      $('#mytable').DataTable().rows().deselect();
      $("th.select-checkbox").removeClass("selected");
    } else {
      $('#mytable').DataTable().rows().select();
      $("th.select-checkbox").addClass("selected");
    }
  }).on("select deselect", function() {
    ("Some selection or deselection going on")
    if ($('#mytable').DataTable().rows({
      selected: true
    }).count() !== $('#mytable').DataTable().rows().count()) {
      $("th.select-checkbox").removeClass("selected");
    } else {
      $("th.select-checkbox").addClass("selected");
    }
  });


</script>

@endsection
@endsection
