@extends('layouts.admin')
@section('content')

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Dosen Wali</h3>
  </div>
  <div class="col-lg-8 text-md-right">
    <button class="btn btn-primary" onclick="$('.dtb-delete').click();" style="min-width: 100px;">
      aktif Selected
    </button>
    <a class="btn btn-success" href="{{ route("dosenwali.create") }}" style="min-width: 100px;">
      create New
    </a>


  </div>
</div>
<div class="card">

  <div class="card-body">
    <div class="table-responsive">
      <table class=" table datatable" id="mytable">
        <thead>
<<<<<<< HEAD
          <tr>
=======
          <tr align="center">
>>>>>>> a16287000cbea9542e6281da2fcc12ba10496a8c
            <th width="10" hidden>
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="customCheck2">
                <label class="custom-control-label" for="customCheck2"></label>
              </div>
            </th>
<<<<<<< HEAD
            <th width="100">action</th>
            <th>tanggal</th>
            <th>nomor</th>
            <th>NIK</th>
            <th>Nama Dosen</th>
            <th>total mahasiswa</th>
=======
            <th width="100">Action</th>
            <th>Tanggal</th>
            <th>Nomor</th>
            <th>NIK</th>
            <th>Nama Dosen</th>
            <th>Total Mahasiswa</th>
>>>>>>> a16287000cbea9542e6281da2fcc12ba10496a8c
            <th width="10">aktif</th>
            
          </tr>
        </thead>
        <tbody>
          @foreach($dosenwalis as $item)
          <tr data-entry-id="{{ $item->id }}">
            <td hidden></td>
            <td>
             {{--  <form action="{{ route('dosenwali.destroy', $item->id) }}" method="POST" onsubmit="return confirm('yakin nih?');" style="display: inline-block;">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="submit" class="btn btn-xs btn-danger" value="X">
              </form> --}}
<<<<<<< HEAD
              <a class="btn btn-xs btn-danger" onclick="return confirm('yakin akand di non-aktif kan ?')" href="dosenwali/deactive/{{$item->id}}">
                <i class="fa fa-times"></i>
              </a>
              <a class="btn btn-xs btn-primary"  href="dosenwali/active/{{$item->id}}">
                <i class="fa fa-power-off"></i>
              </a>
              <a class="btn btn-xs btn-primary" href="{{ route('dosenwali.edit', $item->id) }}">
                <i class="fa fa-edit"></i>
              </a>
              <a class="btn btn-xs btn-success" href="{{ route('dosenwali.show', $item->id) }}">
=======
              <a class="btn btn-xs btn-danger" title="Non Aktif" onclick="return confirm('yakin akan di non-aktif kan ?')" href="dosenwali/deactive/{{$item->id}}">
                <i class="fa fa-times"></i>
              </a>
              <a class="btn btn-xs btn-primary"  title="Aktif" href="dosenwali/active/{{$item->id}}">
                <i class="fa fa-power-off"></i>
              </a>
              <a class="btn btn-xs btn-primary" title="Edit Data" href="{{ route('dosenwali.edit', $item->id) }}">
                <i class="fa fa-edit"></i>
              </a>
              <a class="btn btn-xs btn-success" title="Lihat Data" href="{{ route('dosenwali.show', $item->id) }}">
>>>>>>> a16287000cbea9542e6281da2fcc12ba10496a8c
                <i class="fa fa-check"></i>
              </a>
            </td>
            <td>{{ $item->tanggal }}</td>
            <td>{{ $item->nomor }}</td>
            <td>{{ $item->dosen->nik }}</td>
            <td>{{ $item->dosen->nama }}</td>
<<<<<<< HEAD
            <td>{{ $item->total_mahasiswa }}</td>
            @if ($item->aktif == 1)
            <td>
=======
            <td align="center">{{ $item->total_mahasiswa }}</td>
            @if ($item->aktif == 1)
            <td align="center">
>>>>>>> a16287000cbea9542e6281da2fcc12ba10496a8c
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" checked>
                <label class="custom-control-label" ></label>
              </div>
            </td>            
            @endif
            @if ($item->aktif == 0)
            <td>
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input">
                <label class="custom-control-label" ></label>
              </div>
            </td>
            @endif

          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@section('scripts')
@parent
<script>
  $(function () {
    let deleteButtonTrans = 'delete selected'
    let deleteButton = {
      text: deleteButtonTrans,
      className: 'btn-danger dtb-delete hilang',
      action: function (e, dt, node, config) {
        var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
        });

        if (ids.length === 0) {
          alert('tidak ada yang dipilih !')
          return
        }
        if (confirm('beneran ??')) {
          $.ajax({
            headers: {'x-csrf-token': _token},
            type: "POST",
            data: {ids:ids},
            url: "dosenwali/deletes",
            success: function(){
             location.reload();
           }
         });
        }
      }
    }


    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

    dtButtons.push(deleteButton)


    $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  })

  function deleteSelected(){
    $('.tomboldeleteall').click();
  }

  $("#customCheck2").change( function() {
    if ($("th.select-checkbox").hasClass("selected")) {
      $('#mytable').DataTable().rows().deselect();
      $("th.select-checkbox").removeClass("selected");
    } else {
      $('#mytable').DataTable().rows().select();
      $("th.select-checkbox").addClass("selected");
    }
  }).on("select deselect", function() {
    ("Some selection or deselection going on")
    if ($('#mytable').DataTable().rows({
      selected: true
    }).count() !== $('#mytable').DataTable().rows().count()) {
      $("th.select-checkbox").removeClass("selected");
    } else {
      $("th.select-checkbox").addClass("selected");
    }
  });


</script>

@endsection
@endsection
