@extends('layouts.admin')
@section('content')


<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>dosen Wali</h3>
  </div>
</div>

<form action="{{ route("dosenwali.update", [$dosenwali->id]) }}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('PUT')

  <div class="card" style="margin-top: -15px;">

    <div class="card-body">


      <div class="tab-pane fade show active" id="pills-personal-info" role="tabpanel" aria-labelledby="pills-personal-info-tab">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="tanggal">tanggal</label>
              <input readonly type="date" id="tanggal" name="tanggal" class="form-control" required autocomplete="off" value="{{$dosenwali->tanggal}}">
            </div>
            <div class="form-group">
              <label for="dosen_id">dosen</label>
              <select name="dosen_id" id="dosen_id" class="form-control select2" required>
                <option value="{{$dosenwali->dosen_id}}" selected>{{$dosenwali->dosen->nik}}  --  {{$dosenwali->dosen->nama}}</option>
                @foreach($dosens as $dosen)
                <option value="{{$dosen->id}}" dosen_nama="{{$dosen->nama}}" dosen_nik="{{$dosen->nik}}">{{$dosen->nik}}  --  {{$dosen->nama}}</option>
                @endforeach
              </select>
            </div>

          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label for="nomor">nomor</label>
              <input readonly type="text" id="nomor" name="nomor" class="form-control" required autocomplete="off" value="{{$dosenwali->nomor}}">
            </div>
            <div class="form-group">
              <label for="total_mahasiswa">total mahasiswa</label>
              <input readonly type="text" id="total_mahasiswa" name="total_mahasiswa" class="form-control" required autocomplete="off" value="{{$dosenwali->total_mahasiswa}}">
            </div>
          </div>

          

        </div>
        

        <div class="row">
          <div class="col-md-12">

            <table class="table datatable" id="tabeldetail">
              <thead>
                <tr>
                  <th width="10">
                   {{--  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal" style="margin:-5px -10px !important;"><i class="fa fa-plus"></i></button> --}}
                   <a href='{{ route("data.mahasiswa.show") }}' title='Tambah Mahasiswa' class="btn btn-primary modal-add-row">
                    <i class="fa fa-plus"></i>
                  </a>
                </th>
                <th width="100">act</th>
                <th>nrp</th>
                <th>nama</th>
                <th>status mahasiswa</th>
              </tr>
            </thead>
            <tbody id="tbody-mahasiswa">
              <?php $no=0 ?>
              @foreach($details as $detail)
              <?php $no=$no+1 ?>
              <tr><input type="hidden" value="{{$detail->id}}" name="iddetail[]"><input type="hidden" value="{{$detail->mahasiswa_id}}" name="mahasiswa_id[]">
                <td class="td-id" data-id="{{$detail->id}}"><center>{{$no}}</center></td>
<<<<<<< HEAD
                <td> <a href="#" class="btn btn-danger btn-sm btn-remove-row" id={{$no}}><i class="fa fa-trash"></i>{{$no}}</a></td>
=======
                <td> <a href="#" class="btn btn-danger btn-sm btn-remove-row" id={{$no}}><i class="fa fa-trash"></i></a></td>
>>>>>>> a16287000cbea9542e6281da2fcc12ba10496a8c
                <td>{{$detail->mahasiswa->nrp}}</td>
                <td>{{$detail->mahasiswa->nama}}</td>
                <td>{{$detail->mahasiswa->status_mahasiswa}}</td>
              </tr>
              @endforeach
            </tbody>

          </table>

        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
        </div>
        <div class="col-md-6 text-md-right">

        </div>
      </div>
    </div>

  </div>







</div>
<button class="btn btn-success" type="submit">save All</button>

</div>
{{-- modaltambah --}}

</form>
@endsection
@include('layouts.modal')
<script src="{{ asset('jquery-3.4.1.min.js') }}"></script>

<script>
//function for array checker
function check_array(value, arr){
  var status = false;
  for(var i=0; i<arr.length;i++ ){
    var needle = arr[i].id;
    if(needle == value){
      status = true;
      break;
    }
  }

  return status;
}


$('body').on('click', '#modal-btn-add', function(event){
  event.preventDefault();

  var table = $('#datatable').DataTable();

  var data = table.rows({selected: true}).data();

  var tbody = 'tbody-mahasiswa';

  console.table(data);

  var currentData = [];
  $('#'+tbody+' .td-id').each(function(){
    currentData.push({ id:$(this).data('id') });
  });
    // console.table(currentData);
    var no = currentData.length;
    var currentNo = 0;
    for(var i = 0; i < data.length; i++){
      var check = check_array(data[i].idbarang, currentData);
        // console.log(check);
        if(!check){
          currentNo++;
          $('#'+tbody).append('<tr>'
            +' <input type="hidden" value="'+data[i].id+'" name="iddetail[]"><input type="hidden" value="'+data[i].id+'" name="mahasiswa_id[]">'
            +' <td class="td-id" data-id="'+data[i].id+'"><center>' + (currentNo+no) + '</center></td>'
<<<<<<< HEAD
            +' <td> <a href="#" class="btn btn-danger btn-sm btn-remove-row" id='+ (currentNo+no)+'>'+(currentNo+no)+'<i class="fa fa-trash"></i></a></td>'
=======
            +' <td> <a href="#" class="btn btn-danger btn-sm btn-remove-row" id='+ (currentNo+no)+'><i class="fa fa-trash"></i></a></td>'
>>>>>>> a16287000cbea9542e6281da2fcc12ba10496a8c
            +' <td> ' + data[i].nrp + '</td>'
            +' <td> ' + data[i].nama + '</td>'
            +' <td> ' + data[i].status_mahasiswa + '</td>'
            +'</tr>');
        }
      }
      $('#total_mahasiswa').val(currentNo+no);
      $("#modal").modal('hide');

    });

$('body').on('click','.modal-add-row', function(event){
  event.preventDefault();

  var me = $(this),
  url = me.attr('href'),
  title = me.attr('title');

  $('#modal-title').text(title);
  $('#modal-btn-save').addClass('hide');
  $('#modal-btn-custom').addClass("hide");
  $('.modal-dialog').addClass('modal-lg');

  $('#modal-btn-add').removeClass('hide');

  $.ajax({
    url: url,
    dataType: 'html',
    success: function (response) {
      $('#datatable').DataTable().destroy();
      $('#modal-body').html(response);
    }
  });

  $("#modal").modal('show');

});


$('body').on('click','.btn-remove-row',function(event){
  event.preventDefault();

<<<<<<< HEAD


  // 
=======
>>>>>>> a16287000cbea9542e6281da2fcc12ba10496a8c
  var baris = $(this).attr('id');
  document.getElementById("tabeldetail").deleteRow(baris);  
  var totalbaris = $('#tabeldetail tr').length;
  var i = 0;
  for (i = 1; i < totalbaris; i++) {
    document.getElementById('tabeldetail').rows[i].cells[0].innerHTML = '<center>' + i + '</center>' ;         
 }
 $('#total_mahasiswa').val(totalbaris-1);

});


</script>