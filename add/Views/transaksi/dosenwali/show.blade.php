@extends('layouts.admin')
@section('content')

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Dosen Wali {{$dosenwali->dosen->nik}}  --  {{$dosenwali->dosen->nama}}</h3>
</div>

</div>

<div class="card">
    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>tanggal</th>
                    <td>{{ $dosenwali->tanggal }}</td>
                </tr>
                <tr>
                    <th>nomor</th>
                    <td>{{ $dosenwali->nomor }}</td>
                </tr>
                <tr>
                    <th>total mahasiswa</th>
                    <td>{{ $dosenwali->total_mahasiswa }}</td>
                </tr>
            </tbody>
        </table>

        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th width="15">no</th>
                    <th >nrp</th>
                    <th >mahasiswa</th>
                    <th width="200">status mahasiswa</th>
                </tr>
            </thead>
            <tbody>
                <?php $no=0?>
             @foreach($details as $item)
             <?php $no=$no+1?>
             <tr>
                <td>{{ $no }}</td>
                <td>{{ $item->mahasiswa->nrp }}</td>
                <td>{{ $item->mahasiswa->nama }}</td>
                <td>{{ $item->mahasiswa->status_mahasiswa }}</td>
            </tr>
            @endforeach

        </tbody>
    </table>
    <a href="{{ route('dosenwali.index') }}">
        <input type="button" class="btn btn-warning" value="back">
    </a>
</div>
</div>

@endsection