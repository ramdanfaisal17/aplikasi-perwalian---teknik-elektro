@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('perwalian.index') }}">Perwalian</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $jadwalperwalian->kode_perwalian }}</li>
  </ol>
</nav>
@endsection
@section('content')


<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-12">
    <h3>Perwalian Detail #{{ $jadwalperwalian->kode_perwalian }}</h3>
    @include('transaksi.perwalian.statusperwalian')
  </div>

</div>



<div class="card">

  <div class="card-body">
    <div class="table-responsive">
      <table class=" table datatable" id="mytable">
        <thead>
          <tr>
            <th width="10">
              <th>Aksi</th>
              <th>NIK Dosen</th>
              <th>Nama Dosen</th>
              <!-- <th>Mahasiswa yang perwalian</th> -->
              <th>Total Hadir</th>
            </tr>
          </thead>
          <tbody>
            @foreach($jadwalperwalian->perwalian as $item)
            <tr data-entry-id="{{ $item->id }}">
              <td>
              </td>
              <td> <a href="{{ route('perwalian.show_detail', [$jadwalperwalian->id_jadwal_perwalian, $item->dosen_id]) }}" class="btn btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="Show detail"><i class="far fa-eye"></i></a> </td>
              <td>{{ $item->dosen->nik }}</td>
              <td>{{ $item->dosen->nama }}</td>
              <!-- <td>{{ ($item->mahasiswaPerwalianCount > 0) ? $item->mahasiswaPerwalianCount : '0' }}</td> -->
              <td>{{ $item->perwalian_detail->count() }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @section('scripts')
  @parent
  <script>

  $(function () {
    let deleteButtonTrans = 'delete selected'
    let deleteButton = {
      text: deleteButtonTrans,
      className: 'btn-danger dtb-delete hilang',
      action: function (e, dt, node, config) {
        var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
        });

        if (ids.length === 0) {
          alert('tidak ada yang dipilih !')
          return
        }
        if (confirm('beneran ??')) {
          $.ajax({
            headers: {'x-csrf-token': _token},
            type: "POST",
            data: {ids:ids},
            url: "jadwalPerwalian/deletes",
            success: function(){
             location.reload();
           }
         });
        }
      }
    }


    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

    dtButtons.push(deleteButton)


    $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  })

  function deleteSelected(){
    $('.tomboldeleteall').click();
  }

  $("#customCheck2").change( function() {
    if ($("th.select-checkbox").hasClass("selected")) {
      $('#mytable').DataTable().rows().deselect();
      $("th.select-checkbox").removeClass("selected");
    } else {
      $('#mytable').DataTable().rows().select();
      $("th.select-checkbox").addClass("selected");
    }
  }).on("select deselect", function() {
    ("Some selection or deselection going on")
    if ($('#mytable').DataTable().rows({
      selected: true
    }).count() !== $('#mytable').DataTable().rows().count()) {
      $("th.select-checkbox").removeClass("selected");
    } else {
      $("th.select-checkbox").addClass("selected");
    }
  });
  </script>

  @endsection
  @endsection
