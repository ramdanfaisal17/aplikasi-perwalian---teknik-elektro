@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item"><a href="{{ route('perwalian.index') }}">Perwalian</a></li>
    <li class="breadcrumb-item"><a href="{{ route('perwalian.show', $perwalian->jadwal_perwalian->id_jadwal_perwalian) }}">{{ $perwalian->jadwal_perwalian->kode_perwalian }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $perwalian->dosen->nik }} - {{ $perwalian->dosen->nama }}</li>
  </ol>
</nav>
@endsection
@section('content')


<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-12">
    <h3>Perwalian {{ $perwalian->jadwal_perwalian->kode_perwalian }} - {{ $perwalian->dosen->nama }}</h3>
  </div>

</div>



<div class="card">

  <div class="card-body">
    <div class="table-responsive">
      <table class=" table datatable" id="mytable">
        <thead>
          <tr>
            <th width="10">
              <th>NRP Mahasiswa</th>
              <th>Nama Mahasiswa</th>
              <th>Status Pilih Matakuliah</th>
              <th>Status PRS</th>
              <th>Status Konsultasi 1</th>
              <th>Status Konsultasi 2</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach($perwalian->perwalian_detail as $item)
            <tr data-entry-id="{{ $item->id }}">
              <td>
              </td>
              <td>{{ $item->mahasiswa->nrp }}</td>
              <td>{{ $item->mahasiswa->nama }}</td>
              <td>{!! $item->status_pilih_matakuliah ? '<span class="badge badge-success">'.\Carbon\Carbon::parse($item->status_pilih_matakuliah)->format('H:i d/m/Y').'</span> ' : '<span class="badge badge-danger">Belum</span> ' !!}</td>
              <td>{!! $item->status_prs ? '<span class="badge badge-success">'.\Carbon\Carbon::parse($item->status_prs)->format('H:i d/m/Y').'</span> ' : '<span class="badge badge-danger">Belum</span> ' !!}</td>
              <td>{!! $item->status_konsultasi_1 ? '<span class="badge badge-success">'.\Carbon\Carbon::parse($item->status_konsultasi_1)->format('H:i d/m/Y').'</span> ' : '<span class="badge badge-danger">Belum</span> ' !!}</td>
              <td>{!! $item->status_konsultasi_2 ? '<span class="badge badge-success">'.\Carbon\Carbon::parse($item->status_konsultasi_2)->format('H:i d/m/Y').'</span> ' : '<span class="badge badge-danger">Belum</span> ' !!}</td>
              <th><a href="" class="btn btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="Show detail"><i class="far fa-eye"></i></a></th>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @section('scripts')
  @parent
  <script>

  $(function () {
    let deleteButtonTrans = 'delete selected'
    let deleteButton = {
      text: deleteButtonTrans,
      className: 'btn-danger dtb-delete hilang',
      action: function (e, dt, node, config) {
        var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
        });

        if (ids.length === 0) {
          alert('tidak ada yang dipilih !')
          return
        }
        if (confirm('beneran ??')) {
          $.ajax({
            headers: {'x-csrf-token': _token},
            type: "POST",
            data: {ids:ids},
            url: "jadwalPerwalian/deletes",
            success: function(){
             location.reload();
           }
         });
        }
      }
    }


    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

    dtButtons.push(deleteButton)


    $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  })

  function deleteSelected(){
    $('.tomboldeleteall').click();
  }

  $("#customCheck2").change( function() {
    if ($("th.select-checkbox").hasClass("selected")) {
      $('#mytable').DataTable().rows().deselect();
      $("th.select-checkbox").removeClass("selected");
    } else {
      $('#mytable').DataTable().rows().select();
      $("th.select-checkbox").addClass("selected");
    }
  }).on("select deselect", function() {
    ("Some selection or deselection going on")
    if ($('#mytable').DataTable().rows({
      selected: true
    }).count() !== $('#mytable').DataTable().rows().count()) {
      $("th.select-checkbox").removeClass("selected");
    } else {
      $("th.select-checkbox").addClass("selected");
    }
  });

  </script>

  @endsection
  @endsection
