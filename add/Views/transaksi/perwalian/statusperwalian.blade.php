@if($jadwalperwalian->start_genap != null)
    @if(Carbon\Carbon::now()->between(Carbon\Carbon::parse($jadwalperwalian->start_genap), Carbon\Carbon::parse($jadwalperwalian->end_genap)))
      <h5>Status : <span class="badge badge-primary">Pilih Matakuliah</span></h5>
    @elseif(Carbon\Carbon::now() < Carbon\Carbon::parse($jadwalperwalian->start_genap))
      <h5>Status : <span class="badge badge-danger">Perwalian belum dimulai</span></h5>
    @elseif(Carbon\Carbon::now()->between(Carbon\Carbon::parse($jadwalperwalian->start_prs), Carbon\Carbon::parse($jadwalperwalian->end_prs)))
        <h5>Status : <span class="badge badge-warning">Perubahan Rencana Studi</span></h5>
    @elseif(Carbon\Carbon::now()->between(Carbon\Carbon::parse($jadwalperwalian->start_konsultasi1), Carbon\Carbon::parse($jadwalperwalian->end_konsultasi1)))
        <h5>Status : <span class="badge badge-info">Konsultasi 1</span></h5>
    @elseif(Carbon\Carbon::now()->between(Carbon\Carbon::parse($jadwalperwalian->start_konsultasi2), Carbon\Carbon::parse($jadwalperwalian->end_konsultasi2)))
        <h5>Status : <span class="badge badge-info">Konsultasi 2</span></h5>
    @else
        <h5>Status : <span class="badge badge-success">Perwalian selesai</span></h5>
    @endif
@elseif($jadwalperwalian->start_ganjil != null)
    @if(Carbon\Carbon::now()->between(Carbon\Carbon::parse($jadwalperwalian->start_ganjil), Carbon\Carbon::parse($jadwalperwalian->start_ganjil)))
      <h5>Status : <span class="badge badge-primary">Pilih Matakuliah</span></h5>
    @elseif(Carbon\Carbon::now() < Carbon\Carbon::parse($jadwalperwalian->start_ganjil))
      <h5>Status : <span class="badge badge-danger">Perwalian belum dimulai</span></h5>
    @elseif(Carbon\Carbon::now()->between(Carbon\Carbon::parse($jadwalperwalian->start_prs), Carbon\Carbon::parse($jadwalperwalian->end_prs)))
        <h5>Status : <span class="badge badge-warning">Perubahan Rencana Studi</span></h5>
    @elseif(Carbon\Carbon::now()->between(Carbon\Carbon::parse($jadwalperwalian->start_konsultasi1), Carbon\Carbon::parse($jadwalperwalian->end_konsultasi1)))
        <h5>Status : <span class="badge badge-info">Konsultasi 1</span></h5>
    @elseif(Carbon\Carbon::now()->between(Carbon\Carbon::parse($jadwalperwalian->start_konsultasi2), Carbon\Carbon::parse($jadwalperwalian->end_konsultasi2)))
        <h5>Status : <span class="badge badge-info">Konsultasi 2</span></h5>
    @else
        <h5>Status : <span class="badge badge-success">Perwalian selesai</span></h5>
    @endif
@elseif($jadwalperwalian->start_antara != null)
    @if(Carbon\Carbon::now()->between(Carbon\Carbon::parse($jadwalperwalian->start_antara), Carbon\Carbon::parse($jadwalperwalian->end_antara)))
      <h5>Status : <span class="badge badge-primary">Pilih Matakuliah</span></h5>
    @elseif(Carbon\Carbon::now() < Carbon\Carbon::parse($jadwalperwalian->start_antara))
      <h5>Status : <span class="badge badge-danger">Perwalian belum dimulai</span></h5>
    @elseif(Carbon\Carbon::now()->between(Carbon\Carbon::parse($jadwalperwalian->start_prs), Carbon\Carbon::parse($jadwalperwalian->end_prs)))
        <h5>Status : <span class="badge badge-warning">Perubahan Rencana Studi</span></h5>
    @elseif(Carbon\Carbon::now()->between(Carbon\Carbon::parse($jadwalperwalian->start_konsultasi1), Carbon\Carbon::parse($jadwalperwalian->end_konsultasi1)))
        <h5>Status : <span class="badge badge-info">Konsultasi 1</span></h5>
    @elseif(Carbon\Carbon::now()->between(Carbon\Carbon::parse($jadwalperwalian->start_konsultasi2), Carbon\Carbon::parse($jadwalperwalian->end_konsultasi2)))
        <h5>Status : <span class="badge badge-info">Konsultasi 2</span></h5>
    @else
        <h5>Status : <span class="badge badge-success">Perwalian selesai</span></h5>
    @endif
@endif
