@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item active" aria-current="page">Index Hasil Perwalian</li>
  </ol>
</nav>
@endsection
@section('content')

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Laporan Hasil Perwalian</h3>
  </div>
</div>

<!-- Flash message here -->
@include('_partial.flash_message')

<div class="card">
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-striped table-bordered datatable" style="width:100%" id="absentable">
        <thead>
          <tr>
            <th style="display: none"></th>
            <th>No</th>
            <th>Tanggal</th>
            <th>NRP</th>
            <th>Nama</th>
            <th>Nama Proyek Pendidikan</th>
            <th>Program Study</th>
            <th>Total SKS</th>
            <th>IPK</th>
            <th>Semester</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1;
                foreach($datadetail as $mk) {
          ?>

            <tr>
                <td style="display: none"></td>

                <td><?php echo $i; ?></td>
                <td>{{ date('d-m-Y', strtotime($mk->created_at)) }}</td>
                <td>{{ $mk->nrp }}</td>
                <td>{{ $mk->nama }}</td>
                <td>{{ $mk->nama_proyek }}</td>
                <td>{{ $mk->namaprog }}</td>
                <td>{{ $mk->sks }}</td>
                <td>{{ $mk->nilai }}</td>
                <td>{{ $mk->semester }}</td>
                
            </tr>
          <?php $i++; } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
@section('scripts')
@parent
<script>
    $(document).ready(function() {
    $('#absentable').DataTable();
} );
</script>
@endsection
@endsection
