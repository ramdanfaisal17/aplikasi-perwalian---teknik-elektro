@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item active" aria-current="page">Laporan Konsultasi</li>
  </ol>
</nav>
@endsection
@section('content')

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Laporan Konsultasi</h3>
  </div>
</div>

<!-- Flash message here -->
@include('_partial.flash_message')

<div class="card">
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-striped table-bordered datatable" style="width:100%" id="absentable">
        <thead>
          <tr>
            <th style="display: none"></th>
            <th>No</th>
            <th>Tanggal</th>
            <th>NRP</th>
            <th>Nama</th>
            <th>Nama Proyek Pendidikan</th>
            <th>Konsultasi 1</th>
            <th>Konsultasi 2</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1;
                foreach($dataheader as $mk) {
          ?>

            <tr>
                <td style="display: none"></td>

                <td><?php echo $i; ?></td>
                <td>{{ date('d-m-Y', strtotime($mk->created_at)) }}</td>
                <td><?php echo $mk->nrp; ?></td>
                <td><?php echo $mk->nama; ?></td>
                <td>{{$mk->nama_proyek}}</td>
                <td><?php if($mk->status3 == 't') { ?>
                    <a class="btn btn-xs btn-success" href="{{url('detailkonsul1')}}/<?php echo $mk->id; ?>" title="lihat data">
                    <i class="fa fa-eye"></i>
                    </a>
                    <?php }else{ ?>
                    <a class="btn btn-xs btn-primary" href="#" title="lihat data">
                    <i class="fa fa-eye"></i>
                    </a>
                    <?php } ?>
                </td>
                <td><?php if($mk->status4 == 't') { ?>
                    <a class="btn btn-xs btn-success" href="{{url('detailkonsul2')}}/<?php echo $mk->id; ?>" title="lihat data">
                    <i class="fa fa-eye"></i>
                    </a>
                    <?php }else{ ?>
                    <a class="btn btn-xs btn-primary" href="#" title="lihat data">
                    <i class="fa fa-eye"></i>
                    </a>
                    <?php } ?>
                </td>
            </tr>
          <?php $i++; } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
@section('scripts')
@parent
<script>
    $(document).ready(function() {
    $('#absentable').DataTable();
} );
</script>
@endsection
@endsection
