@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item active" aria-current="page">Master User</li>
  </ol>
</nav>
@endsection
@section('content')

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Laporan konsultasi 1 <br><?php echo $mhs->nama ?></h3>
  </div>
  <div class="col-lg-8 text-md-right">
    
    {{-- notifikasi form validasi --}}
    @if ($errors->has('file'))
    <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('file') }}</strong>
    </span>
    @endif
    
    </div>

  </div>
</div>

<!-- Flash message here -->
@include('_partial.flash_message')

<div class="card">
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-striped table-bordered" style="width:100%" id="absentable">
        <thead>
          <tr>
            <th style="display: none"></th>
            <th>No</th>
            <th>Tanggal</th>
            <th>Semester</th>
            
          </tr>
        </thead>
        <tbody>
          <?php $i=1;
                foreach($datadetail as $detail) {
          ?>
          
            <tr>
                <td style="display: none"></td>
                <td><?php echo $i ?></td>
                <td><?php echo date('d M Y', strtotime($detail->status_konsultasi_1)); ?></td>
                <td><?php echo $detail->konsultasi_1; ?></td>
                
            </tr>
          <?php $i++; } ?>
        </tbody>
      </table>
      <a href="{{url('laporankonsul')}}"><button class="btn btn-primary">Kembali</button></a>
    </div>
  </div>
</div>
@section('scripts')
@parent
<script>
    $(document).ready(function() {
    $('#absentable').DataTable();
} );
</script>
@endsection
@endsection
