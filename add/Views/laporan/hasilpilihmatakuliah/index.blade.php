@extends('layouts.admin')
@section('content')

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Data Mahasiswa</h3>
  </div>
</div>

<div class="card">

  <div class="card-body">
    <div class="table-responsive">
      <table class=" table datatable" id="mytable">
        <thead>
          <tr align="center">
            <th width="10">
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="customCheck2">
                <label class="custom-control-label" for="customCheck2"></label>
              </div>
            </th>
            <th width="130">Action</th>
            <th>NRP</th>
            <th>Nama</th>
            <th>Total SKS</th>
            <th>IPK</th>
            <th>Semester</th>
            <th>Program Studi</th>
          </tr>
        </thead>
        <tbody>
          @foreach($all_mahasiswa as $key => $mahasiswa)
          <tr data-entry-id="{{ $mahasiswa->id }}">
            <td align="center"></td>
            <td>
                <a class="btn btn-xs btn-primary" title="Show Data" href="{{ route('nilai.laporan.show', $mahasiswa->id) }}">
                  <i class="fa fa-eye" aria-hidden="true"></i>
                </a>
            </td>
            <td>{{$mahasiswa->nrp}}</td>
            <td>{{$mahasiswa->nama}}</td>
            <td>
              <?php $total_sks = 0; ?>
              @if($mahasiswa->nilai->count() > 0)
               @foreach($mahasiswa->nilai as $nilai)
                <?php $total_sks += $nilai->sks_count ?>
               @endforeach
              @endif
              {{ $total_sks }}
            </td>
            <td>
              <?php $IPK = 0; ?>
              @if($mahasiswa->nilai->count() > 0)
               @foreach($mahasiswa->nilai as $nilai)
                <?php $IPK += $nilai->ipk ?>
               @endforeach
              @endif
              {{ number_format((float)$IPK, 2, '.', ',') }}
            </td>
            <td>{{$mahasiswa->semester}}</td>
            <td>{{ $mahasiswa->programstudi->nama }}</td>
            </tr>
            @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

@section('scripts')
@parent
<script>
  $(function () {
    let deleteButtonTrans = 'delete selected'
    let deleteButton = {
      text: deleteButtonTrans,
      className: 'btn-danger dtb-delete hilang',
      action: function (e, dt, node, config) {
        var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
        });

        if (ids.length === 0) {
          alert('tidak ada yang dipilih !')
          return
        }
        if (confirm('beneran ??')) {
          $.ajax({
            headers: {'x-csrf-token': _token},
            type: "POST",
            data: {ids:ids},
            url: "dosen/deletes",
            success: function(){
             location.reload();
           }
         });
        }
      }
    }


    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

    dtButtons.push(deleteButton)


    $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })

    $('#proyek_pendidikan_id').on('change',function(){

            location.href='/hasilperwalian?proyek_pendidikan_id='+ $('#proyek_pendidikan_id').val()+'&semester='+$('#semester').val()+'&angkatan='+$('#angkatan').val();
    })

    $('#semester').on('change',function(){
            location.href='/hasilperwalian?proyek_pendidikan_id='+ $('#proyek_pendidikan_id').val()+'&semester='+$('#semester').val()+'&angkatan='+$('#angkatan').val();
    })

       $('#angkatan').on('change',function(){
            location.href='/hasilperwalian?proyek_pendidikan_id='+ $('#proyek_pendidikan_id').val()+'&semester='+$('#semester').val()+'&angkatan='+$('#angkatan').val();
    })

    $('#btn').on('click', function() {
      window.open('hasilperwalian/print', "cetak", 'width=1000,height=800');
    })

  })


  function deleteSelected(){
    $('.tomboldeleteall').click();
  }

  $("#customCheck2").change( function() {
    if ($("th.select-checkbox").hasClass("selected")) {
      $('#mytable').DataTable().rows().deselect();
      $("th.select-checkbox").removeClass("selected");
    } else {
      $('#mytable').DataTable().rows().select();
      $("th.select-checkbox").addClass("selected");
    }
  }).on("select deselect", function() {
    ("Some selection or deselection going on")
    if ($('#mytable').DataTable().rows({
      selected: true
    }).count() !== $('#mytable').DataTable().rows().count()) {
      $("th.select-checkbox").removeClass("selected");
    } else {
      $("th.select-checkbox").addClass("selected");
    }
  });


  function getid(id){
    $("#idupdate").val(id);
  }



</script>

@endsection
@endsection
