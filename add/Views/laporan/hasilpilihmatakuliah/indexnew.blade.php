@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item active" aria-current="page">Master User</li>
  </ol>
</nav>
@endsection
@section('content')

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Laporan Hasil Pemilihan Mata Kuliah</h3>
  </div>
  <div class="col-lg-8 text-md-right">
    
    {{-- notifikasi form validasi --}}
    @if ($errors->has('file'))
    <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('file') }}</strong>
    </span>
    @endif
    
    </div>

  </div>
</div>

<!-- Flash message here -->
@include('_partial.flash_message')

<div class="card">
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-striped table-bordered" style="width:100%" id="absentable">
        <thead>
          <tr>
            <th style="display: none"></th>
            <th>Aksi</th>
            <th>NRP</th>
            <th>Nama</th>
            <th>Semester</th>
            <th>Nama Proyek Pendidikan</th>
            <th>Total SKS</th>
            
          </tr>
        </thead>
        <tbody>
          <?php $i=1;
                foreach($datamatkul as $mk) {
          ?>
          
            <tr>
                <td style="display: none"></td>
                <td><a class="btn btn-xs btn-success" href="{{url('detailmatakuliahmhs')}}/<?php echo $mk->id; ?>" title="lihat data">
                    <i class="fa fa-eye"></i>
                    </a>
                </td>
                <td><?php echo $mk->nrp; ?></td>
                <td><?php echo $mk->nama; ?></td>
                <td><?php echo $mk->semester; ?></td>
                <td>{{ $mk->nama_proyek }}</td>
                <td><?php echo $mk->sks; ?></td>
                
            </tr>
          <?php $i++; } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
@section('scripts')
@parent
<script>
    $(document).ready(function() {
    $('#absentable').DataTable();
} );
</script>
@endsection
@endsection
