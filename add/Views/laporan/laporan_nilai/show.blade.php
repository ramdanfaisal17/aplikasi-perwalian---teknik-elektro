@extends('layouts.admin')
@section('content')

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-12">
    <h3>Nilai {{$mahasiswa->nama}} - {{$mahasiswa->nrp}}</h3>
  </div>
  <div class="col-md-12">
    <table class="table table-bordered table-striped">
        <tbody>
            <tr>
                <td>Semester</td>
                <td>{{ $mahasiswa->semester }}</td>
            </tr>
            <tr>
                <td>Tahun Ajaran</td>
                <td>{{ $mahasiswa->tahun_ajaran }}</td>
            </tr>
            <tr>
                <td>Total SKS</td>
                <td>
                  <?php $total_sks = 0; ?>
                  @if($mahasiswa->nilai->count() > 0)
                   @foreach($mahasiswa->nilai as $nilai)
                    <?php $total_sks += $nilai->sks_count ?>
                   @endforeach
                  @endif
                 {{ $total_sks }}
                </td>
            </tr>
            <tr>
                <td>IPK</td>
                <td>
                  <?php $IPK = 0; ?>
                  @if($mahasiswa->nilai->count() > 0)
                   @foreach($mahasiswa->nilai as $nilai)
                    <?php $IPK += $nilai->ipk ?>
                   @endforeach
                  @endif
                  {{ number_format((float)$IPK, 2, '.', ',') }}
                </td>
            </tr>
        </tbody>
    </table>  </div>

</div>

<div class="card">
    <div class="card-body">
      @foreach($mahasiswa->nilai as $nilai)
        <h2>Semester - {{ $nilai->semester }}</h2>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th width="49%">Mata Kuliah</th>
                    <th>H</th>
                    <th>A</th>
                    <th>K</th>
                    <th>M</th>
                </tr>
            </thead>
            <tbody>

                  @foreach($nilai->nilai_detail as $nilaiDetail)
                    <tr>
                        <td>{{ $nilaiDetail->matakuliah->nama }}</td>
                        <td>{{ $nilaiDetail->H }}</td>
                        <td>{{ $nilaiDetail->A }}</td>
                        <td>{{ $nilaiDetail->K }}</td>
                        <td>{{ $nilaiDetail->M }}</td>
                    </tr>
                  @endforeach

            </tbody>
        </table>
        @endforeach
    </div>
</div>

@endsection
