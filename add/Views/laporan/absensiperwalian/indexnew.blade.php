@extends('layouts.admin')
@section('breadcrumb')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
    <li class="breadcrumb-item active" aria-current="page">Laporan Absensi Perwalian</li>
  </ol>
</nav>
@endsection
@section('content')

<div style="margin-bottom: 10px;" class="row">
  <div class="col-md-4">
    <h3>Laporan Absensi Perwalian</h3>
  </div>
  <div class="col-lg-8 text-md-right">

    {{-- notifikasi form validasi --}}
    @if ($errors->has('file'))
    <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('file') }}</strong>
    </span>
    @endif

    </div>

</div>

<!-- Flash message here -->
@include('_partial.flash_message')

<div class="card">
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-striped table-bordered datatable" style="width:100%" id="absentable">
        <thead>
          <tr>
            <th style="display: none"></th>
            <th>No</th>
            <th>Tanggal</th>
            <th>NRP</th>
            <th>Nama</th>
            <th>Nama Proyek Pendidikan</th>
            <th>Dosen Wali</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          <?php $i=1;
                foreach($dataabsen as $absen) {
          ?>

            <tr>
                <td style="display: none"></td>
                <td><?php echo $i ?></td>
                <td><?php echo date('d M Y', strtotime($absen->tanggal)) ?></td>
                <td><?php echo $absen->nrp; ?></td>
                <td><?php echo $absen->nama_mahasiswa; ?></td>
                <td><?php echo $absen->nama_proyek; ?></td>
                <td><?php echo $absen->nama_dosen; ?></td>
                <?php if($absen->status4=='t') { ?>
                    <td><i class="fa fa-check-circle-o fa-2x" style="color: green"></i></td>
                <?php }else{ ?>
                    <td><i class="fa fa-info-circle fa-2x" style="color: red"></i></td>
                <?php } ?>
            </tr>
          <?php $i++; } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
@section('scripts')
@parent
<script>
    $(document).ready(function() {
    $('#absentable').DataTable();
} );
</script>
@endsection
@endsection
