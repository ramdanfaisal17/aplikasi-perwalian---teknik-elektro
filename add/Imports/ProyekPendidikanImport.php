<?php

namespace Add\Imports;

use Add\Models\proyekpendidikan;
use Maatwebsite\Excel\Concerns\ToModel;

class ProyekPendidikanImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new proyekpendidikan([
        'kode'=> $row[1],
        'nama' => $row[2],
        ]);
    }
}

