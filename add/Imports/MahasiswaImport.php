<?php

namespace Add\Imports;

use Add\Models\mahasiswa;
use Maatwebsite\Excel\Concerns\ToModel;

class MahasiswaImport implements ToModel
{

  public function model(array $row)
  {
    return new mahasiswa([
      'nrp' => $row[1],
      'nama' => $row[2],
      //'tanggal_lahir' => $row[3],
      //'tempat_lahir' => $row[4],
      'jenis_kelamin' => $row[3],
      //'agama' => $row[6],
      'email' => $row[4],
      'no_hp' => $row[5],
      'alamat' => $row[6],
      //'provinsi_id' => $row[10],
      //'kota_id' => $row[11],
      //'kecamatan_id' => $row[12],
      //'desa_id' => $row[13],
      //'kode_pos' => $row[14],
      //'warga_negara' => $row[15],
      //'golongan_darah' => $row[16],
      //'rhesus' => $row[17],
      //'no_ijazah' => $row[18],
      //'tanggal_ijazah' => $row[19],
      //'nem' => $row[20],
      //'nilai_toefel' => $row[21],
      //'jalur_masuk' => $row[22],
      //'status_mahasiswa' => $row[23],
    ]);
  }
}

