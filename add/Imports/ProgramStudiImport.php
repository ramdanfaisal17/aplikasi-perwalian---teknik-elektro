<?php

namespace Add\Imports;

use Add\Models\programstudi;
use Maatwebsite\Excel\Concerns\ToModel;

class ProgramStudiImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new programstudi([
        'kode'=> $row[1],
        'nama' => $row[2],
        ]);
    }
}

