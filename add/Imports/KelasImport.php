<?php

namespace Add\Imports;

use Add\Models\kela;
use Maatwebsite\Excel\Concerns\ToModel;

class KelasImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new kela([
        'kode'=> $row[1],
        'nama' => $row[2],
        ]);
    }
}

