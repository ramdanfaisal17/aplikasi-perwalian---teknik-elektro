<?php

namespace Add\Imports;

use Add\Models\matakuliah;
use Maatwebsite\Excel\Concerns\ToModel;

class MatakuliahImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new matakuliah([
        'kode'=> $row[1],
        'nama'=> $row[2],
        'sks'=> $row[3],
        'prasyarat'=> $row[4],
        'semester'=> $row[5],
        'sifat'=> $row[6],
        ]);
    }
}

