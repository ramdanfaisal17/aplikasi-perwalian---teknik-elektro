<?php

namespace Add\Imports;

use Add\Models\dosen;
use Maatwebsite\Excel\Concerns\ToModel;

class DosenImport implements ToModel
{

    public function model(array $row)
    {
        return new dosen([
            
            'nik'=> $row[1],
            'nama' => $row[2],
            'email' => $row[3],
        ]);
    }
}

