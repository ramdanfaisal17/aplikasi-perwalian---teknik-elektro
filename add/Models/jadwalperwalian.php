<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class jadwalperwalian extends Model
{
  protected $primaryKey='id_jadwal_perwalian';
  protected $table = 'jadwal_perwalian';
  protected $fillable = [
    'id_jadwal_perwalian',
    'kode_perwalian',
    'proyek_pendidikan_id',
    'tanggal',
    'program_studi_id',
    'start_prs',
    'end_prs',
    'start_konsultasi1',
    'end_konsultasi1',
    'start_konsultasi2',
    'end_konsultasi2',
    'start_antara',
    'end_antara',
    'start_ganjil',
    'end_ganjil',
    'start_genap',
    'end_genap',
  ];

  protected $dateFormat = 'Y-m-d H:i:s';

  // protected $date = [
  //     'tanggal',
  //     'start_prs',
  //     'end_prs',
  //     'start_konsultasi1',
  //     'end_konsultasi1',
  //     'start_konsultasi2',
  //     'end_konsultasi2',
  //     'start_antara',
  //     'end_antara',
  //     'start_ganjil',
  //     'end_ganjil',
  //     'start_genap',
  //     'end_genap'
  // ];

  // protected $appends = [
  //      // 'address_line_1', // hacky, but fixes the issue...
  //      'start_prs',
  //   ];

  // Relation
  public function programstudi()
  {
    return $this->belongsto('Add\Models\programstudi', 'program_studi_id');
  }

  public function proyekpendidikan()
  {
    return $this->belongsto('Add\Models\proyekpendidikan', 'proyek_pendidikan_id');
  }

  public function nilai()
  {
    return $this->belongsTo('Add\Models\nilai','id_jadwal_perwalian');
  }

  public function perwalian()
  {
    return $this->hasMany('Add\Models\perwalian', 'jadwal_perwalian_id', 'id_jadwal_perwalian');
  }


  // ================================= Accessor ====================================================================================
    //
    // public function getStartPrsAttribute()
    // {
    //     return $this->attributes['start_prs'] ? Carbon::parse($this->attributes['start_prs'])->format('d-m-Y') : null;
    // }
    //
    // public function getEndPrsAttribute()
    // {
    //     return $this->attributes['end_prs'] ? Carbon::parse($this->attributes['end_prs'])->format('d-m-Y') : null;
    // }
    //
    // public function getStartKonsultasi1Attribute()
    // {
    //     return $this->attributes['start_konsultasi1'] ? Carbon::parse($this->attributes['start_konsultasi1'])->format('d-m-Y') : null;
    // }
    //
    // public function getEndKonsultasi1Attribute()
    // {
    //     return $this->attributes['end_konsultasi1'] ? Carbon::parse($this->attributes['end_konsultasi1'])->format('d-m-Y') : null;
    // }
    //
    //
    // public function getStartKonsultasi2Attribute()
    // {
    //     return Carbon::parse($this->attributes['start_konsultasi2'])->format('d-m-Y');
    // }
    //
    // public function getEndKonsultasi2Attribute()
    // {
    //     return Carbon::parse($this->attributes['end_konsultasi2'])->format('d-m-Y');
    // }
    //
    // public function getStartGanjilAttribute()
    // {
    //     return $this->attributes['start_ganjil'] ? Carbon::parse($this->attributes['start_ganjil'])->format('d-m-Y') : null;
    // }
    //
    // public function getEndGanjilAttribute()
    // {
    //     return $this->attributes['end_ganjil'] ? Carbon::parse($this->attributes['end_ganjil'])->format('d-m-Y') : null;
    // }
    //
    // public function getStartGenapAttribute()
    // {
    //     return $this->attributes['start_genap'] ? Carbon::parse($this->attributes['start_genap'])->format('d-m-Y') : null;
    // }
    //
    // public function getEndGenapAttribute()
    // {
    //     return $this->attributes['end_genap'] ? Carbon::parse($this->attributes['end_genap'])->format('d-m-Y') : null;
    // }
    //
    // public function getStartAntaraAttribute()
    // {
    //     return $this->attributes['start_antara'] ? Carbon::parse($this->attributes['start_antara'])->format('d-m-Y') : null;
    // }
    //
    // public function getEndAntaraAttribute()
    // {
    //     return $this->attributes['end_antara'] ? Carbon::parse($this->attributes['end_antara'])->format('d-m-Y') : null;
    // }

 // ================================= End Of Accessor ===========================================================


 // ================================= Mutator ===================================================================

   public function setTanggalAttribute($tanggal)
   {
       $this->attributes['tanggal'] = $tanggal ? Carbon::parse($this->tanggal)->format('Y-m-d') : null;
   }

   public function setStartPrsAttribute($start_prs)
   {
       $this->attributes['start_prs'] = $start_prs ? Carbon::parse($start_prs)->format('Y-m-d') : null;
   }

   public function setEndPrsAttribute($end_prs)
   {
      $this->attributes['end_prs'] =  $end_prs ? Carbon::parse($end_prs)->format('Y-m-d') : null;
   }

   public function setStartKonsultasi1Attribute($start_konsultasi1)
   {
       $this->attributes['start_konsultasi1'] = $start_konsultasi1 ? Carbon::parse($start_konsultasi1)->format('Y-m-d') : null;
   }

   public function setEndKonsultasi1Attribute($end_konsultasi1)
   {
       $this->attributes['end_konsultasi1'] = $end_konsultasi1 ? Carbon::parse($end_konsultasi1)->format('Y-m-d') : null;
   }


   public function setStartKonsultasi2Attribute($start_konsultasi2)
   {
       $this->attributes['start_konsultasi2'] = Carbon::parse($start_konsultasi2)->format('Y-m-d');
   }

   public function setEndKonsultasi2Attribute($end_konsultasi2)
   {
       $this->attributes['end_konsultasi2'] = Carbon::parse($end_konsultasi2)->format('Y-m-d');
   }

   public function setStartGanjilAttribute($start_ganjil)
   {
       $this->attributes['start_ganjil'] = $start_ganjil ? Carbon::parse($start_ganjil)->format('Y-m-d') : null;
   }

   public function setEndGanjilAttribute($end_ganjil)
   {
       $this->attributes['end_ganjil'] = $end_ganjil ? Carbon::parse($end_ganjil)->format('Y-m-d') : null;
   }

   public function setStartGenapAttribute($start_genap)
   {
       $this->attributes['start_genap'] = $start_genap ? Carbon::parse($start_genap)->format('Y-m-d') : null;
   }

   public function setEndGenapAttribute($end_genap)
   {
       $this->attributes['end_genap'] = $end_genap ? Carbon::parse($end_genap)->format('Y-m-d') : null;
   }

   public function setStartAntaraAttribute($start_antara)
   {
       $this->attributes['start_antara'] =  $start_antara ? Carbon::parse($start_antara)->format('Y-m-d') : null;
   }

   public function setEndAntaraAttribute($end_antara)
   {
       $this->attributes['end_antara'] =  $end_antara ? Carbon::parse($end_antara)->format('Y-m-d') : null;
   }

// ========================================= END OF MUTATOR =======================================================

  public function getDosenWaliCountAttribute()
  {
    return $this->perwalian()->count();
  }

  public function getMahasiswaCountAttribute()
  {
    $mahasiswa = 0;
    $all_perwalian = $this->perwalian()->get();
    foreach ($all_perwalian as $perwalian) {
        $mahasiswa =+ $perwalian->mahasiswaCount;
    }
    return $mahasiswa;
  }

  public function getTotalHadirCountAttribute()
  {
    $all_perwalian = $this->perwalian()->where('status_prs', '!=', null)->get();
    foreach ($all_perwalian as $key => $value) {

    }
  }

  public function getMahasiswaHadirPRSCountAttribute()
  {
      $hadir = 0;
      $all_perwalian = $this->perwalian()->get();
      foreach ($all_perwalian as $perwalian) {
          $hadir =+ $perwalian->mahasiswaHadirPRSCount;
      }
      return $hadir;
  }

  public function getMahasiswaHadirKonsultasi1CountAttribute()
  {
      $hadir = 0;
      $all_perwalian = $this->perwalian()->get();
      foreach ($all_perwalian as $perwalian) {
          $hadir =+ $perwalian->mahasiswaHadirKonsultasi1Count;
      }
      return $hadir;
  }

  public function getMahasiswaHadirKonsultasi2CountAttribute()
  {
      $hadir = 0;
      $all_perwalian = $this->perwalian()->get();
      foreach ($all_perwalian as $perwalian) {
          $hadir =+ $perwalian->mahasiswaHadirKonsultasi2Count;
      }
      return $hadir;
  }

  public function getMahasiswaHadirMatakuliahCountAttribute()
  {
      $hadir = 0;
      $all_perwalian = $this->perwalian()->get();
      foreach ($all_perwalian as $perwalian) {
          $hadir =+ $perwalian->mahasiswaHadirPRSCount;
      }
      return $hadir;
  }

  //fungsi check mahasiswa hadir saat ini
  public function getMahasiswaHadirCountAttribute()
  {
      $hadir = 0;
      $all_perwalian = $this->perwalian()->get();
      return $this->_getHadirByStatus($hadir, $all_perwalian);
  }

  private function _getHadirByStatus($hadir, $all_perwalian)
  {
      if(date('Y-m-d') >= $this->start_prs && date('Y-m-d') <= $this->end_prs)
      {
          return $this->getMahasiswaHadirPRSCountAttribute();
      }
      else if(date('Y-m-d') >= $this->start_konsultasi1 && date('Y-m-d') <= $this->end_konsultasi1)
      {
          return $this->getMahasiswaHadirKonsultasi1CountAttribute();
      }
      else if(date('Y-m-d') >= $this->start_konsultasi2 && date('Y-m-d') <= $this->end_konsultasi2)
      {
          return $this->getMahasiswaHadirKonsultasi2CountAttribute();
      }
      else
      {
          return 0;
      }
  }

}
