<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class hasilperwalian extends Model
{
  protected $table = 'vhasilperwalian';

  protected $fillable = [
    'Proyek_Pendidikan',
    'angkatan',
    'semester',
    'tanggal',
    'nrp',
    'Mahasiswa',
    'status_pilih_matakuliah',
    'status_pembahasan',
    'nama_dosen',
  ];
}
