<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class perwalian extends Model
{
  protected $table = 'tabel_perwalian';

  protected $fillable = [
    'dosen_id',
    'jadwal_perwalian_id'
  ];

  public function perwalian_detail()
  {
      return $this->hasMany('Add\Models\perwalian_detail', 'perwalian_id', 'id');
  }

  public function jadwal_perwalian()
  {
      return $this->belongsTo('Add\Models\jadwalperwalian', 'jadwal_perwalian_id');
  }

  public function dosen()
  {
      return $this->belongsTo('Add\Models\dosen', 'dosen_id');
  }

  public function getMahasiswaCountAttribute()
  {
      return $this->perwalian_detail()->count();
  }

  public function getMahasiswaHadirKonsultasi2CountAttribute()
  {
        $hadir = 0;
        $all_perwalian_detail = $this->perwalian_detail()->get();
        foreach ($all_perwalian_detail as $key => $perwalian_detail) {
          $hadir =+ $perwalian_detail->Konsultasi1HadirCount;
        }
        return $hadir;
  }

  public function getMahasiswaHadirKonsultasi1CountAttribute()
  {
        $hadir = 0;
        $all_perwalian_detail = $this->perwalian_detail()->get();
        foreach ($all_perwalian_detail as $key => $perwalian_detail) {
          $hadir =+ $perwalian_detail->Konsultasi2HadirCount;
        }
        return $hadir;
  }

  public function getMahasiswaHadirPRSCountAttribute()
  {
        return $this->perwalian_detail()->where('status_prs', '!=', null)->count();
  }

  public function getMahasiswaHadirMatakuliahCountAttribute()
  {
        return $this->perwalian_detail()->where('status_pilih_matakuliah', '!=', null)->count();
  }


}
