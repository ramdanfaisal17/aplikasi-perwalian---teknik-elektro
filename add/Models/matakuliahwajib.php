<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class matakuliahwajib extends Model
{
  protected $table = 'mata_kuliah_wajibs';

  protected $fillable = [
    'tanggal',
    'nomor',
    'semester',
    'aktif',
  ];

  public function matakuliahwajibdetail()
  {
    return $this->hasMany('Add\Models\matakuliahwajib_detail', 'id');
  }
}
