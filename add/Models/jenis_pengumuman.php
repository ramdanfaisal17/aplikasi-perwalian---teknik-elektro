<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class jenis_pengumuman extends Model
{
  protected $table = 'jenis_pengumuman';

  protected $fillable = [
    'nama',
  ];

  public function pengumuman(){
      return $this->hasMany('Add\Models\pengumuman', 'jenis_pengumuman_id', 'id');
  }

}
