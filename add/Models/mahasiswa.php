<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class mahasiswa extends Model
{
    protected $fillable = [
        'nrp',
        'nama',
        'tanggal_lahir',
        'tempat_lahir',
        'jenis_kelamin',
        'agama',
        'email',
        'no_hp',
        'alamat',
        'provinsi_id',
        'kota_id',
        'kecamatan_id',
        'desa_id',
        'kode_pos',
        'warga_negara',
        'golongan_darah',
        'rhesus',
        'no_ijazah',
        'tanggal_ijazah',
        'nem',
        'nilai_toefel',
        'jalur_masuk',
        'status_mahasiswa',
        'angkatan',
        'semester',
        'dosen_id',
        'program_studi_id'
    ];

    public function nilai()
    {
        return $this->hasMany('Add\Models\nilai', 'mahasiswa_id', 'id');
    }

    public function dosenwali_detail()
    {
        return $this->belongsTo('Add\Models\dosenwali_detail');
    }

    public function dosen()
    {
        return $this->belongsTo('Add\Models\dosen', 'dosen_id');
    }

    public function programstudi()
    {
        return $this->belongsTo('Add\Models\programstudi','program_studi_id');
    }

    public function getSksCountAttribute()
    {
        $nilai = $this->nilai()->with('nilai_detail');
        return $nilai;
    }

    public function provinsi(){
        return $this->belongsTo('Add\Models\Alamat\provinsi', 'provinsi_id');
    }

    public function kota(){
        return $this->belongsTo('Add\Models\Alamat\kota', 'kota_id');
    }

    public function kecamatan(){
        return $this->belongsTo('Add\Models\Alamat\kecamatan', 'kecamatan_id');
    }

    public function desa(){
        return $this->belongsTo('Add\Models\Alamat\desa', 'desa_id');
    }

    public function nilaiberkas()
    {
        return $this->hasMany('Add\Models\nilai_berkas', 'mahasiswa_id', 'id');
    }

    public function getLatestPDFAttribute(){
        return $this->nilaiberkas()->orderBy('created_at', 'desc')->first();
    }

}
