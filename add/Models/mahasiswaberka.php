<?php
namespace Add\Models;
 
use Illuminate\Database\Eloquent\Model;
 
class mahasiswaberka extends Model
{
    protected $table = "mahasiswa_berkas";
 
    protected $fillable = ['mahasiswa_id','file','keterangan'];
}