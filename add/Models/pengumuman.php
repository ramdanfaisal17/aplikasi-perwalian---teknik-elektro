<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class pengumuman extends Model
{
  protected $table = 'tabel_pengumuman';

  protected $fillable = [
    'tanggal',
    'jenis_pengumuman_id',
    'judul',
    'status',
    'isi',
    'thumbnail_1',
    'thumbnail_2',
    'thumbnail_3',
  ];

  public function jenis_pengumuman()
  {
      return $this->belongsTo('Add\Models\jenis_pengumuman', 'jenis_pengumuman_id');
  }

  public function gambar()
  {
      return $this->hasMany('Add\Models\PengumumanGambar', 'pengumuman_id', 'id');
  }
}
