<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class nilai_detail extends Model
{
  protected $table = 'nilai_details';

  protected $fillable = [
    'nilai_id',
    'mata_kuliah_id',
    'H',
    'A',
    'K',
    'M',
  ];
  
    protected $appends = [
       'MatakuliahNama', 'MatakuliahKode'
   ];

  public function nilai()
  {
    return $this->belongsTo('Add\Models\nilai');
  }

  public function matakuliah()
  {
    return $this->belongsTo('Add\Models\matakuliah','mata_kuliah_id');
  }
  
  public function getMatakuliahNamaAttribute()
  {
        return $this->matakuliah['nama'];
  }
  
  public function getMatakuliahKodeAttribute()
  {
      return $this->matakuliah['kode'];
  }
}
