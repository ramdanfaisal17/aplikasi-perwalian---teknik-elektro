<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class matakuliahwajib_detail extends Model
{
  protected $table = 'mata_kuliah_wajib_details';

  protected $fillable = [
    'mata_kuliah_wajib_id',
    'mata_kuliah_id',
  ];

  public function matakuliah()
  {
    return $this->belongsTo('Add\Models\matakuliah','mata_kuliah_id');
  }
}
