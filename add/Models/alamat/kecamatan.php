<?php

namespace Add\Models\Alamat;

use Illuminate\Database\Eloquent\Model;

class kecamatan extends Model
{
  protected $table = 'indonesia_districts';

  protected $fillable = [
    'city_id',
    'nama',
  ];


}
