<?php

namespace Add\Models\Alamat;

use Illuminate\Database\Eloquent\Model;

class provinsi extends Model
{
  protected $table = 'indonesia_provinces';

  protected $fillable = [
    'nama',
  ];
}
