<?php

namespace Add\Models\Alamat;

use Illuminate\Database\Eloquent\Model;

class desa extends Model
{
  protected $table = 'indonesia_villages';

  protected $fillable = [
    'district_id',
    'nama',
  ];


}
