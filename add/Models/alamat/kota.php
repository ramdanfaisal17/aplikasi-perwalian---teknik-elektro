<?php

namespace Add\Models\Alamat;

use Illuminate\Database\Eloquent\Model;

class kota extends Model
{
  protected $table = 'indonesia_cities';

  protected $fillable = [
    'province_id',
    'nama',
  ];


}
