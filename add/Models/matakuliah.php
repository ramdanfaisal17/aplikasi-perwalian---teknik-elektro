<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class matakuliah extends Model
{
    protected $table = 'mata_kuliahs';
    protected $fillable = [
        'kode',
        'nama',
        'sks',
        'prasyarat',
        'semester',
        'jenis_semester',
        'sifat',
    ];
    protected $primaryKey = "id";


    public function matakuliahdetail()
    {
        return $this->hasMany('Add\Models\matakuliah_detail', 'id');
    }
    public function nilai_detail()
    {
        return $this->belongsTo('Add\Models\nilai_detail');
    }

}
