<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class dosenwali_detail extends Model
{
  protected $table = 'dosen_wali_details';

  protected $fillable = [
    'dosen_wali_id',
    'mahasiswa_id',
  ];
  
  public function mahasiswa()
  {
    return $this->belongsTo('Add\Models\mahasiswa');
  }


}
