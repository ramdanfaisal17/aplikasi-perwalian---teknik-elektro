<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class proyekpendidikan extends Model
{
  protected $table = 'proyek_pendidikans';

  protected $fillable = [
    'kode',
    'nama',
    'mulai',
    'berakhir',
    'jenis_semester',
  ];

  public function jadwalperwalian()
  {
      return $this->hasOne('Add\Models\jadwalperwalian','proyek_pendidikan_id', 'id');
  }

  // public function nilai()
  // {
  //   return $this->belongsTo('Add\Models\nilai','id_jadwal_perwalian');
  // }

  // ============================ ACCESSOR =======================================================
  public function getMulaiAttribute()
  {
      return isset($this->attributes['mulai']) ? Carbon::parse($this->attributes['mulai'])->format('d-m-Y') : null;
  }

  public function getBerakhirAttribute()
  {
      return isset($this->attributes['berakhir']) ? Carbon::parse($this->attributes['berakhir'])->format('d-m-Y') : null;
  }
}
