<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class nilai extends Model
{
  protected $table = 'nilais';

  protected $fillable = [
    'tanggal',
    'nomor',
    'mahasiswa_id',
    'jadwal_perwalian_id',
    'semester',
    'tahun_ajaran',
    'status',
    'approve',
  ];

  protected $appends = [
       'sksCount', 'IPCount'
   ];

  public function nilai_detail()
  {
    return $this->hasMany('Add\Models\nilai_detail', 'nilai_id', 'id');
  }

  public function getSksCountAttribute()
  {
      return $this->nilai_detail()->sum('K');
  }

  public function getIPCountAttribute()
  {
      return $this->nilai_detail()->sum('M');
  }

  public function getIPKAttribute(){
     return $this->getIPCountAttribute() / $this->getSksCountAttribute();
  }

  public function mahasiswa()
  {
    return $this->belongsTo('Add\Models\mahasiswa');
  }

  // public function jadwalperwalian()
  // {
  //   return $this->belongsTo('Add\Models\jadwalperwalian','jadwal_perwalian_id');
  // }


}
