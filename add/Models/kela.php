<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class kela extends Model
{
  protected $table = 'kelas';

  protected $fillable = [
    'kode',
    'nama',
  ];
}
