<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class konsultasi extends Model
{
   protected $table = 'konsultasi';

   protected $fillable = [
    'perwalian_detail_id',
    'pembahasan',
    'status_konsultasi',
    'ke'
  ];

  public function perwalian_detail()
  {
    return $this->belongsTo('Add\Models\perwalian_detail', 'perwalian_detail_id');
  }

}
