<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class PengumumanGambar extends Model
{
    //
    protected $table = 'pengumuman_gambar';

    protected $appends = [
        'url'
    ];
    public $timestamps = false;

    protected $fillable = [
	    'gambar',
	    'pengumuman_id',
  	];

  	public function pengumuman()
  	{
  		return $this->belongsTo('Add\Models\pengumuman', 'pengumuman_id', 'id');
  	}

    public function getUrlAttribute(){
      return Storage::disk('berita-gambar')->url($this->gambar);
    }

}
