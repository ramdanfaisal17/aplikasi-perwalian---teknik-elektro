<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class dosenwali extends Model
{
  protected $table = 'dosen_walis';

  protected $fillable = [
    'tanggal',
    'nomor',
    'dosen_id',
    'total_mahasiswa',
    'aktif',
  ];

  public function dosen()
  {
    return $this->belongsTo('Add\Models\dosen', 'dosen_id', 'id');
  }

  public function dosen_wali_detail()
  {
      return $this->hasMany('Add\Models\dosenwali_detail', 'dosen_wali_id', 'id');
  }

  public function getMahasiswaCountAttribute()
  {
      return $this->dosen_wali_detail()->count();
  }


}
