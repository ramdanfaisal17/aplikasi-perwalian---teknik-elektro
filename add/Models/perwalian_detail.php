<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class perwalian_detail extends Model
{
  protected $table = 'tabel_perwalian_detail';

  protected $fillable = [
    'perwalian_id',
    'mahasiswa_id',
    'status_prs',
    'status_pilih_matakuliah',
  ];

  public function perwalian()
  {
      return $this->belongsTo('Add\Models\perwalian', 'perwalian_id');
  }

  public function mahasiswa()
  {
      return $this->belongsTo('Add\Models\mahasiswa', 'mahasiswa_id');
  }

  public function konsultasi()
  {
      return $this->hasMany('Add\Models\konsultasi', 'perwalian_detail_id', 'id');
  }

  public function getKonsultasi1HadirCount()
  {
      return $this->konsultasi()->where([['ke', '=',  1], ['status_konsultasi', '!=', null]])->count('status_konsultasi');
  }

  public function getKonsultasi2HadirCount()
  {
      return $this->konsultasi()->where([['ke', '=',  2], ['status_konsultasi', '!=', null]])->count('status_konsultasi');
  }

  public function getAllKonsultasiHadirCount()
  {
      return $this->konsultasi()->where([['status_konsultasi', '!=', null]])->count('status_konsultasi');
  }
}
