<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class nilai_berkas extends Model
{
  protected $table = 'nilai_berkas';

  protected $fillable = [
    'keterangan',
    'mahasiswa_id',
    'url',
  ];

  public function mahasiswa()
  {
    return $this->belongsTo('Add\Models\mahasiswa', 'mahasiswa_id', 'id');
  }


}
