<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class dosen extends Model
{
  protected $fillable = [
    'nik',
    'nama',
    'email',
    'total_mahasiswa',
  ];

  public function dosenwali()
  {
      return $this->hasOne('Add\Models\dosenwali', 'dosen_id');
  }


  public function mahasiswa()
  {
      return $this->belongsTo('Add\Models\mahasiswa');
  }
}
