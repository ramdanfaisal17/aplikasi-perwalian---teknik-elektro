<?php

namespace Add\Models;

use Illuminate\Database\Eloquent\Model;

class programstudi extends Model
{
  protected $table = 'program_studis';

  protected $fillable = [
    'kode',
    'nama',
  ];
  // public function jadwalperwalian()
  // {
  //   return $this->belongsTo('Add\Models\jadwalperwalian','id_jadwal_perwalian');
  // }
  // public function nilai()
  // {
  //   return $this->belongsTo('Add\Models\nilai','id_jadwal_perwalian');
  // }
}
