<?php

namespace Add\Controllers;

use Illuminate\Http\Request;
use DB;
use Add\Models\dosen;
use Add\Models\mahasiswa;
use Add\Models\user;
use Add\Models\jadwalperwalian;
use Add\Models\proyekpendidikan;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $proyekpendidikan = proyekpendidikan::where([['mulai', '<=' , date('Y-m-d')], ['berakhir', '>=', date('Y-m-d')]])->orderBy('berakhir','desc')->first();

        if(!$proyekpendidikan){
              $proyekpendidikan = proyekpendidikan::orderBy('berakhir', 'desc')->first();
        }

        $jadwalperwalian = $proyekpendidikan->jadwalperwalian ? $proyekpendidikan->jadwalperwalian : null;
        $perwalian = $jadwalperwalian ? ($jadwalperwalian->perwalian ? $jadwalperwalian->perwalian : null) : null;

	      return view('home', compact('proyekpendidikan', 'jadwalperwalian', 'perwalian'));
    }

}
