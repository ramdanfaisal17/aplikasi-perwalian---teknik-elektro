<?php

namespace Add\Controllers;

use Add\Models\dosenwali;
use Add\Models\dosenwali_detail;
use Add\Models\mahasiswa;
use Add\Models\dosen;
use Illuminate\Http\Request;
use Add\Controllers\Controller;
use DataTables;
use DB;

class dosenwaliController extends Controller
{
 public static function autonumber()
 {
    $ln = dosenwali::orderBy('nomor','desc')->first();

    if($ln){
        $new_nomor = substr($ln->nomor, -3,3)+1;
        $new_nomor = substr('000'.$new_nomor,-3,3);
    }else{
        $new_nomor = '001';
    }
    $new_nomor1 = 'DSW/'.date('y/m/').$new_nomor;

    return $new_nomor1;
}

public function index()
{
    $dosenwalis = dosenwali::orderby('aktif','desc')->orderby('created_at','desc')->get();
    return view('transaksi.dosenwali.index',compact('dosenwalis'));
}


public function create()
{
    $dosens = dosen::orderby('nama','asc')->get();
    $mahasiswas = mahasiswa::orderby('nama','asc')->get();
    return view('transaksi.dosenwali.create',compact('mahasiswas','dosens'));
}

public function store(Request $request)
{
    $header = [
        'tanggal' => $request->input('tanggal'),
        'nomor' => $request->input('nomor'),
        'dosen_id' => $request->input('dosen_id'),
        'total_mahasiswa' => $request->input('total_mahasiswa'),
        'aktif' => $request->input('aktif')
    ];

    $createheader=dosenwali::create($header);

    $count = count($request->iddetail);

    for($i = 0 ; $i < $count; $i++){
        $detail[] = [
           'dosen_wali_id' => $createheader->id,
           'mahasiswa_id' => $request->iddetail[$i]
       ];
       $createdetail = dosenwali_detail::create($detail[$i]);

   }
   return redirect()->route('dosenwali.index');


}

public function deactive($id)
{
    $data = dosenwali::find($id);
    $data->aktif = '0';
    $data->save();
    return back();
    
}

public function active($id)
{
    $data = dosenwali::find($id);
    $data->aktif = '1';
    $data->save();
    return back();
    
}

public function show(dosenwali $dosenwali)
{
    $details = dosenwali_detail::where('dosen_wali_id',$dosenwali->id)->get();
    return view('transaksi.dosenwali.show',compact('dosenwali','details'));

}   

public function edit(dosenwali $dosenwali)
{
    $details = dosenwali_detail::where('dosen_wali_id',$dosenwali->id)->get();
    $dosens = dosen::orderby('nama','asc')->get();
    $mahasiswas = mahasiswa::orderby('nama','asc')->get();
    return view('transaksi.dosenwali.edit',compact('dosenwali','details','dosens','mahasiswas'));
}

public function update(Request $request,dosenwali $dosenwali)
{

    dosenwali_detail::where('dosen_wali_id',$dosenwali->id)->delete();
    $data = dosenwali::find($dosenwali->id);
    $data->dosen_id = $request->dosen_id;
    $data->total_mahasiswa = $request->total_mahasiswa;
    $data->save();

    $count = count($request->mahasiswa_id);

    for($i = 0 ; $i < $count; $i++){
        $detail[] = [
           'dosen_wali_id' => $dosenwali->id,
           'mahasiswa_id' => $request->mahasiswa_id[$i]
       ];
       $createdetail = dosenwali_detail::create($detail[$i]);

   }
   return redirect()->route('dosenwali.index');
}

}
