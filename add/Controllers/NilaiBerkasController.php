<?php

namespace Add\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NilaiBerkasRequest;
use Add\Models\mahasiswa;
use Add\Models\nilai_berkas;
use Storage;

class NilaiBerkasController extends Controller
{
    private function uploadPDF($filePDF, $user, $diskName)
    {
        $pdf = $filePDF;
        $judul = str_replace(' ', '-', $user);
        $ext = $filePDF->getClientOriginalExtension();
        if($filePDF->isValid()){
            $filename = date('Y-m-d').".$user.$ext";
            $dataFile = Storage::disk($diskName)->put($filename, file_get_contents($filePDF));
            if($dataFile){
              return $filename;
            }else{
              return false;
            }
        }
        return false;
    }

    public function index()
    {
        $mahasiswas = mahasiswa::all();
        // dd($mahasiswas[0]->nilaiberkas->where(''));
        return view('transaksi.nilaiberkas.index', compact('mahasiswas'));
    }

    public function create()
    {
        $mahasiswas = mahasiswa::orderBy('nama', 'asc')->get();
        return view('transaksi.nilaiberkas.create', compact('mahasiswas'));
    }

    public function store(NilaiBerkasRequest $request)
    {
      // dd($request->all());
      $data = $request->all();
      $mahasiswa = mahasiswa::find($request->mahasiswa_id);
      //Upload thumbnail
      if($request->pdfFile){
          $data['url'] = $this->uploadPDF($request->pdfFile, $mahasiswa->nrp, 'nilai-berkas');
      }
      $store = nilai_berkas::create($data);
      // dd($data);
      // Cek inputan kalo berhasil, dilempar ke halaman indexya dan dikasih flash message
      if($store){
              return redirect()->route('nilaiberkas.index')
                            ->with('alert-class', 'alert-success')
                            ->with('flash_message', 'Data berhasil dimasukkan kedalam database');
      }
      // return kalo gagal input model
      return redirect()->back()
                        ->with('alert-class', 'alert-danger')
                        ->with('flash_message', 'Data gagal dimasukkan kedalam database karena ada kesalahan inputan');


    }
    public function show($id)
    {
        $mahasiswa = mahasiswa::find($id);
        $arr_nilaiBerkas = nilai_berkas::where('mahasiswa_id', $id)->get();
        // dd($mahasiswa);
        return view('transaksi.nilaiberkas.show', compact('mahasiswa','arr_nilaiBerkas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nilaiBerkas = nilai_berkas::findOrFail($id);
        Storage::disk('nilai-berkas')->delete($nilaiBerkas->url);
        $nilaiBerkas->delete();
        return redirect()->back()
                          ->with('alert-class', 'alert-success')
                          ->with('flash_message', 'Data berhasil dihapus dari database !');
    }

    public function pdfStream($filePath){
      $file = Storage::disk('nilai-berkas')->get($filePath);
      // dd($file);
      if($file){
        return response()->file(
          Storage::disk('nilai-berkas')->path($filePath, 200, [
              'Content-Type' => 'application/pdf',
              'Content-Disposition' => 'inline; filename="'.$filePath.'"'
          ])
        );

      }else{
        return abort(404);
      }
    }
}
