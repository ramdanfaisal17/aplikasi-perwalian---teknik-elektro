<?php

namespace Add\Controllers;

use Add\Models\matakuliah;
use Add\Models\matakuliah_detail;
use Illuminate\Http\Request;
use Add\Imports\MataKuliahImport;
use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use App\Http\Requests\MatakuliahRequest;
use DB;
use DataTables;
use Session;


class MataKuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matakuliahs = matakuliah::orderby('created_at','desc')->get();
        return view('masters.matakuliah.index',compact('matakuliahs'));
    }

    public function import(Request $request)
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_matakuliah',$nama_file);

        // import data
        $import = Excel::import(new MataKuliahImport, public_path('/file_matakuliah/'.$nama_file));
        if($import){
            return back()->with('alert-class', 'alert-success')->with('flash_message', 'Data Excel berhasil diimpot ke database !!');
        }
        return back()->with('alert-class', 'alert-success')->with('flash_message', 'Data Excel gagal diimpot ke database !!');

    }

    public function create()
    {
        $matakuliah = new matakuliah;
        return view('masters.matakuliah.create', compact('matakuliah'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MatakuliahRequest $request)
    {
        // dd($request->all());
        $creatematakuliah = matakuliah::create($request->all());
        $storeDetail = $this->storedetail($request,$creatematakuliah);
        if($storeDetail){
            return redirect()->route('matakuliah.index')->with('alert-class', 'alert-success')->with('flash_message', 'Data Mata Kuliah berhasil ditambahkan !!');
        }
        return redirect()->route('matakuliah.index')->with('alert-class', 'alert-success')->with('flash_message', 'Data Mata Kuliah berhasil ditambahkan !!');
    }

    public function storedetail(Request $request,$creatematakuliah)
    {
         $matakuliahDetail = array();
         $idDosen = $request->iddetail;
         if (isset($request->iddetail)){
             for($i = 0 ; $i < sizeof($idDosen); $i++){
                $matakuliahDetail[] = [
                   'dosen_id' => $request->iddetail[$i],
                   'mata_kuliah_id' => $creatematakuliah->id
               ];

             }
         }


        $idpmatakuliahDetail = array();
        for($i=0;$i < sizeof($matakuliahDetail);$i++)
        {
            $idpmatakuliahDetail[] = matakuliah_detail::create($matakuliahDetail[$i]);
        }

        if(sizeof($idpmatakuliahDetail) != sizeof($matakuliahDetail))
        {
            DB::rollback();
            return $res['status'] = 'Faild On Create Permintaan Pembelian Detail';
        }

        DB::commit();

        // return redirect()->route('matakuliah.index')->with('alert-class', 'alert-success')->with('flash_message', 'Data Matakuliah berhasil diperbaharui !!');
    }



    public function show($id)
    {
        $model = matakuliah::where('id',$id)->first();
        // $matkuldetail = matakuliah_detail::with('dosen')
        // // ->where('mata_kuliah_id', $id)
        // ->where('id', $model->id)
        //->get();
        $matkuldetail = matakuliah_detail::join('dosens', 'mata_kuliah_details.dosen_id', '=', 'dosens.id')
        ->where('mata_kuliah_id', $model->id)
        ->get();
        //$pp = PPembelian::where('idppembelian',$id)->get();
        return view('masters.matakuliah.show')
        ->with('model',$model)
            //->with('pp',$pp)
        ->with('matkuldetail',$matkuldetail);

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\matakuliah  $matakuliah
     * @return \Illuminate\Http\Response
     */
    public function edit(matakuliah $matakuliah)
    {
        return view('masters.matakuliah.edit', compact('matakuliah'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\matakuliah  $matakuliah
     * @return \Illuminate\Http\Response
     */
    public function update(MatakuliahRequest $request, matakuliah $matakuliah)
    {
        // dd($request->all());
        $matakuliah->update($request->all());
        $matkuldetail = matakuliah_detail::join('dosens', 'mata_kuliah_details.dosen_id', '=', 'dosens.id')
                                          ->where('mata_kuliah_id', $matakuliah->id)
                                          ->get();
        // return view('masters.matakuliah.edit')
        // ->with('model',$model)
        // ->with('matkuldetail',$matkuldetail);

        $this->destroydetail($matakuliah->id);
        $this->storedetail($request, $matakuliah);
        return redirect()->route('matakuliah.index')->with('alert-class', 'alert-success')->with('flash_message', 'Data Mata Kuliah berhasil diperbaharui !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\matakuliah  $matakuliah
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = matakuliah::where('id',$id)->delete();
        // matakuliah_detail::where('mata_kuliah_id',$id)->delete();
//        return redirect('/matakuliah')->with('status','berhasil menghapus data');
        if($destroy){
            $this->destroydetail($id);
            return back()->with('alert-class', 'alert-success')->with('flash_message', 'Data Mata Kuliah berhasil dihapus !!');
        }
        return back()->with('alert-class', 'alert-danger')->with('flash_message', 'Data Mata Kuliah gagal dihapus !!');
    }

    public function destroydetail($id)
    {

        matakuliah_detail::where('mata_kuliah_id',$id)->delete();
//        return redirect('/matakuliah')->with('status','berhasil menghapus data')

    }



    public function deletes(Request $request)
    {
        matakuliah::whereIn('id', request('ids'))->delete();
        matakuliah_detail::whereIn('mata_kuliah_id', request('ids'))->delete();
        Session::flash('alert-class', 'alert-success');
        Session::flash('flash_message', 'Beberapa data berhasil dihapus !!');
        return response(null, 204);
    }

    public function show_data(){
        return view('masters.matakuliah.data');
    }

    public function dataTable_for_show_data(){

     $model = matakuliah::all();
     return  DataTables::of($model)
     ->make(true);


 }


}
