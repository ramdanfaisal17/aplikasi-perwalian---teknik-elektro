<?php

namespace Add\Controllers;

use Add\Models\programstudi;
use Add\Imports\ProgramStudiImport;
use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ProgramStudiRequest;
use Session;


class ProgramStudiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programstudis = programstudi::orderby('kode','desc')->get();
        return view('masters.programstudi.index',compact('programstudis'));
    }

    public function import(Request $request)
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_programstudi',$nama_file);

        // import data
        Excel::import(new ProgramStudiImport, public_path('/file_programstudi/'.$nama_file));

        return back()->with('alert-class', 'alert-success')->with('flash_message', 'Data Excel berhasil diimport ke database !!');
    }

    public function create()
    {
        $programstudi = new programstudi; // bikin objek kosong sengaja, soaknya 1 form sama edit
        return view('masters.programstudi.create', compact('programstudi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProgramStudiRequest $request)
    {
        $store = programstudi::create($request->all());
        if($store){
            return redirect()->route('programstudi.index')->with('alert-class', 'alert-success')->with('flash_message', 'Data program studi berhasil dimasukkan !!');
        }
        return redirect()->route('programstudi.index')->with('alert-class', 'alert-danger')->with('flash_message', 'Data program studi gagal dimasukkan !!');
      }

    /**
     * Display the specified resource.
     *
     * @param  \App\programstudi  $programstudi
     * @return \Illuminate\Http\Response
     */
    public function show(programstudi $programstudi)
    {
        return view('masters.programstudi.show', compact('programstudi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\programstudi  $programstudi
     * @return \Illuminate\Http\Response
     */
    public function edit(programstudi $programstudi)
    {
        return view('masters.programstudi.edit', compact('programstudi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\programstudi  $programstudi
     * @return \Illuminate\Http\Response
     */
    public function update(ProgramStudiRequest $request, programstudi $programstudi)
    {
        $update = $programstudi->update($request->all());
        if($update){
              return redirect()->route('programstudi.index')->with('alert-class', 'alert-success')->with('flash_message', 'Data Program Studi berhasil diubah !!');
        }
        return redirect()->route('programstudi.index')->with('alert-class', 'alert-danger')->with('flash_message', 'Data Program Studi gagal diubah !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\programstudi  $programstudi
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = programstudi::where('id',$id)->delete();
        if($destroy){
              return back()->with('alert-class', 'alert-success')->with('flash_message', 'Data Program Studi berhasil dihapus !!');
        }
        return back()->with('alert-class', 'alert-danger')->with('flash_message', 'Data Program Studi gagal dihapus !!');
    }

    public function deletes(Request $request)
    {
        programstudi::whereIn('id', request('ids'))->delete();
        Session::flash('alert-class', 'alert-success');
        Session::flash('flash_message', 'Beberapa data berhasil dihapus !!');
        return response(null, 204);
    }

    public function getprogramstuid(Request $request){

        $programstudis = programstudi::where("id",$request->id)->pluck("nama","id");
        return response()->json($programstudis);
    }
}
