<?php
namespace Add\Controllers;

use Add\Models\mahasiswaberka;
use Add\Models\mahasiswa;
use Illuminate\Http\Request; 

class MahasiswaBerkaController extends Controller
{
	public function indexberkas($id){
		$fotos = mahasiswaberka::where('mahasiswa_id',$id)->where('keterangan','foto')->get();
		$fotoktps = mahasiswaberka::where('mahasiswa_id',$id)->where('keterangan','fotoktp')->get();
		$fotoktpayahs = mahasiswaberka::where('mahasiswa_id',$id)->where('keterangan','fotoktpayah')->get();
		$fotoktpibus = mahasiswaberka::where('mahasiswa_id',$id)->where('keterangan','fotoktpibu')->get();
		$fotokks = mahasiswaberka::where('mahasiswa_id',$id)->where('keterangan','fotokk')->get();

		$mahasiswas = mahasiswa::where('id',$id)->get();
		return view('masters.mahasiswaberkas.index', compact('fotos','fotoktps','fotoktpayahs','fotoktpibus','fotokks','mahasiswas'));
	}

	public function proses_upload(Request $request){
		$this->validate($request, [
			'files' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',

		]);
		
		$id = $request->input('id');
		$data = mahasiswaberka::where('mahasiswa_id',$id)->first();

		$mahasiswa_id = $request->input('mahasiswa_id');
		$file = $request->input('file');
		$keterangan = $request->input('keterangan');

//  menyimpan data file yang diupload ke variabel $file
		$files = $request->file('files');
		
//	$nama_file = $file->getClientOriginalName();
		$nama_file = $keterangan.'----'.$mahasiswa_id.'.png';

//hapus file
		if (file_exists( 'data_file/'.$keterangan.'----'.$mahasiswa_id.'png')) {
			unlink('data_file/'.'foto----'.$mahasiswa_id.'png');
		}     


// isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'data_file';
		$files->move($tujuan_upload,$nama_file);
		
		$cek = mahasiswaberka::where('mahasiswa_id',$mahasiswa_id)->where('keterangan',$keterangan)->get();

		if($cek!=null){
			mahasiswaberka::where('mahasiswa_id',$mahasiswa_id)->where('keterangan',$keterangan)->delete();
		}

		
		$data = mahasiswaberka::create($request->all());
		//$data->mahasiswa_id = $mahasiswa_id;
		//$data->file = $nama_file;
		//$data->keterangan = $keterangan;
		//$data->save();
		
		return redirect()->back();
		//return response()->json($data);

	}
	public function hapus($id)
	{
		$data = mahasiswaberka::find($id);

		if (file_exists( 'data_file/'.$data->file)) {
			unlink('data_file/'.$data->file);
		} 
		mahasiswaberka::where('id',$id)->delete();

		return back();
	}


}
