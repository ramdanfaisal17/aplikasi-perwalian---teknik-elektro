<?php

namespace Add\Controllers;

use Add\Models\matakuliahwajib;
use Add\Models\matakuliahwajib_detail;
use Add\Models\matakuliah;
use Illuminate\Http\Request;
use Add\Controllers\Controller;
use DataTables;
use DB;

class MataKuliahWajibController extends Controller
{
 public static function autonumber()
 {
    $ln = matakuliahwajib::orderBy('nomor','desc')->first();

    if($ln){
        $new_nomor = substr($ln->nomor, -3,3)+1;
        $new_nomor = substr('000'.$new_nomor,-3,3);
    }else{
        $new_nomor = '001';
    }
    $new_nomor1 = 'MKW/'.date('y/m/').$new_nomor;

    return $new_nomor1;
}

public function index()
{
    $matakuliahwajibs = matakuliahwajib::orderby('aktif','desc')->orderby('created_at','desc')->get();
    return view('transaksi.matakuliahwajib.index',compact('matakuliahwajibs'));
}


public function create()
{
    $matakuliahs = matakuliah::get();
    return view('transaksi.matakuliahwajib.create',compact('matakuliahs'));
}

public function store(Request $request)
{
    $header = [
        'tanggal' => $request->input('tanggal'),
        'nomor' => $request->input('nomor'),
        'semester' => $request->input('semester'),
    ];

    $createheader=matakuliahwajib::create($header);

    $count = count($request->iddetail);

    for($i = 0 ; $i < $count; $i++){
        $detail[] = [
           'mata_kuliah_wajib_id' => $createheader->id,
           'mata_kuliah_id' => $request->iddetail[$i]
       ];
       $createdetail = matakuliahwajib_detail::create($detail[$i]);

   }
   return redirect()->route('matakuliahwajib.index');


}

public function deactive($id)
{
    $data = matakuliahwajib::find($id);
    $data->aktif = '0';
    $data->save();
    return back();

}

public function active($id)
{
    $data = matakuliahwajib::find($id);
    $data->aktif = '1';
    $data->save();
    return back();

}

public function show(Matakuliahwajib $matakuliahwajib)
{
    $details = matakuliahwajib_detail::where('mata_kuliah_wajib_id',$matakuliahwajib->id)->get();
    return view('transaksi.matakuliahwajib.show',compact('matakuliahwajib','details'));

}

public function edit(matakuliahwajib $matakuliahwajib)
{
    $details = matakuliahwajib_detail::where('mata_kuliah_wajib_id',$matakuliahwajib->id)->get();
    return view('transaksi.matakuliahwajib.edit',compact('matakuliahwajib','details'));
}

public function update(Request $request,matakuliahwajib $matakuliahwajib)
{

    matakuliahwajib_detail::where('mata_kuliah_wajib_id',$matakuliahwajib->id)->delete();
    $data = matakuliahwajib::find($matakuliahwajib->id);
    $data->semester = $request->semester;
    $data->save();

    $count = count($request->iddetail);

    for($i = 0 ; $i < $count; $i++){
        $detail[] = [
           'mata_kuliah_wajib_id' => $matakuliahwajib->id,
           'mata_kuliah_id' => $request->mata_kuliah_id[$i]
       ];
       $createdetail = matakuliahwajib_detail::create($detail[$i]);

   }
   return redirect()->route('matakuliahwajib.index');
}

}
