<?php

namespace Add\Controllers;

use Add\Models\jadwalperwalian;
use Add\Models\proyekpendidikan;
use Add\Models\programstudi;
use Add\Models\mahasiswa;
use Add\Models\dosenwali;
use Add\Models\perwalian;
use Add\Models\perwalian_detail;
// use Maatwebsite\Excel\Facades\Excel;
// use Add\Imports\ProgramStudiImport;
use Add\Controllers\Controller;
use App\Http\Requests\JadwalPerwalianRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JadwalPerwalianController extends Controller
{

    public function index()
    {
        $jadwalperwalian = jadwalperwalian::orderby('kode_perwalian','desc')->get();
        return view('transaksi.jadwalperwalian.index',compact('jadwalperwalian'));
    }

    public function hasil()
    {
        $all_mahasiswa = Mahasiswa::all();
        $all_programstudi = programstudi::all();
        $all_proyekpendidikan = proyekpendidikan::all();
        return view('transaksi.jadwalperwalian.hasil', compact('all_mahasiswa', 'all_proyekpendidikan', 'all_programstudi'));
    }


    public function create()
    {
        $jadwalperwalian = new jadwalperwalian;
        $proyek_pendidikans = proyekpendidikan::doesnthave('jadwalperwalian')->get();
        $programstudis=programstudi::all();
        return view('transaksi.jadwalperwalian.create',compact('proyek_pendidikans','programstudis','jadwalperwalian'));

    }

    public function store(JadwalPerwalianRequest $request)
    {
        // dd($request->all());
        $jadwalperwalian = jadwalperwalian::create($request->all()); //biar return id, biar idnya bisa dipake
        $all_dosenwali = dosenwali::all();
        $this->_generateDosenMahasiswa($all_dosenwali, $jadwalperwalian);
        return redirect()->route('jadwalPerwalian.index')
                        ->with('alert-class', 'alert-success')
                        ->with('flash_message', 'Data jadwal perwalian berhasil dibuat !!');
    }

    public function show($id)
    {
        $jadwalperwalian = jadwalperwalian::findOrFail($id);
        return view('transaksi.jadwalperwalian.show', compact('jadwalperwalian'));
    }


    public function edit($id)
    {
        $jadwalperwalian = jadwalperwalian::findOrFail($id);
        // dd($jadwalperwalian);
        $proyek_pendidikans = proyekpendidikan::all();
        $programstudis = programstudi::all();
        return view('transaksi.jadwalperwalian.edit', compact('jadwalperwalian','proyek_pendidikans','programstudis'));
    }

    public function update(JadwalPerwalianRequest $request, $id)
    {
        $jadwalperwalian = jadwalperwalian::findOrFail($id);
        //pryek penddikan dari inputan
        $proyekpendidikan = proyekpendidikan::find($request->proyek_pendidikan_id);
        $data = $request->all();

        if($proyekpendidikan->jenis_semester == 'genap'){
            $data['start_genap'] = null;
            $data['end_genap'] = null;
        }else if($proyekpendidikan->jenis_semester == 'ganjil'){
            $data['start_ganjil'] = null;
            $data['end_ganjil'] = null;
            $data['start_antara'] = null;
            $data['end_antara'] = null;
        }else{
            $data['start_ganjil'] = null;
            $data['end_ganjil'] = null;
            $data['start_genap'] = null;
            $data['end_genap'] = null;
        }

        $jadwalperwalian->update($data);
        return redirect()->route('jadwalPerwalian.index')
                          ->with('alert-class', 'alert-success')
                          ->with('flash_message', 'Data jadwal perwalian berhasil diubah !!');
    }

    public function destroy($id)
    {
        jadwalperwalian::where('id_jadwal_Perwalian',$id)->delete();
        return back()->with('alert-class', 'alert-success')
                     ->with('flash_message', 'Data jadwal perwalian berhasil dihapus !!');

    }

    public function deletes(Request $request)
    {
        jadwalperwalian::whereIn('id_jadwal_Perwalian', request('ids'))->delete();
        return response(null, 204);
    }

    public static function autonumber()
    {
        $ln = DB::table('jadwal_perwalian')->orderBy('kode_perwalian','desc')->first();
        if($ln){
           $new_nomor = substr($ln->kode_perwalian, -3,3)+1;
           $new_nomor = substr('000'.$new_nomor,-3,3);
        }else{
           $new_nomor = '001';
        }
        $new_nomor1 = 'PRW/'.date('y/m/').$new_nomor;

       return $new_nomor1;
    }

    public function getjadwalperwalian(Request $request){

        $proyekpendidikans = proyekpendidikan::where("id",$request->id)->pluck("nama","id");
        $programstudis = programstudi::where("id",$request->id)->pluck("nama","id");
       // $jadwalperwalians = jadwalperwalian::where("id_jadwal_Perwalian",$request->id)->pluck("kode","id_jadwal_perwalian");
        return response()->json([$proyekpendidikans,$programstudis]);
    }

    private function _generateDosenMahasiswa($all_dosenwali, $jadwalperwalian)
    {
        foreach ($all_dosenwali as $key => $dosenwali) {
            $perwalian = perwalian::create(['dosen_id' => $dosenwali->dosen_id, 'jadwal_perwalian_id' => $jadwalperwalian->id_jadwal_perwalian]);
            foreach ($dosenwali->dosen_wali_detail as $key => $dosen_wali_detail) {
                $perwalian_detail = perwalian_detail::create(['perwalian_id' => $perwalian->id, 'mahasiswa_id' => $dosen_wali_detail->mahasiswa_id]);
            }
        }
    }

}
