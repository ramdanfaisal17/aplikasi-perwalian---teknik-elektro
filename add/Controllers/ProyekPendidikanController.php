<?php

namespace Add\Controllers;

use Add\Models\proyekpendidikan;
use Illuminate\Http\Request;
use Add\Imports\ProyekPendidikanImport;
use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ProyekPendidikanRequest;
use Session;

class ProyekPendidikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proyekpendidikans = proyekpendidikan::orderby('kode','desc')->get();
        return view('masters.proyekpendidikan.index',compact('proyekpendidikans'));
    }

    public function import(Request $request)
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_proyekpendidikan',$nama_file);

        // import data
        Excel::import(new ProyekPendidikanImport, public_path('/file_proyekpendidikan/'.$nama_file));

        return back()->with('alert-class', 'alert-success')->with('flash_message', 'Data Excel berhasil diimport ke database !!');
    }

    public function create()
    {
        $proyekpendidikan = new proyekpendidikan;
        return view('masters.proyekpendidikan.create', compact('proyekpendidikan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProyekPendidikanRequest $request)
    {
        $data = $request->all();
        $data['mulai'] = date('Y-m-d', strtotime($data['mulai']));
        $data['berakhir'] = date('Y-m-d', strtotime($data['berakhir']));
        $store = proyekpendidikan::create($data);
        if($store){
            return redirect()->route('proyekpendidikan.index')->with('alert-class', 'alert-success')->with('flash_message', 'Data proyek pendidikan berhasil dimasukkan !!');
        }
        return redirect()->route('proyekpendidikan.index')->with('alert-class', 'alert-danger')->with('flash_message', 'Data proyek pendidikan gagal dimasukkan !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\proyekpendidikan  $proyekpendidikan
     * @return \Illuminate\Http\Response
     */
    public function show(proyekpendidikan $proyekpendidikan)
    {
        return view('masters.proyekpendidikan.show', compact('proyekpendidikan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\proyekpendidikan  $proyekpendidikan
     * @return \Illuminate\Http\Response
     */
    public function edit(proyekpendidikan $proyekpendidikan)
    {
        return view('masters.proyekpendidikan.edit', compact('proyekpendidikan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\proyekpendidikan  $proyekpendidikan
     * @return \Illuminate\Http\Response
     */
    public function update(ProyekPendidikanRequest $request, proyekpendidikan $proyekpendidikan)
    {
        $data = $request->all();
        $data['mulai'] = date('Y-m-d', strtotime($data['mulai']));
        $data['berakhir'] = date('Y-m-d', strtotime($data['berakhir']));
        $update = $proyekpendidikan->update($data);
        if($update){
            return redirect()->route('proyekpendidikan.index')->with('alert-class', 'alert-success')->with('flash_message', 'Data proyek pendidikan berhasil diubah !!');
        }
        return redirect()->route('proyekpendidikan.index')->with('alert-class', 'alert-danger')->with('flash_message', 'Data proyek pendidikan gagal diubah !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\proyekpendidikan  $proyekpendidikan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = proyekpendidikan::where('id',$id)->delete();
        if($destroy){
            return back()->with('alert-class', 'alert-success')->with('flash_message', 'Data proyek pendidikan berhasil dihapus !!');
        }
        return back()->with('alert-class', 'alert-danger')->with('flash_message', 'Data proyek pendidikan gagal dihapus !!');
    }

    public function deletes(Request $request)
    {
        proyekpendidikan::whereIn('id', request('ids'))->delete();
        Session::flash('alert-class', 'alert-success');
        Session::flash('flash_message', 'Beberapa data berhasil dihapus !!');
        return response(null, 204);
    }

    public function getproyekpendidikan(Request $request){

        $proyekpendidikans = proyekpendidikan::where("id",$request->id)->pluck("name","id");
        return response()->json($proyekpendidikans);
    }

}
