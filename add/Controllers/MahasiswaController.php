<?php

namespace Add\Controllers;

use Add\Models\mahasiswa;
use Add\Models\dosen;
use Add\Models\dosenwali;
use Add\Models\dosenwali_detail;
use Add\Models\programstudi;
use Add\Models\proyekpendidikan;
use Add\Models\mahasiswakeluarga;
use Add\Models\alamat\provinsi;
use Add\Models\alamat\kota;
use Add\Models\alamat\kecamatan;
use Add\Models\alamat\desa;
use Illuminate\Http\Request;
use Add\Imports\MahasiswaImport;
use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use App\Http\Requests\MahasiswaRequest;
use DB;
use DataTables;
use Session;
use Hash;
use Add\Models\user;


class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $mahasiswas = mahasiswa::orderBy('created_at', 'desc');
        // Ngakalinnya, ngambil semua dulu, baru di sort menggunakan collection
        $mahasiswas = mahasiswa::all();
        $mahasiswas = $mahasiswas->sortBy('dosen.nama');

        $dosens = dosen::orderby('nama','desc')->get();
        return view('masters.mahasiswa.index',compact('mahasiswas','dosens'));
    }

    public function import(Request $request)
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_mahasiswa',$nama_file);

        // import data
        Excel::import(new MahasiswaImport, public_path('/file_mahasiswa/'.$nama_file));

        return back()->with('alert-class', 'alert-success')->with('flash_message', 'Data Excel berhasil diupload ke database !!');
    }

    public function create()
    {
        $provinsis = provinsi::orderBy('name','asc')->get();
        $programstudis = programstudi::all();
        $kota = collect(new kota);
        $desa = collect(new desa);
        $kecamatan = collect(new kecamatan);
        $dosens = dosen::all();
        $mahasiswa = new mahasiswa;
        return view('masters.mahasiswa.create',compact('provinsis','programstudis','dosens', 'mahasiswa', 'kecamatan', 'desa', 'kota'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MahasiswaRequest $request)
    {
        // dd($request->all());
        $store = mahasiswa::create($request->all());
        // $proyekpendidikan = proyekpendidikanp::where([['mulai', '>=', date('Y-m-d')], ['berakhir', '<=', date('Y-m-d')]])->first();
        // dd($request->all());
        //Create User after mahasiswa stored in database
        $user = user::create([
          'name' => $request['nama'],
          'email' => $request['nrp'].'@email.com',
          'password' => Hash::make('maranatha'),
          'nik' => $request['nrp'],
          'relasi' => 'Mahasiswa',
          'relasi_id' => $store->id,
        ]);

        $dosenwali = dosenwali::where('dosen_id', $request->dosen_id)->first();

        if(!$dosenwali){
            $dosenwali = dosenwali::create(['dosen_id' =>$request->dosen_id]);
        }
        dosenwali_detail::create(['dosen_wali_id' => $dosenwali->id, 'mahasiswa_id' => $store->id]);
        if($store){
              return redirect()->route('mahasiswa.index')->with('alert-class', 'alert-success')->with('flash_message', 'Data Mahasiswa berhasil ditambahkan !!');
        }
        return redirect()->route('mahasiswa.index')->with('alert-class', 'alert-danger')->with('flash_message', 'Data Mahasiswa gagal ditambahkan !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function show(mahasiswa $mahasiswa)
    {
       $provinsi = provinsi::where('id',$mahasiswa->provinsi_id)->get();
       $kota = kota::where('id',$mahasiswa->kota_id)->get();
       $kecamatan = kecamatan::where('id',$mahasiswa->kecamatan_id)->get();
       $desa = desa::where('id',$mahasiswa->desa_id)->get();
       $programstudi = programstudi::where('id',$mahasiswa->program_studi_id)->get();
       return view('masters.mahasiswa.show', compact('mahasiswa','provinsi','kota','kecamatan','desa','programstudi'));
   }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function edit(mahasiswa $mahasiswa)
    {
        $provinsis = provinsi::all();
        $programstudis = programstudi::all();
        $kota = kota::where('province_id', $mahasiswa->provinsi_id)->get();
        $kecamatan = kecamatan::where('city_id', $mahasiswa->kota_id)->get();
        $desa = desa::where('district_id', $mahasiswa->kecamatan_id)->get();
        // dd($mahasiswa->provinsi());
        //$programstudi = programstudi::where('id',$mahasiswa->program_studi_id)->get();
        $dosens = dosen::all();
        return view('masters.mahasiswa.edit', compact('mahasiswa','provinsis','provinsi','kota','kecamatan','desa','programstudis','dosens'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function update(MahasiswaRequest $request, mahasiswa $mahasiswa)
    {
        $update = $mahasiswa->update($request->all());
        if($update){
            $user = User::where([['relasi_id', '=', $mahasiswa->id], ['relasi', '=', 'mahasiswa']])->update(['name' => $request->nama, 'email' => $request->email]);
            $dosenwali = dosenwali::where('dosen_id', $request->dosen_id)->first();
            if(!$dosenwali){
                $dosenwali = dosenwali::create(['dosen_id' => $request->dosen_id]);
            }
            dosenwali_detail::where('mahasiswa_id', $request->id)->update(['dosen_wali_id' => $dosenwali->id]);

            return redirect()->route('mahasiswa.index')->with('alert-class', 'alert-success')->with('flash_message', 'Data Mahasiswa berhasil diperbaharui !!');
        }
        return redirect()->route('mahasiswa.index')->with('alert-class', 'alert-danger')->with('flash_message', 'Data Mahasiswa gagal diperbaharui !!');
    }

    public function updatedosenwali(Request $request)
    {

        $ids = array_map('intval', explode(',', $request['id']));
        mahasiswa::whereIn('id', $ids)->update(['dosen_id' => $request['dosen_id']]);
        //dosen::where('id',$request['dosen_id'])->update(['total_mahasiswa' => $total]);

        return back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = mahasiswa::where('id',$id)->delete();
        if($destroy){
            return back()->with('alert-class', 'alert-success')->with('flash_message', 'Data Mahasiswa berhasil dihapus !!');
        }
        return back()->with('alert-class', 'alert-gagal')->with('flash_message', 'Data Mahasiswa gagal dihapus !!');
    }

    public function massDestroy(Request $request)
    {

    }

    public function getkota(Request $request){

        $kota = DB::table("indonesia_cities")->where("province_id",$request->province_id)->pluck("name","id");
        return response()->json($kota);
    }

    public function getkecamatan(Request $request){

        $kecamatan = DB::table("indonesia_districts")->where("city_id",$request->city_id)->pluck("name","id");
        return response()->json($kecamatan);
    }

    public function getdesa(Request $request){

        $desa = DB::table("indonesia_villages")->where("district_id",$request->district_id)->pluck("name","id");
        return response()->json($desa);
    }

    public function indexkeluarga($id)
    {

        $mahasiswas = mahasiswa::where('id',$id)->get();
        $mahasiswakeluargas = mahasiswakeluarga::where('mahasiswa_id',$id)->get();
        return view('masters.mahasiswakeluarga.index',compact('mahasiswas','mahasiswakeluargas'));

    }

    public function indexberkas(){
        return view('masters.mahasiswaberkas.index');
    }

    public function deletes(Request $request)
    {
        mahasiswa::whereIn('id', request('ids'))->delete();
        Session::flash('alert-class', 'alert-success');
        Session::flash('flash_message', 'Beberapa data berhasil dihapus !!');
        return response(null, 204);
    }

    public function show_data(){
        return view('masters.mahasiswa.data');
    }

    public function dataTable_for_show_data(){

     $model = mahasiswa::where('dosen_id', '=', null)->orderBy('nama', 'asc')->get();//all();
     return  DataTables::of($model)->make(true);


 }

    public function show_data_filtered(){
        return view('masters.mahasiswa.datafiltered');
    }

   //  public function dataTable_for_show_data(){

   //     $model = mahasiswa::whereNull('dosen_id')->orWhere('dosen_id',0)->get();
   //     return  DataTables::of($model)
   //     ->make(true);



   // }


}
