<?php

namespace Add\Controllers;

use Add\Models\nilai;
use Add\Models\nilai_detail;
use Add\Models\programstudi;
use Add\Models\proyekpendidikan;
use Add\Models\mahasiswa;
use Add\Models\matakuliah;
use Add\Models\matakuliahwajib_detail;
use Add\Models\matakuliahwajib;
use Add\Models\jadwalperwalian;
use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class nilaiController extends Controller
{

    public function index()
    {
        $nilais = nilai::orderby('created_at','desc')->get();
        return view('transaksi.nilai.index',compact('nilais'));
    }


    public function create()
    {
        $mahasiswas = mahasiswa::orderby('nama','asc')->get();
        $jadwalperwalians = jadwalperwalian::orderby('created_at','desc')->get();
        return view('transaksi.nilai.create',compact('mahasiswas','jadwalperwalians'));
    }

    public function store(Request $request)
    {

        $nilai = [
            'tanggal' => $request->input('tanggal'),
            'nomor' => $request->input('nomor'),
            'mahasiswa_id' => $request->input('mahasiswa_id'),
            // 'jadwal_perwalian_id' => $request->input('jadwal_perwalian_id'),
            'semester' => $request->input('semester'),
            'tahun_ajaran' => $request->input('tahun_ajaran'),
            'status' => $request->input('status'),
            'approve' => 'Belum'
        ];

        // dd($nilai);

        $createnilai=nilai::create($nilai);

        $count = count($request->mata_kuliah_id);

        for($i = 0 ; $i < $count; $i++){
            $nilaiDetail[] = [
             'nilai_id' => $createnilai->id,
             'mata_kuliah_id' => $request->mata_kuliah_id[$i],
             'H' => $request->h[$i],
             'A' => $request->a[$i],
             'K' => $request->k[$i],
             'M' => $request->m[$i]
         ];
         $createnilaidetail = nilai_detail::create($nilaiDetail[$i]);

     }
     return redirect()->route('nilai.index');


 }

 public function show(nilai $nilai)
 {
    $nilaidetails = nilai_detail::where('nilai_id',$nilai->id)->get();
    // dd($nilaidetails->matakuliah->nama);
    return view('transaksi.nilai.show', compact('nilai','nilaidetails'));
}

public function edit(nilai $nilai)
{
    $mahasiswas = mahasiswa::orderby('nama','asc')->get();
    $jadwalperwalians = jadwalperwalian::orderby('created_at','desc')->get();
    $nilaidetails = nilai_detail::where('nilai_id',$nilai->id)->get();
    return view('transaksi.nilai.edit', compact('nilai','nilaidetails','mahasiswas','jadwalperwalians'));


}

public function update(Request $request, nilai $nilai)
{
    // var_dump($request->H);
    // die;
   nilai_detail::where('nilai_id',$nilai->id)->delete();
   $count = count($request->mata_kuliah_id);

   for($i = 0 ; $i < $count; $i++){
    $detail[] = [
        'nilai_id' => $nilai->id,
        'mata_kuliah_id' => $request->mata_kuliah_id[$i],
        'H' => $request->H[$i],
        'A' => $request->A[$i],
        'K' => $request->K[$i],
        'M' => $request->M[$i]
    ];
    $createdetail = nilai_detail::create($detail[$i]);

}
return redirect()->route('nilai.index');
}

public function destroy($id)
{
    nilai::where('id',$id)->delete();
    return back();

}

public function laporan_nilai()
{
    $all_mahasiswa = mahasiswa::with('nilai.nilai_detail')->get();
    return view('laporan.laporan_nilai.index', compact('all_mahasiswa'));
}

public function laporan_nilai_show($id)
{
    $mahasiswa = mahasiswa::with('nilai.nilai_detail')->find($id);
    if($mahasiswa)
    {
        return view('laporan.laporan_nilai.show', compact('mahasiswa'));
    }
}

public function deletes(Request $request)
{
    nilai::whereIn('id', request('ids'))->delete();
    return response(null, 204);
}

public static function autonumber()
{
    $ln = nilai::orderBy('nomor','desc')->first();

    if($ln){
        $new_nomor = substr($ln->nomor, -3,3)+1;
        $new_nomor = substr('000'.$new_nomor,-3,3);
    }else{
        $new_nomor = '001';
    }
    $new_nomor1 = 'NIL/'.date('y/m/').$new_nomor;

    return $new_nomor1;
}

public function getmatakuliahperwalian(Request $request){

    // if($request->semester < 3){
        // $matakuliahwajib = matakuliahwajib::where('semester',$request->data_semester)
        // ->where('aktif','1')
        // ->orderby('created_at','asc')
        // ->first();


        $matakuliahwajib_detail = //matakuliahwajib_detail::where('mata_kuliah_wajib_id',$matakuliahwajib->id)->get();
                                    DB::table('mahasiswa_matakuliah')->join('mata_kuliahs', 'mata_kuliahs.id', '=', 'mahasiswa_matakuliah.mata_kuliah_id')
                                    ->where('mahasiswa_matakuliah.semester', '=', $request->data_semester)
                                    ->where('mahasiswa_matakuliah.mahasiswa_id', '=', $request->data_mahasiswa_id)
                                    ->select('mahasiswa_matakuliah.*')
                                    ->get();

        foreach ($matakuliahwajib_detail as $matkul){
            $data[] = matakuliah::where('id',$matkul->mata_kuliah_id)->get();
        }
        return response()->json($data);


    // }
    
}

}
