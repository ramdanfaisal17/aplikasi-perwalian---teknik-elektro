<?php
namespace Add\Controllers\api;

use Add\Models\mahasiswaberka;
use Add\Models\mahasiswa;
use Illuminate\Http\Request; 
use Add\Controllers\Controller;


class MahasiswaBerkaController extends Controller
{

	public function indexberkas($id){

		$mahasiswaberkas = mahasiswaberka::where('mahasiswa_id',$id)->get();
		return response()->json($mahasiswaberkas);
	}

	public function indexberkasfoto($id){
		return response()->download(public_path($path = '/data_file/foto----'.$id.'.png'));		
	}

	public function indexberkasfotoktp($id){
		return response()->download(public_path($path = '/data_file/fotoktp----'.$id.'.png'));		
	}

	public function indexberkasfotoktpayah($id){
		return response()->download(public_path($path = '/data_file/fotoktpayah----'.$id.'.png'));		
	}

	public function indexberkasfotoktpibu($id){
		return response()->download(public_path($path = '/data_file/fotoktpibu----'.$id.'.png'));		
	}

	public function indexberkasfotokk($id){
		return response()->download(public_path($path = '/data_file/fotokk----'.$id.'.png'));		
	}


	public function proses_upload(Request $request){

		$this->validate($request, [
			'file' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',

		]);

		$id = $request['mahasiswa_id'];

		$data = mahasiswaberka::where('mahasiswa_id',$id)->first();
		
		$mahasiswa_id = $request['mahasiswa_id'];
		$file = $request->input('file');
		$keterangan = $request['keterangan'];

//  menyimpan data file yang diupload ke variabel $file
		$files = $request->file('file');
		
//	$nama_file = $file->getClientOriginalName();
		$nama_file = $keterangan.'----'.$mahasiswa_id.'.png';
		$file = $keterangan.'----'.$mahasiswa_id.'.png';


//hapus file
		if (file_exists( 'data_file/'.$keterangan.'----'.$mahasiswa_id.'png')) {
			unlink('data_file/'.'foto----'.$mahasiswa_id.'png');
		}     


// isi dengan nama folder tempat kemana file diupload
		$tujuan_upload = 'data_file';

		$files->move($tujuan_upload,$nama_file);
		
		$cek = mahasiswaberka::where('mahasiswa_id',$mahasiswa_id)->where('keterangan',$keterangan)->get();

		if($cek!=null){
			mahasiswaberka::where('mahasiswa_id',$mahasiswa_id)->where('keterangan',$keterangan)->delete();
		}

		$data = [
			'mahasiswa_id' => $mahasiswa_id,
			'file' => $nama_file,
			'keterangan' => $keterangan
		];

		$createberkas = mahasiswaberka::create($data);

		
		//return redirect()->back();
		return response()->json('upload selesai');

	}
	public function hapus($id)
	{
		$data = mahasiswaberka::find($id);

		if (file_exists( 'data_file/'.$data->file)) {
			unlink('data_file/'.$data->file);
		} 
		mahasiswaberka::where('id',$id)->delete();

		return response()->json('delete success!');
		
	}


}
