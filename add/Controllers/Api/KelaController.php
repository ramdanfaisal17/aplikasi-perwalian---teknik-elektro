<?php

namespace Add\Controllers\api;

use Add\Models\kela;
use Illuminate\Http\Request;
use Add\Controllers\Controller;


class KelaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kelas = kela::orderby('created_at','desc')->get();
        return response()->json($kelas);
    }

    public function store(Request $request)
    {
        $kela = kela::create($request->all());
        return response()->json($kela);
        
    }

    public function update(Request $request, kela $kela)
    {
        $kela->update($request->all());
        return response()->json($kela);

    }

    public function show(kela $kela)
    {
        $show = kela::where('id',$kela->id)->get();
       return response()->json($show);
   }

   public function destroy($id)
   {
    $kela = kela::where('id',$id)->delete();
    return response()->json('delete success');
}

public function massDestroy(Request $request)
{

}


}
