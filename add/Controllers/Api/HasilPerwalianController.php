<?php

namespace Add\Controllers\Api;

use Add\Models\hasilperwalian;
use Add\Models\proyekpendidikan;
use Add\Models\programstudi;
use Add\Models\mahasiswa;
use Illuminate\Http\Request;
use Add\Controllers\Controller;
use DataTables;
use DB;
use Input;


class HasilPerwalianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query= hasilperwalian::orderby('tanggal','desc');
        if (Input::get('proyek_pendidikan_id')){
        	if (Input::get('proyek_pendidikan_id') != 'null') {
        		$query->where('Proyek_Pendidikan',Input::get('proyek_pendidikan_id'));
        	}
        }
        if (Input::get('semester')){
        	if (Input::get('semester') != 'null') {
             $query->where('semester',Input::get('semester'));
         }
     }
     if (Input::get('angkatan')){
         if (Input::get('angkatan') != 'null') {
             $query->where('angkatan',Input::get('angkatan'));
         }
     }
     $hasilperwalian=$query->get();
     $proyek_pendidikans = proyekpendidikan::all();
     $programstudis=programstudi::all();
     $angkatan=mahasiswa::select('angkatan')->distinct()->orderBy('angkatan','asc')->get();
     $semester=mahasiswa::select('semester')->distinct()->orderBy('semester','asc')->get();
//        return view('laporan.hasilperwalian.index',compact('hasilperwalian','proyek_pendidikans','programstudis','angkatan','semester'));
     return response()->json($hasilperwalian);

 }
 public function print ()
 {
  $query= hasilperwalian::orderby('tanggal','desc');
  if (Input::get('proyek_pendidikan_id')){
     if (Input::get('proyek_pendidikan_id') != 'null') {
      $query->where('Proyek_Pendidikan',Input::get('proyek_pendidikan_id'));
  }
}
if (Input::get('semester')){
 if (Input::get('semester') != 'null') {
     $query->where('semester',Input::get('semester'));
 }
}
if (Input::get('angkatan')){
 if (Input::get('angkatan') != 'null') {
     $query->where('angkatan',Input::get('angkatan'));
 }
}
$hasilperwalian=$query->get();
//return view('laporan.hasilperwalian.print',compact('hasilperwalian','proyek_pendidikans','programstudis','angkatan','semester'));
return response()->json($hasilperwalian);

}

}
