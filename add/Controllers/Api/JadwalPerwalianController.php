<?php

namespace Add\Controllers\Api;

use Add\Models\jadwalperwalian;
use Add\Models\proyekpendidikan;
use Add\Models\programstudi;
// use Add\Imports\ProgramStudiImport;
// use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JadwalPerwalianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jadwalperwalian = jadwalperwalian::orderby('kode_perwalian','desc')->get();
        return response()->json($jadwalperwalian);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jadwalperwalian = jadwalperwalian::create($request->all());
        return response()->json($jadwalperwalian);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\programstudi  $programstudi
     * @return \Illuminate\Http\Response
     */
    // public function show($id )
    // {
       
    //    $proyekpendidikan = proyekpendidikan::where('id',$id->proyek_pendidikan_id)->get();
    //     $programstudi = programstudi::where('id',$id->program_studi_id)->get();
    //    // return view('masters.mahasiswa.show', compact('mahasiswa','provinsi','kota','kecamatan','desa'));    
    //     return view('transaksi.jadwalperwalian.show', compact('jadwalperwalian','proyekpendidikan','programstudi'));
    // }

     public function show($id)
    {
        $jadwalperwalian = jadwalperwalian::findOrFail($id);
        return response()->json($jadwalperwalian);
        
    }



 public function update(Request $request ,jadwalperwalian $jadwalperwalian)
    {
         $jadwalperwalian->update($request->all());
        return response()->json($jadwalperwalian);
          
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\programstudi  $programstudi
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        jadwalperwalian::where('id_jadwal_Perwalian',$id)->delete();
//        return redirect('/programstudi')->with('status','berhasil menghapus data');
        return response()->json('delete success');
        

    }

   public function deletes(Request $request)
    {
        jadwalperwalian::whereIn('id_jadwal_Perwalian', request('ids'))->delete();
        return response()->json('delete(s) success');
        
    }

    public static function autonumber()
    {
       $ln = DB::table('jadwal_perwalian')->orderBy('kode_perwalian','desc')->first();
        if($ln){
            $new_nomor = substr($ln->kode_perwalian, -3,3)+1;
            $new_nomor = substr('000'.$new_nomor,-3,3);
        }else{
            $new_nomor = '001';
        }
            $new_nomor1 = 'PRW/'.date('y/m/').$new_nomor;

            //return $new_nomor1;
        return response()->json($new_nomor1);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
}