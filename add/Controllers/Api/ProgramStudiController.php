<?php

namespace Add\Controllers\api;

use Add\Models\programstudi;
use Illuminate\Http\Request;
use Add\Controllers\Controller;


class ProgramStudiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programstudis = programstudi::orderby('created_at','desc')->get();
        return response()->json($programstudis);
    }

    public function store(Request $request)
    {
        $programstudi = programstudi::create($request->all());
        return response()->json($programstudi);
        
    }

    public function update(Request $request, programstudi $programstudi)
    {
        $programstudi->update($request->all());
        return response()->json($programstudi);

    }

    public function show(programstudi $programstudi)
    {
        $show = programstudi::where('id',$programstudi->id)->get();
       return response()->json($show);
   }

   public function destroy($id)
   {
    $programstudi = programstudi::where('id',$id)->delete();
    return response()->json('delete success');
}

public function massDestroy(Request $request)
{

}


}
