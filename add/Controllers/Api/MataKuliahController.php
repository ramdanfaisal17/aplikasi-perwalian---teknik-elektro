<?php

namespace Add\Controllers\api;

use Add\Models\matakuliah;
use Illuminate\Http\Request;
use Add\Controllers\Controller;
use DB;

class MataKuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matakuliahs = matakuliah::orderby('semester','asc')->get();
        return response()->json($matakuliahs);
    }

    public function store(Request $request)
    {
        $matakuliah = matakuliah::create($request->all());
        return response()->json($matakuliah);
        
    }

    public function update(Request $request, matakuliah $matakuliah)
    {
        $matakuliah->update($request->all());
        return response()->json($matakuliah);

    }

    public function show(matakuliah $matakuliah)
    {
        $show = matakuliah::where('id',$matakuliah->id)->get();
       return response()->json($show);
   }

   public function destroy($id)
   {
    $matakuliah = matakuliah::where('id',$id)->delete();
    return response()->json('delete success');
}

public function massDestroy(Request $request)
{

}

    public function matakuliahbyid(Request $request, $id)
    {
        $idmhs = $request->id;
        $carisemester = DB::table('mahasiswas')->where('id', '=', $idmhs)->first();
        $matakuliahs = matakuliah::where('semester', '=', $carisemester->semester)->orderby('semester','asc')->get();
        return response()->json($matakuliahs);
    }
    
    public function matakuliahbyid2(Request $request, $id)
    {
        $idmhs = $request->id;
        $carisemester = DB::table('mahasiswas')->where('id', '=', $idmhs)->first();
        
        $matakuliahs = DB::table('nilai_details')
                            ->join('nilais', 'nilais.id', '=', 'nilai_details.nilai_id')
                            ->join('mata_kuliahs', 'mata_kuliahs.id', '=', 'nilai_details.mata_kuliah_id')
                            ->where('nilais.mahasiswa_id', '=', $idmhs)
                            ->where('nilais.semester', '=', $carisemester->semester)
                            ->select('mata_kuliahs.nama', 
                                    'mata_kuliahs.sks',
                                    'mata_kuliahs.semester',
                                    'mata_kuliahs.sifat',
                                    'nilais.approve'
                                    )
                            ->get();
        
        return response()->json($matakuliahs);
    }
    
    public function statusnilai(Request $request, $id)
    {
        $idmhs = $request->id;
        $carisemester = DB::table('mahasiswas')->where('id', '=', $idmhs)->first();
        $nilaistatus = DB::table('nilais')->where('mahasiswa_id', '=', $request->id)->where('semester', '=', $carisemester->semester)->first();
        
        if($nilaistatus)
        {
            return response()->json($nilaistatus);
        }else{
            return abort(404);
        }
    }
    
    public function updatestatusnilai(Request $request)
    {
        $idnilai = $request->get('id_nilai');
        $dataupdate = [
                'approve' => 'yes',
            ];
        
        $updatenilai = DB::table('nilais')->where('id', '=', $idnilai)->update($dataupdate);
        
        if($updatenilai)
        {
            return response()->json($updatenilai);
        }else{
            return abort(404);
        }
    }
    
    public function carikonsul1(Request $request)
    {
        $idmhs = $request->id;
        $carisemester = DB::table('mahasiswas')->where('id', '=', $idmhs)->first();
        $datakonsul = DB::table('konsultasi')
                        ->where('mahasiswa_id', '=', $idmhs)
                        ->where('semester', '=', $carisemester->semester)
                        ->where('jenis', '=', '1')
                        ->first();
        if($datakonsul)
        {
            return response()->json($datakonsul);
        }else{
            return abort(404);
        }
    }
    
    public function carikonsul2(Request $request)
    {
        $idmhs = $request->id;
        $carisemester = DB::table('mahasiswas')->where('id', '=', $idmhs)->first();
        $datakonsul = DB::table('konsultasi')
                        ->where('mahasiswa_id', '=', $idmhs)
                        ->where('semester', '=', $carisemester->semester)
                        ->where('jenis', '=', '2')
                        ->first();
        if($datakonsul)
        {
            return response()->json($datakonsul);
        }else{
            return abort(404);
        }
    }
    


}
