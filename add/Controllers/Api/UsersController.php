<?php

namespace Add\Controllers\api;

use Add\Models\user;
use Illuminate\Http\Request;
use Add\Controllers\Controller;
use auth;
use Session;

class UsersController extends Controller
{

    public function index()
    {
        $users = user::all();
        return response()->json([
                 'success' => true,
                 'code' => 200,
                 'message' => 'Success',
                 'data' => $users
               ], 200);
    }

    public function store(Request $request)
    {
        $user = user::create($request->all());
        return response()->json([
                  'success' => true,
                  'code' => 201,
                  'message' => 'Data berhasil disimpan',
                  'data' => $user
                ], 201);
    }

    public function update(Request $request, user $user)
    {
        $user->update($request->all());
        return response()->json([
                  'success' => true,
                  'code' => 201,
                  'message' => 'Data berhasil disimpan',
                  'data' => $data
                ], 204);
    }

    public function show(user $user)
    {
      return response()->json([
               'success' => true,
               'code' => 200,
               'message' => 'Success',
               'data' => $user
             ], 200);
    }

    public function destroy(user $user)
    {
        $user->delete();
        return response()->json([
                  'success' => true,
                  'code' => 201,
                  'message' => 'Data berhasil disimpan',
                  'data' => $data
                ], 204);
    }

    public function gantipassword(Request $request){

        if($request['password'] != $request['password-confirmation']){
          return response()->json('konfirmasi password tidak cocok !');
        }
        else{
          $user = user::findOrFail($request['id']);
          $user->password = hash::make($request['password']);
          $user->password_change = '1';
          $user->save();
          $success['password_change'] = '1';
          $success['msg'] = 'password berhasil di ganti';

          return response()->json(['success' => $success], 200);

        }

  }





}
