<?php

namespace Add\Controllers\Api;

use Add\Models\nilai;
use Add\Models\nilai_detail;
use Add\Models\programstudi;
use Add\Models\proyekpendidikan;
use Add\Models\mahasiswa;
use Add\Models\matakuliah;
use Add\Models\matakuliahwajib_detail;
use Add\Models\matakuliahwajib;
use Add\Models\perwalian;
use Add\Models\jadwalperwalian;
use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NilaiController extends Controller
{

    public function index()
    {
        $arr_nilai = nilai::orderby('created_at','desc')->get();
        return response()->json([
                 'success' => true,
                 'code' => 200,
                 'message' => 'Success',
                 'data' => $arr_nilai,
               ], 200);

    }

     public function store(Request $request)
    { 

    $mahasiswa = mahasiswa::where('id',$request->mahasiswa_id)->first();
    $perwalian = perwalian::where('id',$request->perwalian_id)->with(['jadwal_perwalian.proyekpendidikan'])->first();
    $nilai=[
      'tanggal' => $request->tanggal,
      'nomor' => $request->nomor,
      'mahasiswa_id' => $request->mahasiswa_id,
      'jadwal_perwalian_id' => $request->perwalian_id,
      'semester' => $mahasiswa->semester,
      'tahun_ajaran' => $perwalian->jadwal_perwalian->proyekpendidikan->mulai,
      'status' => $request->status
    ];

    $nilai=nilai::create($nilai);

    $count = count($request->mata_kuliah_id);

    if ($count>0){
      for($i = 0 ; $i < $count; $i++){
        $nilaiDetail[] = [
         'nilai_id' => $nilai->id,
         'mata_kuliah_id' => $request->mata_kuliah_id[$i],
         'H' => "0",
         'A' => "0",
         'K' => "0",
         'M' => "0"
       ];

       $createdetail = nilai_detail::create($nilaiDetail[$i]);

     }
   }

    return response()->json([
     'success' => true,
     'code' => 201,
     'message' => 'Data berhasil disimpan',
     'data' => 'sukses'
   ], 201);

 }
 
 public function store_prs(Request $request)
 {
    $mahasiswa = mahasiswa::where('id',$request->mahasiswa_id)->first();
    $perwalian = perwalian::where('id',$request->perwalian_id)->with(['jadwal_perwalian.proyekpendidikan'])->first();
    $nilai = nilai::where([['mahasiswa_id', '=', $request->mahasiswa_id], ['jadwal_perwalian_id', '=', $perwalian->jadwal_perwalian_id]])->first();
    
    $nilai->update(['status' => $request->status]);
    
    //Dihapus nilai detailnya, soalnya kan PRS
    if(isset($nilai->nilai_detail)){
        $nilai->nilai_detail()->delete();
    }

    $count = count($request->mata_kuliah_id);
    if ($count>0){
      for($i = 0 ; $i < $count; $i++){
        $nilaiDetail[] = [
         'nilai_id' => $nilai->id,
         'mata_kuliah_id' => $request->mata_kuliah_id[$i],
         'H' => "0",
         'A' => "0",
         'K' => "0",
         'M' => "0"
       ];

       $createdetail = nilai_detail::create($nilaiDetail[$i]);

     }
   }

    return response()->json([
     'success' => true,
     'code' => 201,
     'message' => 'Data berhasil disimpan',
     'data' => 'sukses'
   ], 201);
     
 }

 public function show($id)
 {
    $all_nilai = nilai::select('id','semester')->where('mahasiswa_id',$id)->with(['nilai_detail:id,nilai_id,H as index,A as bobot,K as SKS,M as total_index_sks,mata_kuliah_id'])->get();
    if(count($all_nilai) > 0){
        return response()->json($all_nilai);    
    }
    else
    {
        return abort(404);
    }
    
}

public function update(Request $request, nilai $nilai)
{

     nilai_detail::where('nilai_id',$nilai->id)->delete();
     $count = count($request->mata_kuliah_id);

     for($i = 0 ; $i < $count; $i++){
          $detail[] = [
              'nilai_id' => $nilai->id,
              'mata_kuliah_id' => $request->mata_kuliah_id[$i],
              'H' => $request->H[$i],
              'A' => $request->A[$i],
              'K' => $request->K[$i],
              'M' => $request->M[$i]
          ];
          $createdetail = nilai_detail::create($detail[$i]);
     }

      return response()->json([
        'success' => true,
        'code' => 204,
        'message' => 'Data berhasil disimpan',
        'data' => $data
      ], 204);
}

public function destroy($id)
{
    nilai::where('id',$id)->delete();
    return response()->json([
              'success' => true,
              'code' => 200,
              'message' => 'Data berhasil dihapus',
              'data' => $data
            ], 200);


}


}
