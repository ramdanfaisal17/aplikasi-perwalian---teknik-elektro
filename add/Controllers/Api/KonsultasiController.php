<?php

namespace Add\Controllers\Api;

use Add\Models\proyekpendidikan;
use Add\Models\programstudi;
use Add\Models\mahasiswa;
use Add\Models\dosen;
use Add\Models\konsultasi;
use Add\Models\perwalian;
use Add\Models\perwalian_detail;
use Illuminate\Http\Request;
use Add\Controllers\Controller;
use DataTables;
use DB;
use Input;


class KonsultasiController extends Controller
{

    public function index()
    {
        $konsultasis = konsultasi::with(['perwalian_detail.mahasiswa','perwalian_detail.perwalian.jadwal_perwalian'])->get();
        return response()->json([
           'success' => true,
           'code' => 200,
           'message' => 'Success',
           'data' => $konsultasis
       ], 200);
    }

    public function store(Request $request){

      $data = [
          'perwalian_detail_id' => $request->input('perwalian_detail_id'),
          'pembahasan' => $request->input('pembahasan'),
          'status_konsultasi' => date('Y-m-d H:i:s'),
          'ke' => $request->input('ke'),
      ];

      $konsultasi = konsultasi::create($data);

      if ($konsultasi){
          return response()->json([
             'success' => true,
             'code' => 200,
             'message' => 'pembahasan sudah di tambahkan',
             'data' => $konsultasi
         ], 200);
      }
      return response()->json([
         'success' => true,
         'code' => 400,
         'message' => 'pembahasan gagal di tambahkan',
         'data' => null
     ], 400);

 }

 public function update(Request $request, $id){

     $konsultasi = konsultasi::find($id);
     if($konsultasi){
         $update = $konsultasi->update($request->all());
         if ($update){
             return response()->json([
               'success' => true,
               'code' => 200,
               'message' => 'Pembahasan Konsultasi berhasil diubah',
               'data' => $update
             ], 200);
         }
         return response()->json([
           'success' => false,
           'code' => 500,
           'message' => 'Pembahasan Konsultasi gagal diupdate',
           'data' => $update
         ], 500);
     }
     return response()->json([
       'success' => false,
       'code' => 500,
       'message' => 'Konsultasi tidak ditemukan',
       'data' => null
     ], 500);

 }

 public function show($id){
     $konsultasi = konsultasi::find($id);
     if($konsultasi){
         return response()->json([
            'success' => true,
            'code' => 200,
            'message' => 'Data Konsultasi ditemukan',
            'data' => $konsultasi
        ], 200);
     }
     return response()->json([
        'success' => false,
        'code' => 404,
        'message' => 'Data Konsultasi tidak ditemukan',
        'data' => null,
    ], 404);
 }

}
