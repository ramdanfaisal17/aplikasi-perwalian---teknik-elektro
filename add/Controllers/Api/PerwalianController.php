<?php

namespace Add\Controllers\api;

use Add\Models\jadwalperwalian;
use Add\Models\perwalian;
use Illuminate\Http\Request;
use Add\Controllers\Controller;
use DB;
use Add\Models\user;

class PerwalianController extends Controller
{

    private function _updateJenisPerwalian(Request $request, $jadwalperwalian, $perwalian_detail)
    {
        if(date('Y-m-d H:i:s') >= $jadwalperwalian->start_ganjil && date('Y-m-d H:i:s') <= $jadwalperwalian->end_ganjil){
          //Kalo presensinya jaraknya antara start ganjil dan end ganjil, maka presensinya bakal pilih matakuliah
            if($perwalian_detail->status_pilih_matakuliah == null){
                return $perwalian_detail->update(['status_pilih_matakuliah' => date('Y-m-d H:i:s')]);
            }
            // // return false dan nampilin message ini untuk response
            // $request->session()->flash('message', 'Data gagal simpan, karena mahasiswa sudah pilih matakuliah');
            // return false;

        }else if(date('Y-m-d H:i:s') >= $jadwalperwalian->start_prs && date('Y-m-d H:i:s') <= $jadwalperwalian->end_prs){
            //Kalo presensinya jaraknya antara start prs dan end prs, maka presensinya bakal PRS
            if($perwalian_detail->status_prs == null){
                return $perwalian_detail->update(['status_prs' => date('Y-m-d H:i:s')]);
            }
            // return false dan nampilin message ini untuk response
            // $request->session()->flash('message', 'Data gagal simpan, karena mahasiswa sudah pilih matakuliah');
            // return false;

        }else if(date('Y-m-d H:i:s') >= $jadwalperwalian->start_konsultasi1 && date('Y-m-d H:i:s') <= $jadwalperwalian->end_konsultasi1){
            //Kalo presensinya jaraknya antara start konsultasi1 dan end konsultasi1, maka presensinya bakal Konsultasi1
            if($perwalian_detail->konsultasi[0]->status_konsultasi == null){
                return $perwalian_detail->konsultasi[0]->update(['status_konsultasi' => date('Y-m-d H:i:s')]);
            }
            // return false dan nampilin message ini untuk response
            // $request->session()->flash('message', 'Data gagal simpan, karena mahasiswa sudah pilih matakuliah');
            // return false;

        }else if(date('Y-m-d H:i:s') >= $jadwalperwalian->start_konsultasi2 && date('Y-m-d H:i:s') <= $jadwalperwalian->end_konsultasi2){
          //Kalo presensinya jaraknya antara start konsultasi1 dan end Konsultasi2, maka presensinya bakal Konsultasi2
            if($perwalian_detail->konsultasi[1]->status_konsultasi == null){
                $update = $perwalian_detail->konsultasi[0]->update(['status_konsultasi' => date('Y-m-d H:i:s')]);
                return $update;
            }
            // return false dan nampilin message ini untuk response
            // $request->session()->flash('message', 'Data gagal simpan, karena mahasiswa sudah pilih matakuliah');
            // return false;
        }
        //return false kalo data perwalian detail not found, atau waktunya tidak ada di jadwal perwalian
        return false;

    }

    public function index()
    {
        $perwalian = jadwalperwalian::join('proyek_pendidikans', 'proyek_pendidikans.id', '=', 'jadwal_perwalian.proyek_pendidikan_id')->first();
        return response()->json($perwalian);
    }

    //store mahasiswa presensi
    public function store(Request $request)
    {
        $perwalian = perwalian::where([['jadwal_perwalian_id', '=', $request->jadwal_perwalian_id], ['dosen_id', '=', $request->dosen_id]])->first();
        if($perwalian){
            //get mahasiswa yang perwalian sama dosennya
            $perwalian_detail = $perwalian->perwalian_detail->where('mahasiswa_id', $request->mahasiswa_id)->first();
            //Kalo mahasiswanya ada di perwalian dosennya
            if($perwalian_detail){
                //get jadwal perwalian sesuai dengan perwaliannya
                $jadwalperwalian = $perwalian->jadwal_perwalian;

                // Melakukan update waktu
                $data = $this->_updateJenisPerwalian($request, $jadwalperwalian, $perwalian_detail);

                //Kalo update berhasil, maka response true
                if($data){
                    return response()->json([
                              'success' => true,
                              'code' => 201,
                              'message' => 'Data berhasil disimpan',
                              'data' => $data
                            ], 201);

                }
                // response ini antara waktunya tidak masuk perwalian atau mahasiswanya udah perwalian
                return response()->json([
                         'success' => false,
                         'code' => 400,
                         'message' => 'Data gagal disimpan, karena tidak ada jadwal perwalian',
                         'data' => null
                       ], 400);
            }
            return response()->json([
                     'success' => false,
                     'code' => 400,
                     'message' => 'Data gagal disimpan, karena mahasiswa tidak ditemukan diperwalian dosen tersebut',
                     'data' => null
                   ], 400);
        }
        return response()->json([
                 'success' => false,
                 'code' => 400  ,
                 'message' => 'Data gagal disimpan, karena perwalian tidak ditemukan',
                 'data' => null
               ], 400);
    }

    public function show($id)
    {
        $perwalian = jadwalperwalian::where('id_jadwal_perwalian', '=', $id)->with(["perwalian", "perwalian.dosen","proyekpendidikan"])->first();
        // perwaliannya collection
        if($perwalian){
            return response()->json([
                     'success' => true,
                     'code' => 200,
                     'message' => 'Success',
                     'data' => $perwalian
                   ], 200);
        }
        return response()->json([
                 'success' => false,
                 'code' => 404,
                 'message' => 'Data Not Found',
                 'data' => null
               ], 404);
    }

    public function destroy($id)
    {
        return abort(404);
    }

    public function show_detail($id, $dosen_id)
    {
        $perwalian = perwalian::where([['jadwal_perwalian_id', '=', $id], ['dosen_id', '=', $dosen_id]])->with(['dosen', 'jadwal_perwalian',  'perwalian_detail', 'perwalian_detail.konsultasi', 'perwalian_detail.mahasiswa:id,nama'])->first();
        // perwalian nya object
        if($perwalian){
            return response()->json([
                     'success' => true,
                     'code' => 200,
                     'message' => 'Success',
                     'data' => $perwalian
                   ], 200);
        }
        return response()->json([
                 'success' => false,
                 'code' => 404,
                 'message' => 'Data Not Found',
                 'data' => null
               ], 404);
    }

    public function postKonsultasi(Request $request)
    {
        // dd($request->all());
        $perwalian = perwalian::where([['jadwal_perwalian_id', '=', $request->jadwal_perwalian_id], ['dosen_id', '=', $request->dosen_id]])->first();
        if($perwalian){
            $perwalian_detail = $perwalian->perwalian_detail->where('mahasiswa_id', $request->mahasiswa_id)->first();
            if($perwalian_detail){
                $jadwalperwalian = $perwalian->jadwal_perwalian;
                if(date('Y-m-d') >= $jadwalperwalian->start_konsultasi1 && date('Y-m-d') <= $jadwalperwalian->end_konsultasi1)
                {
                    $perwalian_detail->update(['konsultasi_1' => $request->konsultasi]);
                }
                else if(date('Y-m-d') >= $jadwalperwalian->start_konsultasi2 && date('Y-m-d') <= $jadwalperwalian->end_konsultasi2)
                {
                    $perwalian_detail->update(['konsultasi_1' => $request->konsultasi]);
                }
                else
                {
                    return response()->json([
                      'success' => false,
                      'code' => 400,
                      'message' => 'Bukan jadwal konsultasi',
                      'data' => null
                    ], 400);
                }
                return response()->json([
                  'success' => true,
                  'code' => 200,
                  'message' => 'Success',
                  'data' => $perwalian
                ], 200);
            }
        }

        return response()->json([
          'success' => false,
          'code' => 400,
          'message' => 'Dosen ID atau Mahasiswa ID atau Jadwal Perwalian ID Salah, jadi data tidak ditemukan',
          'data' => null
        ], 400);

    }

    public function absen(Request $request, $id)
    {
        $tanggal = date('Y-m-d');
                                          
            $absen = DB::table('absen')->where('idmhs', '=', $id)->where('tanggal', '=', $tanggal)->first();
            if($absen){
                return response()->json($absen);
            }else{
                return abort(404);
            }
        


    }


    public function createabsen(Request $request)
    {
        $idmhs      = $request->get('idmhs');
        $iddosen    = $request->get('iddosen');
        $tanggal    = date('Y-m-d');
        $cekjadwalperwalian = DB::select('SELECT * FROM `jadwal_perwalian` 
                                            WHERE start_prs BETWEEN \''.$tanggal.'\' AND \''.$tanggal.'\' 
                                            OR start_konsultasi1 BETWEEN \''.$tanggal.'\' AND \''.$tanggal.'\'
                                            OR start_konsultasi2 BETWEEN \''.$tanggal.'\' AND \''.$tanggal.'\' 
                                            OR end_prs BETWEEN \''.$tanggal.'\' AND \''.$tanggal.'\' 
                                            OR end_konsultasi1 BETWEEN \''.$tanggal.'\' AND \''.$tanggal.'\' 
                                            OR end_konsultasi2 BETWEEN \''.$tanggal.'\' AND \''.$tanggal.'\'');
        
        if($cekjadwalperwalian==''){
            return abort(404);
        }else{  
            $cekmhsada = DB::select('SELECT dosen_wali_details.* 
                                            FROM dosen_wali_details, dosen_walis 
                                            WHERE dosen_walis.id=dosen_wali_details.dosen_wali_id 
                                            AND dosen_walis.dosen_id =\''.$iddosen.'\' 
                                            AND dosen_wali_details.mahasiswa_id=\''.$idmhs.'\'');
            if($cekmhsada==''){
                    $header = [
                        'idmhs'     => $idmhs,
                        'iddosen'   => $iddosen,
                        'tanggal'   => date('Y-m-d H:i:s'),
                        'status'       => 'Oke',
                    ];
                $simpanabsen = DB::table('absen')->insert($header);
                if($simpanabsen){
                    return response()->json($simpanabsen);
                }
            }else{
                return abort(404);
            }
        }
    }

    public function createkonsul(Request $request)
    {
        $idmhs      = $request->get('mahasiswa_id');
        $iddsn      = $request->get('dosen_id');
        $konsultasi = $request->get('konsultasi');
        $jenis      = $request->get('jenis');
        $semester   = $request->get('semester');
        $cekdata    = DB::table('konsultasi')->where('mahasiswa_id', '=', $idmhs)->where('dosen_id', '=', $iddsn)->where('jenis', '=', $jenis)->first();

        if($cekdata==''){
            $header = [
                'mahasiswa_id' => $idmhs,
                'dosen_id'      => $iddsn,
                'konsultasi'    => $konsultasi,
                'jenis'         => $jenis,
                'tanggal'       => date('Y-m-d'),
                'semester'      => $semester
            ];
            $simpankonsul = DB::table('konsultasi')->insert($header);
                if($simpankonsul){
                    return response()->json($simpankonsul);
                }
        }else{
            $header = [
                'konsultasi'    => $konsultasi,
                'jenis'         => $jenis,
                'tanggal'       => date('Y-m-d')
            ];
            $updatekonsul = DB::table('konsultasi')->where('mahasiswa_id', '=', $idmhs)->where('dosen_id', '=', $iddsn)->update($header);
                if($updatekonsul){
                    return response()->json($updatekonsul);
                }
        }
    }

    public function getPerwalianNowDosen($dosen_id){
      $jadwalperwalian = jadwalperwalian::where([['start_ganjil', '<=', date('Y-m-d')], ['end_ganjil','>=', date('Y-m-d')]])->first();
      if($jadwalperwalian){
          $perwalian = Perwalian::where([['dosen_id', '=', $dosen_id], ['jadwal_perwalian_id', '=', $jadwalperwalian->id_jadwal_perwalian]])->first();
          return response()->json([
                   'success' => true,
                   'code' => 200,
                   'message' => 'Data perwalian di jadwal sekarang ditemukan',
                   'data' => $perwalian,
                 ], 200);
      }
      return response()->json([
               'success' => false,
               'code' => 404,
               'message' => 'Data Not Found',
               'data' => null
             ], 404);

    }

    public function detailabsen(Request $request, $id)
    {
        $tanggal = date('Y-m-d');
        $caridata = DB::table('absen')
                    ->join('dosens', 'dosens.nik', '=', 'absen.iddosen')
                    ->join('mahasiswas', 'mahasiswas.id', '=', 'absen.idmhs')
                    ->where('dosens.id', '=', $id)
                    //->where('absen.tanggal', '=', $tanggal)
                    ->select('mahasiswas.nama', 'mahasiswas.id')
                    ->groupBy('mahasiswas.nama', 'mahasiswas.id')
                    ->get();
        if($caridata)
        {
            return response()->json($caridata);
        }else{
            return abort(404);
        }
    }




}
