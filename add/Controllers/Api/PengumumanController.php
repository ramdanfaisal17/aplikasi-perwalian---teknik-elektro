<?php

namespace Add\Controllers\Api;

use Add\Models\pengumuman;
use Add\Models\PengumumanGambar;
use Illuminate\Http\Request;
use App\Http\Requests\PengumumanRequest;
use Add\Imports\DosenImport;
use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use DataTables;
use DB;
use Request as slug;
use Storage;

class PengumumanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return abort(404); //soalnya link http://url.com/berita mau dikosongkin
        $arr_berita = pengumuman:://join('pengumuman_gambar', 'pengumuman_gambar.pengumuman_id', '=', 'tabel_pengumuman.id')
            //with(['gambar'])
            //->select('tabel_pengumuman.id','tabel_pengumuman.judul', 'tabel_pengumuman.isi', 'pengumuman_gambar.gambar as url', 'tabel_pengumuman.created_at')
            where('id', '!=', '2')
            ->orderBy('created_at', 'desc')->get();
        // return response()->json([
        //          'success' => true,
        //          'code' => 200,
        //          'message' => 'Success',
        //          'data' => $arr_berita,
        //       ], 200);
        return response()->json($arr_berita);
    }

    public function detail(Request $request)
    {
        $arr_berita = pengumuman::join('pengumuman_gambar', 'pengumuman_gambar.pengumuman_id', '=', 'tabel_pengumuman.id')
            ->where('tabel_pengumuman.id', '=', $request->id)
            ->select('tabel_pengumuman.id','tabel_pengumuman.judul', 'tabel_pengumuman.isi', 'pengumuman_gambar.gambar as url', 'tabel_pengumuman.created_at')
            ->orderBy('created_at', 'desc')->get();

        return response()->json($arr_berita);
    }

    public function index_berita_terbaru()
    {
        $arr_berita = pengumuman::orderBy('created_at', 'desc')->take(5)->get();
        return response()->json([
                 'success' => true,
                 'code' => 200,
                 'message' => 'Success',
                 'data' => $arr_berita,
               ], 200);
    }

    public function index_pengumuman()
    {
        $pengumuman = pengumuman::where('jenis_pengumuman_id','=', 1)->orderby('created_at','desc')->get();
        //return view('masters.pengumuman.index',compact('pengumuman'));
        return response()->json([
                 'success' => true,
                 'code' => 200,
                 'message' => 'Success',
                 'data' => $pengumuman
               ], 200);

    }

    public function index_akademik()
    {
        $akademik = pengumuman::where('jenis_pengumuman_id','=', '2')->orderby('created_at','desc')->get();
        //return view('masters.pengumuman.index_akademik', compact('akademik'));
        return response()->json($akademik);

    }

    public function index_prestasi()
    {
        $prestasi = pengumuman::where('jenis_pengumuman_id','=', 3)->orderby('created_at','desc')->get();
        //return view('masters.pengumuman.index_prestasi', compact('prestasi'));
        return response()->json($prestasi);

    }

    private function uploadGambar($fileGambar, $judul, $diskName)
    {
        $gambar = $fileGambar;
        $judul = str_replace(' ', '-', $judul);
        $ext = $gambar->getClientOriginalExtension();
        if($fileGambar->isValid()){
            $filename = date('Ymd').".$judul.$ext";
            Storage::disk($diskName)->put($filename, file_get_contents($fileGambar));
            return $filename;
        }
        return false;
    }

    private function hapusGambar($gambar, $diskName)
    {
        $exist = Storage::disk($diskName)->exists($gambar);
        if(isset($gambar) && $exist){
            $delete = Storage::disk($diskName)->delete($gambar);
            return $delete; //Kalo delete gagal, bakal return false, kalo berhasil bakal return true
        }
    }

    // Storenya pasti prestasi, yang sama dengan jenis_pengumuman_id nya 3
    public function store(PengumumanRequest $request)
    {
    	$data = $request->all();

        //Upload thumbnail
        if($request->thumbnail){
        	if(array_key_exists(0, $request->thumbnail)){
                $data['thumbnail_1'] = $this->uploadGambar($request->thumbnail[0], $request->judul.'(1)', 'berita-thumbnail');
            }

            if(array_key_exists(1, $request->thumbnail)){
                $data['thumbnail_2'] = $this->uploadGambar($request->thumbnail[1], $request->judul.'(2)', 'berita-thumbnail');
            }

            if(array_key_exists(2, $request->thumbnail)){
                $data['thumbnail_3'] = $this->uploadGambar($request->thumbnail[2], $request->judul.'(3)', 'berita-thumbnail');

            }
        }

        $data['jenis_pengumuman_id'] = 3; // 3 adalah idjenisnya prestasi

        $pengumuman = pengumuman::create($data);

        if($request->gambar){
            foreach ($request->gambar as $key => $gambar) {
                $dataGambar['gambar'] = $this->uploadGambar($gambar, $request->judul.$key, 'berita-gambar');
                $dataGambar['pengumuman_id'] = $pengumuman->id;
                $berita_gambar = PengumumanGambar::create($dataGambar);
            }
        }

        if($pengumuman){
			         return response()->json([
			                 'success' => true,
			                 'code' => 201,
			                 'message' => 'Success',
			                 'data' => $pengumuman
			               ], 201);
        }
        return response()->json([
                 'success' => false,
                 'code' => 400,
                 'message' => 'Data gagal diinput',
                 'data' => null
               ], 400);

    }

    public function show($id)
    {
        $pengumuman = Pengumuman::find($id);
        if($pengumuman){
            return view('masters.pengumuman.show', compact('pengumuman'));
        }
        return abort(404);
    }


    // public function show_data(){
    //     return view('masters.pengumuman.data');
    // }


    public function edit($id)
    {
        $pengumuman = Pengumuman::find($id);
        if($pengumuman){
            return view('masters.pengumuman.edit', compact('pengumuman'));
        }
        return abort(404);
    }


    public function update(Request $request, $id)
    {

        $pengumuman = pengumuman::find($id);


        if($pengumuman){
            if($request->thumbnail){

            }

            if($request->gambar){

            }
            $update = $pengumuman->update($request->all());
            if($update){
                if($pengumuman->jenis_pengumuman_id == 1){
                    //return redirect()->route('berita.pengumuman.index')->with('alert-class', 'alert-success')->with('flash_message', 'Data Pengumuman berhasil diperbaharui !!');
                    return response()->json($pengumuman);

                }
                else if($pengumuman->jenis_pengumuman_id == 2){
                    return response()->json($pengumuman);
                    //return redirect()->route('berita.pengumuman.index')->with('alert-class', 'alert-success')->with('flash_message', 'Data Info Akademik berhasil diperbaharui !!');
                }
            }
        }
//        return abort(404);
        return response()->json('pengumuman tidak ditemukan');

    }
    
    public function simpanprestasi(Request $request)
    {
        $data = $request->all();
        
        $datasimpan = [
                'jenis_pengumuman_id' => '3',
                'judul' => $data['judul'],
                'isi' => $data['isi'],
                'thumbnail_1' => '',
                'thumbnail_2' => '',
                'thumbnail_3' => ''
            ];
        $respondata = DB::table('tabel_pengumuman')->insert($datasimpan);
        if($respondata){
            return response()->json($respondata);
        }else{
            return abort(404);
        }
    }
    
    public function uploadprestasi(Request $request)
    {
        $data = $request->all();
        $cekgambar = DB::table('tabel_pengumuman')->where('judul', 'like', $data['name'])->first();
        
        $dataupdate1 = [
                'thumbnail_1' => $data['uri']
            ];
        $dataupdate2 = [
                'thumbnail_2' => $data['uri']
            ];
        $dataupdate3 = [
                'thumbnail_3' => $data['uri']
            ];
            
        if($cekgambar!=''){
            if($cekgambar->thumbnail_1==''){
                $datasuccess = DB::table('tabel_pengumuman')->where('judul', 'like', $data['name'])->update($dataupdate1);
                return response()->json($datasuccess);
            }else if($cekgambar->thumbnail_2==''){
                $datasuccess = DB::table('tabel_pengumuman')->where('judul', 'like', $data['name'])->update($dataupdate2);
                return response()->json($datasuccess);
            }else if($cekgambar->thumbnail_3==''){
                $datasuccess = DB::table('tabel_pengumuman')->where('judul', 'like', $data['name'])->update($dataupdate3);
                return response()->json($datasuccess);
            }else{
               return abort(404); 
            }
        }else{
            return abort(404);
        }
    }

}
