<?php

namespace Add\Controllers\api;

use Add\Models\mahasiswakeluarga;
use Add\Models\mahasiswa;
use Illuminate\Http\Request;
use Add\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MahasiswaKeluargaController extends Controller
{
    public function creates($id)
    {
        $mahasiswas = mahasiswa::where('id',$id)->get();
        return response()->json($mahasiswas);
    }

    public function store(Request $request)
    {
        $mahasiswakeluarga = mahasiswakeluarga::create($request->all());
        $id = $request->mahasiswa_id;
        return response()->json($mahasiswakeluarga);
    }
    
    public function show(mahasiswakeluarga $mahasiswakeluarga)
    {
        $mahasiswas = mahasiswa::where('id',$mahasiswakeluarga->mahasiswa_id)->get();
        return response()->json($mahasiswakeluarga);
    }


    public function update(Request $request, mahasiswakeluarga $mahasiswakeluarga)
    {
        $mahasiswakeluarga->update($request->all());

        return response()->json($mahasiswakeluarga);        
    }

    public function destroy($id)
    {
        mahasiswakeluarga::where('id',$id)->delete();
//        return redirect('/mahasiswakeluarga')->with('status','berhasil menghapus data');
        return response()->json('delete success');

    }

    public function massDestroy(Request $request)
    {

        var_dump(request('ids'));
        mahasiswakeluarga::whereIn('id', request('ids'))->delete();
        return response(null, 204);


    }

}
