<?php

namespace Add\Controllers\api;

use Add\Models\proyekpendidikan;
use Illuminate\Http\Request;
use Add\Controllers\Controller;


class ProyekPendidikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proyekpendidikans = proyekpendidikan::orderby('created_at','desc')->get();
        return response()->json($proyekpendidikans);
    }

    public function store(Request $request)
    {
        $proyekpendidikan = proyekpendidikan::create($request->all());
        return response()->json($proyekpendidikan);
        
    }

    public function update(Request $request, proyekpendidikan $proyekpendidikan)
    {
        $proyekpendidikan->update($request->all());
        return response()->json($proyekpendidikan);

    }

    public function show(proyekpendidikan $proyekpendidikan)
    {
        $show = proyekpendidikan::where('id',$proyekpendidikan->id)->get();
       return response()->json($show);
   }

   public function destroy($id)
   {
    $proyekpendidikan = proyekpendidikan::where('id',$id)->delete();
    return response()->json('delete success');
}

public function massDestroy(Request $request)
{

}


}
