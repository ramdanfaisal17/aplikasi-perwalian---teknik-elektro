<?php

namespace Add\Controllers\api;

use Add\Models\mahasiswa;
use Add\Models\mahasiswakeluarga;
use Add\Models\alamat\provinsi;
use Add\Models\alamat\kota;
use Add\Models\alamat\kecamatan;
use Add\Models\alamat\desa;
use Illuminate\Http\Request;
use Add\Controllers\Controller;
use DB;
use Add\Models\user;


class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mahasiswas = mahasiswa::orderby('created_at','desc')->get();
        return response()->json($mahasiswas);
    }

    public function store(Request $request)
    {
        $mahasiswa = mahasiswa::create($request->all());
        return response()->json($mahasiswa);

    }

    public function update(Request $request, mahasiswa $mahasiswa)
    {
        $data = $request->all();
        $update = $mahasiswa->update($data);
        if($update){
          $data['name'] = $request->nama;
          $data['profile_update'] = 1;
          $user = user::where([['relasi_id', '=', $mahasiswa->id], ['relasi', '=', 'Mahasiswa']])->first();
          $user->update($data);
        }
        return response()->json($mahasiswa);

    }

    public function show(mahasiswa $mahasiswa)
    {
       $provinsi = provinsi::where('id',$mahasiswa->provinsi_id)->get();
       $kota = kota::where('id',$mahasiswa->kota_id)->get();
       $kecamatan = kecamatan::where('id',$mahasiswa->kecamatan_id)->get();
       $desa = desa::where('id',$mahasiswa->desa_id)->get();
       return response()->json($mahasiswa);
   }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function edit(mahasiswa $mahasiswa)
    {
        $provinsis = provinsi::all();
        $provinsi = provinsi::where('id',$mahasiswa->provinsi_id)->get();
        $kota = kota::where('id',$mahasiswa->kota_id)->get();
        $kecamatan = kecamatan::where('id',$mahasiswa->kecamatan_id)->get();
        $desa = desa::where('id',$mahasiswa->desa_id)->get();
        return view('masters.mahasiswa.edit', compact('mahasiswa','provinsis','provinsi','kota','kecamatan','desa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\mahasiswa  $mahasiswa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mahasiswa = mahasiswa::where('id',$id)->delete();
        return response()->json('delete success');
    }

    public function massDestroy(Request $request)
    {

    }

    public function getprovinsi(Request $request){

        $provinsis = provinsi::all()->orderBy('name', 'asc');
        return response()->json($provinsis);
    }

    public function getkota(Request $request){

        $kota = DB::table("indonesia_cities")->where("province_id",$request->province_id)->pluck("name","id")->orderBy('name', 'asc');
        return response()->json($kota);
    }

    public function getkecamatan(Request $request){

        $kecamatan = DB::table("indonesia_districts")->where("city_id",$request->city_id)->pluck("name","id")->orderBy('name', 'asc');
        return response()->json($kecamatan);
    }

    public function getdesa(Request $request){

        $desa = DB::table("indonesia_villages")->where("district_id",$request->district_id)->pluck("name","id")->orderBy('name', 'asc');
        return response()->json($desa);
    }

    public function indexkeluarga(Request $request)
    {
        $mahasiswakeluargas = mahasiswakeluarga::where('mahasiswa_id',$request->id)->orderby('created_at','desc')->get();
        $mahasiswa = mahasiswa::where('id',$request->id)->get();
        return response()->json($mahasiswakeluargas);
    }


}
