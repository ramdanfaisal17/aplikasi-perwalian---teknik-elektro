<?php

namespace Add\Controllers;

use Add\Models\user;
use Add\Models\dosen;
use Add\Models\mahasiswa;
use Add\Models\perwalian;
use Illuminate\Http\Request;
use Add\Imports\UserImport;
use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\UserRequest;
use auth;
use Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $users = user::orderby('created_at','desc')->get();
      return view('masters.user.index',compact('users'));
    }


    public function import(Request $request)
    {
        // validasi
      $this->validate($request, [
        'file' => 'required|mimes:csv,xls,xlsx'
      ]);

        // menangkap file excel
      $file = $request->file('file');

        // membuat nama file unik
      $nama_file = rand().$file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
      $file->move('file_user',$nama_file);

        // import data
      Excel::import(new UserImport, public_path('/file_user/'.$nama_file));

      return back()->with('alert-class', 'alert-success')
      ->with('flash_message', 'Data Excel berhasil diimport ke database !!');
    }

     public function login(){
        if(Auth::attempt(['nik' => request('nik'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] = bcrypt($user->email);
            $success['nik'] = $user->nik;
            $success['relasi'] = $user->relasi;
            $success['relasi_id'] = $user->relasi_id;
            $success['profile_update'] = $user->profile_update;
            $success['password_change'] = $user->password_change;
            return response()->json(['success' => $success], $this->successStatus);
        }
        else{
            return response()->json(['error'=>'failed'], 401);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'nik' => 'required',
            'email' => 'unique:users|required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  bcrypt($user->email);
        $success['name'] =  $user->name;

        return response()->json(['success'=>$success], $this->successStatus);
    }

    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }


    public function create()
    {
      return view('masters.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        //$user = user::create($request->all());
        // validator dipindahkan ke userrequest
      $relasi_id='';

      try {

        if ($request['relasi']=='Dosen'){
          $dosen = dosen::create([
            'nik' => $request['nik'],
            'nama' => $request['name'],
            'email' => $request['nik'].'@email.com',
          ]);
          $relasi_id = $dosen->id;
        }

        if ($request['relasi']=='Mahasiswa'){
          $mahasiswa = mahasiswa::create([
            'nrp' => $request['nik'],
            'nama' => $request['name'],
            'email' => $request['nik'].'@email.com',
            'program_studi_id' => 1,
          ]);
          $relasi_id = $mahasiswa->id;
        }

        $user = user::create([
          'name' => $request['name'],
          'email' => $request['nik'].'@email.com',
          'password' => Hash::make('maranatha'),
          'nik' => $request['nik'],
          'relasi' => $request['relasi'],
          'relasi_id' => $relasi_id,
        ]);

        return redirect()->route('user.index')
        ->with('alert-class', 'alert-success')
        ->with('flash_message', 'Data User berhasil ditambahkan !!');

      } catch (Exception $exc) {
        abort(404, $exc->getMessage());
      }
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function show(user $user)
    {
      return view('masters.user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(user $user)
    {
      return view('masters.user.gantipassword', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, user $user)
    {
        //$user->update($request->all());

      $validator = $request->validate([
       'name' => ['required', 'string', 'max:255'],
       'nik' => ['required', 'string', 'min:6', 'unique:users'],
       'relasi' => ['required','string'],
       'email' => ['required','string','unique:users'],
     ]);



      $update = $user->update([
        'name' => $request['name'],
        'email' => $request['email'],
        'nik' => $request['nik'],
        'relasi' => $request['relasi'],
      ]);


      return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $destroy = user::where('id',$id)->delete();
      if($destroy){
        return back()->with('alert-class', 'alert-success')->with('flash_message', 'Data akun berhasil dihapus !!');
      }
      return back()->with('alert-class', 'alert-danger')->with('flash_message', 'Data akun gagal dihapus !!');

    }

    public function deletes(Request $request)
    {
      $deletes = user::whereIn('id', request('ids'))->delete();
      if($deletes){
        Session::flash('alert-class', 'alert-success');
        Session::flash('flash_message', 'Beberapa data berhasil dihapus !!');
        return response(null, 204);
      }else{
        return response(null, 404);
      }

    }

    public function reset($id)
    {

      $reset = user::where('id',$id)->update(['password' =>  Hash::make('maranatha')]);
      if($reset){
        Session::flash('alert-class', 'alert-success');
        Session::flash('flash_message', 'Data user berhasil direset password !!');
        return response(null, 204);
      }else{
        return response(null, 404);
      }
    }

    public function gantipassword(Request $request){




        // if (!(Hash::check($request['current-password'], Auth::user()->password))) {
        //     //return redirect()->back()->with("error","passwrod yang di pakai login salah");
        //     return response()->json(['error'=>"passwrod yang di pakai login salah"], 401);
        // }

        // if(strcmp($request->get('current-password'), $request->get('password')) == 0){
        //     //return redirect()->back()->with("error","password nya sama, ngapain di ganti bos !");
        //     return response()->json(['error'=>"password baru dan lama sama !"], 401);
        // }
        // if(!(strcmp($request->get('password'), $request->get('password-confirmation'))) == 0){
        //     //return redirect()->back()->with("error","password baru tidak cocok !");
        //     return response()->json(['error'=>"password baru tidak cocok !"], 401);
        // }

        //if ($request['current-password'] != $request['current-password-confirmation']){
        //    return response()->json('password yang di pakai salah !');
        //}
        //else


        // dd($request->all());

     if($request['password'] != $request['password-confirmation']){
      return response()->json('konfirmasi password tidak cocok !');
    }
    else{

      $user = user::findOrFail($request['id']);
      $user->password = hash::make($request['password']);
      $user->password_change = '1';
      $user->save();
      $success['password_change'] = '1';
      $success['msg'] = 'password berhasil di ganti';

      return response()->json('berhasil ganti password');

    }


        // $user = Auth::user();
        // $user->password = hash::make($request['password']);
        // $user->save();

        // // return redirect()->back()->with("success","Password berhasil di ganti !");
        // return response()->json(['success' => "password berhasil di ganti"], $this->successStatus);



  }



}
