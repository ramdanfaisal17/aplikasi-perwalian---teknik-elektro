<?php

namespace Add\Controllers;

use Add\Models\pengumuman;
use Illuminate\Http\Request;
use App\Http\Requests\PengumumanRequest;
use Add\Imports\DosenImport;
use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use DataTables;
use DB;
use Request as slug;
use Storage;


class PengumumanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     private function uploadGambar($fileGambar, $judul, $diskName)
     {
         $gambar = $fileGambar;
         $judul = str_replace(' ', '-', $judul);
         $ext = $gambar->getClientOriginalExtension();
         if($fileGambar->isValid()){
             $filename = date('Ymd').".$judul.$ext";
             $dataFile = Storage::disk($diskName)->put($filename, file_get_contents($fileGambar));
             return $filename;
         }
         return false;
     }

    public function index()
    {
        return abort(404); //soalnya link http://url.com/berita mau dikosongkin
    }

    public function index_pengumuman()
    {
        $pengumuman = pengumuman::where('jenis_pengumuman_id','=', 1)->orderby('created_at','desc')->get();
        return view('masters.pengumuman.index',compact('pengumuman'));
    }

    public function index_akademik()
    {
        $akademik = pengumuman::where('jenis_pengumuman_id','=', 2)->orderby('created_at','desc')->get();
        return view('masters.pengumuman.index_akademik', compact('akademik'));
    }

    public function index_prestasi()
    {
        $prestasi = pengumuman::where('jenis_pengumuman_id','=', 3)->orderby('created_at','desc')->get();
        return view('masters.pengumuman.index_prestasi', compact('prestasi'));
    }

    public function create()
    {
        //buat ngebedain create dari pengumuman atau akademik
        if(slug::segment(2) == 'pengumuman'){
           $berita = 1; // pengumuman
        }else{
           $berita = 2; // akademik
        }
        $pengumuman = new pengumuman; // bikin objek kosong
        return view('masters.pengumuman.create', compact('berita', 'pengumuman'));
    }

    public function store(PengumumanRequest $request)
    {
        // dd($request->all());
        $data = $request->all();

        //Upload thumbnail
        if($request->thumbnail){
          if(array_key_exists(0, $request->thumbnail)){
                $data['thumbnail_1'] = $this->uploadGambar($request->thumbnail[0], $request->judul.'(1)', 'berita-thumbnail');
            }

            if(array_key_exists(1, $request->thumbnail)){
                $data['thumbnail_2'] = $this->uploadGambar($request->thumbnail[1], $request->judul.'(2)', 'berita-thumbnail');
            }

            if(array_key_exists(2, $request->thumbnail)){
                $data['thumbnail_3'] = $this->uploadGambar($request->thumbnail[2], $request->judul.'(3)', 'berita-thumbnail');

            }
        }


        $store = pengumuman::create($data);
        // dd($data);
        // Cek inputan kalo berhasil, dilempar ke halaman indexya dan dikasih flash message
        if($store){
            if($request->jenis_pengumuman_id == 1){
                return redirect()->route('berita.pengumuman.index')
                              ->with('alert-class', 'alert-success')
                              ->with('flash_message', 'Data berhasil dimasukkan kedalam database');
            }else{
                return redirect()->route('berita.akademik.index')
                              ->with('alert-class', 'alert-success')
                              ->with('flash_message', 'Data berhasil dimasukkan kedalam database');
            }
        }
        // return kalo gagal input model
        return redirect()->back()
                          ->with('alert-class', 'alert-danger')
                          ->with('flash_message', 'Data gagal dimasukkan kedalam database karena ada kesalahan inputan');

    }

    public function prestasi_approve(Request $request)
    {
        $pengumuman = pengumuman::where([['id', '=', $request->id], ['jenis_pengumuman_id', '=', 3], ['status', '=', null]])->first();
        if($pengumuman){
            $update = $pengumuman->update(['status' => 3]);
            if($update){
                return redirect()->route('pengumuman.prestasi.index')
                                ->with('alert-class', 'alert-success')
                                ->with('flash_message', 'Data prestasi berhasil disetujui');

            }
            return redirect()->route('pengumuman.prestasi.index')
                          ->with('alert-class', 'alert-danger')
                          ->with('flash_message', 'Data prestasi gagal disetujui');

        }
        return redirect('berita/prestasi')
                    ->with('alert-class', 'alert-danger')
                    ->with('flash_message', 'Data tidak ditemukan');

    }

    public function show($id)
    {
        $pengumuman = Pengumuman::find($id);
        if($pengumuman){
            return view('masters.pengumuman.show', compact('pengumuman'));
        }
        return abort(404);
    }


    // public function show_data(){
    //     return view('masters.pengumuman.data');
    // }


    public function edit($id)
    {
        $pengumuman = Pengumuman::find($id);
        if($pengumuman){
            return view('masters.pengumuman.edit', compact('pengumuman'));
        }
        return abort(404);
    }


    public function update(Request $request, $id)
    {
        $pengumuman = pengumuman::find($id);
        if($pengumuman){
            $update = $pengumuman->update($request->all());
            if($update){
                if($pengumuman->jenis_pengumuman_id == 1){
                    return redirect()->route('berita.pengumuman.index')->with('alert-class', 'alert-success')->with('flash_message', 'Data Pengumuman berhasil diperbaharui !!');
                }
                else if($pengumuman->jenis_pengumuman_id == 2){
                    return redirect()->route('berita.pengumuman.index')->with('alert-class', 'alert-success')->with('flash_message', 'Data Info Akademik berhasil diperbaharui !!');
                }
            }
        }
        return abort(404);
    }

    public function destroy($id)
    {
        pengumuman::where('id',$id)->delete();
//        return redirect('/dosen')->with('status','berhasil menghapus data');
        return back();

    }

    public function deletes(Request $request)
    {
        pengumuman::whereIn('id', request('ids'))->delete();
        return response(null, 204);
    }
    
    public function laporanabsen(Request $request)
    {
        DB::enableQueryLog();
        
        $dataabsen          = DB::table('absen')
                                ->join('mahasiswas', 'mahasiswas.id', '=', 'absen.idmhs')
                                ->join('dosens', 'dosens.id', '=', 'absen.iddosen')
                                ->join('tabel_perwalian_detail', 'tabel_perwalian_detail.mahasiswa_id', '=', 'mahasiswas.id')
                                ->join('tabel_perwalian', 'tabel_perwalian_detail.perwalian_id', '=', 'tabel_perwalian.id')
                                ->join('proyek_pendidikans', 'proyek_pendidikans.id', '=', 'tabel_perwalian.jadwal_perwalian_id')
                                ->select('absen.tanggal',
                                            'mahasiswas.nrp',
                                            'mahasiswas.nama as nama_mahasiswa',
                                            'mahasiswas.semester',
                                            'mahasiswas.angkatan',
                                            'dosens.nama as nama_dosen',
                                            'absen.status',
                                            'proyek_pendidikans.nama as nama_proyek',
                                            'tabel_perwalian_detail.status4'
                                            )
                                ->where('absen.status', '=', 'nonaktif')
                                ->groupBy('absen.tanggal',
                                            'mahasiswas.nrp',
                                            'mahasiswas.nama',
                                            'mahasiswas.semester',
                                            'mahasiswas.angkatan',
                                            'dosens.nama',
                                            'absen.status',
                                            'proyek_pendidikans.nama',
                                            'tabel_perwalian_detail.status4'
                                            )
                                ->get();
        
        
        // $query = DB::getQueryLog();
        // print_r($query);
        // die();
        return view('laporan/absensiperwalian/indexnew', compact('dataabsen'));
    }
}
