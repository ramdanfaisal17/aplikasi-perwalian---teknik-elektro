<?php

namespace Add\Controllers;

use Add\Models\jadwalperwalian;
use Add\Models\perwalian;


use Illuminate\Http\Request;

class PerwalianController extends Controller
{

    public function index()
    {
        $jadwalperwalian = jadwalperwalian::orderby('kode_perwalian','desc')->get();
        return view('transaksi.perwalian.index',compact('jadwalperwalian'));
    }

    // fungsi buat menutup perwalian, jadi nanti dosen
    public function perwalian_tutup(Request $request)
    {
        $perwalian_detail = perwalian_detail::findOrFail($id); // idnya dosen dapet dari orang yang nge qr
        $perwalian_detail->update(['status_pilih_matakuliah' => '',
                                    'status_pembahasan' => '',]);
    }

    public function show($id)
    {
        $jadwalperwalian = jadwalperwalian::findOrFail($id);
        return view('transaksi.perwalian.show', compact('jadwalperwalian'));
    }

    public function show_detail($id, $dosen_id)
    {
        $perwalian = perwalian::where([['jadwal_perwalian_id', '=', $id], ['dosen_id', '=', $dosen_id]])->firstOrFail();
        // dd($perwalian);
        return view('transaksi.perwalian.show_detail', compact('perwalian'));
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
