<?php

namespace Add\Controllers;

use Add\Models\dosen;
use Add\Models\dosen_detail;
use Add\Models\dosenwali;
use Add\Models\dosenwali_detail;
use Add\Models\mahasiswa;
use Illuminate\Http\Request;
use Add\Imports\DosenImport;
use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use App\Http\Requests\DosenRequest;
use DataTables;
use DB;
use Session;
use Validator;
use Illuminate\Support\Facades\Hash;


class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dosens = Dosen::orderby('nik','asc')->get();
        return view('masters.dosen.index',compact('dosens'));
    }


    public function import(Request $request)
    {
        // validasi
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        if ($validator->fails())
        {
            return back()->with('alert-class', 'alert-danger')->with('flash_message', 'Data Excel Gagal diimport karena tipe file bukan csv, xls, xlsx !!');
        }

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_dosen',$nama_file);

        // import data
        Excel::import(new DosenImport, public_path('/file_dosen/'.$nama_file));

        return back()->with('alert-class', 'alert-success')->with('flash_message', 'Data Excel berhasil diimport ke database !!');
    }


    public function create()
    {
        return view('masters.dosen.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DosenRequest $request)
    {
        $header = [
            'nik' => $request->input('nik'),
            'nama' => $request->input('nama'),
            'email' => $request->input('email'),
            'total_mahasiswa' => $request->input('total_mahasiswa'),
        ];

        $createheader=dosen::create($header);
        if (isset($request->iddetail)){
            $count = count($request->iddetail);

            $dosenwali = dosenwali::create(['dosen_id' => $createheader->id]);

            for($i = 0 ; $i < $count; $i++){
               DB::update('update mahasiswas set dosen_id = ? where id = ?',array($createheader->id,$request->iddetail[$i]));
               $dosenwali_detail = dosenwali_detail::create(['dosen_wali_id' => $dosenwali->id, 'mahasiswa_id' => $request->iddetail[$i]]);
            }

       }

       //========update tabel user
       $ambiliddosen = DB::table('dosens')->where('nik', '=', $request->input('nik'))->first();
       $datauser = [
            'relasi_id'         => $ambiliddosen->id,
            'nik'               => $request->input('nik'),
            'relasi'            => 'Dosen',
            'profile_update'    => '0',
            'password_change'   => '0',
            'name'              => $request->input('nama'),
            'email'             => $request->input('email'),
            'password'          => Hash::make('maranatha'),
           ];
        DB::table('users')->insert($datauser);

       return redirect()->route('dosen.index')->with('alert-class', 'alert-success')->with('flash_message', 'Data Dosen berhasil dimasukkan !!');
   }

    /**
     * Display the specified resource.
     *
     * @param  \App\dosen  $dosen
     * @return \Illuminate\Http\Response
     */
    public function show(dosen $dosen)
    {
        $dosendetails = mahasiswa::where('dosen_id',$dosen->id)->get();
        return view('masters.dosen.show', compact('dosen','dosendetails'));
    }

    public function show_data(){

        return view('masters.dosen.data');
    }

    public function dataTable_for_show_data(){

       $model = Dosen::all();
       return  DataTables::of($model)
       ->make(true);


   }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\dosen  $dosen
     * @return \Illuminate\Http\Response
     */
    public function edit(dosen $dosen)
    {
        $details = mahasiswa::where('dosen_id',$dosen->id)->orderBy('nama', 'asc')->get();
        return view('masters.dosen.edit', compact('dosen','details'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\dosen  $dosen
     * @return \Illuminate\Http\Response
     */
    public function update(DosenRequest $request, dosen $dosen)
    {
       //update header atau dosen disingkat jadi 1 line
       //dd($request->all());
       //dd($request->iddetail->rename('iddetail', 'mahasiswa_id'));
       $update = $dosen->update($request->all());
       $dosenwali = dosenwali::updateOrCreate(['dosen_id' => $dosen->id]);
       if($update){
           $null = 0;
           mahasiswa::where('dosen_id', $dosen->id)->update(['dosen_id' => $null]);
           dosenwali_detail::where('dosen_wali_id', $dosenwali->id)->delete();
           if(isset($request->iddetail)){
             for($i = 0; $i < count($request->iddetail); $i++){
                  dosenwali_detail::create(['dosen_wali_id' => $dosenwali->id, 'mahasiswa_id' => $request->iddetail[$i]]);
             }
           }
           // cek kalo ada mahasiswa nambah atau lebih dari 0
           if($request['mahasiswa_id'] && $request['mahasiswa_id'] > 0){
                $update_mahasiswa = mahasiswa::whereIn('id', $request->iddetail)->update(['dosen_id' => $dosen['id']]);
           }
           return redirect()->route('dosen.index')->with('alert-class', 'alert-success')->with('flash_message', 'Data Dosen berhasil diubah !!');
       }
       return redirect()->route('dosen.index')->with('alert-class', 'alert-danger')->with('flash_message', 'Data Dosen gagal diubah !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\dosen  $dosen
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Dosen::where('id',$id)->delete();
        if($destroy){
            return back()->with('alert-class', 'alert-success')->with('flash_message', 'Data dosen berhasil dihapus !!');
        }
        return back()->with('alert-class', 'alert-danger')->with('flash_message', 'Data dosen gagal dihapus !!');
    }

    public function deletes(Request $request)
    {
        dosen::whereIn('id', request('ids'))->delete();
        Session::flash('alert-class', 'alert-success');
        Session::flash('flash_message', 'Beberapa data berhasil dihapus !!');
        return response(null, 204);
    }
}
