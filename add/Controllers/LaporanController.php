<?php

namespace Add\Controllers;

use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LaporanController extends Controller
{

    public function pilihmatkul(Request $request)
    {
        $datamatkul     = DB::table('mahasiswa_matakuliah')
                            ->join('mahasiswas', 'mahasiswas.id', '=', 'mahasiswa_matakuliah.mahasiswa_id')
                            ->join('mata_kuliahs', 'mata_kuliahs.id', '=', 'mahasiswa_matakuliah.mata_kuliah_id')
                            ->join('tabel_perwalian_detail', 'tabel_perwalian_detail.mahasiswa_id', '=', 'mahasiswas.id')
                            ->join('tabel_perwalian', 'tabel_perwalian_detail.perwalian_id', '=', 'tabel_perwalian.id')
                            ->join('proyek_pendidikans', 'proyek_pendidikans.id', '=', 'tabel_perwalian.jadwal_perwalian_id')
                            
                            ->select('mahasiswas.id', 'mahasiswas.nama', 'mahasiswas.nrp', 'mahasiswas.semester', DB::raw('SUM(mata_kuliahs.sks) as sks'), 'proyek_pendidikans.nama as nama_proyek')
                            
                            ->where('mahasiswa_matakuliah.status', '=', 't')
                            ->where('tabel_perwalian_detail.status1', '=', 't')
                            ->groupBy('mahasiswas.id', 'mahasiswas.nama', 'mahasiswas.nrp', 'mahasiswas.semester', 'proyek_pendidikans.nama')
                            ->get();
        return view('laporan/hasilpilihmatakuliah/indexnew', compact('datamatkul'));
    }

    public function detailpilihmatkul(Request $request)
    {
        $idmhs = $request->id;
        $mhs            = DB::table('mahasiswas')->where('id', '=', $idmhs)->first();
        $datadetail     = DB::table('mahasiswa_matakuliah')
                                ->join('mata_kuliahs', 'mata_kuliahs.id', '=', 'mahasiswa_matakuliah.mata_kuliah_id')
                                ->where('mahasiswa_matakuliah.status', '=', 't')
                                ->get();
        return view('laporan/hasilpilihmatakuliah/detailnew', compact('mhs', 'datadetail'));
    }

    public function indexkonsul(Request $request)
    {
        $dataheader     = DB::table('tabel_perwalian_detail')
                            ->join('mahasiswas', 'mahasiswas.id', '=', 'tabel_perwalian_detail.mahasiswa_id')
                            ->join('tabel_perwalian', 'tabel_perwalian_detail.perwalian_id', '=', 'tabel_perwalian.id')
                            ->join('proyek_pendidikans', 'proyek_pendidikans.id', '=', 'tabel_perwalian.jadwal_perwalian_id')
                            ->select('mahasiswas.id','tabel_perwalian_detail.created_at', 'mahasiswas.nrp','mahasiswas.nama', 'proyek_pendidikans.nama as nama_proyek', 'tabel_perwalian_detail.status3', 'tabel_perwalian_detail.status4')
                            ->get();
        return view('laporan/konsultasi/indexnew', compact('dataheader'));
    }

    public function detailkonsul1(Request $request)
    {
        $idmhs = $request->id;
        $mhs            = DB::table('mahasiswas')->where('id', '=', $idmhs)->first();
        $datadetail     = DB::table('tabel_perwalian_detail')
                            ->join('mahasiswas', 'mahasiswas.id', '=', 'tabel_perwalian_detail.mahasiswa_id')
                            ->get();
        return view('laporan/konsultasi/detailkonsul1', compact('mhs', 'datadetail'));
    }

    public function detailkonsul2(Request $request)
    {
        $idmhs = $request->id;
        $mhs            = DB::table('mahasiswas')->where('id', '=', $idmhs)->first();
        $datadetail     = DB::table('tabel_perwalian_detail')
                            ->join('mahasiswas', 'mahasiswas.id', '=', 'tabel_perwalian_detail.mahasiswa_id')
                            ->get();
        return view('laporan/konsultasi/detailkonsul2', compact('mhs', 'datadetail'));
    }
    
    public function indexhasil(Request $request)
    {
        DB::enableQueryLog();
        $datadetail     = DB::table('mahasiswas')
                            ->join('program_studis', 'program_studis.id', '=', 'mahasiswas.program_studi_id')
                            ->join('mahasiswa_matakuliah', 'mahasiswa_matakuliah.mahasiswa_id', '=', 'mahasiswas.id')
                            ->join('mata_kuliahs', 'mata_kuliahs.id', '=', 'mahasiswa_matakuliah.mata_kuliah_id')
                            ->join('nilais', 'nilais.mahasiswa_id', '=', 'mahasiswas.id')
                            ->join('nilai_details', 'nilais.id', '=', 'nilai_details.nilai_id')
                            ->join('tabel_perwalian_detail', 'tabel_perwalian_detail.mahasiswa_id', '=', 'mahasiswas.id')
                            ->join('tabel_perwalian', 'tabel_perwalian_detail.perwalian_id', '=', 'tabel_perwalian.id')
                            ->join('proyek_pendidikans', 'proyek_pendidikans.id', '=', 'tabel_perwalian.jadwal_perwalian_id')
                            ->select('tabel_perwalian_detail.created_at',
                                        'mahasiswas.nama',
                                        'mahasiswas.nrp',
                                        'program_studis.nama as namaprog',
                                        DB::raw('SUM(mata_kuliahs.sks) as sks'),
                                        'mahasiswas.semester',
                                        DB::raw('SUM(nilai_details.K) as nilai'),
                                        'proyek_pendidikans.nama as nama_proyek'
                                    )
                            ->where('tabel_perwalian_detail.status4', '=', 't')
                            ->groupBy('proyek_pendidikans.nama', 'tabel_perwalian_detail.created_at', 'mahasiswas.nama', 'mahasiswas.nrp', 'program_studis.nama', 'mahasiswas.semester')
                            ->get();
        // $query = DB::getQueryLog();
        // print_r($query);
        // die();
        return view('laporan/hasilperwalian/indexnew', compact('datadetail'));
    }

}
