<?php

namespace Add\Controllers;

use Illuminate\Http\Request;
use Add\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;


class UserController extends Controller
{

    public $successStatus = 200;

    public function login(){
        if(Auth::attempt(['nik' => request('nik'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] = bcrypt($user->email);
            $success['user_id'] = $user->id;
            $success['nik'] = $user->nik;
            $success['relasi'] = $user->relasi;
            $success['relasi_id'] = $user->relasi_id;
            $success['profile_update'] = $user->profile_update;
            $success['password_change'] = $user->password_change;
            return response()->json(['success' => $success], $this->successStatus);
        }
        else{
            return response()->json(['error'=>'failed'], 401);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'nik' => 'required',
            'email' => 'unique:users|required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  bcrypt($user->email);
        $success['name'] =  $user->name;

        return response()->json(['success'=>$success], $this->successStatus);
    }

    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }
}