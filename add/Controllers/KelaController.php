<?php

namespace Add\Controllers;

use Add\Models\kela;
use Illuminate\Http\Request;
use Add\Imports\KelasImport;
use Maatwebsite\Excel\Facades\Excel;
use Add\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class KelaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kelas = Kela::orderby('kode','desc')->get();
        return view('masters.kelas.index',compact('kelas'));
    }

    public function import(Request $request) 
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        // menangkap file excel
        $file = $request->file('file');

        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('file_kelas',$nama_file);

        // import data
        Excel::import(new KelasImport, public_path('/file_kelas/'.$nama_file));

        // notifikasi dengan session
        // Session::flash('sukses','Data Dosen Berhasil Diimport!');

        // alihkan halaman kembali
        //return redirect('/dosen');
        return back();
    }

    public function create()
    {
        return view('masters.kelas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kela = kela::create($request->all());
        return redirect()->route('kelas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function show(kela $kela)
    {
        return view('masters.kelas.show', compact('kela'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function edit(kela $kela)
    {
        return view('masters.kelas.edit', compact('kela'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, kela $kela)
    {
        $kela->update($request->all());

        return redirect()->route('kelas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        kela::where('id',$id)->delete();
//        return redirect('/kelas')->with('status','berhasil menghapus data');
        return back();

    }

    public function deletes(Request $request)
    {
        kela::whereIn('id', request('ids'))->delete();
        return response(null, 204);
    }

}
