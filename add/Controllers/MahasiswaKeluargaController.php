<?php

namespace Add\Controllers;

use Add\Models\mahasiswakeluarga;
use Add\Models\mahasiswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaKeluargaController extends Controller
{
    public function creates($id)
    {
        $mahasiswas = mahasiswa::where('id',$id)->get();
        return view('masters.mahasiswakeluarga.create',compact('id','mahasiswas'));
    }

    public function store(Request $request)
    {
        $mahasiswakeluarga = mahasiswakeluarga::create($request->all());
        $id = $request->mahasiswa_id;
        return redirect()->action('MahasiswaController@indexkeluarga', ['id' => $mahasiswakeluarga->mahasiswa_id]);
    }
    
    public function show(mahasiswakeluarga $mahasiswakeluarga)
    {
        $mahasiswas = mahasiswa::where('id',$mahasiswakeluarga->mahasiswa_id)->get();
        return view('masters.mahasiswakeluarga.show', compact('mahasiswakeluarga','mahasiswas'));
    }

    public function edit(mahasiswakeluarga $mahasiswakeluarga)
    {
        $mahasiswas = mahasiswa::where('id',$mahasiswakeluarga->mahasiswa_id)->get();
        return view('masters.mahasiswakeluarga.edit', compact('mahasiswakeluarga','mahasiswas'));
    }

    public function update(Request $request, mahasiswakeluarga $mahasiswakeluarga)
    {
        $mahasiswakeluarga->update($request->all());

        return redirect()->action('MahasiswaController@indexkeluarga', ['id' => $mahasiswakeluarga->mahasiswa_id]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\mahasiswakeluarga  $mahasiswakeluarga
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        mahasiswakeluarga::where('id',$id)->delete();
//        return redirect('/mahasiswakeluarga')->with('status','berhasil menghapus data');
        return back();

    }

    public function massDestroy(Request $request)
    {

        var_dump(request('ids'));
        mahasiswakeluarga::whereIn('id', request('ids'))->delete();
        return response(null, 204);


    }
    public function deletes(Request $request)
    {
        mahasiswakeluarga::whereIn('id', request('ids'))->delete();
        return response(null, 204);
    }
}
