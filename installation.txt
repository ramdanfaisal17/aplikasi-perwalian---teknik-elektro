download
https://github.com/LaravelDaily/Laravel-CoreUI-AdminPanel.git

set env

composer install
php artisan key:generate

set file migration delete index()
composer require laravolt/indonesia
composer require maatwebsite/excel

php artisan migrate:refresh --seed
php artisan laravolt:indonesia:seed

admin@admin.com
password