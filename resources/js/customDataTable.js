//tinggal declare dataTablesColumns dan Url

//checking if table need date range filter or not
var setDom = '<"dt-daterange">lfrt<"toolbar"Bp>i';
if(typeof dtDom != "undefined"){
    setDom = dtDom;
} else if($.inArray("action", dataTablesColumns)){
    setDom = 'lfrtip';
}

//checking if table need checbox or not
if(typeof dtCheckbox == "undefined" || dtCheckbox == true) {
  var columnNumber = 1;
  var columnDef = [
    { targets: 0,
      width: 20,
      data: null,
      defaultContent: '',
      orderable: false,
      className: 'select-checkbox not_print',
      searchable: false,
    },
    { targets: 1,
      data: null,
      className: 'not_print',
      searchable: false ,
      orderable: false
    },
  ];
  console.log(true);
} else {
  var columnNumber = 0;
  var columnDef = [
    { targets: 0,
      data: null,
      className: 'not_print',
      searchable: false ,
      orderable: false
    },
  ];
}
var table =  $("#datatable").DataTable({
  dom: setDom,
  select: true,
  columnDefs: columnDef,
  order: [0],
  select: {
      style:    'multi',
      selector: 'td:first-child'
  },
  buttons: [ {  dom: { button: { tag: 'button', className: 'btn-default' } },
                extend: 'collection',
                className: 'btn btn-default',
                text: '<i class="fa fa-document"></i> Export',
                buttons: [  { extend: 'excel',
                              exportOptions: {
                                columns: [':visible','th:not(".not_print")']
                              }
                            },
                            { extend: 'pdf',
                              exportOptions: {
                                columns: 'th:not(".not_print")'
                              }
                            }
  ] }
  ],
  responsive: true,
  processing: true,
  serverSide: true,
  ajax: {
    url: url,
    dataSrc: "data",
    method: "GET",
    data: function(d) {
        d.selector = "datecreated";
        d.min_date = $("input[name='min_date']").val();
        d.max_date = $("input[name='max_date']").val();
    }
  },
  columns: dataTablesColumns
});
function formatDate(date) {
  var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
}

//add toolbar multiple delete
//need declare var permissionDelete
$('div.toolbar').append(' <button class="btn btn-danger btn-multiple-delete"> Delete <small class="btn-select-item"></small> </button>');

//add toolbar date range
var date = new Date();
var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
var firstDay = formatDate(firstDay);
var lastDay = formatDate(lastDay);
$('div.dt-daterange').append(   '<div class=input-group>'
                                + ' <select type="text" name="selector" class="form-control date_selector form-control-sm" placeholder aria-controls="datatable"> </select>'
                                + ' <input type="text" name="min_date" class="form-control form-control-sm datepicker" placeholder aria-controls="datatable" value="'+firstDay+'"> '
                                + ' <div class=input-group-prepend> <span class=input-group-text>-</span></div>'
                                + ' <input type="text" name="max_date" class="form-control form-control-sm datepicker" placeholder aria-controls="datatable" value="'+lastDay+'"> '
                                + ' </div>  ');
$('div.dt-daterange').addClass('dataTables_filter');

//removing default data table stylesheets
$('div.dt-buttons').removeClass('dt-buttons');

//give option for date filter 
//need to declare var date_selectors = [] 
if(typeof date_selectors !== 'undefined' ){
  $.each(date_selectors, function(key, value){
      $(".date_selector").append("<option value='"+key+"'><i class='fa fa-cog'></i>"+value+"</option>");
  });
}

//event for selected check All
$('#checkBox').on('click', function() {
  if ($('#checkBox').is(':checked')) {
    table.rows().select();
  }
  else {
    table.rows().deselect();
  }
});

//event for selected checbox
table.on('select deselect', function(){
    $('.btn-select-item').html($('.select-item').html() ? $('.select-item').html() : '' );
})

//give row number

table.on('draw', function () {
  var PageInfo = $('#datatable').DataTable().page.info();
      table.column(columnNumber, { page: 'current' }).nodes().each( function (cell, i) {
        cell.innerHTML = i + 1 + PageInfo.start;
    } );
} );

//event for btn multiple delete
$('.btn-multiple-delete').on('click', function(){
    var selectedItems = table.rows('.selected').data();
    var count = selectedItems.length;
    var idSelected = [];
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var deleteUrl = $('.btn-delete').attr('href');
    $.each($(selectedItems),function(key, value){
        var key = Object.keys(value)[0];
        idSelected.push(value[key]);
    });
    if(count > 0){
        swal({
            type: 'warning',
            title: 'Apakah anda yakin?',
            text: 'Anda akan menghapus '+count+' Barang',
            showCancelButton:true,
            cancelButtonText: "Batal",
            confirmButtonText: "Ya, Hapus Semua!"
        }).then((result)=>{
            if(result.value)
            {
                $.ajax({
                    url:deleteUrl+"/multiple",
                    data:{'items':idSelected,'_method':'DELETE','_token':csrf_token},
                    type:"POST",
                    success:function(responses){
                        $('#datatable').DataTable().ajax.reload();
                        swal({
                          type: 'success',
                          title: 'Success!',
                          text: count+' Data telah dihapus!'
                        });
                        $('.btn-select-item').html("");
                    }
                })
            } else {
                console.log('Batal Menghapus');
            }
        });
    } else {
        swal({
            type: 'warning',
            title: 'Tidak ada yang dipilih'
        })
    }
});

//event for filtering table with date range
$(".datepicker").on('change', function(){
    let min_date = $("input[name='min_date']").val();
    let max_date = $("input[name='max_date']").val();
    table.draw();
});