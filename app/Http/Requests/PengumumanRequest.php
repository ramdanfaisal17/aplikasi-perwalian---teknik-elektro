<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class PengumumanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'POST'){
           $judul = 'required|string|min:5|max:100|unique:tabel_pengumuman';
           $arr_thumbnail = 'required|array|min:1|max:3';
           $thumbnail_value = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048';
           $gambar_value = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048';
           if(url()->current() == route('pengumuman.api.store')){
             $arr_gambar = 'required|array|min:1|max:5';
             $jenis_pengumuman_id = 'sometimes';
             $tanggal = 'sometimes';
           }else{
             $arr_gambar = 'sometimes';
             $jenis_pengumuman_id = 'required';
             $tanggal = 'required|date|date_format:Y-m-d|before:tommorow';

           }
       }else{
           $judul = 'required|string|min:5|max:100|unique:blog,judul,'.$this->get('id');
           $tanggal = 'required|date';
           $arr_thumbnail = 'sometimes|array|min:1|max:3';
           $thumbnail_value = 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:2048';
           $arr_gambar = 'sometimes|array|min:1|max:5';
           $gambar_value = 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:2048';
           $jenis_pengumuman_id = 'required';
       }
       return [
           'judul' => $judul,
           'tanggal' => $tanggal,
           'thumbnail' => $arr_thumbnail,
           'thumbnail.*' => $thumbnail_value,
           'gambar' => $arr_gambar,
           'gambar.*' => $gambar_value,
           'jenis_pengumuman_id' => $jenis_pengumuman_id,
           'isi' => 'required|string|min:5',
       ];
      }

      public function messages()
      {
          return [
              'judul.required' => 'Judul harus diisi',
              'judul.min' => 'Judul minimal harus 5 karakter',
              'judul.max' => 'Judul maksimal 100 karakter',
              'judul.unique' => 'Judul sudah digunakan',
              'isi.required' => 'Isi tidak boleh kosong',
              'isi.min' => 'Isi minimal harus 5 karakter',
              'thumbnail.max' => 'Thumbnail maksimal hanya 3',
              'thumbnail.min' => 'Thumbnail harus diisi minimal 1',
              'gambar.max' => 'Gambar maksimal hanya 5',
              'gambar.min' => 'Gambar minimal 1',
              'tanggal.required' => 'Tanggal tidak boleh kosong',
          ];
      }

}
