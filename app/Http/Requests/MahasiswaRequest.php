<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MahasiswaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
     public function authorize()
     {
         return true;
     }

     /**
      * Get the validation rules that apply to the request.
      *
      * @return array
      */
      public function rules()
      {
          if($this->method() == 'POST'){
              $nrp = 'required|string|min:4|max:100|unique:mahasiswas';
              $email = 'required|string|email|min:6|max:100|unique:mahasiswas';
          }else{
             $nrp = 'required|string|min:4|unique:mahasiswas,nrp,'.$this->get('id');
             $email = 'required|string|email|min:6|unique:mahasiswas,email,'.$this->get('id');
          }
          return [
            'nrp' => $nrp,
            'nama' => 'required|string|min:3|max:100|',
            'tanggal_lahir' => 'required|date|before:today',
            'tempat_lahir' => 'required|string',
            'jenis_kelamin' => 'required|in:laki-laki, perempuan',
            'agama' => 'required|string',
            'email' => $email,
            'no_hp' => 'required|numeric',
            'alamat' => 'sometimes',
            'provinsi_id' => 'sometimes',
            'kota_id' => 'sometimes',
            'kecamatan_id' => 'sometimes',
            'desa_id' => 'sometimes',
            'nrp_pos' => 'sometimes',
            'warga_negara' => 'sometimes',
            'golongan_darah' => 'sometimes',
            'rhesus' => 'sometimes',
            'no_ijazah' => 'sometimes',
            'tanggal_ijazah' => 'sometimes',
            'nem' => 'sometimes',
            'nilai_toefel' => 'sometimes',
            'jalur_masuk' => 'sometimes',
            'status_mahasiswa' => 'sometimes',
            'angkatan' => 'sometimes|numeric',
            'semester' => 'required|numeric',
            'dosen_id' => 'required',
            'program_studi_id' => 'required',

          ];
      }

      public function messages()
      {
          return [
              'nrp.required' => 'NRP tidak boleh kosong',
              'nrp.min' => 'NRP minimal harus 4 karakter',
              'nrp.max' => 'NRP maksimal 100 karakter',
              'nrp.unique' => 'NRP sudah digunakan',
              'nama.unique' => 'Nama mata kuliah sudah digunakan',
              'nama.required' => 'Nama tidak boleh kosong',
              'nama.string' => 'Nama harus string',
              'nama.min' => 'Nama minimal harus 6 karakter',
              'sks.required' => 'SKS tidak boleh kosong',
              'sks.numeric' => 'SKS harus diisi dengan angka',
              'prasyarat.required' => 'Prasyarat tidak boleh kosong',
              'semester.required' => 'Semester tidak boleh kosong',
              'semester.numeric' => 'Semester harus diisi dengan angka',
              'program_studi_id.required' => 'Program Studi tidak boleh kosong',
              'dosen_id.required' => 'Dosen tidak boleh kosong',
              'no_hp.required' => 'No HP tidak boleh kosong',
              'no_hp.numeric' => 'No HP harus menggunakan angka',
              'email.required' => 'Email tidak boleh kosong',
              'email.unique' => 'Email sudan digunakan',
              'email.email' => 'Email harus menggunakan format yang benar',
              'agama.required' => 'Agama tidak boleh kosong',
              'jenis_kelamin.required' => 'Jenis Kelamin tidak boleh kosong',
              'jenis_kelamin.in' => 'Jenis Kelamin harus diantara laki-laki atau perempuan',
              'tempat_lahir.required' => 'Tempat Lahir tidak boleh kosong',
              'tanggal_lahir.required' => 'Tanggal Lahir tidak boleh kosong',
              'tanggal_lahir.date' => 'Tanggal Lahir harus dengan format yang benar',
              'tanggal_lahir.before' => 'Tanggal Lahir tidak bisa melebihi hari ini',
          ];
      }
}
