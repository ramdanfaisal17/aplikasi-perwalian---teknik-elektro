<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DosenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
         if($this->method() == 'POST'){
            $nik = 'required|string|min:6|max:100|unique:dosens';
            $email = 'required|email|min:6|max:100|unique:dosens';
         }else{
            $nik = 'required|string|min:6|unique:dosens,nik,'.$this->get('id');
            $email = 'required|string|email|unique:dosens,email,'.$this->get('id');
         }
         return [
           'nama' => ['required', 'min:3', 'string', 'max:255'],
           'nik' => $nik,
           'email' => $email,
         ];
     }

     public function messages()
     {
         return [
             'nik.required' => 'NIK harus diisi',
             'nik.min' => 'NIK minimal harus 6 karakter',
             'nik.max' => 'NIK maksimal 100 karakter',
             'nik.unique' => 'NIK sudah digunakan',
             'email.unique' => 'Email sudah digunakan',
             'nama.required' => 'Nama tidak boleh kosong',
             'nama.string' => 'Nama harus string',
             'nama.min' => 'Nama minimal harus 3 karakter',
             'relasi.required' => 'Level user tidak boleh kosong',
         ];
     }
}
