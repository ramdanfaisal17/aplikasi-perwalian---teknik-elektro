<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NilaiBerkasRequest extends FormRequest
{


 public function authorize()
 {
     return true;
 }

 /**
  * Get the validation rules that apply to the request.
  *
  * @return array
  */
 public function rules()
 {
     if($this->method() == 'POST'){
        $mahasiswa_id = 'required';
        $pdfFile = "required|mimes:pdf|max:10000";
        $keterangan = 'required|min:2|max:255';
    }else{
        $mahasiswa_id = 'required|string';
        $pdfFile = "required|mimes:pdf|max:10000";
        $keterangan = 'required|min:2|max:255';
    }
    return [
        'mahasiswa_id' => $mahasiswa_id,
        'pdfFile' => $pdfFile,
        'keterangan' => $keterangan
    ];
   }

   public function messages()
   {
       return [
           'mahasiswa_id.required' => 'Mahasiswa harus diisi',
           'pdfFile.required' => 'PDF harus dipilih untuk diupload',
           'pdfFile.mimes' => 'Format yang diupload harus PDF',
           'pdfFile.max' => 'Maksimal ukuran file PDF 1 MB',
           'keterangan.required' => 'Keterangan harus diisi',
           'keterangan.min' => 'Keterangan minimal 2 karakter',
           'keterangan.max' => 'Keterangan maksimal 255 karakter',
       ];
   }
}
