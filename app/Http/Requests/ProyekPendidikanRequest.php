<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProyekPendidikanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
     public function authorize()
     {
         return true;
     }

     /**
      * Get the validation rules that apply to the request.
      *
      * @return array
      */
      public function rules()
      {
          if($this->method() == 'POST'){
             $kode = 'required|string|min:4|max:100|unique:proyek_pendidikans';
             $nama = 'required|string|min:6|max:100|unique:proyek_pendidikans';
          }else{
             $kode = 'required|string|min:4|unique:proyek_pendidikans,kode,'.$this->get('id');
             $nama = 'required|string|min:6|unique:proyek_pendidikans,nama,'.$this->get('id');
          }
          return [
            'kode' => $kode,
            'nama' => $nama,
            'jenis_semester' => 'required',
            'mulai' => 'required|date',
            'berakhir' => 'required|date|after_or_equal:mulai',
          ];
      }

      public function messages()
      {
          return [
              'kode.required' => 'Kode tidak boleh kosong',
              'kode.min' => 'Kode minimal harus 4 karakter',
              'kode.max' => 'Kode maksimal 100 karakter',
              'kode.unique' => 'kode sudah digunakan',
              'nama.unique' => 'Nama sudah digunakan',
              'nama.required' => 'Nama tidak boleh kosong',
              'nama.string' => 'Nama harus string',
              'nama.min' => 'Nama minimal harus 6 karakter',
              'jenis_semester.required' => 'Jenis Semester harus dipilih',
              'mulai.required' => 'Tanggal mulai harus diisi',
              'berakhir.required' => 'Tanggal berakhir harus diisi',
              'berakhir.after_or_equal' => 'Tanggal berakhir tidak boleh kurang dari tanggal mulai',
          ];
      }
}
