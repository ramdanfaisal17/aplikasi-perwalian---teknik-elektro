<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProgramStudiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
     public function authorize()
     {
         return true;
     }

     /**
      * Get the validation rules that apply to the request.
      *
      * @return array
      */
      public function rules()
      {
          if($this->method() == 'POST'){
             $kode = 'required|string|min:4|max:100|unique:program_studis';
             $nama = 'required|string|min:6|max:100|unique:program_studis';
          }else{
             $kode = 'required|string|min:4|unique:program_studis,kode,'.$this->get('id');
             $nama = 'required|string|min:6|unique:program_studis,nama,'.$this->get('id');
          }
          return [
            'kode' => $kode,
            'nama' => $nama,
          ];
      }

      public function messages()
      {
          return [
              'kode.required' => 'Kode tidak boleh kosong',
              'kode.min' => 'Kode minimal harus 4 karakter',
              'kode.max' => 'Kode maksimal 100 karakter',
              'kode.unique' => 'kode sudah digunakan',
              'nama.unique' => 'Nama program studi sudah digunakan',
              'nama.required' => 'Nama tidak boleh kosong',
              'nama.string' => 'Nama harus string',
              'nama.min' => 'Nama minimal harus 6 karakter',
          ];
      }
}
