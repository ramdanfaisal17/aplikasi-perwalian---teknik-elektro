<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'POST'){
           $nik = 'required|string|min:6|max:100|unique:users';
           $email = 'sometimes|email';
        }else{
           $nik = 'required|string|min:6|unique:users,nik,'.$this->get('id');
           $email = 'required|string|email|unique:users,email,'.$this->get('id');
        }
        return [
          'name' => ['required', 'min:3', 'string', 'max:255'],
          'nik' => $nik,
          'email' => $email,
          'relasi' => ['required','string'],
        ];
    }

    public function messages()
    {
        return [
            'nik.required' => 'NIK harus diisi',
            'nik.min' => 'NIK minimal harus 6 karakter',
            'nik.max' => 'NIK maksimal 100 karakter',
            'nik.unique' => 'NIK sudah digunakan',
            'name.required' => 'Nama tidak boleh kosong',
            'name.string' => 'Nama harus string',
            'name.min' => 'Nama minimal harus 3 karakter',
            'relasi.required' => 'Level user tidak boleh kosong',
        ];
    }
}
