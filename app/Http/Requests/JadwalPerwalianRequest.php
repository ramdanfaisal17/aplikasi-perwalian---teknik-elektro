<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Add\Models\proyekpendidikan;

class JadwalPerwalianRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
     public function authorize()
     {
         return true;
     }

     /**
      * Get the validation rules that apply to the request.
      * @return array
      */
      public function rules()
      {
          if($this->method() == 'POST'){
             $kode = 'required|string|min:4|max:100|unique:jadwal_perwalian';
             //Ifnya buat nentuin, kalo proyek pendidikannya ganjil, berarti yang wajib diisi ganjil doang, ini masih rada ribet rules nya hampura !!
          }else{
             $kode = 'required|string|min:4|max:100|unique:jadwal_perwalian,kode_perwalian,'.$this->get('id').',id_jadwal_perwalian';
          }

          $proyekpendidikan = proyekpendidikan::find($this->get('proyek_pendidikan_id'));
          if($proyekpendidikan->jenis_semester == 'genap'){
             $start_ganjil = 'nullable|date';
             $end_ganjil = 'nullable|date|after_or_equal:start_ganjil';
             $start_genap = 'required|date';
             $end_genap = 'required|date|after_or_equal:start_genap';
             $start_antara = 'nullable|date';
             $end_antara = 'nullable|date|after_or_equal:start_ganjil';
          }else if($proyekpendidikan->jenis_semester == 'ganjil'){
             $start_genap = 'nullable|date';
             $end_genap = 'nullable|date|after_or_equal:start_genap';
             $start_ganjil = 'required|date';
             $end_ganjil = 'required|date|after_or_equal:start_ganjil';
             $start_antara = 'nullable|date';
             $end_antara = 'nullable|date|after_or_equal:start_antara';
          }else{
             $start_antara = 'required|date';
             $end_antara = 'required|date|after_or_equal:start_antara';
             $start_genap = 'nullable|date';
             $end_genap = 'nullable|date|after_or_equal:start_genap';
             $start_ganjil = 'nullable|date';
             $end_ganjil = 'nullable|date:after_or_equal:start_ganjil';
          }

          return [
            'kode_perwalian' => $kode,
            'program_studi_id' => 'required',
            'start_konsultasi1' => 'sometimes|date',
            'end_konsultasi1' => 'sometimes|date|after_or_equal:start_konsultasi1',
            'start_konsultasi2' => 'sometimes|date',
            'end_konsultasi2' => 'sometimes|date|after_or_equal:start_konsultasi2',
            'start_prs' => 'sometimes|date',
            'end_prs' => 'sometimes|date|after_or_equal:start_prs',
            'start_antara' => $start_antara,
            'end_antara' => $end_antara,
            'start_ganjil' => $start_ganjil,
            'end_ganjil' => $end_ganjil,
            'start_genap' => $start_genap,
            'end_genap' => $end_genap,
          ];
      }

      public function messages()
      {
          return [
              'kode.required' => 'Kode tidak boleh kosong',
              'kode.min' => 'Kode minimal harus 4 karakter',
              'kode.max' => 'Kode maksimal 100 karakter',
              'kode.unique' => 'kode sudah digunakan',
              'start_konsultasi1.date' => 'Data Mulai Konsultasi 1 harus berupa format tanggal',
              'end_konsultasi1.date' => 'Data Berakhir Konsultasi 1 harus berupa format tanggal',
              'end_konsultasi1.after_or_equal' => 'Tanggal Berakhir Konsultasi 1 tidak boleh kurang dari tanggal mulainya',
              'start_konsultasi2.date' => 'Data Mulai Konsultasi 2 harus berupa format tanggal',
              'end_konsultasi2.date' => 'Data Berakhir Konsultasi 2 harus berupa format tanggal',
              'end_konsultasi2.after_or_equal' => 'Tanggal Berakhir Konsultasi 2 tidak boleh kurang dari tanggal mulainya',
              'start_prs.date' => 'Data Mulai PRS harus berupa format tanggal',
              'end_prs.date' => 'Data Berakhir PRS harus berupa format tanggal',
              'end_prs.after_or_equal' => 'Tanggal Berakhir PRS tidak boleh kurang dari tanggal mulainya',
              'start_antara.date' => 'Data Mulai Antara harus berupa format tanggal',
              'start_antara.required' => 'Tanggal mulai tidak boleh kosong',
              'end_antara.date' => 'Data Berakhir Antara harus berupa format tanggal',
              'end_antara.after_or_equal' => 'Tanggal Berakhir Antara tidak boleh kurang dari tanggal mulainya',
              'end_antara.required' => 'Tanggal berakhir tidak boleh kosong',
              'start_genap.date' => 'Data Mulai Genap harus berupa format tanggal',
              'start_genap.required' => 'Tanggal mulai tidak boleh kosong',
              'end_genap.date' => 'Data Berakhir Genap harus berupa format tanggal',
              'end_genap.after_or_equal' => 'Tanggal Berakhir Genap tidak boleh kurang dari tanggal mulainya',
              'end_genap.required' => 'Tanggal berakhir tidak boleh kosong',
              'start_ganjil.date' => 'Data Mulai Ganjil harus berupa format tanggal',
              'start_ganjil.required' => 'Tanggal mulai tidak boleh kosong',
              'end_ganjil.date' => 'Data Berakhir Ganjil harus berupa format tanggal',
              'end_ganjil.after_or_equal' => 'Tanggal Berakhir PRS tidak boleh kurang dari tanggal mulainya',
              'end_ganjil.required' => 'Tanggal berakhir tidak boleh kosong',

          ];
      }
}
