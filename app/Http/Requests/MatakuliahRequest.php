<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MatakuliahRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
         if($this->method() == 'POST'){
            $kode = 'required|string|min:4|max:100|unique:mata_kuliahs';
            $nama = 'required|string|min:6|max:100|unique:mata_kuliahs';
         }else{
            $kode = 'required|string|min:4|unique:mata_kuliahs,kode,'.$this->get('id');
            $nama = 'required|string|min:6|unique:mata_kuliahs,nama,'.$this->get('id');
         }
         return [
           'kode' => $kode,
           'nama' => $nama,
           'sks' => 'required|numeric',
           'prasyarat' => 'required|string',
           'semester' => 'required|numeric',
           'sifat' => 'required|string',
           'jenis_semester' => 'required|in:ganjil,genap,antara',
         ];
     }

     public function messages()
     {
         return [
             'kode.required' => 'Kode tidak boleh kosong',
             'kode.min' => 'Kode minimal harus 4 karakter',
             'kode.max' => 'Kode maksimal 100 karakter',
             'kode.unique' => 'kode sudah digunakan',
             'nama.unique' => 'Nama mata kuliah sudah digunakan',
             'nama.required' => 'Nama tidak boleh kosong',
             'nama.string' => 'Nama harus string',
             'nama.min' => 'Nama minimal harus 6 karakter',
             'sks.required' => 'SKS tidak boleh kosong',
             'sks.numeric' => 'SKS harus diisi dengan angka',
             'prasyarat.required' => 'Prasyarat tidak boleh kosong',
             'semester.required' => 'Semester tidak boleh kosong',
             'semester.numeric' => 'Semester harus diisi dengan angka',
             'jenis_semester.required' => 'Jenis Semester Harus diisi',
             'jenis_semester.in' => 'Jenis Semester harus ganjil atau genap atau antara',
         ];
     }
}
