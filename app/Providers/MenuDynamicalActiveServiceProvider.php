<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Request;


class MenuDynamicalActiveServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
      $halaman = 'dashboard'; // default
      $title = 'Aplikasi Perwalian | Teknik Elektro';
      $masterHalaman = '';

      if($this->app->request->is('login')){
        $halaman = 'login'; // default
        $title = 'Login - Aplikasi Perwalian | Teknik Elektro';
        $masterHalaman = '';
      }

      // Modul Transaksi
      if($this->app->request->is('matakuliahwajib*') || $this->app->request->is('jadwalPerwalian*') || $this->app->request->is('perwalian*') || $this->app->request->is('nilai*') || $this->app->request->is('nilaiberkas*')) {
             $masterHalaman = 'transaksi';
             if($this->app->request->is('matakuliahwajib*')){
               $halaman = 'matakuliahwajib';
               $title = 'Matakuliah Wajib - Aplikasi Perwalian | Teknik Elektro';
             }else if($this->app->request->is('jadwalPerwalian*')){
               $halaman = 'jadwalPerwalian';
               $title = 'Jadwal Perwalian - Aplikasi Perwalian | Teknik Elektro';
             }else if($this->app->request->is('perwalian*')){
               $halaman = 'perwalian';
               $title = 'Perwalian - Aplikasi Perwalian | Teknik Elektro';
             }else if($this->app->request->is('nilai*')){
               $halaman = 'nilai';
               $title = 'Nilai - Aplikasi Perwalian | Teknik Elektro';
             }else if($this->app->request->is('nilaiberkas*')){
               $halaman = 'nilaiberkas';
               $title = 'Nilai Berkas - Aplikasi Perwalian | Teknik Elektro';
             }
      }
      // Halaman Master
      else if ($this->app->request->is('user*') || $this->app->request->is('dosen*') || $this->app->request->is('programstudi*') || $this->app->request->is('proyekpendidikan*') || $this->app->request->is('matakuliah*') || Request::is('mahasiswa*')) {
           $masterHalaman = 'master';
           if($this->app->request->is('user*')){
             $halaman = 'user';
             $title = 'Master User - Aplikasi Perwalian | Teknik Elektro';
           }else if($this->app->request->is('dosen*')){
             $halaman = 'dosen';
             $title = 'Master Dosen - Aplikasi Perwalian | Teknik Elektro';
           }else if($this->app->request->is('programstudi*')){
             $halaman = 'programstudi';
             $title = 'Master Program Studi - Aplikasi Perwalian | Teknik Elektro';
           }else if($this->app->request->is('proyekpendidikan*')){
             $halaman = 'proyekpendidikan';
             $title = 'Master Matakuliah - Aplikasi Perwalian | Teknik Elektro';
           }else if($this->app->request->is('matakuliah*')){
             $halaman = 'matakuliah';
             $title = 'Master Matakuliah - Aplikasi Perwalian | Teknik Elektro';
           }else if($this->app->request->is('mahasiswa*')){
             $halaman = 'mahasiswa';
             $title = 'Master Mahasiswa - Aplikasi Perwalian | Teknik Elektro';
           }
      }

      view()->share(compact('halaman', 'title', 'masterHalaman'));
    }
}
